Configure **paraprobe-autoreporter**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE autoreporter is an experimental utility tool to conveniently explore and post-process the results from PARAPROBE tool runs. It parses results from HDF5 container files and uses the SciPy/Numpy and Python code to render figures automatically and compose them into a rudimentary analysis report. This report is meant as a guide to the scientists to offer them an economic measure to assess the quantitative details of their datasets from the parameter sweeping studies that tools such as paraprobe-spatstat offer.

Users can directly modify the Python script to configure and run the tool incrementally. Parallelization is in most cases not necessary. Currently it is at least not employed.