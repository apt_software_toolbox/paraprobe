Configure **paraprobe-spatstat**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE spatstat is the tool to compute descriptive one- and two-point spatial statistics. It allows users to define internal batch queues with an  arbitrary number of iontype combinations, spherical regions of interest of arbitrary size. PARAPROBE spatstat takes an APTH5 file with the ion positions and range labels. Hence, no additional range file is required. Furthermore, paraprobe-spatstat expects a HDF5 file with the distances of each ion as input, a supplementary XML file which specifies the periodic table of elements, and a single XML settings file which instructs the analysis tasks. The results are stored in a separate HDF5 file. They can be compiled into a Latex report using paraprobe-autoreporter. The possible settings for paraprobe-spatstat are as follows:

|	**InputfilePSE**
Specifies like in paraprobe-ranger the periodic table of elements.

|	**InputfileReconstruction**
Specifies the ion positions and iontype labels.

|	**InputfileHullAndDistances**
Specifies the ion distances.

|	**AnalyzeRDF**
|	**AnalyzeKNN**
|	**AnalyzeSDM**
Switch on 1 to instruct radial distribution, k-nearest neighbor, and/or spatial distribution map (two-point statistics) analyses.

|	**ROIRadiusRDFMin**
|	**ROIRadiusRDFIncr**
|	**ROIRadiusRDFMax**
|	**ROIRadiusKNNMin**
|	**ROIRadiusKNNIncr**
|	**ROIRadiusKNNMax**
|	**ROIRadiusSDMMin**
|	**ROIRadiusSDMIncr**
|	**ROIRadiusSDMMax**
Specify the minimum (0.0), binning width, and maximum of the spherical region of
interest radii to use for RDF, KNN, and SDM respectively. Settings can differ and reads in nanometer.

|	**KOrderForKNN**
|	**KOrderForSDM**
Semicolon-separated list which k-th nearest neighbors to compute for KNN and SDM respectively. Settings can differ for KNN vs SDM. As an example, the computation of the first-, tenth-, and hundredth- neighbor is instructed by setting 1;10;100.

|	**ROIVolumeInsideOnly**
Is always recommended to activate with setting it to 1 to remove bias from the fact that ROIs protruding out of the point cloud at the dataset boundary have limited counting support.

|	**IontypeCombinations**
Specify for which iontype combinations above statistics should be computed. Targets are those at which positions ROIs are placed. Neighbors are the ion types which are considered to contribute a distance value in the analysis of each ROI, statistic, and combination. Multiple ions can be defined as targets or neighbors. In this case separate using commas. Each line add an additional set of analyses. Analyses are nested and apply to RDF, KNN, and SDM with original and randomized labels. Element names with a single character like H, C, B, F, N require the addition of a colon. Otherwise these elements are ignored! Examples how to define combinations are described here using the following syntax::

   <entry targets="Al" neighbors="Al"/>
  
This means place ROIs only at aluminium ions and compute statistics for their aluminium neighbors if any exist. An example with comma separation and colon syntax::

   <entry targets="Al,H:" neighbors="Cu"/>
   
This means place ROIs at either aluminium or hydrogen ions (as targets) and compute statistics for their copper neighbors if any exist. An example for molecular ions::

   <entry targets="AlO:,Al" neighbors="Ga:,C:"/>
   
This means place ROIs at either aluminium-oxide molecular ions or aluminium ions and compute statistics for their Gallium or carbon neighbors.
Add as many lines as desired. There is one practical restriction in the current code thought: result buffers per thread are generated to decoupled the writing of results. As a compromise, though, this may result in a too high memory consumption for the results buffers. The expenditures can be computed as follows::

    ForRDF = NumberOfCombinations * 2 * 1 * (ROIRadiusRDFMax-ROIRadiusRDFMin)/ROIRadiusRDFIncr * 8 Byte
    ForKNN = NumberOfCombinations * 2 * 1 * (ROIRadiusKNNMax-ROIRadiusKNNMin)/ROIRadiusKNNIncr * 8 Byte
    ForSDM = NumberOfCombinations * 2 * 1 * (1+ceil((ROIRadiusSDMMax-ROIRadiusSDMMin)/ROIRadiusSDMIncr)*2)^3 * 4 Byte
    InTotal = (1 + OMP_NUM_THREADS) * (ForRDF + ForKNN + ForSDM)

The factor 2 accounts for the fact that statistics are by default computed for original and randomized labels.
Evidently, for extremely diverse parameter sweeping studies especial for two-point statistics, the analysis might have to be split into multiple runs.
Taking realistic values for OMP_NUM_THREADS=20, 16 combinations, and [0.0, 10.0] histogram with 0.001 binning for RDF and KNN, and 201^3 bins for SDM results in::

    ForRDF / ForKNN = 16 * 2 * 1 * 10000 * 8 Byte = 2500 KB
    ForSDM = 16 * 2 * 1 * 201^3 * 4 Byte = 0.968 GB

In effect, a total buffer size of approximately 42 GB buffers where most buffer consumptions comes from SDM. Only 105 MB of which is for RDF and KNN.
