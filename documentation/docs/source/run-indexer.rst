Run **paraprobe-indexer**
^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all input file(s) exist in the local directory (HDF5 file)::

    WHICH_ARE_THESE is still missing ... 

**Secondly**, assure that there is a properly configured XML paraprobe-indexer settings file::

    HOW_DOES_THE_CONTENT_OF_SUCH_LOOK_LIKE is missing ... ?

**Thirdly**, assure that all GPUs to be used are visible at runtime::

    export CUDA_VISIBLE_DEVICES=comma_separated_list_of_device_ids

Comma_separated_list_of_device_ids is typically 0 for a single and 0,1 for two GPUs.

**Fourth**, configure the shell environment that PGI and CUDA locations are found::

    export PGI_CURR_CUDA_HOME=/mypath/to/cuda/10.0.130
    export PGI=/mypath/to/pgi/19.9

**Fifth**, allow for multi-threading::

    export OMP_NUM_THREADS=20
    export OMP_NESTED=true
    ulimit -s unlimited

Otherwise, similar comments as for paraprobe-spatstat apply. If workpackages are small, it is recommended to use a single thread per MPI process.

**Finally**, paraprobe-indexer is executed as follows::

    export MYRANKS_EMPLOYED=2
    export SIMID=1
    export XML_TXT=PARAPROBE.Indexer.xml
    export STDOUT_RUN_TXT = PARAPROBE.Indexer.SimID.${SIMID}.STDOUT.txt
    export STDERR_RUN_TXT = PARAPROBE.Indexer.SimID.${SIMID}.STDERR.txt

    mpiexec -n ${MYRANKS_EMPLOYED} ./paraprobe_indexer ${SIMID} ${XML_TXT} 1>${STDOUT_RUN_TXT} 2>${STDERR_RUN_TXT}

**Make sure that every MPI process has access to an own GPU! So far this is not catched at runtime and not following this rule might cause undefined behavior!** PARAPROBE indexer is an MPI/OpenMP/OpenACC program. Therefore, similar comments as for paraprobe-surfacer and paraprobe-spatstat apply.