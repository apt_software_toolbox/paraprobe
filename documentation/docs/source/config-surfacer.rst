Configure **paraprobe-surfacer**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE surfacer is the tool which computes an alpha-shape triangle hull to the entire dataset. The CGAL library is used to compute the alpha shape and export the triangles as objects. They can be visualized using Paraview and the generated XDMF/HDF5 file combination. PARAPROBE surfacer uses a filtering algorithm which thins the point cloud. Thereby, most ions in the interior are not at all passed to CGAL such that triangle hulls can be computed even for datasets as large as two billion ion point clouds without ad hoc sub-sampling of requiring random thinning like in classical APT studies. The resulting triangle hulls are not necessarily water-tight. Feel free to contact me in case there is interest in collaborating to patch these holes. The resulting triangle hull is used thereafter to compute analytically the distance of each ion to the closest triangle.

PARAPROBE surfacer takes an APTH5 file with the ion positions and returns triangles and distance values. The tool is controlled with a single XML settings file. Distances are stored in a separate HDF5 file. The possible settings for paraprobe-surfacer are as follows:


|	**SurfacingMode**
Set to 1 to use CGAL and compute an alpha shape to the dataset.

|	**Inputfile**
The ion positions as an APTH5 file. Needs to have file ending *.apth5

|	**AlphaShapeAlphaValue**
Choose 0 to pick the alpha shape with the smallest alpha value to get a solid through the data points. Choose 1 to pick the alpha shape which CGAL considers to be the optimal value. Consult the CGAL library for further details.

|	**AdvIonPruneBinWidthMin**
|	**AdvIonPruneBinWidthIncr**
|	**AdvIonPruneBinWidthMax**
Specify in nanometer the width of the rectangular bins chosen during the initial filtering stage. Values of 0.5 nm are suited for most datasets. If value is larger unnecessarily many ions are passed to CGAL, thereby slowing down the alpha shape computation. Values smaller may result in gaps in the dataset or percolation of triangles through the point cloud such that a bi-layer hull is generated.

|	**DistancingMode**
Set to 0 if no distancing is desired. Typically results in bias for ions at the dataset boundary and is not recommended when reporting spatial statistics. Set to 1 to compute for every ion analytically a distance value. Can be time consuming. However, this is where the multi-level parallelization of PARAPROBE assists. Set to 2 if spatial statistics and all other subsequent analyses should take into account only regions of interest with a maximal radius of DistancingRadiusMax. In this case, PARAPROBE surfacer sets DistancingRadiusMax as the default distance for all ions which are farther away from the boundary than DistanceRadiusMax; thereby the tool skips these and is therefore faster.

|	**DistancingRadiusMax**
Only significant when DistancingMode is set to 2. Defines the threshold distance up to which ions are exactly distanced. For all other ions this threshold is returned.