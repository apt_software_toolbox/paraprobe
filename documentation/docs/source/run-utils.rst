Run **paraprobe-utils**
^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE utils alone can not be executed. It is just a compilation of object code which contains functionality which the other tools can include to reduce compilation time.
