Get **cmake**
^^^^^^^^^^^^^

CMake is a Linux software which can be used to automatize the process of collecting hard- and software pieces of information and dependencies to create a Makefile with which tools like PARAPROBE can be compiled from the source code into an executable program.

In what follows, I will detail the steps to install cmake on a clean installation of Ubuntu 18.04 LTS. As always, it is expected that things might not work (like in experiment), so feel free to contact me to offer advice::

1. Check if cmake is already installed and which version it is::

    cmake -version

If this returns nothing, cmake needs installation. We need cmake at least of version 3.10 or higher.

2. Install cmake using administrator rights::

    sudo apt-get update
    sudo apt-get install cmake
    sudo apt-get upgrade
    cmake –version

