Compile **paraprobe-araullo**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all relevant third-party dependencies have been properly installed and configured. Especial to mention here for PARAPROBE araullo is the PGI compiler and the CUDA API. See corresponding details specified for PARAPROBE fourier.

**Secondly**, it is essential to set the environment variables such that the PGI compiler can find CUDA::

    ... add a description here ...

**Thirdly**, one needs to understand that also PARAPROBE araullo makes use of the PARAPROBE utils object files. One is better off with recompiling these object files with the PGI compiler before including them while compiling PARAPROBE araullo. Like it applies when compiling PARAPROBE fourier. See the corresponding details in the sub-section on compiling PARAPROBE utils.

**Fourthly**, the tools CMakeLists.txt configuration file should be checked to assure that HDF5 can be found.

**Finally**, the compilation of the PARAPROBE araullo is straightforward::

   export STDOUT_CMKE_TXT = PARAPROBE.Araullo.CMakeSettings.STDOUT.txt
   export STDERR_CMKE_TXT = PARAPROBE.Araullo.CMakeSettings.STDERR.txt
   export STDOUT_MAKE_TXT = PARAPROBE.Araullo.MakeSettings.STDOUT.txt
   export STDERR_MAKE_TXT = PARAPROBE.Araullo.MakeSettings.STDERR.txt

Use the PGI compiler::

    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_CUDACXX=nvcc -DCMAKE_C_COMPILER=pgcc -DCMAKE_CXX_COMPILER=pgc++ .. 1>${STDOUT_CMKE_TXT} 2>${STDERR_CMKE_TXT}
    make 1>${STDOUT_MAKE_TXT} 2>${STDERR_MAKE_TXT}

