Compile **paraprobe-surfacer**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all relevant third-party dependencies have been properly installed and configured. Identify especial the local path of the top-level directory where the CGAL library source code is stored. This is the CGAL_DIR directory. It contains a CMakeLists.txt file. This file contains a configuration line which needs to read::

    option(CGAL_HEADER_ONLY "Enable the header only version of CGAL" ON )

If instead CGAL is freshly downloaded and compiled from source code, this line may by default be configured to OFF. Instead, switch it ON. **Secondly**, specify the location of the local or global HDF5 location in the CMakeLists.txt file. **Thirdly**, go into the build sub-directory and build the code using ideally two steps::

   export STDOUT_CMKE_TXT = PARAPROBE.Surfacer.CMakeSettings.STDOUT.txt
   export STDERR_CMKE_TXT = PARAPROBE.Surfacer.CMakeSettings.STDERR.txt
   export STDOUT_MAKE_TXT = PARAPROBE.Surfacer.MakeSettings.STDOUT.txt
   export STDERR_MAKE_TXT = PARAPROBE.Surfacer.MakeSettings.STDERR.txt
   export THIS_IS_MYCGAL=/this/is/the/path/where/you/have/stored/cgal

Above log file are especially useful when debugging configuration errors associated with the CGAL library. Feel free to send them in this case.
Only the Intel or GNU compiler is currently supported. Primarily, because compiling the CGAL library with the PGI compiler is known to be tricky and may result
eventually in non-deterministic code creating different results than the Intel or GNU compilers.

For the Intel compiler::

   cmake -DCMAKE_BUILD_TYPE=Release -DCGAL_DIR=${THIS_IS_MYCGAL} -DCMAKE_C_COMPILER=icc -DCMAKE_CXX_COMPILER=icpc .. 1>${STDOUT_CMKE_TXT} 2>${STDERR_CMKE_TXT}
    make 1>${STDOUT_MAKE_TXT} 2>${STDERR_MAKE_TXT}

For the GNU/GCC compiler::

    cmake -DCMAKE_BUILD_TYPE=Release -DCGAL_DIR=${THIS_IS_MYCGAL} -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ .. 1>${STDOUT_CMKE_TXT} 2>${STDERR_CMKE_TXT}
    make 1>${STDOUT_MAKE_TXT} 2>${STDERR_MAKE_TXT}

When using the GNU compiler, the CMakeLists.txt file needs potentially a modification. I have so far always used the Intel compiler.
