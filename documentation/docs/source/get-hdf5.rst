Get **HDF5**
^^^^^^^^^^^^

The HDF5 library comes in two principle flavors. As a sequential and a parallel library. I have used both. The sequential library allows that only one process at a time is granted write access to the harddisk. The parallel HDF5 library, also known as PHDF5, requires a parallel file system, i.e. a special storage solution where multiple harddisks are used in conjunction to increase the attainable I/O throughput. The usage model is that then multiple processes write to specific locations in an initialized file in parallel. Using the parallel library comes with additional challenges and more complex code.

Several reasons convinced me that at least for data mining atom probe tomography in most cases the sequential library suffices. The main argument is that most APT analysis worldwide are still executed on single desktop or workstation computers which do not have access to a parallel file system. PARAPROBE now allows to use computing cluster, which frequently have access to a parallel file system. The typical results of PARAPROBE runs for e.g. spatial statistics seldom exceed more than a few gigabytes. Typically these data are written in less than a few percent of the entire time which PARAPROBE spends in computing the results. Therefore, I will share the already existent but experimental stage implementation of PARAPROBE's interface to the PHDF5 library at a later stage.

Rather I focus now to detail how to configure the HDF5 library. As HDF5 comes shipped with a good manual how to do so, I just stress the important modifications.
Using version 1.10.2 suffices completely for all I/O activities within PARARPROBE tools.

1. Download the version here http://www.hdfgroup.org/downloads/hdf5/ and compile it from source. Check the manual against the following loose cooking recipe::

    sudo mkdir HDF5Local
    sudo cd HDF5Local
    sudo tar –xvf hdf5-1.10.2.tar.bz2
    cd hdf5-1.10.2

2. Check additional compile flags according to http://support.hdfgroup.org/HDF5/release/chgcmkbuild.html
The following commands will build the dynamic libraries, perform tests, and do not encode the compression. Currently, PARAPROBE HDF5 I/O routines do not use compression.

    cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE:STRING=Release -DBUILD_SHARED_LIBS:BOOL=OFF -DBUILD_TESTING:BOOL=ON -DHDF5_BUILD_TOOLS:BOOL=ON -DHDF5_BUILD_FORTRAN:BOOL=ON ../hdf5-1.10.2
    cmake -build . -config Release
    ctest . -C Release
    cpack -C Release CPackConfig.cmake
    ./HDF5-1.10.2-Linux.sh
    cd HDF5-1.10.2-Linux
    cd HDF_Group
    cd HDF5
    cd 1.10.2

Now you are in the folder which has to be mentioned in the CMakeLists.txt files of the individual PARAPROBE tools.
I know that the compilation of HDF5 can be tricky. Feel free to ask for help_ !

 .. _help: http://bigmax.iwww.mpg.de/39151/bigmax-software-engineering-consultant
