Configure **paraprobe-synthetic**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE synthetic is the tool to generate synthetic specimens. These can be single-crystalline, polycrystalline, and be supplemented with solutes, and precipitates. The tool is controlled with a single XML settings file. One run creates one specimen. The possible settings for paraprobe-transcoder are as follows:

| **SynthesizingMode**
Set 1 to generate single-crystalline specimen. Set 2 to generate polycrystalline specimen as a Poisson-Voronoi instantiation.

| **SimRelBottomRadius**
| **SimRelTopRadius**
| **SimRelBottomCapHeight**
| **SimRelTopCapHeight**
Specimens are build as a conical frustum with potentially a spherical cap on top and one carved out at the bottom. Above values specify in multiples of the conical frustum height H.

| **SimNumberOfAtoms**
Specify how many ions in total in the specimen. In case of limited detection efficiency lattice might be thinned but geometry is retained.

| **SimMatrixOrientation**
Specify Bunge-Euler passive orientation of specimen in degrees e.g. 90;35;45.

| **SimDetectionEfficiency**
Specifies which fraction of the lattice sites are occupied all (1.0) or fewer.

| **SimFiniteSpatResolutionX**
| **SimFiniteSpatResolutionY**
| **SimFiniteSpatResolutionZ**
Specifies variance of a simple two-dimensional Gaussian noise model to account for finite spatial resolution. Variance of normal distribution in nanometer.

| **SimNumberOfCluster**
How many precipitates should be placed in the bounding box of the specimen.
Actual specimen typically contains thus a fraction of these only.

| **SimClusterRadiusMean**
Radius of these precipitates.

.. | **SimClusterRadiusSigmaSqr**

| **a1**
| **b1**
| **c1**
| **alpha1**
| **beta1**
| **gamma1**
In a,b, c in nanometer, angles in degrees.
Lattice constants of lattice 1 which are used to synthesize the matrix lattice.

| **a2**
| **b2**
| **c2**
| **alpha2**
| **beta2**
| **gamma2**
Lattice constants of lattice 2 which are used to synthesize the precipitates.

| **SimPrecipitateOrientation**
Like SimMatrixOrientation Bunge-Euler passive orientation assigned to precipitates.

| **CrystalloVoroMeanRadius**
Specifies in nanometer the average volume the Poisson-Voronoi cells for polycrystalline specimens should have. Volume is computed as spherical equivalent volume using this radius.

 