Run **paraprobe-transcoder**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all input file(s) exist in the local directory. **Secondly**, assure that there is a properly configured XML paraprobe-transcoder settings file. **Thirdly**, configure the shell environment to allow for multi-threading::

    export OMP_NUM_THREADS=1
    export OMP_NESTED=true
    ulimit -s unlimited

The number of OpenMP threads to use at most can be changed. Here it is set to 1, i.e. quasi-sequential execution. Best practice guidelines how to set the total number of threads are provided in the in the section best practices for multi-threading sub-section.

**Finally**, paraprobe-transcoder is executed::

    export SIMID=1
    export XML_TXT=PARAPROBE.Transcoder.xml
    export STDOUT_RUN_TXT = PARAPROBE.Transcoder.SimID.${SIMID}.STDOUT.txt
    export STDERR_RUN_TXT = PARAPROBE.Transcoder.SimID.${SIMID}.STDERR.txt

    ./paraprobe_transcoder ${SIMID} ${XML_TXT} 1>${STDOUT_RUN_TXT} 2>${STDERR_RUN_TXT}

Here, SIMID is a user-defined integer on the interval between 0 and 2^31. It can be used to distinguish multiple runs on the same datasets, for instance to try different settings or parallelization levels. **Analyses with the same SIMID will overwrite previous results or may fail!**. XML_TXT is a PARAPROBE transcoder settings files which can be arbitrarily renamed but needs to remain proper XML syntax and file ending. Subsequent commands instruct that all console output should be re-directed to files; one for verbose another one for error messages. These files serve as a future reference of the settings and the specific runtime environment used. Furthermore, they are a very useful tool to debug potential errors during code execution. **Therefore, users are encouraged to send these files when experiencing problems with PARAPROBE!**

