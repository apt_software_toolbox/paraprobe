Get **compiler**
^^^^^^^^^^^^^^^^

For Intel follow the instructions to download and install the Intel Studio compiler. Choose the version for your specific system and make sure that it contains the Intel MPI library.

For PGI follow the instructions to download and install the PGI compiler. There is a non-commercial, so called community version available. Make sure that this version contains an MPI library.