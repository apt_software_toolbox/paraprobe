Compile **paraprobe-fourier**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all relevant third-party dependencies have been properly installed and configured. Especial to mention here for PARAPROBE fourier is the PGI compiler and the CUDA API. For single desktop or workstations, Ubuntu 16.04 and newer and CUDA there is a fairly good documentation how to set this up online by Achintha Ihalage on medium.com::

    How To Install Nvidia Drivers and CUDA-10.0 for RTX 2080 Ti GPU on Ubuntu 16.04/18.04

**Secondly**, it is essential to set the environment variables such that the PGI compiler can find CUDA::

    ... add a description here ...

**Thirdly**, one needs to understand that also PARAPROBE fourier makes use of the PARAPROBE utils object files. One is better off with recompiling these object files with the PGI compiler before including them while compiling PARAPROBE fourier. See the corresponding details in the sub-section on compiling PARAPROBE utils.

**Fourthly**, the tools CMakeLists.txt configuration file should be checked to assure that HDF5 can be found.

**Finally**, the compilation of the PARAPROBE fourier is straightforward::

   export STDOUT_CMKE_TXT = PARAPROBE.Fourier.CMakeSettings.STDOUT.txt
   export STDERR_CMKE_TXT = PARAPROBE.Fourier.CMakeSettings.STDERR.txt
   export STDOUT_MAKE_TXT = PARAPROBE.Fourier.MakeSettings.STDOUT.txt
   export STDERR_MAKE_TXT = PARAPROBE.Fourier.MakeSettings.STDERR.txt

Use the PGI compiler::

    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=pgcc -DCMAKE_CXX_COMPILER=pgc++ .. 1>${STDOUT_CMKE_TXT} 2>${STDERR_CMKE_TXT}
    make 1>${STDOUT_MAKE_TXT} 2>${STDERR_MAKE_TXT}

It should also be possible to compile the code with the GCC compiler. The standard Linux toolchain GNU compiler, though, will not generate OpenACC code. 
For this a customized version of GCC NVTPX_ is required until the compiler developers have implemented these features into their compilers.

 .. _NVTPX: http://www.heise.de/developer/artikel/Accelerator-Offloading-mit-GCC-3317330.html?seite=all
