Configure **paraprobe-transcoder**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE transcoder is the link between third-party APT data analysis software like APSuite/IVAS and PARAPROBE.
The tool takes POS or EPOS files and translates them into a HDF5 file. The file ending of the output file is APTH5.

Like all tools, paraprobe-transcoder is controlled with an input file and a single XML settings file. One run transcodes one file. The possible settings for paraprobe-transcoder are as follows:

| **TranscodingMode**
Choose 1 if desiring POS to APTH5
Choose 2 if desiring EPOS to APTH5
Choose 3 if desiring APSuite/IVAS6 APTV2 to APTH5

| **Inputfile**
Specify name of inputfile. Needs file ending. 
Needs to be *.pos for TranscodingMode 1.
Needs to be *.epos for TrancodingMode 2.
