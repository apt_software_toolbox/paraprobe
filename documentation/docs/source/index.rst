.. figure:: ../images/PARAPROBEFront_02.png
   :scale: 50%
   :align: center
   :target: http://gitlab.mpcdf.mpg.de/mpie-aptfim-toolbox/paraprobe

**PARAPROBE** is a collection of **open source tools** for **efficient mining** of spatial statistics and computational geometry for **Atom Probe Tomography** (APT) **data**. The software does not replace but complement existent software tools of the APT community.

.. such as AMETEK's APSuite/IVAS, 3depict, Scito, or a number of community scripts out there.

PARAPROBE is a **collection of individual tools**. One tool per specific analysis task. These tasks encompass the loading of data from e.g. IVAS, the creation of synthetic specimens, the computation of an alpha shape triangle hull to the specimen volume, spatial statistics, atom probe crystallography, such as direct Fourier transforms, and full volume tessellations.

PARAPROBE was designed to offer **strong scaling performance** to make the most **desktop**, **workstation**, and use even large computer clusters with **thousands of cores and dozens of accelerators**. The tools achieve their performance through algorithmic optimizations, multi-level parallelization including vectorization, shared and distributed memory data parallelism, and the utilization of **CPUs** and graphic cards (accelerators) **GPGPUs**.

The tool_ is **developed by Markus Kühbach**, a scientific computing **Postdoc with** the Max-Planck BiGmax_'s research network at the **Max-Planck-Institut für Eisenforschung GmbH** (MPIE_) in Düsseldorf.

**Feel free to utilize the tool**. In doing so, feel free to suggest me_ any improvements or desirable analysis features you find useful to add into PARAPROBE. 

 .. _tool: http://bigmax.iwww.mpg.de/39151/bigmax-software-engineering-consultant
 .. _MPIE: http://www.mpie.de
 .. _me: http://www.mpie.de/person/51206/2656491
 .. _here: http://gitlab.mpcdf.mpg.de/mpie-aptfim-toolbox/paraprobe


1. Getting started
^^^^^^^^^^^^^^^^^^
This documentation should serve as a guide through PARAPROBE. In case of questions, feel free to ask me_.
The individual sections of the documentation always start with a summary of the essentials. Readers interested in further details are encouraged to read the preceeding sub-sections. 

.. toctree::
   :maxdepth: 3

   basics
   
2. Understand the toolbox
^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3
   
   prerequisites
   get-optionaltools


3. Prepare your system
^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3
   
   get-cmake
   get-compiler
   get-mpi
   get-hdf5
   get-cgal


4. Compile the tools
^^^^^^^^^^^^^^^^^^^^
The compilation of the tools differs. Therefore, there is a detailed description for each tool.
After having ticked off the third-party dependencies, the first step is always to compile the PARAPROBE utils.

.. toctree::
   :maxdepth: 3
   
   compile-utils
   compile-transcoder
   compile-synthetic
   compile-ranger
   compile-surfacer

   compile-spatstat
   compile-autoreporter
   compile-fourier
   compile-araullo
   compile-indexer

..   compile-distancer


5. Define your analyses
^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3
   
   config-utils
   config-transcoder
   config-synthetic
   config-ranger
   config-surfacer

   config-spatstat
   config-autoreporter
   config-fourier
   config-araullo
   config-indexer
   
..   gui
..   config-distancer


6. Execute parallelized
^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3

   run-utils
   run-transcoder
   run-synthetic
   run-ranger
   run-surfacer

   run-spatstat
   run-autoreporter
   run-fourier
   run-araullo
   run-indexer

   bestpracticesomp

..   setenvironment
..   run-distancer


7. Work through examples
^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3

..   example-synthetic
..   example-ranger
..   example-surfacer
..   example-spatstat


8. Open source, how to cite
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3

   licence
   refs


9. How to contribute
^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3

   developers

.. contact


10. Funding
^^^^^^^^^^^
Markus Kühbach gratefully acknowledges the support from the Deutsche Forschungsgemeinschaft (DFG_) through project BA 4253/2-1, the provisioning of computing resources by the Max-Planck-Gesellschaft, and the funding received through BiGmax_, the Max-Planck-Research Network on Big-Data-Driven Materials Science.

 .. _DFG: https://www.dfg.de/
 .. _BiGmax: https://bigmax.iwww.mpg.de/
