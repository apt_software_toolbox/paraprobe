Compile **paraprobe-synthetic**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all relevant third-party dependencies have been properly installed and configured, especial that src/thirdparty contains the Voro++ source code files. **Secondly**, specify the location of the local or global HDF5 location in the CMakeLists.txt file. **Thirdly**, go into the build sub-directory and build the code using ideally two steps::

   export STDOUT_CMKE_TXT = PARAPROBE.Transcoder.CMakeSettings.STDOUT.txt
   export STDERR_CMKE_TXT = PARAPROBE.Transcoder.CMakeSettings.STDERR.txt
   export STDOUT_MAKE_TXT = PARAPROBE.Transcoder.MakeSettings.STDOUT.txt
   export STDERR_MAKE_TXT = PARAPROBE.Transcoder.MakeSettings.STDERR.txt
   
   
This specifies where to store information which compiler and settings were used.
   
For the Intel compiler::
 
   cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=icc -DCMAKE_CXX_COMPILER=icpc .. 1>${STDOUT_CMKE_TXT} 2>${STDERR_CMKE_TXT}
   make 1>${STDOUT_MAKE_TXT} 2>${STDERR_MAKE_TXT}
   
For the PGI compiler::

   cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=pgcc -DCMAKE_CXX_COMPILER=pgc++ .. 1>${STDOUT_CMKE_TXT} 2>${STDERR_CMKE_TXT}
   make 1>${STDOUT_MAKE_TXT} 2>${STDERR_MAKE_TXT} 
