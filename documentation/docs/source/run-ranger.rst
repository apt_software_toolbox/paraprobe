Run **paraprobe-ranger**
^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all input file(s) exist in the local directory (HDF5 file and periodic table of elements and rrng range file). **Secondly**, assure that there is a properly configured XML paraprobe-synthetic settings file. **Thirdly**, configure the shell environment to allow for multi-threading::

    export OMP_NUM_THREADS=1
    export OMP_NESTED=true
    ulimit -s unlimited

The same comments as detailed in paraprobe-synthetic apply.

**Finally**, paraprobe-ranger is executed::

    export SIMID=1
    export XML_TXT=PARAPROBE.Ranger.xml
    export STDOUT_RUN_TXT = PARAPROBE.Ranger.SimID.${SIMID}.STDOUT.txt
    export STDERR_RUN_TXT = PARAPROBE.Ranger.SimID.${SIMID}.STDERR.txt

    mpiexec -n 1 ./paraprobe_ranger ${SIMID} ${XML_TXT} 1>${STDOUT_RUN_TXT} 2>${STDERR_RUN_TXT}

PARAPROBE ranger is an MPI program. It allows currently, though, execution with a single process only.
The same comments as detailed in paraprobe-transcoder apply.
