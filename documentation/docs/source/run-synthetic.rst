Run **paraprobe-synthetic**
^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all input file(s) exist in the local directory. **Secondly**, assure that there is a properly configured XML paraprobe-synthetic settings file. **Thirdly**, configure the shell environment to allow for multi-threading::

    export OMP_NUM_THREADS=20
    export OMP_NESTED=true
    ulimit -s unlimited

The number of OpenMP threads to use at most can be changed. Here it is set to 20, i.e. multi-threaded execution. Best practice guidelines how to set the total number of threads are provided in the section best practices for multi-threading sub-section.

**Finally**, paraprobe-synthetic is executed::

    export SIMID=1
    export XML_TXT=PARAPROBE.Synthetic.xml
    export STDOUT_RUN_TXT = PARAPROBE.Synthetic.SimID.${SIMID}.STDOUT.txt
    export STDERR_RUN_TXT = PARAPROBE.Synthetic.SimID.${SIMID}.STDERR.txt

    mpiexec -n 1 ./paraprobe_synthetic ${SIMID} ${XML_TXT} 1>${STDOUT_RUN_TXT} 2>${STDERR_RUN_TXT}

PARAPROBE synthetic is an MPI program. It allows currently, though, execution with a single process only.
The same comments as detailed in paraprobe-transcoder apply.
