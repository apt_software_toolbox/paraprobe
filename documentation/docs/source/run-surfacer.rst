Run **paraprobe-surfacer**
^^^^^^^^^^^^^^^^^^^^^^^^^^

**Firstly**, assure that all input file(s) exist in the local directory (HDF5 file). **Secondly**, assure that there is a properly configured XML paraprobe-surfacer settings file. **Thirdly**, configure the shell environment to allow for multi-threading::

    export OMP_NUM_THREADS=20
    export OMP_NESTED=true
    ulimit -s unlimited

The same comments as detailed in paraprobe-synthetic apply.

**Finally**, paraprobe-surfacer is executed::

    export MYRANKS_EMPLOYED=2
    export SIMID=1
    export XML_TXT=PARAPROBE.Surfacer.xml
    export STDOUT_RUN_TXT = PARAPROBE.Surfacer.SimID.${SIMID}.STDOUT.txt
    export STDERR_RUN_TXT = PARAPROBE.Surfacer.SimID.${SIMID}.STDERR.txt

    mpiexec -n ${MYRANKS_EMPLOYED} ./paraprobe_surfacer ${SIMID} ${XML_TXT} 1>${STDOUT_RUN_TXT} 2>${STDERR_RUN_TXT}

PARAPROBE surfacer is an MPI program. MYRANKS_EMPLOYED controls how many processes to use. I recommmend to use one MPI process per CPU socket, not per CPU core. The program has been tested for 150 processes using 20 threads each. The same comments as detailed in paraprobe-transcoder apply.
If executed on a single workstation with multiple CPUs sockets it is useful to use MPI process parallelism. In this case, though, there is potentially no other computer to send messages to. Newer MPI API in this case may use the shared memory routines which demands the setting of environment variables. Typically, hints are provided upon first execution of above commands. These hints will direct users to the I_MPI_LMT_SHM=shm environment variables.
