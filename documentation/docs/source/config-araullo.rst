Configure **paraprobe-araullo**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

PARAPROBE araullo is executes atom probe crystallography methods in reconstruction space proposed by V. Araullo-Peters et al. The motivation is to identify regions within the reconstruction that show substantial long-range periodicity of the point cloud which can be interpreted as a signature of extractable crystallographic pieces of information. The method probes a regular grid or randomly chosen set of regions of interest. For each such ROI the ions inside are queried. Thereafter, one computes one-dimensional projections of the ions on a predefined set of crystallographic oriented planes cutting through the center of the ROI. Next, periodicity of these 1D spatial distribution maps is mined by applying one-dimensional fast Fourier transforms. PARAPROBE araullo is a parallel implementation of this method with several modifications. The tool is under development. So far it allows to execute above analysis and compose from each ROI a set of Fourier space reflector intensity images probed at specific reciprocal space positions. Assuming a set of possible crystal lattices, these positions are chosen reciprocal space locations where intensity for the pristine lattices is expected. These Fourier space images can be passed to PARAPROBE indexer which performs template matching to index the orientations. PARAPROBE araullo reads in an APTH5 file with the ion positions and range labels. Furthermore, a predefined set of projection directions is required. The analysis is parameterized through a single XML settings file. The results are stored in a separate HDF5 file. The possible settings for a PARAPROBE araullo settings file are as follows:

|	**InputfilePeriodicTableOfElements**
Specifies like in paraprobe-ranger the periodic table of elements.

|	**InputfileReconstruction**
Specifies the ion positions and range labels.

.. |	**InputfileHullAndDistances**
.. Specifies precomputed ion to triangle hull distances to avoid bias for ROIs which lay too close to the dataset boundary.

|	**VolumeSamplingMethod**
Set to 0 when a specimen axis-centered cuboidal ROI grid should be used.
This is comparable to instructing a 3D EBSD mapping of the specimen volume.
Set 1 to set a single position at which a ROI is placed and use SamplingPosition to tell where to place this ROI.

|	**ROIRadiusMax**
Specifies in nanometer the maximum radius of the spherical regions of interest to use. Currently the same ROI radius is used for all ROIs.


|	**SamplingGridBinWidthX**
|	**SamplingGridBinWidthY**
|	**SamplingGridBinWidthZ**
Specifies in nanometer the spacing of the cuboidal ROI grid. Significant only when VolumeSamplingMethod is 1.

|	**SamplingPosition**
Specifies in nanometer the position where to place a ROI in specimen reference coordinate system.
Only coordinates above the lower halfspace are accepted. XYZ coordinates are separated using semicola like so 0.0;0.0;0.0.

|	**SDMBinExponent**
Needs to be a positive integer. Is interpreted as 2^SDMBinExponent to define spatial resolution of the binning.
Uses one padding bin on either side.

|	**SDMNormalize**
Switch on 1 to normalize individually the intensity of the Fourier space images.

|	**WindowingMethod**
Set to 0 to use a rectangular binning. This offers a high frequency resolution but also high side lobes.

|	**LatticeIontypeCombinations**
Specify for which particular iontypes at all projections should be executed and at which frequency positions the resulting Fourier spectra are probed to compose a Fourier intensity image.
Every line is one image. Iontype naming convention from the PARAPROBE spatstat tool apply. Examples how to define what to probe use the following syntax::

    <entry targets="Al" realspace="0.404"/>

This means project, for each ROI, only the aluminium ions. From the resulting Fourier spectra compose the intensity image from the frequency bin which resembles closest the one for 0.404 nanometer in real space. One could compose another image like so::

    <entry targets="Sc" realspace="0.410"/>

This projects in addition for each ROI only Scandium ions and composes a separate image picking the Fourier spectrum power of the bin which resembles closest the one for 0.410 nanometer.
This could be an approach to separate e.g. Al3Sc precipitates from the aluminium matrix. It uses that within the precipitate the Sc ions form a Sc sub-lattice with 0.410 nanometer spacing. By contrast, aluminium should have a lower spacing.

|	**IOStoreHistoCnts**
Set to 1 to export for every ROI and every direction the raw histogram counts. **Be careful, this will consume very much memory and thus should only be used when a few ROIs for debugging purposes. Consult the memory consumption estimates below.**

|	**IOStoreHistoFFTMagn**
Set to 1 to export for every ROI the magnitude of the fast Fourier transform for the raw histogram counts. **Be careful, also this will consume very much memory which is why the above said applies.**


|	**IOStoreSpecificPeaks**
Set to 1 to store the FFT signal magnitude for the specific target bins detailed under LatticeIontypeCombinations.

|	**IOStoreThreeStrongestPeaks**
Set to 1 to store the FFT signal magnitude for the three strongest bins for every ROI and direction. Can be used to measure the signal-to-noise level.

Memory consumption per ROI and option::

    Ndir = NumberOfDirections
    Nbin = NumberOfBins (2^SDMBinExponent)
    Nimg = NumberOfLatticeIontypeCombinations
    HistoCnts = Nimg * Ndir * Nbin * 4 B/ROI
    HistoFFTMagn = Nimg * Ndir * Nbin * 4 B/ROI
    ThreeStrongest = Nimg * Ndir * 6 * 4 B/ROI
    SpecificPeak = Nimg * Ndir * 1 * 4 B/ROI

Assuming typical values::

    Ndir = 40962
    Nbin = 1024
    Nimg = 2
    HistoCnts = 320 MB/ROI
    HistoFFTMagn = 320 MB/ROI
    ThreeStrongest = 1.9 MB/ROI
    SpecificPeak = 0.32 MB/ROI

|	**InputfileElevAzimGrid**
Specifies the name of the H5 file which stores the directions along which to project the ions per ROI.

|	**GPUsPerComputingNode**
Specifies how many GPUs have been configured for the run, because currently this is not parsed from the operating system or runtime environment. Possible values are 0, in which cases GPUs will not be used and instead CPUs compute everything. Setting n should be used, with n equals 1 or 2 respectively. **This setting must match with the number of MPI process to use per node!** In other words, either no MPI process uses a GPU or every process an own one. In the second case GPUs per computing node are distributed modulo-based.

|	**GPUWorkload**
Empirical integer which specifies how many transforms should be executed per GPU, specifically how many more than on CPUs. Currently, PARAPROBE araullo employs heterogeneous parallelism. OpenMP threads query the ions within the ROIs. Thereafter, GPUs and possibly CPUs machine off these transforms in parallel. As modern GPUs like the V100 offer floating point performance equivalent to between 50 and 100 CPU cores, we use this WorkloadFactor to give the GPUs more work than the CPUs. In this way we reduce waiting of the GPUs for the CPUs to finish their work per cycle.


.. ROICenterInsideOnly
.. ROIVolumeInsideOnly
.. MaxMemoryPerNode
.. **KaiserAlpha**
.. Positive floating point value.
.. Set to 1 to use Kaiser windowing with Kaiser alpha, this reduces frequency resolution but also dampens side lobes.
