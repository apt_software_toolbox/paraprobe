Compile **paraprobe-autoreporter**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As a collection of Python scripts, the autoreporter does not need to be compiled.
It can be executed directly from Spyder or a Jupyter notebook. See corresponding hints in run paraprobe-autoreporter.
