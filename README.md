# paraprobe

**Purpose:**  
Strong scaling distributed- and shared-memory CPU-parallelized tools for mining Atom Probe Tomography data and GPU-parallelized Atom Probe Crystallography tools.

**Documentation:**  
A detailed documentation is available here:  
https://paraprobe-toolbox.readthedocs.io  

**Dependencies:**  
HDF5, MPI, OpenMP, (OpenACC, CUDA), CGAL, Voro++, Intel compiler or PGI compiler, Python, Matlab

**Execution:**  
Write command-line scripts to execute tools in orchestration.
Individual tools after proper environment setting according to mpiexec.

**Output:**  
The individual tools read from HDF5, POS, EPOS, RRNGg and XML files.
The individual tools write HDF5 and CSV files respectively.

**How to cite:**  
The code in this repository is under active development.   
All utilization of code portions or ideas from it should be fairly cited::  

    Markus Kühbach, Priyanshu Bajaj, Andrew Breen, Eric A. Jägle, and Baptiste Gault
    On Strong Scaling Open Source Tools for Mining Atom Probe Tomography Data
    Microscopy and Microanalysis, Vol 25, Supplement S2, 2019
    https://dx.doi.org/10.1017/S1431927619002228


**PARAPROBE** Strong Scaling Tools for Atom Probe Tomography  
https://gitlab.mpcdf.mpg.de/mpie-aptfim-toolbox/paraprobe  

Initial versions of a proof of concept implementation to these tools are hosted here:  
https://github.com/mkuehbach/PARAPROBE  
https://paraprobe.readthedocs.io  
