//##MK::CODE

#include "PARAPROBE_SyntheticHDF5.h"

//https://www.learncpp.com/cpp-tutorial/114-constructors-and-initialization-of-derived-classes/


synthetic_h5::synthetic_h5()
{
}


synthetic_h5::~synthetic_h5()
{
}


int synthetic_h5::create_synthetic_apth5( const string h5fn )
{
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

	//generate a PARAPROBE APTH5 HDF5 file
	//##MK::catch errors

	//domain specific HDF5 keywords and data fields
	groupid = H5Gcreate2(fileid,  PARAPROBE_SYNTH_VOLRECON, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << " PARAPROBE_SYNTH_VOLRECON " << status << "\n";

	groupid = H5Gcreate2(fileid,  PARAPROBE_SYNTH_VOLRECON_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << " PARAPROBE_SYNTH_VOLRECON_META " << status << "\n";

	groupid = H5Gcreate2(fileid,  PARAPROBE_SYNTH_VOLRECON_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << " PARAPROBE_SYNTH_VOLRECON_RES " << status << "\n";

	groupid = H5Gcreate2(fileid, PARAPROBE_HARDWARE, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
	status = H5Gclose(groupid);
cout << " PARAPROBE_HARDWARE_META " << status << "\n";
	groupid = H5Gcreate2(fileid, PARAPROBE_HARDWARE_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
	status = H5Gclose(groupid);
cout << " PARAPROBE_HARDWARE_META " << status << "\n";

	status = H5Fclose(fileid);
cout << "Closing APTH5 file" << "\n";

	return WRAPPED_HDF5_SUCCESS;
}
