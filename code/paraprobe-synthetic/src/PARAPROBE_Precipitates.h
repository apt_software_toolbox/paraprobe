//##MK::GPLV3

#ifndef __PARAPROBE_SYNTHETIC_PRECIPITATES_H__
#define __PARAPROBE_SYNTHETIC_PRECIPITATES_H__

#include "PARAPROBE_SoluteModel.h"

bool SortRadiiDesc( apt_real & first, apt_real & second );


struct spherical_particle
{
	p3d center;
	apt_real radius;
	spherical_particle() : center(p3d()), radius(0.0) {}
	spherical_particle( const p3d _c, const apt_real _r) : center(_c), radius(_r) {}

	void get_atoms( unitcell_geometry const & ez, t3x3 const & ori, vector<pos> & out );
};


class precipitateModel
{
public:
	precipitateModel();
	~precipitateModel();
	//precipitateModel(const geomodel & geom,
	//		const size_t N, const apt_real rm, const apt_real rvar );

	//void reportParticles( const unsigned int simid, const int rank );
	//void reportParticlesVTK( const unsigned int simid, const int rank );
	void initialize( geomodel const & geom, const size_t N, const apt_real rmean, const apt_real rvar );

	vector<spherical_particle> particles;
	vector<squat> orientation;
	
//private:
	mt19937 urng;			//a random number generator to sample from distribution curve types
};


#endif

