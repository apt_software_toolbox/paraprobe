//##MK::CODESPLIT

#include "PARAPROBE_SyntheticHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2


bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigSynthetic::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigSynthetic::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << endl << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << endl;
	}
	cout << endl;
	return true;
}


void synthesize_specimens()
{
	//allocate sequential transcoder instance
	double tic = omp_get_wtime();

	syntheticHdl* synthetic = NULL;
	try {
		synthetic = new syntheticHdl;
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate a sequential syntheticHdl instance" << "\n";
		delete synthetic; synthetic = NULL; return;
	}

	if ( synthetic->init_target_file() == true ) {
		cout << "Rank " << MASTER << " results file initialized!" << "\n";
	}
	else {
		cerr << "Rank " << MASTER << " results file creation failed!" << "\n";
		delete synthetic; synthetic = NULL; return;
	}

	double toc = omp_get_wtime();
	memsnapshot mm = memsnapshot();
	synthetic->synthetic_tictoc.prof_elpsdtime_and_mem( "InitResultsFile", APT_XX, APT_IS_SEQ, mm, tic, toc );

	//build first the single/polycrystalline atomic geometry
	switch (ConfigSynthetic::SynthesizingMode)
	{
		case E_SYNTHESIS_DEVELOPMENT_SX:
			if( synthetic->generate_synthetic_sx_mmm_omp() == true ) {
				cout << "Synthetic single-phase, single-crystalline specimen successfully generated" << "\n";
			}
			else {
				cerr << "Unable to synthesize a specimen" << "\n";
				delete synthetic; synthetic = NULL; return;
			}
			break;
		case E_SYNTHESIS_DEVELOPMENT_PX:
			if( synthetic->generate_synthetic_px_mmm_omp() == true ) {
				cout << "Synthetic single-phase, polycrystalline specimen with Voronoi successfully generated" << "\n";
			}
			else {
				cerr << "Unable to synthesize a specimen" << "\n";
				delete synthetic; synthetic = NULL; return;
			}
			break;
		default:
			cerr << "Unable to synthesize a specimen" << "\n";
			delete synthetic; synthetic = NULL; return;
	}

	//optionally add precipitates
	if (ConfigSynthetic::NumberOfCluster > 0 ) {
		if( synthetic->generate_synthetic_precipitates_debug() == true ) {
			cout << "Adding of precipitates was successful" << "\n";
		}
		else {
			cerr << "Unable to add precipitates" << "\n";
			delete synthetic; synthetic = NULL; return;
		}
	}

	tic = omp_get_wtime();

	synthetic->get_total_ion_count();

	//report results
	synthetic->settings_to_apth5();
	synthetic->specimen_to_apth5();
	synthetic->grains_to_apth5();
	synthetic->precipitates_to_apth5();

	toc = omp_get_wtime();
	mm = synthetic->synthetic_tictoc.get_memoryconsumption();
	synthetic->synthetic_tictoc.prof_elpsdtime_and_mem( "WriteSpecimenToAPTH5andXDMF", APT_XX, APT_IS_SEQ, mm, tic, toc);
cout << "Writing specimen to APTH5 and XDMF took " << (toc-tic) << " seconds" << "\n";
	tic = omp_get_wtime();

	if ( ConfigSynthetic::SynthesizingMode == E_SYNTHESIS_DEVELOPMENT_PX ) {
		synthetic->tessellation_to_apth5();
	}

	toc = omp_get_wtime();
	mm = synthetic->synthetic_tictoc.get_memoryconsumption();
	synthetic->synthetic_tictoc.prof_elpsdtime_and_mem( "WriteTessellationToAPTH5andXDMF", APT_XX, APT_IS_SEQ, mm, tic, toc);
cout << "Writing specimen to APTH5 and XDMF took " << (toc-tic) << " seconds" << "\n";

	synthetic->synthetic_tictoc.spit_profiling( "Synthetic", ConfigShared::SimID, MASTER );

	//release resources
	delete synthetic; synthetic = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	//##MK::n/a

//EXECUTE SPECIFIC TASKS
	synthesize_specimens();

//DESTROY MPI
	//##MK::n/a

	double toc = omp_get_wtime();
	//##MK::synthetic_tictoc.
	cout << "paraprobe-synthetic took " << (toc-tic) << " seconds wall-clock time in total" << endl;

	return 0;
}
