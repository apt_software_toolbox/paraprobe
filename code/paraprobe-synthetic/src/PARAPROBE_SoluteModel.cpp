//##MK::GPLV3

#include "PARAPROBE_SoluteModel.h"


ostream& operator<<(ostream& in, speci const & val)
{
	in << "concentration " << val.c << " mass-to-charge " << val.m2q << " iontype " << (int) val.ityp << "\n";
	return in;
}


bool SortSpeciAscComposition( speci & first, speci & second )
{
	return first.c < second.c;
}


soluteModel::soluteModel()
{
}


void soluteModel::initialize( long customseed )
{
	//##MK::urng.seed( Config)
	urng.seed( customseed );
	urng.discard( ConfigSynthetic::PRNGWarmup );

	//##MK::debug building of composition, must be the same as in the range file
	//https://www.aircraftmaterials.com/data/aluminium/1200.html
	//http://arc.nucapt.northwestern.edu/refbase/files/Royset-2005.pdf

	//there is always the unknown iontype
	speci XX = DEFAULT_SPECI;
	composition.push_back( XX );
	
	//pure aluminium, use dummy isotopes
	speci Al27 = speci( 0.98, 26.8, static_cast<unsigned char>(1) );
	composition.push_back( Al27 );
	
	speci Mg24 = speci( 0.019, 24.3, static_cast<unsigned char>(2) );
	composition.push_back( Mg24 );
	
	speci Cu63 = speci( 0.019, 63.5, static_cast<unsigned char>(3) );
	composition.push_back( Cu63 );
	
	/*speci Al27 = speci( 0.980, 26.8, 1 );
	speci Fe56 = speci( 0.010, 55.93, 2 );
	speci Si28 = speci( 0.008, 27.97, 3 );*/

	speci Sc45 = speci( 0.002, 44.955, static_cast<unsigned char>(4) );
	composition.push_back( Sc45 );

	/*composition.push_back( Al27 );
	composition.push_back( Fe56 );
	composition.push_back( Si28 );*/

	//sort and renormalize
	sort( composition.begin(), composition.end(), SortSpeciAscComposition );

	//as cdf is a copy we can modify now the concentration values to be a cdf
	apt_real renorm = 0.f;
	for(size_t i = 0; i < composition.size(); i++)
		renorm += composition.at(i).c;

	apt_real sum = 0.f;
	for(size_t i = 0; i < composition.size(); i++) {
		sum += composition.at(i).c;
		composition.at(i).c = sum / renorm;

		#pragma omp master
		{
			cout << "i/c " << i << "\t\t" << composition.at(i).c << "\t\t" << composition.at(i).m2q << "\t\t" << (int) composition.at(i).ityp << "\n";
		}
	}
}


/*
soluteModel::soluteModel( vector<speci> const & composition ) 
{
	urng.seed( ConfigSynthetic::PRNGWorldSeed );
	urng.discard( ConfigSynthetic::PRNGWarmup );

	//##MK::debug building of composition, must be the same as in the range file
	//https://www.aircraftmaterials.com/data/aluminium/1200.html
	//http://arc.nucapt.northwestern.edu/refbase/files/Royset-2005.pdf

	//there is always the unknown iontype
	speci XX = DEFAULT_SPECI;
	composition.push_back( XX );

	cerr << "soluteModel::soluteModel constructor not implemented!" << "\n";
}
*/


soluteModel::~soluteModel()
{

}
	
	
speci soluteModel::get_random_speci()
{
	//acceptance rejection sampling approach with composition CDF to pick a random ion's mass2charge
	//key idea sort compositions in ascending order based on composition compute CDF value

	//pick uniform random number on [0,1)
	uniform_real_distribution<apt_real> distribution(0.f,1.f);
	apt_real cdfval = distribution(urng);

	//use in the mentality of rejection sampling to get corresponding speci index
	for(auto it = composition.begin(); it != composition.end(); it++) {
		if ( cdfval > it->c ) { //larger than bin end?
			continue;
		}
		return *it; //##MK::because for synthetic tips we do so far an in-place ranging;
	}

	//use speci index to get mass to charge
	cerr << "SoluteModel faced an unexpected control flow returning UNKNOWN_IONTYPE " << "\n";
	return DEFAULT_SPECI;
}
