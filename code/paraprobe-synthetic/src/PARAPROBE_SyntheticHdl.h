//##MK::CODESPLIT


#ifndef __PARAPROBE_SYNTHETICHDL_H__
#define __PARAPROBE_SYNTHETICHDL_H__

#include "PARAPROBE_Precipitates.h"


class syntheticHdl
{
	//process-level class which implements the worker instance which synthesizes different virtual APT
	//specimens result are written to a specific HDF5 file

public:
	syntheticHdl();
	~syntheticHdl();

	bool generate_synthetic_sx_mmm_omp();
	bool generate_synthetic_px_mmm_omp();
	bool generate_synthetic_precipitates_debug();

	void get_total_ion_count();

	bool init_target_file();
	void settings_to_apth5();
	void specimen_to_apth5();
	void grains_to_apth5();
	void precipitates_to_apth5();
	void tessellation_to_apth5();

	//void set_mpidatatypes( void );
	//inline int get_rank( void ) { return myRank; }
	//inline int get_nranks( void ) { return nRanks; }
	//void set_rank( const int rr ) { myRank = rr; }
	//void set_nranks( const int nnrr ) { nRanks = nnrr; }

	//xdmfHdl debugxdmfHdl;
	synthetic_h5 debugh5Hdl;
	synthetic_xdmf debugxdmf;

	spacebucket rawdata_alf;
	unsigned long nevt;

	geomodel tipgeometry;
	unitcellparms latticeA;
	grainAggregate polycrystal;
	precipitateModel prec;
	tessHdl tess;

	profiler synthetic_tictoc;

private:
	//MPI related
	//int myRank;											//my MPI ID in the MPI_COMM_WORLD
	//int nRanks;
};



#endif

