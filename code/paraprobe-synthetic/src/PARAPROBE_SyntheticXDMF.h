//##MK::CODE

#ifndef __PARAPROBE_SYNTHETIC_XDMF_H__
#define __PARAPROBE_SYNTHETIC_XDMF_H__

#include "PARAPROBE_SyntheticHDF5.h"

class synthetic_xdmf : public xdmfHdl
{
	//tool-specific sub-class of a XDMF inheriting all methods of the base class h5Hdl
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	synthetic_xdmf();
	~synthetic_xdmf();

	int create_volrecon_file( const string xmlfn, const size_t nions, const string h5ref );
	int create_precipitate_file( const string xmlfn, const size_t nprec, const string h5ref );
	int create_voronoicrystal_file( const string xmlfn, const size_t nelem, const size_t ntopo,
			const size_t ngeom, const size_t nhalo, const string h5ref );

//private:
};

#endif
