//##MK::CODE

#include "CONFIG_Synthetic.h"

E_SYNTHESIS_MODE ConfigSynthetic::SynthesizingMode = E_SYNTHESIS_NOTHING;
//string ConfigSynthetic::Outputfile = "";
string ConfigSynthetic::SimMatrixOrientation = "0.0;0.0;0.0";
string ConfigSynthetic::SimPrecipitateOrientation = "0.0;0.0;0.0";

apt_real ConfigSynthetic::RelBottomRadius = 0.10;
apt_real ConfigSynthetic::RelTopRadius = 0.05;
apt_real ConfigSynthetic::RelBottomCapHeight = 0.05;
apt_real ConfigSynthetic::RelTopCapHeight = 0.05;
apt_real ConfigSynthetic::SimDetEfficiency = 1.0;
apt_real ConfigSynthetic::SpatResolutionSigmaX = 0.f; //nm
apt_real ConfigSynthetic::SpatResolutionSigmaY = 0.f; //nm
apt_real ConfigSynthetic::SpatResolutionSigmaZ = 0.f; //nm
apt_real ConfigSynthetic::ClusterRadiusMean = EPSILON; //nm
apt_real ConfigSynthetic::ClusterRadiusSigmaSqr = EPSILON; //nm

unitcell_geometry ConfigSynthetic::LatticeA = unitcell_geometry( 0.404, 0.404, 0.404, 90.0, 90.0, 90.0 );
unitcell_geometry ConfigSynthetic::LatticeB = unitcell_geometry( 0.410, 0.410, 0.410, 90.0, 90.0, 90.0 );

apt_real ConfigSynthetic::CrystalloVoroMeanRadius = 5.f;

size_t ConfigSynthetic::NumberOfAtoms = 0;
size_t ConfigSynthetic::NumberOfCluster = 0;

string ConfigSynthetic::PRNGType = "MT19937";
size_t ConfigSynthetic::PRNGWarmup = 700000;
long ConfigSynthetic::PRNGWorldSeed = -12345678;
size_t ConfigSynthetic::SimAnnealingIterMax = 1000000;
size_t ConfigSynthetic::TessellationPointsPerBlock = 5;


bool ConfigSynthetic::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Input.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigSynthetic")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "SynthesizingMode" );
	switch (mode)
	{
		case E_SYNTHESIS_DEVELOPMENT_SX:
			SynthesizingMode = E_SYNTHESIS_DEVELOPMENT_SX; break;
		case E_SYNTHESIS_DEVELOPMENT_PX:
			SynthesizingMode = E_SYNTHESIS_DEVELOPMENT_PX; break;
		default:
			SynthesizingMode = E_SYNTHESIS_NOTHING;
	}
	//Outputfile = read_xml_attribute_string( rootNode, "Outputfile" );
	SimMatrixOrientation = read_xml_attribute_string( rootNode, "SimMatrixOrientation" );

	RelBottomRadius = read_xml_attribute_float( rootNode, "SimRelBottomRadius" );
	RelTopRadius = read_xml_attribute_float( rootNode, "SimRelTopRadius" );
	RelBottomCapHeight = read_xml_attribute_float( rootNode, "SimRelBottomCapHeight" );
	RelTopCapHeight = read_xml_attribute_float( rootNode, "SimRelTopCapHeight" );
	SimDetEfficiency = read_xml_attribute_float( rootNode, "SimDetectionEfficiency" );
	SpatResolutionSigmaX = read_xml_attribute_float( rootNode, "SimFiniteSpatResolutionX" );
	SpatResolutionSigmaY = read_xml_attribute_float( rootNode, "SimFiniteSpatResolutionY" );
	SpatResolutionSigmaZ = read_xml_attribute_float( rootNode, "SimFiniteSpatResolutionZ" );
	ClusterRadiusMean = read_xml_attribute_float( rootNode, "SimClusterRadiusMean" );
	ClusterRadiusSigmaSqr = read_xml_attribute_float( rootNode, "SimClusterRadiusSigmaSqr" );

	LatticeA = unitcell_geometry( 	read_xml_attribute_float( rootNode, "a1" ),
									read_xml_attribute_float( rootNode, "b1" ),
									read_xml_attribute_float( rootNode, "c1" ),
									read_xml_attribute_float( rootNode, "alpha1" ),
									read_xml_attribute_float( rootNode, "beta1" ),
									read_xml_attribute_float( rootNode, "gamma1" )    );

	LatticeB = unitcell_geometry( 	read_xml_attribute_float( rootNode, "a2" ),
									read_xml_attribute_float( rootNode, "b2" ),
									read_xml_attribute_float( rootNode, "c2" ),
									read_xml_attribute_float( rootNode, "alpha2" ),
									read_xml_attribute_float( rootNode, "beta2" ),
									read_xml_attribute_float( rootNode, "gamma2" )    );

	if ( SynthesizingMode == E_SYNTHESIS_DEVELOPMENT_PX ) {
		CrystalloVoroMeanRadius = read_xml_attribute_float( rootNode, "CrystalloVoroMeanRadius" );
	}

	NumberOfAtoms = read_xml_attribute_float( rootNode, "SimNumberOfAtoms" );
	NumberOfCluster = read_xml_attribute_float( rootNode, "SimNumberOfCluster" );

	if ( NumberOfCluster > EPSILON ) {
		SimPrecipitateOrientation = read_xml_attribute_string( rootNode, "SimPrecipitateOrientation" );
	}

	return true;
}


bool ConfigSynthetic::checkUserInput()
{
	cout << "ConfigSynthetic::" << "\n";
	switch (SynthesizingMode)
	{
		case E_SYNTHESIS_DEVELOPMENT_SX:
			cout << "Building single-phase, single-crystalline Al fcc virtual specimen in orientation as specified" << "\n"; break;
		case E_SYNTHESIS_DEVELOPMENT_PX:
			cout << "Building single-phase, polycrystalline Al fcc virtual specimen from Voronoi-Tessellation with random texture" << "\n"; break;
		default:
			cerr << "No synthesis chosen about to quit" << "\n"; return false;
	}
	//cout << "Output written to " << Outputfile << "\n";

	if ( RelBottomRadius < EPSILON ) {
		cerr << "RelBottomRadius must be positive and non-zero!" << "\n"; return false;
	}
	cout << "RelBottomRadius " << RelBottomRadius << "\n";
	if ( RelTopRadius < EPSILON || RelTopRadius > RelBottomRadius ) {
		cerr << "RelTopRadius must not be larger than bottom, positive and non-zero!" << "\n"; return false;
	}
	cout << "RelTopRadius " << RelTopRadius << "\n";
	if ( RelBottomCapHeight < 0.f || RelBottomCapHeight > RelBottomRadius ) { //##MK::carve out not more than a halfsphere from bottom of the frustum
		cerr << "RelBottomCapHeight must be positive and not larger than RelBottomRadius!" << "\n"; return false;
	}
	cout << "RelBottomCapHeight " << RelBottomCapHeight << "\n";
	if ( RelTopCapHeight < 0.f || RelTopCapHeight > RelTopRadius ) {
		cerr << "RelTopCapHeight must be positive and not larger than RelTopRadius!" << "\n"; return false; //##MK::add further constraints on cap heights
	}
	cout << "RelTopCapHeight " << RelTopCapHeight << "\n";
	if ( NumberOfAtoms < 1 ) {
		cerr << "The tip must contain at least one atom!" << "\n"; return false;
	}
	cout << "Number of atoms " << NumberOfAtoms << "\n";
	cout << "Orientation " << SimMatrixOrientation << "\n";
	if ( SimDetEfficiency < EPSILON || SimDetEfficiency > 1.f ) {
		cerr << "The simulated detection efficiency needs to on interval (0,1]!" << "\n"; return false;
	}
	cout << "DetectionEfficiency " << SimDetEfficiency << "\n";
	if ( SpatResolutionSigmaX < 0.f || SpatResolutionSigmaY < 0.f || SpatResolutionSigmaZ < 0.f ) {
		cerr << "The simulated finite spatial resolutions must be positive if not zero!" << "\n"; return false;
	}
	cout << "FiniteSpatResSigmaX " << SpatResolutionSigmaX << "\n";
	cout << "FiniteSpatResSigmaY " << SpatResolutionSigmaY << "\n";
	cout << "FiniteSpatResSigmaZ " << SpatResolutionSigmaZ << "\n";
	if ( NumberOfCluster < 0 ) { //MK::choose 0 to generate no cluster
		cerr << "The number of cluster must be positive and finite or zero!" << "\n"; return false;
	}
	else {
		cout << "Number of cluster " << NumberOfCluster << "\n";
		cout << "SimPrecipitateOrientation " << SimPrecipitateOrientation << "\n";
	}

	//##MK::at the moment using distribution mu and sigma^2 instead of mean and variance interpreting values in nanometer
	if ( ClusterRadiusMean < 0.f ) {
		cerr << "The mean of the lognormal cluster size distribution must not be negative or non-zero!" << "\n"; return false;
	}
	cout << "ClusterRadiusMean " << ClusterRadiusMean << "\n";
	if ( ClusterRadiusSigmaSqr < EPSILON ) {
		cout << "The sigma(variance) of the lognormal cluster size distribution must not be negative or non-zero!" << "\n"; return false;
	}
	cout << "ClusterRadiusVar " << ClusterRadiusSigmaSqr << "\n";

	if ( LatticeA.a < EPSILON || LatticeA.b < EPSILON || LatticeA.c < EPSILON ) {
		cerr << "LatticeConstantsA a1, b1, and c1 must be positive and non-zero!" << "\n"; return false;
	}
	cout << "a1 " << LatticeA.a << "\n";
	cout << "b1 " << LatticeA.b << "\n";
	cout << "c1 " << LatticeA.c << "\n";
	if ( LatticeA.alpha < EPSILON || LatticeA.alpha > DEGREE2RADIANT(180.f) ||
			LatticeA.beta < EPSILON || LatticeA.beta > DEGREE2RADIANT(180.f) ||
				LatticeA.gamma < EPSILON || LatticeA.gamma > DEGREE2RADIANT(180.f) ) {
		cerr << "LatticeConstantsA alpha1, beta1, gamma1 must be positive and less than 180deg " << "\n"; return false;
	}
	cout << "LatticeA.alpha " << LatticeA.alpha << "\n";
	cout << "LatticeA.beta " << LatticeA.beta << "\n";
	cout << "LatticeA.gamma " << LatticeA.gamma << "\n";

	if ( LatticeB.a < EPSILON || LatticeB.b < EPSILON || LatticeB.c < EPSILON ) {
		cerr << "LatticeConstantsB a2, b2, and c2 must be positive and non-zero!" << "\n"; return false;
	}
	cout << "a2 " << LatticeB.a << "\n";
	cout << "b2 " << LatticeB.b << "\n";
	cout << "c2 " << LatticeB.c << "\n";
	if ( LatticeB.alpha < EPSILON || LatticeB.alpha > DEGREE2RADIANT(180.f) ||
			LatticeB.beta < EPSILON || LatticeB.beta > DEGREE2RADIANT(180.f) ||
				LatticeB.gamma < EPSILON || LatticeB.gamma > DEGREE2RADIANT(180.f) ) {
		cerr << "LatticeConstantsB alpha2, beta2, gamma2 must be positive and less than 180deg " << "\n"; return false;
	}
	cout << "LatticeB.alpha " << LatticeB.alpha << "\n";
	cout << "LatticeB.beta " << LatticeB.beta << "\n";
	cout << "LatticeB.gamma " << LatticeB.gamma << "\n";

	if ( SynthesizingMode == E_SYNTHESIS_DEVELOPMENT_PX ) {
		if ( CrystalloVoroMeanRadius < EPSILON ) {
			cerr << "CrystalloVoroMeanRadius must be positive !" << "\n"; return false;
		}
		cout << "CrystalloVoroMeanRadius " << CrystalloVoroMeanRadius << "\n";
		cout << "TessellationPointsPerBlock " << TessellationPointsPerBlock << "\n";
	}

	cout << "PRNGType " << PRNGType << "\n";
    cout << "PRNGWarmup " << PRNGWarmup << "\n";
	cout << "PRNGWorldSeed " << PRNGWorldSeed << "\n";

	cout << "\n";
	return true;
}

