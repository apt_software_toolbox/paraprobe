//##MK::CODE

#ifndef __PARAPROBE_RANGER_HDF_H__
#define __PARAPROBE_RANGER_HDF_H__

//shared headers
/*
#include "../../paraprobe-utils/src/PARAPROBE_APTH5.h"
#include "../../paraprobe-utils/src/PARAPROBE_XDMF.h"
//##already included through utils/src/PARAPROBE_VolumeSampler.h
*/

#include "../../paraprobe-utils/src/PARAPROBE_APTH5.h"

#include "../../paraprobe-utils/src/metadata/PARAPROBE_RangerMetadataDefsH5.h"

#include "../../paraprobe-utils/src/PARAPROBE_XDMF.h"

//tool-specific headers
#include "CONFIG_Ranger.h"
/*
#include "METADATA_RangerDefsHDF5.h"
*/

class ranger_h5 : public h5Hdl
{
	//tool-specific sub-class of a HDF5 inheriting all methods of the base class h5Hdl but adds tool specific read/write functions
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	ranger_h5();
	~ranger_h5();


	int append_ranger_apth5( const string h5fn );

//private:
};


#endif
