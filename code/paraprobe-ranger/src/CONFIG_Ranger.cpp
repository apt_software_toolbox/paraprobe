//##MK::CODE

#include "CONFIG_Ranger.h"

string ConfigRanger::InputfilePSE = "";
string ConfigRanger::InputfileComposition = "";
string ConfigRanger::InputfileContaminants = "";
string ConfigRanger::InputfileReconstruction = "";
string ConfigRanger::InputfileRangingData = "";

RANGING_METHOD ConfigRanger::RangingMethod = IVAS;
lival ConfigRanger::AMUBinning = lival();

unsigned int ConfigRanger::SNIPIterations = 20;
unsigned int ConfigRanger::SavitzkyGolayM = 0;
unsigned int ConfigRanger::PeaksWindowingHalfwidth = 10;

apt_real ConfigRanger::PeaksSignalToNoiseRatio = 2.0;
apt_real ConfigRanger::RangingFWHM = 0.9;
apt_real ConfigRanger::RangingFlankBumpTol = 1.0;


bool ConfigRanger::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Ranger.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigRanger")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;
  
    InputfilePSE = read_xml_attribute_string( rootNode, "InputfilePSE" );
    InputfileReconstruction = read_xml_attribute_string( rootNode, "InputfileReconstruction" );
	
    mode = read_xml_attribute_uint32( rootNode, "RangingMethod" );
    switch (mode) 
    {
        case IVAS:
            RangingMethod = IVAS; break;
        case PARAPROBE:
            RangingMethod = PARAPROBE; break;
        default:
            RangingMethod = IVAS;
    }
    
	if ( RangingMethod == IVAS ) {
		InputfileRangingData = read_xml_attribute_string( rootNode, "InputfileRangingData" );
	}
	
    if ( RangingMethod == PARAPROBE ) {
        InputfileComposition = read_xml_attribute_string( rootNode, "InputfileComposition" );
		InputfileContaminants = read_xml_attribute_string( rootNode, "InputfileContaminants" );
		
		AMUBinning = lival(     0.0,
                                read_xml_attribute_float( rootNode, "AMUBinningWidth" ),
                                read_xml_attribute_float( rootNode, "AMUBinningMax" )    );
        
        SNIPIterations = read_xml_attribute_uint32( rootNode, "SNIPIterations" );
        SavitzkyGolayM = read_xml_attribute_uint32( rootNode, "SavitzkyGolayM" );
        PeaksWindowingHalfwidth = read_xml_attribute_uint32( rootNode, "PeakDetectionWindowingHalfwidth" );
        PeaksSignalToNoiseRatio = read_xml_attribute_float( rootNode, "PeakDetectionSignalToNoise" );
        RangingFWHM = read_xml_attribute_float( rootNode, "RangingFWHM" );
        RangingFlankBumpTol = read_xml_attribute_float( rootNode, "RangingFlankBumpTol" );
    }
    
	return true;
}


bool ConfigRanger::checkUserInput()
{
	cout << "ConfigRanger::" << "\n";
    if ( RangingMethod == IVAS ) {
        cout << "Using external ranging data from e.g. IVAS" << "\n";
        //check if InputfileRangingData  contains a dot if so check if right part is rng or rrng
        /*
        if ( count(InputfileRangingData.begin(), InputfileRangingData.end(), '.') != 1 ) {
            cerr << "InputfileRangingData seems to contain no file ending!" << "\n"; return false;
        }
        stringstream parsethis;
        parsethis << InputfileRangingData; //e.g. "Dummy.rrng
        string datapiece;
        getline( parsethis, datapiece, '.' ); //Dummy
        getline( parsethis, datapiece ); //.rrng (end terminator?)
        cout << "Datapiece __" << datapiece << "__ " << datapiece.size() << "\n";
		//if ( datapiece != "rrng" && datapiece != "rng" ) {
        */
        if ( InputfileRangingData.substr(InputfileRangingData.length()-4) != ".rng" &&
        		InputfileRangingData.substr(InputfileRangingData.length()-5) != ".rrng" ) {
        	 cerr << "InputfileRangingData contains invalid file format ending, .rng and .rrng is accepted only!" << "\n"; return false;
        }
        cout << "InputfileRangingData read from " << InputfileRangingData << "\n";        
    }
    else { //PARAPROBE
        cout << "Generating ranging data" << "\n";
		cout << "Reading composition from " << InputfileComposition << "\n";
		cout << "Reading contaminants from " << InputfileContaminants << "\n";
        if ( AMUBinning.incr < EPSILON ) {
            cerr << "AMUBinningIncr must be positive and finite!" << "\n"; return false;
        }
        if ( AMUBinning.max < EPSILON ) {
            cerr << "AMUBinningMax must be positive and finite!" << "\n"; return false;
        }
        if ( AMUBinning.max < AMUBinning.min ) {
            cerr << "AMUBinningMax must be at least as large as AMUBinningMin!" << "\n"; return false;
        }
        cout << "AMUBinningMin " << AMUBinning.min << " amu" << "\n";
        cout << "AMUBinningIncr " << AMUBinning.incr << " amu" << "\n"; 
        cout << "AMUBinningMax " << AMUBinning.max << " amu" << "\n";
        //accept SNIPIterations == 0 will not perform SNIP
        cout << "SNIPIterations " << SNIPIterations << "\n";
        //accept SavitzkyGolay == 0 will not do Savitzky Golay Smoothing
        if ( SavitzkyGolayM > 13 ) {
            cerr << "SavitzkyGolayM smoothing currently implemented only for m<=13!" << "\n"; return false;
        }
        cout << "SavitzkyGolayM " << SavitzkyGolayM << "\n";
        if ( PeaksWindowingHalfwidth < 1 ) {
            cerr << "PeaksWindowingHalfwidth must be at least 1!" << "\n"; return false;
        }
        cout << "PeaksWindowingHalfwidth " << PeaksWindowingHalfwidth << "\n";
        if ( PeaksSignalToNoiseRatio <= (1.0 - EPSILON) ) {
            cerr << "PeaksSignalToNoiseRatio too small, peaks have to be at least the noise level!" << "\n"; return false;
        }
        cout << "PeaksSignalToNoiseRatio " << PeaksSignalToNoiseRatio << "\n";
        if ( RangingFWHM < 0.1 ) {
            cerr << "RangingWidth should be at least 0.1!" << "\n"; return false;
        }
        cout << "RangingFWHM " << RangingFWHM << "\n";
        if ( RangingFlankBumpTol < EPSILON ) {
            cerr << "RangingFlankBumpTolerance must be positive and finite!" << "\n"; return false;
        }
        cout << "RangingFlankBumpTol " << RangingFlankBumpTol << "\n";
    }
    
    cout << "InputfilePSE read from " << InputfilePSE << "\n";    
    cout << "InputfileReconstruction read from " << InputfileReconstruction << "\n";
    
	cout << "\n";
	return true;
}

