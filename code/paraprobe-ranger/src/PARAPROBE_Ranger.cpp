//##MK::CODESPLIT

#include "PARAPROBE_RangerHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

//##MK::for querying system maximum physical memory see
//https://stackoverflow.com/questions/2513505/how-to-get-available-memory-c-g


bool init(  int pargc, char** pargv )
{
    ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigRanger::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigRanger::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	
	if ( ConfigRanger::RangingMethod == PARAPROBE ) {
        //##MK::try to load composition and contaminants
		cerr << "RangingMethod PARAPROBE currently not implemented!" << "\n"; return false;
    }
    
	cout << endl;
	return true;
}


void ranging( const int r, const int nr )
{
	//allocate process-level instance of a rangeHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	rangerHdl* rng = NULL;

	int localhealth = 1;
	int globalhealth = nr;
	try {
		rng = new rangerHdl;
		rng->set_myrank(r);
		rng->set_nranks(nr);
		//rng->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate an rangerHdl class object instance" << "\n"; localhealth = 0;
	}

	//load periodic table of elements and isotope to prepare ranging
	if ( rng->worker.nuclides.load_isotope_library( ConfigRanger::InputfilePSE ) == false ) {
		cerr << "Rank " << MASTER << " loading of PeriodicTableOfElements failed!" << "\n";
		delete rng; rng = NULL; return;
	}

	//do we have all processes on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete rng; rng = NULL; return;
	}
	
	//we have all on board, read reconstruction
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast
	//we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( rng->get_myrank() == MASTER ) {
		if ( rng->read_reconxyz_from_apth5() == true ) {
			cout << "MASTER read successfully all ion coordinates of the specimen in reconstruction space" << "\n";
		}
		else {
			cerr << "MASTER was unable to read a specimen in reconstruction space!" << "\n"; localhealth = 0;
		}
		if ( rng->read_mass2charge_from_apth5() == true ) {
            cout << "MASTER read successfully all mass-to-charge values of the specimen" << "\n";
		}
		else {
			cerr << "MASTER was unable to read mass-to-charge values!" << "\n"; localhealth = 0;
		}
		
		if ( rng->xyz.size() != rng->m2q.size() ) {
            cerr << "Not every ion seems to have a mass-to-charge value!" << "\n"; localhealth = 0;
        }
    }
    //else {} ##no slaves
	//##MK::strictly speaking not necessary, but second order issue for about 80 processes as on TALOS...?
	//MPI_Barrier( MPI_COMM_WORLD );

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that all data have arrived!" << "\n";
		delete rng; rng = NULL; 
		return;
	}

	rng->execute_local_workpackage();

	cout << "Rank " << r << " has completed its workpackage" << "\n";
	//no barrier required SINGLE_PROCESS only MPI_Barrier(MPI_COMM_WORLD);

	//no sync of processes required when SINGLE_PROCESS
    //MPI_Barrier( MPI_COMM_WORLD );
    
	if ( rng->get_myrank() == MASTER ) {
        
		if ( rng->init_target_file() == true ) {
			cout << "Rank MASTER successfully initialized APTH5 results file" << "\n";
		}
		else {
			cerr << "Rank MASTER unable to initialize results APTH5 file" << "\n"; localhealth = 0;
		}

        //MK::no initialization of a new file
        //InputfileReconstruction is supplemented!

		if ( rng->write_metadata_to_apth5() == true ) { //does not require MPI communication all results already on the master
			cout << "Rank MASTER successfully wrote all materialpoints to an APTH5 file" << "\n";
		}
		else {
			cerr << "Rank MASTER unable to write all materialpoints to the APTH5 file" << "\n"; localhealth = 0;
		}
		
		if ( rng->write_results_to_apth5() == true ) {
            cout << "Rank MASTER successfully wrote all materialpoints to an APTH5 file" << "\n";
		}
		else {
			cerr << "Rank MASTER unable to write all materialpoints to the APTH5 file" << "\n"; localhealth = 0;
		}
	}
	
	//no sync of processes required when SINGLE_PROCESS
    //MPI_Barrier( MPI_COMM_WORLD );
    
	//release resources
	delete rng; rng = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	
	//##MK::currently using MPI to be prepared for the future but only one process is required
	if ( nr == SINGLEPROCESS ) {
        
        cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
    
//EXECUTE SPECIFIC TASK
        ranging( r, nr );
    }
    else {
        cerr << "Rank " << r << " currently paraprobe-ranger is implemented for a single process only!" << "\n";
    }
    
//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	cout << "Rank " << r << " left MPI_COMM_WORLD deconstructed" << endl;
	double toc = omp_get_wtime();
	
	cout << "paraprobe-ranger took " << (toc-tic) << " seconds wall-clock time in total" << endl;
	return 0;
}
