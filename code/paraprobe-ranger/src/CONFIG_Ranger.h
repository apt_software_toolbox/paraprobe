//##MK::CODE

#ifndef __PARAPROBE_CONFIG_RANGER_H__
#define __PARAPROBE_CONFIG_RANGER_H__

#include "../../paraprobe-utils/src/PARAPROBE_Histogram.h"
#include "../../paraprobe-utils/src/CONFIG_Shared.h"
//#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"


enum RANGING_METHOD
{
	IVAS,						//external ranging information, ranging was performed with APSuite/IVAS or manually
	PARAPROBE					//internal peak search and possible ion type searches
};


class ConfigRanger
{
public:
	
	static string InputfilePSE;			//for loading the periodic table of elements
	static string InputfileComposition;
	static string InputfileContaminants;
	static string InputfileReconstruction;
	static string InputfileRangingData;
	//static string Outputfile;
	
	static RANGING_METHOD RangingMethod;

	static lival AMUBinning;

	//SNIP
	static unsigned int SNIPIterations;
	
	//Smoothing	
	static unsigned int SavitzkyGolayM;

	//Peak detection
	static unsigned int PeaksWindowingHalfwidth;
	static apt_real PeaksSignalToNoiseRatio;
	
	//ranging
	static apt_real RangingFWHM;
	static apt_real RangingFlankBumpTol;
	
	static bool readXML( string filename = "" );
	static bool checkUserInput();
};

#endif

