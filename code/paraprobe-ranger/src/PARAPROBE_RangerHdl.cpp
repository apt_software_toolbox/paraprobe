//##MK::CODESPLIT

#include "PARAPROBE_RangerHdl.h"


rangerHdl::rangerHdl()
{
	myrank = MASTER;
	nranks = 1;
}


rangerHdl::~rangerHdl()
{
    xyz = vector<p3d>();
    m2q = vector<apt_real>();
    itype = vector<unsigned char>();
}


bool rangerHdl::read_reconxyz_from_apth5()
{
    double tic = MPI_Wtime();

	if ( inputReconH5Hdl.read_xyz_from_apth5( 
                ConfigRanger::InputfileReconstruction, xyz ) == true ) {
		cout << ConfigRanger::InputfileReconstruction << " read successfully" << "\n";
	}
	else {
		cerr << ConfigRanger::InputfileReconstruction << " read failed!" << "\n";
		return false;
	}
	
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	ranger_tictoc.prof_elpsdtime_and_mem( "ReadReconstructedXYZFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
    return true;
}


bool rangerHdl::read_mass2charge_from_apth5()
{
    double tic = MPI_Wtime();

	if ( inputReconH5Hdl.read_m2q_from_apth5( 
                ConfigRanger::InputfileReconstruction, m2q ) == true ) {
		cout << ConfigRanger::InputfileReconstruction << " read successfully" << "\n";
	}
	else {
		cerr << ConfigRanger::InputfileReconstruction << " read failed!" << "\n";
		return false;
	}
	
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	ranger_tictoc.prof_elpsdtime_and_mem( "ReadMassToChargeFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
    return true;
}


void rangerHdl::execute_local_workpackage()
{
    double tic = MPI_Wtime();
    
    //assign every ion first the default type
    try {
        //itype = vector<evapion3>( xyz.size(), evapion3() );
        itype = vector<unsigned char>( xyz.size(), static_cast<unsigned char>(UNKNOWN_IONTYPE) );
    }
    catch( bad_alloc &croak) {
        cerr << "Allocation failed for itype!" << "\n";
        itype = vector<unsigned char>(); return;
    }
    
    //assign physical meaningful iontypes based on models, ##MK::could be OpenMP parallelized in the future
    if ( ConfigRanger::RangingMethod == IVAS ) {
        if ( worker.read_rrng_file( ConfigRanger::InputfileRangingData ) == true ) {
            if ( worker.validity_check() == true ) {

            	//##MK::parallelize this part
                for( size_t i = 0; i < m2q.size(); i++ ) {                        
                    apt_real mq = m2q.at(i);
                    unsigned int solution = UNKNOWN_IONTYPE;
                    for( auto it = worker.iontypes.begin(); it != worker.iontypes.end(); it++ ) {
                        //ranges are non-overlapping so only one solution if any
                        for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++ ) {
                            if ( jt->inrange( mq ) == false ) { //most likely case is not in the range
                                continue;
                            }
                            else {
                                solution = it->id;
                            }
                        } //next range same iontype
                    } //next iontype
                    
                    if ( solution < static_cast<unsigned int>(UCHARMX) ) {
                        itype.at(i) = static_cast<unsigned char>(solution);
                    }
                    else {
                        cerr << "Impossible to match any range, should not be encountered!" << "\n";
                        itype = vector<unsigned char>(); return;
                    }
                } //next ion

                cout << "Rank " << get_myrank() << " completed local workpackage!" << "\n";
            }
            else {
                cerr << "Rank " << MASTER << " the range file has invalid ranges (possible empty ranges or overlap)!" << "\n";
                itype = vector<unsigned char>(); return;
            }
        }
        else {
            cerr << "Rank " << MASTER << " could not open the RRNG file so default labels were assigned" << "\n";
            itype = vector<unsigned char>(); return;
        }
    }
    
    if ( ConfigRanger::RangingMethod == PARAPROBE ) {
        cerr << "RangingMethod PARAPROBE is currently not implemented!" << "\n";
        itype = vector<unsigned char>(); return;
    }
    
    double toc = MPI_Wtime();
    memsnapshot mm = memsnapshot();
    ranger_tictoc.prof_elpsdtime_and_mem( "ExecutedRanging", APT_XX, APT_IS_SEQ, mm, tic, toc);
}


bool rangerHdl::init_target_file()
{
	//MK::append results to existent dataset
    //##MK::ID tagging

	if ( debugh5Hdl.append_ranger_apth5( ConfigRanger::InputfileReconstruction ) == WRAPPED_HDF5_SUCCESS ) {
		return true;
	}
	else {
		return false;
	}
}


bool rangerHdl::write_metadata_to_apth5()
{
	double tic = MPI_Wtime();

	if ( xyz.size() != itype.size() ) {
		cerr << "Rank " << MASTER << " detected that not every ion was ranged!" << "\n"; return false;
	}

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";

	//iontype dictionary
	vector<unsigned char> uc8_id;
    vector<unsigned char> uc8_wh;
    vector<float> f32;
    vector<unsigned char> uc8_mq;
    for( auto it = worker.iontypes.begin(); it != worker.iontypes.end(); it++ ) {
        if ( it->id < static_cast<unsigned int>(UCHARMX ) ) {
        	unsigned char thisid = static_cast<unsigned char>(it->id);
            uc8_id.push_back( thisid );
            uc8_wh.push_back( it->strct.Z1 );
            uc8_wh.push_back( it->strct.N1 );
            uc8_wh.push_back( it->strct.Z2 );
            uc8_wh.push_back( it->strct.N2 );
            uc8_wh.push_back( it->strct.Z3 );
            uc8_wh.push_back( it->strct.N3 );
            uc8_wh.push_back( it->strct.sign );
            uc8_wh.push_back( it->strct.charge );
            for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++ ) {
                f32.push_back( jt->lo );
                f32.push_back( jt->hi );
                uc8_mq.push_back( thisid );
            }
        }
        else {
            cerr << "Rank " << MASTER << " failure reporting iontype " << it->id << "\n"; return false;
        }
    }
    
	dsnm = PARAPROBE_RANGER_META_TYPID_DICT_ID;
    ifo = h5iometa( dsnm, uc8_id.size()/PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to create PARAPROBE_RANGER_META_TYPID_DICT_ID metadata!" << "\n"; return false;
	}
	offs = h5offsets( 0, uc8_id.size()/PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX, 0, PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX,
			uc8_id.size()/PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_id );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to store PARAPROBE_RANGER_META_TYPID_DICT_ID metadata!" << "\n"; return false;
	}
	uc8_id = vector<unsigned char>();

	dsnm = PARAPROBE_RANGER_META_TYPID_DICT_WHAT;
    ifo = h5iometa( dsnm, uc8_wh.size()/PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to create PARAPROBE_RANGER_META_TYPID_DICT_WHAT metadata!" << "\n"; return false;
	}
	offs = h5offsets( 0, uc8_wh.size()/PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX, 0, PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX,
			uc8_wh.size()/PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_wh );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to store PARAPROBE_RANGER_META_TYPID_DICT_WHAT metadata!" << "\n"; return false;
	}
	uc8_wh = vector<unsigned char>();
    
    dsnm = PARAPROBE_RANGER_META_TYPID_DICT_IVAL;
    ifo = h5iometa( dsnm, f32.size()/PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to create PARAPROBE_RANGER_META_TYPID_DICT_IVAL metadata!" << "\n"; return false;
	}
	offs = h5offsets( 0, f32.size()/PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX, 0, PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX,
			f32.size()/PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to store PARAPROBE_RANGER_META_TYPID_DICT_IVAL metadata!" << "\n"; return false;
	}
	f32 = vector<float>();

    dsnm = PARAPROBE_RANGER_META_TYPID_DICT_ASSN;
    ifo = h5iometa( dsnm, uc8_mq.size()/PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to create PARAPROBE_RANGER_META_TYPID_DICT_ASSN metadata!" << "\n"; return false;
	}
	offs = h5offsets( 0, uc8_mq.size()/PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX, 0, PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX,
			uc8_mq.size()/PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX, PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_mq );
	if ( status < 0 ) {
		cerr << "Rank " << MASTER << " failed to store PARAPROBE_RANGER_META_TYPID_DICT_ASSN metadata!" << "\n"; return false;
	}
	uc8_mq = vector<unsigned char>();

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	ranger_tictoc.prof_elpsdtime_and_mem( "WriteRangingMetadataAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool rangerHdl::write_results_to_apth5()
{
	double tic = MPI_Wtime();

	if ( xyz.size() != itype.size() ) {
		cerr << "Rank " << MASTER << " detected that not every ion was ranged!" << "\n"; return false;
	}

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
    
    //iontype IDs
    //MK::use itype in-place, works because PARAPROBE_RANGER_RES_TYPID_NCMAX == 1 and we are on MASTER rank only

    dsnm = PARAPROBE_RANGER_RES_TYPID;
	ifo = h5iometa( dsnm, itype.size()/1, 1 );
    status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
	if ( status < 0 ) {
        cerr << "Rank " << MASTER << " failed to create PARAPROBE_RANGER_RES_TYPID results!" << "\n"; return false;
    }
    offs = h5offsets( 0, itype.size()/PARAPROBE_RANGER_RES_TYPID_NCMAX,
    			0, PARAPROBE_RANGER_RES_TYPID_NCMAX, itype.size()/PARAPROBE_RANGER_RES_TYPID_NCMAX, PARAPROBE_RANGER_RES_TYPID_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, itype ); //used in-place
    if ( status < 0 ) {
        cerr << "Rank " << MASTER << " failed to store PARAPROBE_RANGER_RES_TYPID results!" << "\n"; return false;
    }

    string xdmffn = ConfigRanger::InputfileReconstruction + ".Ranging.xdmf";
    debugxdmf.create_volrecon_file( xdmffn, itype.size()/PARAPROBE_RANGER_RES_TYPID_NCMAX, debugh5Hdl.h5resultsfn );

    //earliest point in time when itype is no longer required
    itype = vector<unsigned char>();

    double toc = MPI_Wtime();
    memsnapshot mm = memsnapshot();
    ranger_tictoc.prof_elpsdtime_and_mem( "WriteResultsToAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


int rangerHdl::get_myrank()
{
	return this->myrank;
}


int rangerHdl::get_nranks()
{
	return this->nranks;
}


void rangerHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void rangerHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void rangerHdl::init_mpidatatypes()
{
	/*
	int elementCounts[2] = {3, 1};
	MPI_Aint displacements[2] = {0, 3 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, elementCounts, displacements, oldTypes, &MPI_Ion_Type);
	MPI_Type_commit(&MPI_Ion_Type);
	*/
}
