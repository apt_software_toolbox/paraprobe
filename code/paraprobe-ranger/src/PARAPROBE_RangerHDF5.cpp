//##MK::CODE

#include "PARAPROBE_RangerHDF5.h"
//https://www.learncpp.com/cpp-tutorial/114-constructors-and-initialization-of-derived-classes/


ranger_h5::ranger_h5()
{
}


ranger_h5::~ranger_h5()
{
}


int ranger_h5::append_ranger_apth5( const string h5fn )
{
	cout << "Trying to append new data to " << h5fn << "\n";
	h5resultsfn = h5fn;
	fileid = H5Fopen( h5resultsfn.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
	if ( fileid < 0 ) {
		cerr << "Ranger apth5 file opening failed! " << fileid << "\n"; return WRAPPED_HDF5_FAILED;
	}

	//domain specific HDF5 keywords and data fields
	groupid = H5Gcreate2(fileid, PARAPROBE_RANGER, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_RANGER failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
    
    groupid = H5Gcreate2(fileid, PARAPROBE_RANGER_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_RANGER_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
    
    groupid = H5Gcreate2(fileid, PARAPROBE_RANGER_META_TYPID_DICT, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_RANGER_META_DICT failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
    
    groupid = H5Gcreate2(fileid, PARAPROBE_RANGER_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_RANGER_RES failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
    
	//close file
	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "Close file " << h5resultsfn << " failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}
