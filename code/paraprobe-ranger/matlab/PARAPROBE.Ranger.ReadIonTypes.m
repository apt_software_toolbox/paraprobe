% PARAPROBE.Synthetic
% Markus K\"uhbach, 2019/10/22
tic
    clear;
    clc;
    format long;
    
% load sequentially computed reference result
    dirnm = 'Y:\GITHUB\MPIE_APTFIM_TOOLBOX\paraprobe\code\paraprobe-ranger\run\';
    prefix = 'PARAPROBE.Synthetic.Results.';
    h5fnref = [dirnm prefix 'SimID.2.apth5'];
    dsnm = '/Ranging/Results/IontypeIDs';
    ITYP = h5read(h5fnref, dsnm);
    ecdf(ITYP)    
toc

%% DEBUG
% max(DAT(1,:));
% N = length(DAT(1,:));
% CDF(1,:) = sort(DAT(1,:));
% CDF(2,:) = (1:1:N)./N.*100.0;
% plot(CDF(1,:),CDF(2,:),'.');
% save('PARAPROBE.Araullo.CreateGeodesicSphere.40962.mat','-v7.3');
% h5create(h5fn, dsnm, size(DAT));
% h5write(h5fn, dsnm, DAT);
