/*
	Copyright Max-Planck-Institut f\"ur Eisenforschung, GmbH, D\"sseldorf
	Data structure, code design, parallel implementation:
	Markus K\"uhbach, 2017-2019

	Third-party contributions:
	Andrew Breen - sequential Matlab code snippets for reconstruction and EPOS
	Markus G\"otz et al. - HPDBScan
	Kartik Kukreja - path compressed union/find
	Lester Hedges - AABBTree

	PARAPROBE --- is an MPI/OpenMP/SIMD-parallelized tool for efficient scalable
	processing of Atom Probe Tomography data targeting back-end processing.
	
	This file is part of PARAPROBE.

	PARAPROBE is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

 	PARAPROBE is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with paraprobe.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CONFIG_Transcoder.h"


E_TRANSCODER_MODE ConfigTranscoder::TranscodingMode = E_TRANSCODE_NOTHING;
string ConfigTranscoder::Inputfile = "";
string ConfigTranscoder::Outputfile = "";


bool ConfigTranscoder::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Input.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigTranscoder")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "TranscodingMode" );
	switch (mode)
	{
		case E_TRANSCODE_POS_TO_APTH5:
			TranscodingMode = E_TRANSCODE_POS_TO_APTH5; break;
		case E_TRANSCODE_EPOS_TO_APTH5:
			TranscodingMode = E_TRANSCODE_EPOS_TO_APTH5; break;
		case E_TRANSCODE_APTV2_TO_APTH5:
			TranscodingMode = E_TRANSCODE_APTV2_TO_APTH5; break;
		default:
			TranscodingMode = E_TRANSCODE_NOTHING;
	}
	Inputfile = read_xml_attribute_string( rootNode, "Inputfile" );
	Outputfile = read_xml_attribute_string( rootNode, "Outputfile" );

	return true;
}


bool ConfigTranscoder::checkUserInput()
{
	cout << "ConfigTranscoder::" << "\n";
	//##MK::check if Inputfile ending is POS or EPOS
	//##MK::check that Outputfile ending is APTH5

	if ( TranscodingMode == E_TRANSCODE_POS_TO_APTH5 ) {
		if ( Inputfile.substr(Inputfile.length()-4) != ".pos" ) {
			cout << "Attempting to transcode a POS file to APTH5 without delivering a *.pos Inputfile!" << "\n"; return false;
		}
	}
	if ( TranscodingMode == E_TRANSCODE_EPOS_TO_APTH5 ) {
		if ( Inputfile.substr(Inputfile.length()-5) != ".epos" ) {
			cout << "Attempting to transcode a EPOS file to APTH5 without delivering an *.epos Inputfile!" << "\n"; return false;
		}
	}

	cout << "Input read from " << Inputfile << "\n";
	cout << "Output written to " << Outputfile << "\n";

	cout << "\n";
	return true;
}

