//##MK::CODESPLIT

#include "PARAPROBE_TranscoderHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2


bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigTranscoder::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigTranscoder::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "Input is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void transcode_fileformats()
{
	//allocate sequential transcoder instance
	transcoderHdl* transcoder = NULL;
	try {
		transcoder = new transcoderHdl;
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate a sequential transcoderHdl instance" << "\n";
		return;
	}

	//load source file and prepare APTH5 HDF5 file group structure
	if ( transcoder->load_source_file() == true ) {

		if ( transcoder->init_target_file() == true ) { //transform the specific formats
			switch (ConfigTranscoder::TranscodingMode)
			{
				case E_TRANSCODE_POS_TO_APTH5:
					transcoder->transcode_pos_to_apth5(); break;
				case E_TRANSCODE_EPOS_TO_APTH5:
					transcoder->transcode_epos_to_apth5(); break;
				case E_TRANSCODE_APTV2_TO_APTH5:
					transcoder->transcode_aptv2_to_apth5(); break;
				default:
					break;
			}
		}
		else {
			cerr << "Initializing target APTH5 failed" << "\n";
		}
	}
	else {
		cerr << "Loading source failed" << "\n";
	}

	//release resources
	delete transcoder; transcoder = NULL;
}


int main(int argc, char** argv)
{
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	//##MK::n/a

//EXECUTE SPECIFIC TASKS
	transcode_fileformats();

//DESTROY MPI
	//##MK::n/a

	return 0;
}
