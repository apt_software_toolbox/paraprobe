/*
	Copyright Max-Planck-Institut f\"ur Eisenforschung, GmbH, D\"sseldorf
	Data structure, code design, parallel implementation:
	Markus K\"uhbach, 2017-2019

	Third-party contributions:
	Andrew Breen - sequential Matlab code snippets for reconstruction and EPOS
	Markus G\"otz et al. - HPDBScan
	Kartik Kukreja - path compressed union/find
	Lester Hedges - AABBTree

	PARAPROBE --- is an MPI/OpenMP/SIMD-parallelized tool for efficient scalable
	processing of Atom Probe Tomography data targeting back-end processing.
	
	This file is part of PARAPROBE.

	PARAPROBE is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

 	PARAPROBE is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with paraprobe.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __PARAPROBE_CONFIG_TRANSCODER_H__
#define __PARAPROBE_CONFIG_TRANSCODER_H__

#include "../../paraprobe-utils/src/CONFIG_Shared.h"

enum E_TRANSCODER_MODE {
	E_TRANSCODE_NOTHING,					//nothing
	E_TRANSCODE_POS_TO_APTH5,				//debug transcode POS file to the open source HDF5-based IFES APTTC format
	E_TRANSCODE_EPOS_TO_APTH5,				//debug transcode EPOS
	E_TRANSCODE_APTV2_TO_APTH5				//new IVAS4 open format APTV2
};


class ConfigTranscoder
{
public:
	
	static E_TRANSCODER_MODE TranscodingMode;
	static string Inputfile;
	static string Outputfile;

	static bool readXML( string filename = "" );
	static bool checkUserInput();
};

#endif
