//##MK::CODESPLIT

#include "PARAPROBE_TranscoderHdl.h"


transcoderHdl::transcoderHdl()
{
	nevt = 0;
}


transcoderHdl::~transcoderHdl()
{
	for( auto it = rawdata_pos.begin(); it != rawdata_pos.end(); it++ ) {
		delete *it;
	}
	rawdata_pos.clear();
	for( auto jt = rawdata_epos.begin(); jt != rawdata_epos.end(); jt++ ) {
		delete *jt;
	}
	rawdata_epos.clear();
	nevt = 0;
}


bool transcoderHdl::load_source_file()
{
	switch ( ConfigTranscoder::TranscodingMode )
	{
		case E_TRANSCODE_POS_TO_APTH5:
			return load_pos_seq( ConfigTranscoder::Inputfile );
		case E_TRANSCODE_EPOS_TO_APTH5:
			return load_epos_seq( ConfigTranscoder::Inputfile );
		default:
			return false;
	}
}


bool transcoderHdl::load_pos_seq( const string posfn )
{
	//loads pos file
	//B. Gault et al., Atom Probe Tomography, Springer, 2012, Appendix ##MK
	//*.pos is headerless container of binary big endian float structs (IEEE754?) four to each ion
	//x,y,z, mq interleaved format

//##MK::templatize this function in the future
//##MK::put this section into individual function it is a duplicate from the one in load_epos...
	//get total number of ions by reading file size via OS intrinsics
	unsigned long filesize[1] = {0};
	unsigned long nobjects[1] = {0};
	struct stat oscall;
	if ( stat( posfn.c_str() , &oscall ) != -1 ) { //MK::take care of case when filesize exceeds range of unsigned int...
		filesize[0] = static_cast<unsigned long>(oscall.st_size);
		if ( filesize[0] % static_cast<unsigned long>(sizeof(struct pos_be_2_stl_le)) != 0 ) {
			cerr << "Size of file " << posfn << " is not an integer multiple of expected datatype" << "\n";
			return false;
		}
		else {
			nobjects[0] = filesize[0] / static_cast<unsigned long>(sizeof(struct pos_be_2_stl_le));
			cout << "Dataset " << posfn << " contains " << nobjects[0] << " ions" << "\n";
		}

		if ( nobjects[0] > static_cast<unsigned long>(std::numeric_limits<unsigned int>::max()-2) ) {
			cerr << "Dataset is larger than what the code currently can process (4.2 billion ions, (not 4.2 billion byte...). Change data type to unsigned long!" << "\n";
			//##MK::can be changed by going from unsigned int to unsigned long, but then also the HoshenKopelman needs upgrade, new rawdata structure of lists of buckets of epos objects supports larger than 4.2 billion ions...
			return false;
		}
	}
	else {
		cerr << "File " << posfn << " cannot be read!" << "\n";
		return false;
	}
//##MK::put this section into individual function it is a duplicate from the one in load_epos...

	//assume length of all containers the same, total number of ions
	nevt = nobjects[0]; //MK::implicit contraction from unsigned long and unsigned int is now okay

	//MK::read successively blocks of C structs and store in blocks the rawdata
	//benefit is that such later rawdata can be processes blockwise and deleted successively reducing the total memory footprint of the application for reconstruction by not more than though factor 2
	size_t efirst = 0;
	size_t elast = static_cast<size_t>(nevt);
	size_t etarget = MPIIO_READ_CACHE / sizeof(struct pos_be_2_stl_le);
	size_t ecurrent = 0;

	struct pos_be_2_stl_le* rbuf = NULL;

	//file exists because size query did not return if reaching this point
	unsigned long probe = 0;
	FILE *file = fopen( posfn.c_str(), "rb" );

	//read block by block
	for ( size_t e = efirst; e < elast; 	) { //number of elements, not total bytes
		//update actual read cache length
		ecurrent = ((e + etarget) < elast) ? etarget : elast - e;

		//allocate buffer if none existent, reallocate if of different size
		if ( rbuf != NULL ) {
			if ( ecurrent != etarget ) {
				//reallocate buffer of size ecurrent
				delete [] rbuf; rbuf = NULL;
				try { rbuf = new struct pos_be_2_stl_le[ecurrent]; }
				catch (bad_alloc &ioexc) {
					cerr << "Unable to reallocate rbuf for pos_be_2_stl_le structs in epos_sequential read" << "\n";
					fclose(file);
					return false;
				}
			}
			//else nothing to do reutilize rbuf
		}
		else {
			//least likely case first time allocation
			try { rbuf = new struct pos_be_2_stl_le[ecurrent]; }
			catch (bad_alloc &ioexc) {
				cerr << "Unable to allocate rbuf for pos_be_2_stl_le structs in epos_sequential read" << "\n";
				fclose(file);
				return false;
			}
		}
		//by now buffer allocated or already out of subroutine

		//read from file
		probe = fread( rbuf, sizeof(struct pos_be_2_stl_le), ecurrent, file ); //file pointer is advanced implicitly/automatically

		if ( probe == ecurrent ) { //read success
			//allocate new buffer to store results
			vector<pos>* wbuf = NULL;
			try { wbuf = new vector<pos>; }
			catch (bad_alloc &ioexc) {
				cerr << "Unable to allocate wbuf for epos vector in pos_sequential read" << "\n";
				delete [] rbuf;
				fclose(file);
				return false;
			}
			wbuf->reserve( ecurrent );

			for ( size_t i = 0; i < ecurrent; ++i ) { //swop endianness implicitly within epos object
				wbuf->push_back( pos(rbuf[i]) );
//cout << wbuf->back() << endl;
			}

			//handshake registration data block wbuf in solverHdl
			rawdata_pos.push_back( NULL );
			rawdata_pos.back() = wbuf;

			e = e + ecurrent;
//cout << e << endl;
		} //block read of ecurrent successful
		else {
			cerr << "I/O failed on " << posfn << " at position " << e << "\n";
			delete [] rbuf;
			fclose(file);
			return false;
		}
	} //next block
	fclose(file);

	delete [] rbuf; rbuf = NULL;

	cout << "I/O success on " << posfn << "\n";
	return true;
}


bool transcoderHdl::load_epos_seq( const string eposfn )
{
	//loads native epos file
	//D.J. Larson et al., Local Electrode Atom Probe Tomography: A User\'s Guide
	//DOI 10.1007/978-1-4614-8721-0, Springer Science+Business Media New York 2013 Appendix A
	//*.epos floats are big endian, int as well, file has no header it is just a naive block of the same structured data
	//therefore we can read the file via C char structs and swop bytes for int
	//for floats this is usually dangerous, though, but fact that IVAS writes IEEE754 comes to help
	//see details in functionality of epos struct

	//get total number of ions by reading file size via OS intrinsics
	unsigned long filesize[1] = {0};
	unsigned long nobjects[1] = {0};
	struct stat oscall;
	if ( stat( eposfn.c_str() , &oscall ) != -1 ) { //MK::take care of case when filesize exceeds range of unsigned int...
		filesize[0] = static_cast<unsigned long>(oscall.st_size);
		if ( filesize[0] % static_cast<unsigned long>(sizeof(struct epos_be_2_stl_le)) != 0 ) {
			cerr << "Size of file " << eposfn << " is not an integer multiple of expected datatype" << "\n";
			return false;
		}
		else {
			nobjects[0] = filesize[0] / static_cast<unsigned long>(sizeof(struct epos_be_2_stl_le));
			cout << "Dataset " << eposfn << " contains " << nobjects[0] << " ions" << "\n";
		}

		if ( nobjects[0] > static_cast<unsigned long>(std::numeric_limits<unsigned int>::max()-2) ) {
			cerr << "Dataset is larger than what the code currently can process (4.2 billion ions, (not 4.2 billion byte...). Change data type to unsigned long!" << "\n";
			//##MK::can be changed by going from unsigned int to unsigned long, but then also the HoshenKopelman potentially needs upgrade, new rawdata structure of lists of buckets of epos objects supports larger than 4.2 billion ions...
			return false;
		}
	}
	else {
		cerr << "File " << eposfn << " cannot be read!" << "\n";
		return false;
	}

	//assume length of all containers the same, total number of ions
	nevt = nobjects[0]; //MK::implicit contraction from unsigned long and unsigned int is now okay

	//MK::read successively blocks of C structs and store in blocks the rawdata
	//benefit is that such later rawdata can be processes blockwise and deleted successively reducing the total memory footprint of the application for reconstruction by not more than though factor 2
	size_t efirst = 0;
	size_t elast = static_cast<size_t>(nevt);
	size_t etarget = MPIIO_READ_CACHE / sizeof(struct epos_be_2_stl_le);
	size_t ecurrent = 0;

	struct epos_be_2_stl_le* rbuf = NULL;

	//file exists because size query did not return if reaching this point
	unsigned long probe = 0;
	FILE *file = fopen( eposfn.c_str(), "rb" );

	//read block by block
	for ( size_t e = efirst; e < elast; 	) { //number of elements, not total bytes
		//update actual read cache length
		ecurrent = ((e + etarget) < elast) ? etarget : elast - e;

		//allocate buffer if none existent, reallocate if of different size
		if ( rbuf != NULL ) {
			if ( ecurrent != etarget ) {
				//reallocate buffer of size ecurrent
				delete [] rbuf; rbuf = NULL;
				try { rbuf = new struct epos_be_2_stl_le[ecurrent]; }
				catch (bad_alloc &ioexc) {
					cerr << "Unable to reallocate rbuf for epos_be_2_stl_le structs in epos_sequential read" << "\n";
					fclose(file);
					return false;
				}
			}
			//else nothing to do reutilize rbuf
		}
		else {
			//least likely case first time allocation
			try { rbuf = new struct epos_be_2_stl_le[ecurrent]; }
			catch (bad_alloc &ioexc) {
				cerr << "Unable to allocate rbuf for epos_be_2_stl_le structs in epos_sequential read" << "\n";
				fclose(file);
				return false;
			}
		}
		//by now buffer allocated or already out of subroutine

		//read from file
		probe = fread( rbuf, sizeof(struct epos_be_2_stl_le), ecurrent, file ); //file pointer is advanced implicitly/automatically

		if ( probe == ecurrent ) { //read success
			//allocate new buffer to store results
			vector<epos>* wbuf = NULL;
			try { wbuf = new vector<epos>; }
			catch (bad_alloc &ioexc) {
				cerr << "Unable to allocate wbuf for epos vector in epos_sequential read" << "\n";
				delete [] rbuf;
				fclose(file);
				return false;
			}
			wbuf->reserve( ecurrent );

			for ( size_t i = 0; i < ecurrent; ++i ) { //swop endianness implicitly within epos object
				wbuf->push_back( epos(rbuf[i]) );
//cout << wbuf->back() << endl;
			}

			//handshake registration data block wbuf in solverHdl
			rawdata_epos.push_back( NULL );
			rawdata_epos.back() = wbuf;

			e = e + ecurrent;
//cout << e << endl;
		} //block read of ecurrent successful
		else {
			cerr << "I/O failed on " << eposfn << " at position " << e << "\n";
			delete [] rbuf;
			fclose(file);
			return false;
		}
	} //next block
	fclose(file);

	delete [] rbuf; rbuf = NULL;

	cout << "I/O success on " << eposfn << "\n";
	return true;
}


bool transcoderHdl::load_aptv2_seq( const string aptv2fn )
{
	cout << "TranscoderHdl::load_aptv2::Currently not implemented!" << "\n";
	return true;
}


bool transcoderHdl::init_target_file()
{
	//##MK::generate group hierarchy of an open source IFES APTTC APTH5 HDF5 file
	string h5fn_out = ConfigTranscoder::Outputfile + ".apth5";
cout << "Initializing target file " << h5fn_out << "\n";

	if ( debugh5Hdl.create_paraprobe_apth5( h5fn_out ) == WRAPPED_HDF5_SUCCESS ) {
		return true;
	}
	else {
		return false;
	}
}


void transcoderHdl::transcode_pos_to_apth5()
{
	int status = WRAPPED_HDF5_SUCCESS;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dummy = "";
	string nostring = "This is not contained inside a POS file";

	vector<float> wfbuf;
	vector<pair<string,string>> attrbuf;

	//domain specific HDF5 keywords and data fields
	dummy = "AtomProbeTomographyExperiment";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_00_EXPERIMENT_TYPE, dummy );
cout << "APTH5_ACQ_00_EXPERIMENT_TYPE " << status << "\n";

	//dummy = "TestSample";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_00_SAMPLE_DESCR, nostring );
cout << "APTH5_ACQ_00_SAMPLE_DESCR " << status << "\n";

	//metadata specifying the environment in which the tool was used
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_01_GLOBAL_STARTDATE, nostring );
cout << "APTH5_ACQ_01_GLOBAL_STARTDATE " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_01_LOCAL_STARTDATE, nostring );
cout << "APTH5_ACQ_01_LOCAL_STARTDATE " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_01_GLOBAL_ENDDATE, nostring );
cout << "APTH5_ACQ_01_GLOBAL_ENDDATE " << status << "\n";

	//metadata specifying details and pieces of information about the tool
	//dummy = "SampleComingFromTheBrightFuture";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_SAMPLE_NAME, nostring );
cout << "APTH5_ACQ_02_SAMPLE_NAME " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_DETECTOR_INFO, nostring );
cout << "APTH5_ACQ_02_DETECTOR_INFO " << status << "\n";

	wfbuf.clear();
	wfbuf.push_back( 0.f );
	wfbuf.push_back( 0.f ); //##MK::dummy values do not exist
	ifo = h5iometa( APTH5_ACQ_02_DETECTOR_SIZE, 2, 1);
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
cout << "APTH5_ACQ_02_DETECTOR_SIZE create " << status << "\n";
	offs = h5offsets( 0, 2, 0, 1, 2, 1);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, wfbuf );
cout << "APTH5_ACQ_02_DETECTOR_SIZE write " << status << "\n";
	attrbuf.clear();
	attrbuf.push_back( make_pair("SIUnit_DetX","mm") );
	attrbuf.push_back( make_pair("SIUnit_DetY","mm") );
	status = debugh5Hdl.write_attribute_string( APTH5_ACQ_02_DETECTOR_SIZE, attrbuf );
cout << "APTH5_ACQ_02_DETECTOR_SIZE attr " << status << "\n";

//##MK::write string dummies for which there is at the moment either no
//##MK::clear idea about the format nor pieces of information for such quantity in the source file format
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_DETECTOR_GEOREAL, nostring );
cout << "APTH5_ACQ_02_DETECTOR_GEOREAL " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_DETECTOR_GEOOPTEQV, nostring );
cout << "APTH5_ACQ_02_DETECTOR_GEOOPTEQV " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_INSTRUMENT_INFO, nostring ); 	//dummy = "Max-Planck-Institut fuer Eisenforschung LEAP5000XS";
cout << "APTH5_ACQ_02_INSTRUMENT_INFO " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_FLIGHTPATH_TIMING, nostring );
cout << "APTH5_ACQ_02_FLIGHTPATH_TIMING " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_FLIGHTPATH_SPATIAL, nostring );
cout << "APTH5_ACQ_02_FLIGHTPATH_SPATIAL " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_LASER_WAVELENGTH, nostring );
cout << "APTH5_ACQ_02_LASER_WAVELENGTH " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_LASER_INCIDENCE, nostring );
cout << "APTH5_ACQ_02_LASER_INCIDENCE " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_PULSE_NUMBER, nostring );
cout << "APTH5_ACQ_02_PULSE_NUMBER " << status << "\n";

	//##example 4x4 affine transformation matrix
	wfbuf.clear();
	wfbuf = { 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f };
	ifo = h5iometa( APTH5_ACQ_02_TIP2RECON_MAPPING, 4, 4);
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
cout << "APTH5_ACQ_02_TIP2RECON_MAPPING create " << status << "\n";
	offs = h5offsets( 0, 4, 0, 4, 4, 4);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, wfbuf );
cout << "APTH5_ACQ_02_TIP2RECON_MAPPING write " << status << "\n";

	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_TIPGEOM_REAL, nostring );
cout << "APTH5_ACQ_02_TIPGEOM_REAL " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_TIPGEOM_OPTEQV, nostring );
cout << "APTH5_ACQ_02_TIPGEOM_OPTEQV " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_REFLECTRON_INFO, nostring );
cout << "APTH5_ACQ_02_REFLECTRON_INFO " << status << "\n";

	//measured quantities during the course of this experiment with this specific tool for this specific sample
	//##MK::further dummy values
	//vector<float> wfbuf1 = vector<float>( 1*100, 0.f );
	//vector<float> wfbuf2 = vector<float>( 2*100, 0.f );

	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_REFLECTRON_VOLTAGE, nostring );
cout << "APTH5_ACQ_03_REFLECTRON_VOLTAGE " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_DETECTOR_HITPOS, nostring );
cout << "APTH5_ACQ_03_DETECTOR_HITPOS " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_DETECTOR_HITMULTPLY, nostring );
cout << "APTH5_ACQ_03_DETECTOR_HITMULTPLY " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_DETECTOR_DEADPULSES, nostring );
cout << "APTH5_ACQ_03_DETECTOR_DEADPULSES " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_TIME_OF_FLIGHT, nostring );
cout << "APTH5_ACQ_03_TIME_OF_FLIGHT " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_STANDING_VOLTAGE, nostring );
cout << "APTH5_ACQ_03_STANDING_VOLTAGE " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_PULSE_VOLTAGE, nostring );
cout << "APTH5_ACQ_03_PULSE_VOLTAGE " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_TIP_TEMPERATURE, nostring );
cout << "APTH5_ACQ_03_TIP_TEMPERATURE " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_PULSE_FREQUENCY, nostring );
cout << "APTH5_ACQ_03_PULSE_FREQUENCY " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_LASER_PULSE_ENERGY, nostring );
cout << "APTH5_ACQ_03_LASER_PULSE_ENERGY " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_LASER_POSITION, nostring );
cout << "APTH5_ACQ_03_LASER_POSITION " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_STAGE_POSITION, nostring );
cout << "APTH5_ACQ_03_STAGE_POSITION " << status << "\n";

	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_FINALIZATION_STATUS, APTH5_ACQ_03_FINALIZATION_ASDESIRED_SUCCESS );
cout << "APTH5_ACQ_03_FINALIZATION_STATUS " << status << "\n";
}


void transcoderHdl::transcode_epos_to_apth5()
{
//##MK::take out duplicated parts
	int status = WRAPPED_HDF5_SUCCESS;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dummy = "";
	string nostring = "This is not contained inside an EPOS file";

	vector<unsigned int> ibuf;
	vector<float> wfbuf;
	vector<pair<string,string>> attrbuf;

	//domain specific HDF5 keywords and data fields
	dummy = "AtomProbeTomographyExperiment";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_00_EXPERIMENT_TYPE, dummy );
cout << "APTH5_ACQ_00_EXPERIMENT_TYPE " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_00_SAMPLE_DESCR, nostring );
cout << "APTH5_ACQ_00_SAMPLE_DESCR " << status << "\n";

	//metadata specifying the environment in which the tool was used
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_01_GLOBAL_STARTDATE, nostring );
cout << "APTH5_ACQ_01_GLOBAL_STARTDATE " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_01_LOCAL_STARTDATE, nostring );
cout << "APTH5_ACQ_01_LOCAL_STARTDATE " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_01_GLOBAL_ENDDATE, nostring );
cout << "APTH5_ACQ_01_GLOBAL_ENDDATE " << status << "\n";

	//metadata specifying details and pieces of information about the tool
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_SAMPLE_NAME, nostring );
cout << "APTH5_ACQ_02_SAMPLE_NAME " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_DETECTOR_INFO, nostring );
cout << "APTH5_ACQ_02_DETECTOR_INFO " << status << "\n";

/*
	wfbuf.clear();
	wfbuf.push_back( 0.f );
	wfbuf.push_back( 0.f ); //##MK::dummy values do not exist
	ifo = h5iometa( APTH5_ACQ_02_DETECTOR_SIZE, 2, 1);
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
cout << "APTH5_ACQ_02_DETECTOR_SIZE create " << status << "\n";
	offs = h5offsets( 0, 2, 0, 1, 2, 1);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, wfbuf );
cout << "APTH5_ACQ_02_DETECTOR_SIZE write " << status << "\n";
	//##MK::exemplar adding of attributes

	attrbuf.clear();
	attrbuf.push_back( make_pair("DetX_SIUnit","mm") );
	attrbuf.push_back( make_pair("DetY_SIUnit","mm") );
	status = debugh5Hdl.write_attribute_string( APTH5_ACQ_02_DETECTOR_SIZE, attrbuf );
cout << "APTH5_ACQ_02_DETECTOR_SIZE attr " << status << "\n";
*/
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_DETECTOR_SIZE, nostring );
cout << "APTH5_ACQ_02_DETECTOR_SIZE " << status << "\n";

//##MK::write string dummies for which there is at the moment either no
//##MK::clear idea about the format nor pieces of information for such quantity in the source file format
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_DETECTOR_GEOREAL, nostring );
cout << "APTH5_ACQ_02_DETECTOR_GEOREAL " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_DETECTOR_GEOOPTEQV, nostring );
cout << "APTH5_ACQ_02_DETECTOR_GEOOPTEQV " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_INSTRUMENT_INFO, nostring ); 	//dummy = "Max-Planck-Institut fuer Eisenforschung LEAP5000XS";
cout << "APTH5_ACQ_02_INSTRUMENT_INFO " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_FLIGHTPATH_TIMING, nostring );
cout << "APTH5_ACQ_02_FLIGHTPATH_TIMING " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_FLIGHTPATH_SPATIAL, nostring );
cout << "APTH5_ACQ_02_FLIGHTPATH_SPATIAL " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_LASER_WAVELENGTH, nostring );
cout << "APTH5_ACQ_02_LASER_WAVELENGTH " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_LASER_INCIDENCE, nostring );
cout << "APTH5_ACQ_02_LASER_INCIDENCE " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_PULSE_NUMBER, nostring );
cout << "APTH5_ACQ_02_PULSE_NUMBER " << status << "\n";

/*
	//##example 4x4 affine transformation matrix
	wfbuf.clear();
	wfbuf = { 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f };
	ifo = h5iometa( APTH5_ACQ_02_TIP2RECON_MAPPING, 4, 4);
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
cout << "APTH5_ACQ_02_TIP2RECON_MAPPING create " << status << "\n";
	offs = h5offsets( 0, 4, 0, 4, 4, 4);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, wfbuf );
cout << "APTH5_ACQ_02_TIP2RECON_MAPPING write " << status << "\n";
*/
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_TIP2RECON_MAPPING, nostring );
cout << "APTH5_ACQ_02_TIP2RECON_MAPPING " << status << "\n";

	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_TIPGEOM_REAL, nostring );
cout << "APTH5_ACQ_02_TIPGEOM_REAL " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_TIPGEOM_OPTEQV, nostring );
cout << "APTH5_ACQ_02_TIPGEOM_OPTEQV " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_02_REFLECTRON_INFO, nostring );
cout << "APTH5_ACQ_02_REFLECTRON_INFO " << status << "\n";

	//measured quantities during the course of this experiment with this specific tool for this specific sample
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_REFLECTRON_VOLTAGE, nostring );
cout << "APTH5_ACQ_03_REFLECTRON_VOLTAGE " << status << "\n";

	wfbuf.clear();
	wfbuf.reserve( 2*nevt );
	size_t nb = rawdata_epos.size();
	for( size_t b = 0; b < nb; b++ ) {
		if ( rawdata_epos.at(b) != NULL ) {
			for( auto it = rawdata_epos.at(b)->begin(); it != rawdata_epos.at(b)->end(); ++it ) {
				wfbuf.push_back( it->detx );
				wfbuf.push_back( it->dety ); //interleaved, 2d implicit
			}
		}
	}
	ifo = h5iometa( APTH5_ACQ_03_DETECTOR_HITPOS, wfbuf.size()/2, 2);
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
cout << "APTH5_ACQ_03_DETECTOR_HITPOS create " << status << "\n";
	offs = h5offsets( 0, wfbuf.size()/2, 0, 2, wfbuf.size()/2, 2);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, wfbuf );
cout << "APTH5_ACQ_03_DETECTOR_HITPOS write " << status << "\n";
	attrbuf.clear();
	attrbuf.push_back( make_pair("SIUnit","mm") );
	status = debugh5Hdl.write_attribute_string( APTH5_ACQ_03_DETECTOR_HITPOS, attrbuf );
cout << "APTH5_ACQ_03_DETECTOR_HITPOS attr " << status << "\n";

	ibuf.clear();
	ibuf.reserve( nevt );
	for( size_t b = 0; b < nb; b++ ) {
		if ( rawdata_epos.at(b) != NULL ) {
			for( auto it = rawdata_epos.at(b)->begin(); it != rawdata_epos.at(b)->end(); ++it ) {
				ibuf.push_back( fabs(it->i2) ); //##MK::fabs allowed?? or negative int used in IVAS, possibly ushort sufficient?
			}
		}
	}
	ifo = h5iometa( APTH5_ACQ_03_DETECTOR_HITMULTPLY, ibuf.size(), 1);
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
cout << "APTH5_ACQ_03_DETECTOR_HITMULTPLY create " << status << "\n";
	offs = h5offsets( 0, ibuf.size(), 0, 1, ibuf.size(), 1);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, ibuf );
cout << "APTH5_ACQ_03_DETECTOR_HITMULTPLY write " << status << "\n";

	ibuf.clear();
	ibuf.reserve( nevt );
	for( size_t b = 0; b < nb; b++ ) {
		if ( rawdata_epos.at(b) != NULL ) {
			for( auto it = rawdata_epos.at(b)->begin(); it != rawdata_epos.at(b)->end(); ++it ) {
				ibuf.push_back( fabs(it->i1) );
			}
		}
	}
	ifo = h5iometa( APTH5_ACQ_03_DETECTOR_DEADPULSES, ibuf.size(), 1);
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
cout << "APTH5_ACQ_03_DETECTOR_DEADPULSES create " << status << "\n";
	offs = h5offsets( 0, ibuf.size(), 0, 1, ibuf.size(), 1);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, ibuf );
cout << "APTH5_ACQ_03_DETECTOR_DEADPULSES write " << status << "\n";
	ibuf = vector<unsigned int>();

	wfbuf.clear();
	wfbuf.reserve( nevt ); //##MK::maybe a resize is faster?
	for( size_t b = 0; b < nb; b++ ) {
		if ( rawdata_epos.at(b) != NULL ) {
			for( auto it = rawdata_epos.at(b)->begin(); it != rawdata_epos.at(b)->end(); ++it ) {
				wfbuf.push_back( it->tof );
			}
		}
	}
	ifo = h5iometa( APTH5_ACQ_03_TIME_OF_FLIGHT, wfbuf.size(), 1);
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
cout << "APTH5_ACQ_03_TIME_OF_FLIGHT create " << status << "\n";
	offs = h5offsets( 0, wfbuf.size(), 0, 1, wfbuf.size(), 1);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, wfbuf );
cout << "APTH5_ACQ_03_TIME_OF_FLIGHT write " << status << "\n";
	attrbuf.clear();
	attrbuf.push_back( make_pair("SIUnit","ns") );
	status = debugh5Hdl.write_attribute_string( APTH5_ACQ_03_TIME_OF_FLIGHT, attrbuf );
cout << "APTH5_ACQ_03_TIME_OF_FLIGHT attr " << status << "\n";

	//##MK::strictly speaking we could just overwrite for speed but better be hyperphobic
	wfbuf.clear();
	wfbuf.reserve( nevt );
	for( size_t b = 0; b < nb; b++ ) {
		if ( rawdata_epos.at(b) != NULL ) {
			for( auto it = rawdata_epos.at(b)->begin(); it != rawdata_epos.at(b)->end(); ++it ) {
				wfbuf.push_back( it->vdc );
			}
		}
	}
	ifo = h5iometa( APTH5_ACQ_03_STANDING_VOLTAGE, wfbuf.size(), 1);
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
cout << "APTH5_ACQ_03_STANDING_VOLTAGE create " << status << "\n";
	offs = h5offsets( 0, wfbuf.size(), 0, 1, wfbuf.size(), 1);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, wfbuf );
cout << "APTH5_ACQ_03_STANDING_VOLTAGE write " << status << "\n";
	attrbuf.clear();
	attrbuf.push_back( make_pair("SIUnit","V") );
	status = debugh5Hdl.write_attribute_string( APTH5_ACQ_03_STANDING_VOLTAGE, attrbuf );
cout << "APTH5_ACQ_03_STANDING_VOLTAGE attr " << status << "\n";

	wfbuf.clear();
	wfbuf.reserve( nevt );
	for( size_t b = 0; b < nb; b++ ) { //looping multiple time reduces resident memory footprint at iteration costs
		if ( rawdata_epos.at(b) != NULL ) {
			for( auto it = rawdata_epos.at(b)->begin(); it != rawdata_epos.at(b)->end(); ++it ) {
				wfbuf.push_back( it->vpu );
			}
		}
	}
	ifo = h5iometa( APTH5_ACQ_03_PULSE_VOLTAGE, wfbuf.size(), 1);
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
cout << "APTH5_ACQ_03_PULSE_VOLTAGE create " << status << "\n";
	offs = h5offsets( 0, wfbuf.size(), 0, 1, wfbuf.size(), 1);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, wfbuf );
cout << "APTH5_ACQ_03_PULSE_VOLTAGE write " << status << "\n";
	attrbuf.clear();
	attrbuf.push_back( make_pair("SIUnit","V") );
	status = debugh5Hdl.write_attribute_string( APTH5_ACQ_03_PULSE_VOLTAGE, attrbuf );
cout << "APTH5_ACQ_03_PULSE_VOLTAGE attr " << status << "\n";

	//##MK::later better accumulate blocks successively via hyperslabs but check performance first
	//there by resident memory will be low while looping only once over blocks at higher I/O write costs though ...
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_TIP_TEMPERATURE, nostring );
cout << "APTH5_ACQ_03_TIP_TEMPERATURE " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_PULSE_FREQUENCY, nostring );
cout << "APTH5_ACQ_03_PULSE_FREQUENCY " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_LASER_PULSE_ENERGY, nostring );
cout << "APTH5_ACQ_03_LASER_PULSE_ENERGY " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_LASER_POSITION, nostring );
cout << "APTH5_ACQ_03_LASER_POSITION " << status << "\n";
	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_STAGE_POSITION, nostring );
cout << "APTH5_ACQ_03_STAGE_POSITION " << status << "\n";

	status = debugh5Hdl.write_string_ascii( APTH5_ACQ_03_FINALIZATION_STATUS, APTH5_ACQ_03_FINALIZATION_ASDESIRED_SUCCESS );
cout << "APTH5_ACQ_03_FINALIZATION_STATUS " << status << "\n";
}


void transcoderHdl::transcode_aptv2_to_apth5()
{

}
