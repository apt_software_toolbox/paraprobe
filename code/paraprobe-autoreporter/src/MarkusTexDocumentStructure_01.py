def add_package_definitions():
    str = r'''\documentclass{article}
%\documentclass[review]{elsarticle}
%\documentclass[draft]{elsarticle}

\usepackage{lineno,hyperref}
\usepackage{amssymb}
\usepackage{mathtools}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}

\usepackage[binary-units=true]{siunitx}
\usepackage{subfig}
\modulolinenumbers[5]

\usepackage{cleveref} 
%has to be loaded after hyperref \usepackage[capitalise]{cleveref} % to have all fig. eq. figs. appearing as Fig. https://texblog.org/2013/05/06/cleveref-a-clever-way-to-reference-in-latex/
%on abbreviations see https://tex.stackexchange.com/questions/256849/cleveref-change-behaviour-of-cref-to-use-the-abbreviated-form
%specific modifications to cleveref
\Crefname{equation}{Eq.}{Eqs.}
\Crefname{figure}{Fig.}{Figs.}
\Crefname{tabular}{Tab.}{Tabs.}

\usepackage{xcolor}

%\usepackage{soul}
%turn on draft figures for document
%\setkeys{Gin}{draft}
%%%%\journal{Ultramicroscopy}

%% `Elsevier LaTeX' style
%%%%\bibliographystyle{elsarticle-num}
%%%%\biboptions{sort&compress}

%%user-defined commands
\newcommand{\python}{Python{\,\,}}
\newcommand{\matlab}{MATLAB{\,\,}}
\newcommand{\paraprobe}{PARAPROBE{\,\,}}
\newcommand{\cgal}{CGAL{\,\,}}
\newcommand{\qhull}{QHull{\,\,}}
\newcommand{\voroxx}{Voro$++${\,\,}}
\newcommand{\hdf}{HDF5{\,\,}}
\newcommand{\xdmf}{XDMF{\,\,}}
\newcommand{\hdmf}{HDF5/XDMF{\,\,}}
\newcommand{\omp}{OpenMP{\,\,}}
\newcommand{\imkl}{Intel Math Kernel Library{\,\,}}
\newcommand{\imk}{Intel Math Kernel{\,\,}}
\newcommand{\itbb}{Intel Threaded Building Blocks{\,\,}}
\newcommand{\paraview}{Paraview{\,\,}}
\newcommand{\visit}{VisIt{\,\,}}
\newcommand{\ivas}{IVAS{\,\,}}
\newcommand{\blender}{Blender{\,\,}}
\newcommand{\rapidxml}{RapidXML{\,\,}}
\newcommand{\ashapes}{$\alpha$-shapes{\,\,}}
\newcommand{\ashape}{$\alpha$-shape{\,\,}}
\newcommand{\bunge}[3]{($\varphi_1=\SI[mode=math]{#1}{\degree}, \Phi=\SI[mode=math]{#2}{\degree},\varphi_2=\SI[mode=math]{#3}{\degree}$)}
\newcommand{\whom}[1]{\textcolor{blue}{\textbf{ #1 }}}
\newcommand{\swisswatch}[2]{$\SI[mode=math]{#1}{\hour}$:$\SI[mode=math]{#2}{\minute}$}
\newcommand{\althreesc}{Al${}_3$Sc\,\,}
\newcommand{\ival}[3]{$[#1,#2,#3]$}
\newcommand{\alscsi }[2]{Al-\SI[mode=text]{#1}{}Sc-\SI[mode=text]{#2}{}Si}
\newcommand{\itswtpercent}{(wt. \SI[mode=text]{}{\percent})\,}
\newcommand{\cameca}{CAMECA/AMETEK\,\,}

%simplifying the typesetting of parameter settings
\newcommand{\maxsep}[4]{$d_{max} = [#1,#2,#3] \SI[mode=math]{}{\nano\meter}, N_{min} = \SI[mode=math]{#4}{}$}
\newcommand{\spatstat}[5]{$k = #1, r = [#2,#3,#4] \SI[mode=math]{}{\nano\meter}, d_{srf} \geq \SI[mode=math]{#5}{\nano\meter}$}
\newcommand{\spatstatnok}[4]{$r = [#1,#2,#3] \SI[mode=math]{}{\nano\meter}, d_{srf} \geq \SI[mode=math]{#4}{\nano\meter}$}  %the more general with specific k

\newcommand{\ionsurf}[1]{$d_{prb} = \SI[mode=math]{#1}{\nano\meter}$}
\newcommand{\vorotess}[1]{$d_{ero} = \SI[mode=math]{#1}{\nano\meter}$}
\newcommand{\twostat}[4]{$r = [#1,#2,#3] \SI[mode=math]{}{\nano\meter}, \Delta r = \SI[mode=math]{#4}{\nano\meter}$}
\newcommand{\aptcryst}[8]{$el = [#1,#2,#3], az = [#4,#5,#6], r_{max} = \SI[mode=math]{#7}{\nano\meter}, m = \SI[mode=math]{#8}{}$}
\newcommand{\ashapeinit}[1]{$d_{bin} = \SI[mode=math]{#1}{\nano\meter}$}



%% annotating debug stuff
\newcommand{\dbg}[1]{\textcolor{red}{\textbf{ #1 }}}
%% annotating stuff for the supplementary
\newcommand{\suppl}[1]{\textcolor{green}{#1}}
\newcommand{\kick}[1]{\textcolor{orange}{#1}}

%%tables
\usepackage{booktabs}
\usepackage{tabularx}

%specific implementation algorithmic charts
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\def\BState{\State\hskip-\ALG@thistlm}
\algdef{SE}[DOWHILE]{Do}{doWhile}{\algorithmicdo}[1]{\algorithmicwhile\ #1}
%https://tex.stackexchange.com/questions/115709/do-while-loop-in-pseudo-code

%##MK
%\graphicspath{{../dd_figures/}}
    '''
    return str
	

def add_title_page():
	str = r'''
\title{\paraprobe auto-generated report}
\author{Markus K\"uhbach}
\date{\today}
	'''
	return str

	
def add_header():
    str = r'''
\begin{document}
\maketitle
    '''
    return str	
	
	
def add_footer():
    str = r'''
\end{document}
    '''
    return str