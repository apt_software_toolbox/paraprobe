PARAPROBE_SPATSTAT_RDF_META = 'SpatialStatistics/RDF/Metadata'
PARAPROBE_SPATSTAT_RDF_RES = 'SpatialStatistics/RDF/Results'

PARAPROBE_SPATSTAT_KNN_META = 'SpatialStatistics/KNN/Metadata'
PARAPROBE_SPATSTAT_KNN_RES = 'SpatialStatistics/KNN/Results'

PARAPROBE_SPATSTAT_SDM_META = 'SpatialStatistics/SDM/Metadata'
PARAPROBE_SPATSTAT_SDM_RES = 'SpatialStatistics/SDM/Results'

