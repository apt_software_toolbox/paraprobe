import periodictable as pse

def pp_peakcandidate( pkid, candtbl, scortbl, threshold ):
    N = candtbl.shape[0]
    tbcaption = 'Peak ' + str(int(pkid))
    #tbcaption = 'List of single ion cation and molecular ion cations with not more than 3, not necessarily different, isotopes combined and charge state \+3 at most. The solutions are listed in decreasing order of their abundance score (ABScore). Possible solution with ABScore smaller than 0.001 are not shown.'
    tblabel = 'PeakCandidateTable ' + str(int(pkid))
    s = '\n' + r'''\begin{center}
    \begin{table}[h]
    \caption{''' + tbcaption + r'''}
    \centering
    \begin{tabular}{llllllllll} %\addlinespace[0.2em]
    \toprule
    \bf{No.}  & \bf{N1}       & \bf{Z1}  & \bf{N2}    & \bf{Z2}   & \bf{N3}  & \bf{Z3}  & \bf{$q^{+}$}  & \bf{$m \SI{}{\atomicmassunit^{0.5}}$}  & \bf{ABScore} \\ \midrule''' + '\n'
    #print(N)
    c=0
#    while True:
#        if (scortbl[c,1] > 0.001 and c < N - 1):
#            s = s + str(c) + r''' & ''' + str(candtbl[c,1]) + r''' & ''' + str(pse.elements[candtbl[c,0]].symbol) + r''' & '''
#            if candtbl[c,2] > 0:
#                s = s + str(candtbl[c,3]) + r''' & ''' + str(pse.elements[candtbl[c,2]].symbol) + r''' & '''
#            else:
#                s = s + '---' + r''' & ''' + '---' + r''' & '''
#            if candtbl[c,4] > 0:
#                s = s + str(candtbl[c,5]) + r''' & ''' + str(pse.elements[candtbl[c,4]].symbol) + r''' & '''
#            else:
#                s = s + '---' + r''' & ''' + '---' + r''' & '''
#            s = s + str(candtbl[c,6]) + r''' & ''' + str(scortbl[c,0]) + r''' & ''' + str(scortbl[c,1])
#            s = s + r''' \\''' + '\n'
#            ##handle last loop
#            s = s + r''' \bottomrule ''' + '\n'
#            c = c + 1
#        else:
#            break
        
    for c in range(0, N):
        if scortbl[c,1] > threshold:
            s = s + str(c) + r''' & ''' + str(candtbl[c,1]) + r''' & ''' + str(pse.elements[candtbl[c,0]].symbol) + r''' & '''
            if candtbl[c,2] > 0:
                s = s + str(candtbl[c,3]) + r''' & ''' + str(pse.elements[candtbl[c,2]].symbol) + r''' & '''
            else:
                s = s + '---' + r''' & ''' + '---' + r''' & '''
            if candtbl[c,4] > 0:
                s = s + str(candtbl[c,5]) + r''' & ''' + str(pse.elements[candtbl[c,4]].symbol) + r''' & '''
            else:
                s = s + '---' + r''' & ''' + '---' + r''' & '''
            
            s = s + str(candtbl[c,6]) + r''' & ''' + str(scortbl[c,0]) + r''' & ''' + str(scortbl[c,1])
            #if c > 0 and c < N-1:
            s = s + r''' \\ ''' + '\n'
        else:
            break
    s = s + r''' \bottomrule ''' + "\n"
            #if c == 0:
            #    s = s + r''' \\''' + '\n'    # \midrule
            #else:
            #    s = s + r''' \\ \bottomrule''' + '\n'
    s = s + r'''
    \end{tabular} %\addlinespace[1em]
    \label{''' + tblabel + r'''}
    \end{table}
    \end{center}''' + '\n'
    return s
