# -*- coding: utf-8 -*-
"""
//##MK::GPLV3

paraprobe-autoreporter, 
@author: m.kuehbach, 2019/10/25
"""

MYTOOLPATH='H:/BIGMAX_RELATED/MPIE-APTFIM-TOOLBOX/paraprobe/code/paraprobe-autoreporter'
h5fn=MYTOOLPATH + '/' + 'run' + '/' + 'PARAPROBE.Spatstat.Results.SimID.20001.h5'
texfn=MYTOOLPATH + '/' + 'run' + '/' + 'PARAPROBE.Autoreporter.SimID.20001.tex'

import os, sys, glob, subprocess
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import periodictable as pse

sys.path.append(MYTOOLPATH + '/' + 'src')
sys.path.append(MYTOOLPATH + '/' + 'src' + '/' + 'metadata')
#be determinsitic
np.random.seed(0)
#if it is in Cstyle stored in the HDF5 file it will remain Cstyle ordered in numpy so no implicit transformation
#http://christopherlovell.co.uk/blog/2016/04/27/h5py-intro.html
import h5py 

#tool-specific includes
#from MarkusTexDocumentStructure_01 import *
#from MarkusTexUserTables_01 import *
#from MarkusTexUserFigures_01 import *
#from MarkusTexUserPeakIdent_01 import *

#----------------------------------------------------------->begin JUPYTER notebook part

from PARAPROBE_Numerics import *
from PARAPROBE_CorporateDesign import *
from PARAPROBE_MetadataSpatstatDefsH5 import * 
from PARAPROBE_Profiler import *

#explore_tasks()
#hf = h5py.File(h5fn, 'r')

#append any eventual RDF stuff to the report
#we need a class object with methods
#paraprobe-tool-specific Python classes which translate H5-contained PARAPROBE results
import PARAPROBE_SpatstatHdl as spst

tic()

hdl = spst.spatstatHdl( h5fn )
report_rdf = hdl.explore_rdf()

#report_knn = hdl.explore_knn()
report_sdm = hdl.explore_sdm()

toc()

#general class instance defining the meat of the TeX file and specifying the methods
#which define the Corporate Design of the report
import PARAPROBE_XelatexHdl as xtex

report = xtex.xelatexHdl( texfn );
report.init( r'''Markus K\"uhbach''' ) #adds Tex package definitions

#example of how to add a comment to your analysis
comment = r'''This is a personal comment to this analysis can use heavy formulas and \LaTeX notation and formulas'''

report.add_introduction( comment )

#automatically generate figures histograms, CDFs, tables and append to report
report.add_section_rdf( 'Data mining radial distribution function for multiple ion types ...' )
report.add( report_rdf )

report.add_section_knn( '... and k-nearest neighbor analyses have seldom be so conveniently accessible!' )
#report += spst.explore_knn( h5fn )

report.add_section_sdm( 'Surplus we can have now even more fun two-point statistics of our APT data!' )

#report += spst.explore_sdm( h5fn )

#saves me hours in front of the GUI, fully transparent ...
report.submit()


#go to your favorite xelatex Tex editor and compile into a PDF report
#sweet, now I can flesh out figures from the report into paper
#sweet, now I can focus on the science within the dataset again

#----------------------------------------------------------->end JUPYTER notebook part













#which statistics were harvested?
spatstat=list(hf['SpatialStatistics'])

if 'KNN' in spatstat:
    knn = list(hf['SpatialStatistics/KNN'])
    print('KNN exists')
    if 'Metadata' in knn:
        tasks = list(hf[PARAPROBE_SPATSTAT_KNN_META])
        for tsk in tasks:
            this = list(hf[PARAPROBE_SPATSTAT_KNN_META + '/' + tsk])
            if 'RandomizedLabels' in this:
                print(tsk + ' used randomized labels')
            else:
                print(tsk + ' used original labels')
            if 'kthNearest' in this:
                kth = tsk['kthNearest']
            else:
                kth = 0
            print(kth)
            #get target single/molecular ion type
            if 'IontypeTargets' in this:
                tg = np.array(list(hf[PARAPROBE_SPATSTAT_KNN_META + '/' + tsk + '/IontypeTargets'])).flatten()
                targets = ""
                if tg[2] > 0:
                    targets += str(pse.mass(tg[2]).symbol)
                if tg[4] > 0:
                    targets += str(pse.elements[tg[4]].symbol)
                if tg[6] > 0:
                    targets += str(pse.elements[tg[6]].symbol)
            if 'IontypeNeighbors' in this:
                tg = np.array(list(hf[PARAPROBE_SPATSTAT_KNN_META + '/' + tsk + '/IontypeNeighbors'])).flatten()
                nbors = ""
                if tg[2] > 0:
                    nbors += str(pse.elements[tg[2]].symbol) ###SYMBOLS DONT MATCH!
                if tg[4] > 0:
                    nbors += str(pse.elements[tg[4]].symbol)
                if tg[6] > 0:
                    nbors += str(pse.elements[tg[6]].symbol)
            print('Combination ' + targets + '\t' + nbors)
    if 'Results' in knn:
        tasks = list(hf[PARAPROBE_SPATSTAT_KNN_RES])
        for tsk in tasks: 
            this = list(hf[PARAPROBE_SPATSTAT_KNN_RES + '/' + tsk])
            if 'Cnts' in this:
                hst1d_res = hf[PARAPROBE_SPATSTAT_KNN_RES + '/' + tsk + '/Cnts']
                nl = len(hst1d_res[:,1])
                #is statistic significant, then go from hst1d_res empirical histogram to ECDF
                if np.any(hst1d_res[:,1] > EPSILON):
                    ecdf = np.zeros([nl-1,2],np.float64)
                    ecdf[:,0] = hst1d_res[0:nl-1,0]
                    ecdf[:,1] = np.cumsum(hst1d_res[0:nl-1,1])/np.sum(hst1d_res[0:nl-1,1])
                    
                    #generate file name for the figure like in the HDF5
                    pngfn = (PARAPROBE_SPATSTAT_KNN_RES + '/' + tsk + '/Cnts').replace('/','_') + '.png'
                   
                    ####PLOT( ecdf, pngfn )
                    ####ADD SUBFIGURE( pngfn, targets, nbors, 0.5 )
                    ####plt.plot(ecdf[:,0],ecdf[:,1])



dsnm=list(hf.keys())

def descend_obj(obj,sep='\t'):
    """
    Iterate through groups in a HDF5 file and prints the groups and datasets names and datasets attributes
    """
    if type(obj) in [h5py._hl.group.Group,h5py._hl.files.File]:
        for key in obj.keys():
            print (sep,'-',key,':',obj[key])
            descend_obj(obj[key],sep=sep+'\t')
    elif type(obj)==h5py._hl.dataset.Dataset:
        for key in obj.attrs.keys():
            print(sep,'\t','-',key,':',obj.attrs[key])

descend_obj(hf)
