def pp_peaktable( peaktbl, N, tbcaption, tblabel ):
    M = 3
    s = '\n' + r'''\begin{center}
    \begin{table}[h]
    \caption{''' + tbcaption + r'''}
    \centering
    \begin{tabular}{llll} %\addlinespace[0.2em]
    \toprule
    \bf{Peak ID}                 & \bf{Position (\SI{}{\atomicmassunit^{0.5}})}         & \bf{Cnts}     &   \bf{Histogram bin}   \\ \midrule  ''' + '\n'
    for pkid in range(0, N):
        s = s + str(int(peaktbl[pkid,0])) + r''' & '''
        for i in range(1,M):
            s = s + str(peaktbl[pkid,i]) + r''' & '''
        s = s + str(int(peaktbl[pkid,M]))
        
        if pkid > 0 and pkid < N-1:
            s = s + r''' \\ ''' + '\n'
        else:
            if pkid == 0:
                s = s + r''' \\''' + '\n'    # \midrule
            else:
                s = s + r''' \\ \bottomrule ''' + '\n'
#    #Dost-MS kNN                    & Sc-Sc, \spatstat{1}{0.0}{0.001}{5.0}{0.0}     & 1 & 1            \\ \bottomrule
    s = s + r'''
    \end{tabular} %\addlinespace[1em]
    \label{''' + tblabel + r'''}
    \end{table}
    \end{center}''' + '\n'
    return s
