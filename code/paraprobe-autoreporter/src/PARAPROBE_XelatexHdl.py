import os, sys, glob, subprocess
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import periodictable as pse

from PARAPROBE_Numerics import *
from PARAPROBE_CorporateDesign import *
from PARAPROBE_MetadataSpatstatDefsH5 import * 

class xelatexHdl:
    def __init__(self, texin):
        #(self, name, age)
        self.latexres = texin
        self.document = ""

    def init( self, author ):
        self.document += r'''\documentclass[12pt, a4paper, twoside, titlepage]{article}
%\documentclass{article}
%\documentclass[review]{elsarticle}
%\documentclass[draft]{elsarticle}

\usepackage{lineno,hyperref}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage[binary-units=true]{siunitx}
\usepackage{subfig}

\usepackage{cleveref}
%has to be loaded after hyperref \usepackage[capitalise]{cleveref} % to have all fig. eq. figs. appearing as Fig. https://texblog.org/2013/05/06/cleveref-a-clever-way-to-reference-in-latex/
%on abbreviations see https://tex.stackexchange.com/questions/256849/cleveref-change-behaviour-of-cref-to-use-the-abbreviated-form
%specific modifications to cleveref

\usepackage{xcolor}
%\usepackage{soul}
%turn on draft figures for document
%\setkeys{Gin}{draft}
%%%%\journal{Ultramicroscopy}

%text alignment
%\modulolinenumbers[5]    
        
%custom declares for mathematical notation
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\Crefname{equation}{Eq.}{Eqs.}
\Crefname{figure}{Fig.}{Figs.}
\Crefname{tabular}{Tab.}{Tabs.}

%%LaTeX' style
%%%%\bibliographystyle{elsarticle-num}
%%%%\biboptions{sort&compress}

%% annotating debug stuff
\newcommand{\dbg}[1]{\textcolor{red}{\textbf{ #1 }}}
%% annotating stuff for the supplementary
\newcommand{\suppl}[1]{\textcolor{green}{#1}}
\newcommand{\kick}[1]{\textcolor{orange}{#1}}
%%tables
\usepackage{booktabs}
\usepackage{tabularx}

%specific implementation algorithmic charts
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\def\BState{\State\hskip-\ALG@thistlm}
\algdef{SE}[DOWHILE]{Do}{doWhile}{\algorithmicdo}[1]{\algorithmicwhile\ #1}
%https://tex.stackexchange.com/questions/115709/do-while-loop-in-pseudo-code

%##MK
%\graphicspath{{../figures/}}
    
%%user-defined commands
\newcommand{\python}{Python{\,\,}}
\newcommand{\matlab}{MATLAB{\,\,}}
\newcommand{\paraprobe}{PARAPROBE{\,\,}}
\newcommand{\cgal}{CGAL{\,\,}}
\newcommand{\qhull}{QHull{\,\,}}
\newcommand{\voroxx}{Voro$++${\,\,}}
\newcommand{\hdf}{HDF5{\,\,}}
\newcommand{\xdmf}{XDMF{\,\,}}
\newcommand{\hdmf}{HDF5/XDMF{\,\,}}
\newcommand{\omp}{OpenMP{\,\,}}
\newcommand{\imkl}{Intel Math Kernel Library{\,\,}}
\newcommand{\imk}{Intel Math Kernel{\,\,}}
\newcommand{\itbb}{Intel Threaded Building Blocks{\,\,}}
\newcommand{\paraview}{Paraview{\,\,}}
\newcommand{\visit}{VisIt{\,\,}}
\newcommand{\ivas}{IVAS{\,\,}}
\newcommand{\blender}{Blender{\,\,}}
\newcommand{\rapidxml}{RapidXML{\,\,}}
\newcommand{\ashapes}{$\alpha$-shapes{\,\,}}
\newcommand{\ashape}{$\alpha$-shape{\,\,}}
\newcommand{\bunge}[3]{($\varphi_1=\SI[mode=math]{#1}{\degree}, \Phi=\SI[mode=math]{#2}{\degree},\varphi_2=\SI[mode=math]{#3}{\degree}$)}
\newcommand{\whom}[1]{\textcolor{blue}{\textbf{ #1 }}}
\newcommand{\swisswatch}[2]{$\SI[mode=math]{#1}{\hour}$:$\SI[mode=math]{#2}{\minute}$}
\newcommand{\althreesc}{Al${}_3$Sc\,\,}
\newcommand{\ival}[3]{$[#1,#2,#3]$}
\newcommand{\alscsi }[2]{Al-\SI[mode=text]{#1}{}Sc-\SI[mode=text]{#2}{}Si}
\newcommand{\itswtpercent}{(wt. \SI[mode=text]{}{\percent})\,}
\newcommand{\cameca}{CAMECA/AMETEK\,\,}
    
%simplifying the typesetting of parameter settings
\newcommand{\maxsep}[4]{$d_{max} = [#1,#2,#3] \SI[mode=math]{}{\nano\meter}, N_{min} = \SI[mode=math]{#4}{}$}
\newcommand{\spatstat}[5]{$k = #1, r = [#2,#3,#4] \SI[mode=math]{}{\nano\meter}, d_{srf} \geq \SI[mode=math]{#5}{\nano\meter}$}
\newcommand{\spatstatnok}[4]{$r = [#1,#2,#3] \SI[mode=math]{}{\nano\meter}, d_{srf} \geq \SI[mode=math]{#4}{\nano\meter}$}  %the more general with specific k
    
\newcommand{\ionsurf}[1]{$d_{prb} = \SI[mode=math]{#1}{\nano\meter}$}
\newcommand{\vorotess}[1]{$d_{ero} = \SI[mode=math]{#1}{\nano\meter}$}
\newcommand{\twostat}[4]{$r = [#1,#2,#3] \SI[mode=math]{}{\nano\meter}, \Delta r = \SI[mode=math]{#4}{\nano\meter}$}
\newcommand{\aptcryst}[8]{$el = [#1,#2,#3], az = [#4,#5,#6], r_{max} = \SI[mode=math]{#7}{\nano\meter}, m = \SI[mode=math]{#8}{}$}
\newcommand{\ashapeinit}[1]{$d_{bin} = \SI[mode=math]{#1}{\nano\meter}$}
\renewcommand*{\thesubfigure}{(\arabic{subfigure})}
%%begin with the document
\title{\paraprobe auto-generated report}
        '''
        self.document += r'''\author{''' + author + r'''}'''
        self.document += r''' 
        '''
        self.document += r'''\date{\today}'''
        self.document += r''' 
        '''
        self.document += r'''\begin{document}'''
        self.document += r''' 
        '''
        self.document += r'''\maketitle
        '''


    def add_introduction( self, latex ):
        self.document += latex
        self.document += r''' 
        '''
  
    
    def add( self, latex ):
        self.document += latex
    
    def submit( self ):
        self.document += r'''
        '''
        self.document += r'''\end{document}'''
        
        #remove '        ' indents
        self.document.replace('        ','')
        #export to file
        
        with open(self.latexres,'w') as f:
            f.write(self.document)
        print('Finished writing report ' + self.latexres)
 
       
    def add_section_rdf( self, latex ):
        self.document += r'''
        '''
        self.document += r'''\section{Radial distribution function}'''
        self.document += r''' 
        '''
        self.document += latex


    def add_section_knn( self, latex ):
        self.document += r'''
        '''
        self.document += r'''\section{K-nearest neighbor analysis}'''
        self.document += r'''
        '''
        self.document += latex
 
       
    def add_section_sdm( self, latex ):
        self.document += r'''
        '''
        self.document += r'''\section{Two-point statistics / spatial distribution maps}'''
        self.document += r'''
        '''
        self.document += latex