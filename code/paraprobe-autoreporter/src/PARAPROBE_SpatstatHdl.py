import os, sys, glob, subprocess
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import periodictable as pse

MYTOOLPATH='H:/BIGMAX_RELATED/MPIE-APTFIM-TOOLBOX/paraprobe/code/paraprobe-autoreporter'
sys.path.append(MYTOOLPATH + '/' + 'src')
sys.path.append(MYTOOLPATH + '/' + 'src' + '/' + 'metadata')

np.random.seed(0) #to be determinsitic
#if it is in Cstyle stored in the HDF5 file it will remain Cstyle ordered in numpy so no implicit transformation
#http://christopherlovell.co.uk/blog/2016/04/27/h5py-intro.html
import h5py

from PARAPROBE_Numerics import *
from PARAPROBE_CorporateDesign import *
from PARAPROBE_MetadataSpatstatDefsH5 import * 

class spatstatHdl:
    def __init__(self, h5in):
        #(self, name, age)
        self.h5res = h5in
        self.document = ''

    def explore_rdf(self):
        #self.h5res = h5in
        print('Exploring RDF on ' + self.h5res)
        #hf = h5py.File(h5fn, 'r')
        hf = h5py.File(self.h5res, 'r')
        spatstat=list(hf['SpatialStatistics'])
        
        self.document += r'''\begin{figure}[!ht]
        \centering'''

        if 'RDF' in spatstat:
            rdf = list(hf['SpatialStatistics/RDF'])
            print('RDF exists')
            if 'Metadata' in rdf and 'Results' in rdf:
                #get meta data for the tasks and check if corresponding results exists
                #if they do plot them
                tasks = list(hf[PARAPROBE_SPATSTAT_RDF_META])
                for tsk in tasks:
                    if 'Task' in tsk:
                        print(tsk)
                        #get the metadata first
                        thismeta = list(hf[PARAPROBE_SPATSTAT_RDF_META + '/' + tsk])
                        org_itypes = 1
                        if 'RandomizedLabels' in thismeta:
                            print(tsk + ' used randomized labels')
                            org_itypes = 0
                        else:
                            print(tsk + ' used original labels')
                        targets = ""
                        nbors = ""
                        #get target single/molecular ion type
                        if 'IontypeTargets' in thismeta:
                            tg = np.array(list(hf[PARAPROBE_SPATSTAT_RDF_META + '/' + tsk + '/IontypeTargets'])).flatten()
                            if tg[1] > 0:
                                targets += str(pse.elements[tg[1]].symbol)
                            if tg[3] > 0:
                                targets += str(pse.elements[tg[3]].symbol)
                            if tg[5] > 0:
                                targets += str(pse.elements[tg[5]].symbol)
                        if 'IontypeNeighbors' in thismeta:
                            tg = np.array(list(hf[PARAPROBE_SPATSTAT_RDF_META + '/' + tsk + '/IontypeNeighbors'])).flatten()
                            if tg[1] > 0:
                                nbors += str(pse.elements[tg[1]].symbol)
                            if tg[3] > 0:
                                nbors += str(pse.elements[tg[3]].symbol)
                            if tg[5] > 0:
                                nbors += str(pse.elements[tg[5]].symbol)
                        print('Combination ' + targets + '\t' + nbors)
                        if 'Results' in rdf:
                            thisres = hf[PARAPROBE_SPATSTAT_RDF_RES + '/' + tsk]
                            if 'Cnts' in thisres:
                                hst1d_res = np.array(list(hf[PARAPROBE_SPATSTAT_RDF_RES + '/' + tsk + '/Cnts']))
                                nl = len(hst1d_res[:,1])
                                nl = nl-1
                                #is statistic significant, then go from hst1d_res empirical histogram to ECDF
                                if np.any(hst1d_res[0:nl,1] > EPSILON):
                                    #following the definitions on page 281ff of B. Gault, M. P. Moody, J. M. Cairney and S. P. Ringer
                                    #Atom Probe Microscopy, dx.doi.org/10.1007/978-1-4614-3436-8
                                    #RDF(r) = \frac{1}{\bar{\rho}} \frac{n_{RDF}(r)}{\frac{4}{3}\pi(r+0.5\Delta r)^3-(r-0.5\Delta r)^3}$
                                    #mind that n_{RDF}(r) in between we find that accumulated counts estimates RipleyK but not the RDF
                                    #we report r, accumulated count 
                                    rdfdist = np.zeros([nl,4],np.float64)
                                    norm = 1.0; ####MK::incorrect need to read from file!
                                    for i in range(1,nl,1):
                                        ##MK::possibly an r-offset here by half the bin-width!
                                        rdfdist[i,0] = 0.5*(hst1d_res[i,0]+hst1d_res[i-1,0]) #r
                                        rdfdist[i,1] = hst1d_res[i,1]-hst1d_res[i-1,1] #diff
                                        rdfdist[i,2] = rdfdist[i,0]**3 - rdfdist[i-1,0]**3 #sphvol
                                        if rdfdist[i,2] > EPSILON:
                                            rdfdist[i,3] = rdfdist[i,1] / rdfdist[i,2] * norm; #norm*diff/sphvol
                                    #label=mylabels[i]
                                    plt.plot(rdfdist[:,0],rdfdist[:,3], color=myparula[0], alpha=myalpha, linewidth=mylinewidth )
                                    if org_itypes == 1:
                                        plt.legend(['Original ion labels'])
                                    else:
                                        plt.legend(['Randomized ion labels'])
                                    #plt.title(r'$\sigma_{xy}$, $\mu = 0.0$ nm, $a = 0.404$ nm')
                                    plt.title(r'RDF, ' + tsk + ', ' + targets + ' vs ' + nbors)
                                    plt.xlabel('Distance to target ion (nm)')
                                    plt.ylabel(r'Unnormalized RDF')
                                    #plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
                                    #plt.yscale('log')
                                    xmi = min(rdfdist[:,0])
                                    xmx = max(rdfdist[:,0])
                                    plt.xlim([-0.05*(xmx-xmi)+xmi, +0.05*(xmx-xmi)+xmx])
                                    ymi = min(rdfdist[:,3])
                                    ymx = max(rdfdist[:,3])
                                    plt.ylim([-0.05*(ymx-ymi)+ymi, +0.05*(ymx-ymi)+ymx])
                                    #ymi = 0.0
                                    #ymx = 1.0
                                    #plt.ylim([-0.05, +1.05])
                                    #plt.yticks([0.0:0.1:1.0]) #, ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
                                    fig = plt.gcf()
                                    fig.set_size_inches(myfigure_width, myfigure_height)
                                    #plt.scatter(qq[:,0],qq[:,1],s=30,color='#9DC943',alpha=1.0,linewidths=0.0)
                                    #generate file name for the figure like in the HDF5
                                    #self.h5res + '/' + 
                                    pngfn = (PARAPROBE_SPATSTAT_RDF_RES + '/' + tsk + '/Cnts').replace('/','_') + '.pdf' #'.png'
                                    fig.savefig(pngfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
                                                transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
                                    fig.clf()
                                    plt.close()
        
                                    self.document += r'''
                                    '''
                                    self.document += r'''\subfloat[][''' + r'RDF, ' + tsk + ', ' + targets + ' vs ' + nbors + r''']'''
                                    self.document += r'''{\includegraphics[width=0.45\textwidth]{''' + pngfn + r'''}\label{''' + tsk + r'''}}'''
                                    self.document += r'''\quad'''
                                    self.document += r''' '''
        self.document += r'''\caption{Summary of all computed RDFs}
        '''
        
        self.document += r'''\label{RDFs}
        '''
        self.document += r'''\end{figure}
        '''

        return self.document



    def explore_knn(self):
        #self.h5res = h5in
        print('Exploring KNN on ' + self.h5res)
        #hf = h5py.File(h5fn, 'r')
        hf = h5py.File(self.h5res, 'r')
        spatstat=list(hf['SpatialStatistics'])
        if 'KNN' in spatstat:
            knn = list(hf['SpatialStatistics/KNN'])
            print('KNN exists')
            if 'Metadata' in knn and 'Results' in knn:
                #get meta data for the tasks and check if corresponding results exists
                #if they do plot them
                tasks = list(hf[PARAPROBE_SPATSTAT_KNN_META])
                for tsk in tasks:
                    if 'Task' in tsk:
                        print(tsk)
                        #get the metadata first
                        thismeta = list(hf[PARAPROBE_SPATSTAT_KNN_META + '/' + tsk])
                        org_itypes = 1
                        if 'RandomizedLabels' in thismeta:
                            print(tsk + ' used randomized labels')
                            org_itypes = 0
                        else:
                            print(tsk + ' used original labels')
                        kth = 0
                        if 'kthNearestNeighbor' in thismeta:
                            kth = np.array(list(hf[PARAPROBE_SPATSTAT_KNN_META + '/' + tsk + '/' + 'kthNearestNeighbor'])).flatten()[0] #MK::re-name to be consistent
                        print(kth)
                        targets = ""
                        nbors = ""
                        #get target single/molecular ion type
                        if 'IontypeTargets' in thismeta:
                            tg = np.array(list(hf[PARAPROBE_SPATSTAT_KNN_META + '/' + tsk + '/IontypeTargets'])).flatten()
                            if tg[1] > 0:
                                targets += str(pse.elements[tg[1]].symbol)
                            if tg[3] > 0:
                                targets += str(pse.elements[tg[3]].symbol)
                            if tg[5] > 0:
                                targets += str(pse.elements[tg[5]].symbol)
                        if 'IontypeNeighbors' in thismeta:
                            tg = np.array(list(hf[PARAPROBE_SPATSTAT_KNN_META + '/' + tsk + '/IontypeNeighbors'])).flatten()
                            if tg[1] > 0:
                                nbors += str(pse.elements[tg[1]].symbol)
                            if tg[3] > 0:
                                nbors += str(pse.elements[tg[3]].symbol)
                            if tg[5] > 0:
                                nbors += str(pse.elements[tg[5]].symbol)
                        print('Combination ' + targets + '\t' + nbors)
                        if 'Results' in knn:
                            thisres = hf[PARAPROBE_SPATSTAT_KNN_RES + '/' + tsk]
                            if 'Cnts' in thisres:
                                hst1d_res = hf[PARAPROBE_SPATSTAT_KNN_RES + '/' + tsk + '/Cnts']
                                nl = len(hst1d_res[:,1])
                                #eliminate highest dump
                                nl = nl-1
                                #is statistic significant, then go from hst1d_res empirical histogram to ECDF
                                if np.any(hst1d_res[0:nl,1] > EPSILON):
                                    ecdf = np.zeros([nl,2],np.float64)
                                    ecdf[:,0] = hst1d_res[0:nl,0]
                                    ecdf[:,1] = np.cumsum(hst1d_res[0:nl,1])/np.sum(hst1d_res[0:nl,1])
                                    print(np.sum(hst1d_res[0:nl,1]))
        
                                    #label=mylabels[i]
                                    plt.plot(ecdf[:,0],ecdf[:,1], color=myparula[0], alpha=myalpha, linewidth=mylinewidth )
                                    if org_itypes == 1:
                                        plt.legend(['Original ion labels'])
                                    else:
                                        plt.legend(['Randomized ion labels'])
                                    #plt.title(r'$\sigma_{xy}$, $\mu = 0.0$ nm, $a = 0.404$ nm')
                                    plt.title(r'KNN, ' + tsk + ', ' + 'kth = ' + str(kth) + ', ' + targets + ' vs ' 
                                              + nbors, fontsize=14)
                                    plt.xlabel('Distance to target ion (nm)', fontsize=18)
                                    plt.ylabel(r'Cumulative distribution', fontsize=18)
                                    #plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
                                    #plt.yscale('log')
                                    xmi = min(ecdf[:,0])
                                    xmx = max(ecdf[:,0])
                                    plt.xlim([-0.05*(xmx-xmi)+xmi, +0.05*(xmx-xmi)+xmx])
                                    #ymi = min(ecdf[:,1])
                                    #ymx = max(ecdf[:,1])
                                    #plt.xlim([-0.05*(ymx-ymi)+ymi, +0.05*(ymx-ymi)+ymx])
                                    ymi = 0.0
                                    ymx = 1.0
                                    plt.ylim([-0.05, +1.05])
                                    #plt.yticks([0.0:0.1:1.0]) #, ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
                                    fig = plt.gcf()
                                    fig.set_size_inches(myfigure_width, myfigure_height)
                                    #plt.scatter(qq[:,0],qq[:,1],s=30,color='#9DC943',alpha=1.0,linewidths=0.0)
                                    #plt.rcParams.update({'font.size': 14})
                                    #generate file name for the figure like in the HDF5
                                    #self.h5res + '/' + 
                                    pngfn = (PARAPROBE_SPATSTAT_KNN_RES + '/' + tsk + '/Cnts').replace('/','_') + '.pdf' # '.png'
                                    fig.savefig(pngfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
                                                transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)
                                    fig.clf()
                                    plt.close()
                            #thisres = np.array(hf[PARAPROBE_SPATSTAT_SDM_RES + '/' + tsk + '/Cnts']).flatten()
                            print('Read ' + PARAPROBE_SPATSTAT_KNN_RES + '/' + tsk + '/Cnts')
        return 'Finished KNN'


    def explore_sdm(self):
        #self.h5res = h5in
        print('Exploring SDM on ' + self.h5res)
        #hf = h5py.File(h5fn, 'r')
        hf = h5py.File(self.h5res, 'r')
        spatstat=list(hf['SpatialStatistics'])
        if 'SDM' in spatstat:
            sdm = list(hf['SpatialStatistics/SDM'])
            print('SDM exists')
            if 'Metadata' in sdm and 'Results' in sdm:
                #get meta data for the tasks and check if corresponding results exists
                #if they do plot them

                tasks = list(hf[PARAPROBE_SPATSTAT_SDM_META])
                #get Bincenters
                if 'BincenterXYZ' in tasks:
                    thisres = np.array(hf[PARAPROBE_SPATSTAT_SDM_META + '/' + 'BincenterXYZ'])
                    nx = int(np.ceil(len(thisres)**(1/3))) #to care for floating point inaccuracies
                    if ( nx*nx*nx != len(thisres) ):
                        nx = int(np.floor(len(thisres)**(1/3)))
                    if ( nx*nx*nx == len(thisres) ):
                        print('Dimensions of thisres are ' + str(nx*nx*nx) )
                        #x,y,z implicit order
                        if ( (nx-1) % 2 == 0 ): #always odd cubes
                            ##MK::start reading first nx X values, and given that we have always cubes these are also the Ys!
                            #x = thisres[0:nx,0]
                            ##MK::HACK for NRW2019! because there was a bug in the writing of BincenterXYZ, has now been corrected, actual cnts are correct
                            x = np.linspace(-82.5*0.05,+82.5*0.05,163,endpoint=True)
                            y = x
                            #start = 0 + 0*nx + int((nx-1)/2)*nx*nx    
                            print('BincenterXYZ loaded...')
                for tsk in tasks:
                    if 'Task' in tsk:
                        print(tsk)
                        #get the metadata first
                        thismeta = list(hf[PARAPROBE_SPATSTAT_SDM_META + '/' + tsk])
                        org_itypes = 1
                        if 'RandomizedLabels' in thismeta:
                            print(tsk + ' used randomized labels')
                            org_itypes = 0
                        else:
                            print(tsk + ' used original labels')
                        kth = 0
                        if 'kthNearestNeighbor' in thismeta:
                            kth = np.array(list(hf[PARAPROBE_SPATSTAT_SDM_META + '/' + tsk + '/' + 'kthNearestNeighbor'])).flatten()[0] #MK::re-name to be consistent
                        print(kth)
                        targets = ""
                        nbors = ""
                        #get target single/molecular ion type
                        if 'IontypeTargets' in thismeta:
                            tg = np.array(list(hf[PARAPROBE_SPATSTAT_SDM_META + '/' + tsk + '/IontypeTargets'])).flatten()
                            if tg[1] > 0:
                                targets += str(pse.elements[tg[1]].symbol)
                            if tg[3] > 0:
                                targets += str(pse.elements[tg[3]].symbol)
                            if tg[5] > 0:
                                targets += str(pse.elements[tg[5]].symbol)
                        if 'IontypeNeighbors' in thismeta:
                            tg = np.array(list(hf[PARAPROBE_SPATSTAT_SDM_META + '/' + tsk + '/IontypeNeighbors'])).flatten()
                            if tg[1] > 0:
                                nbors += str(pse.elements[tg[1]].symbol) ###SYMBOLS DONT MATCH!
                            if tg[3] > 0:
                                nbors += str(pse.elements[tg[3]].symbol)
                            if tg[5] > 0:
                                nbors += str(pse.elements[tg[5]].symbol)
                        print('Combination ' + targets + '\t' + nbors)
                        
                        #do results for this task exist
                        if 'Results' in sdm:
                            thisres = np.array(hf[PARAPROBE_SPATSTAT_SDM_RES + '/' + tsk + '/Cnts']).flatten()
                            print('Read ' + PARAPROBE_SPATSTAT_SDM_RES + '/' + tsk + '/Cnts')
                            ##MK::for now we know we have 3D cubes
                            nx = int(np.ceil(len(thisres)**(1/3))) #to care for floating point inaccuracies
                            if ( nx*nx*nx != len(thisres) ):
                                nx = int(np.floor(len(thisres)**(1/3)))
                            if ( nx*nx*nx == len(thisres) ):
                                print('Dimensions of thisres are ' + str(nx*nx*nx) )
                                #x,y,z implicit order
                                if ( (nx-1) % 2 == 0 ): #always odd cubes
                                    start = 0 + 0*nx + int((nx-1)/2)*nx*nx
                                    #extract xy section at z = 0
                                    xyplane = thisres[start:start+nx*nx:1]
                                    xysection = np.reshape(xyplane,(nx,nx))
                                    ##label=mylabels[i]
                                    plt.contourf(x, y, xysection) #chose confourf to fill it for paper #color=myparula[0], alpha=myalpha, linewidth=mylinewidth )
                                    plt.gcf().set_size_inches(myfigure_width+sdm_colorbar_width_offset, myfigure_height)
                                    if org_itypes == 1:
                                        plt.legend(['Original ion labels'])
                                    else:
                                        plt.legend(['Randomized ion labels'])
                                    plt.colorbar() #cax=cax)
                                    ##plt.title(r'$\sigma_{xy}$, $\mu = 0.0$ nm, $a = 0.404$ nm')
                                    plt.title(r'SDM, ' + tsk + ', kth = ' + str(kth) + ', ' + targets + ' vs ' + nbors)
                                    plt.xlabel(r'x (nm)')
                                    plt.ylabel(r'y (nm)')
                                    ##plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
                                    ##plt.yscale('log')
                                    #xmi = min(ecdf[:,0])
                                    #xmx = max(ecdf[:,0])
                                    #plt.xlim([-0.05*(xmx-xmi)+xmi, +0.05*(xmx-xmi)+xmx])
                                    ##ymi = min(ecdf[:,1])
                                    ##ymx = max(ecdf[:,1])
                                    ##plt.xlim([-0.05*(ymx-ymi)+ymi, +0.05*(ymx-ymi)+ymx])
                                    #ymi = 0.0
                                    #ymx = 1.0
                                    #plt.ylim([-0.05, +1.05])
                                    #plt.yticks([0.0:0.1:1.0]) #, ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
                                    ##plt.scatter(qq[:,0],qq[:,1],s=30,color='#9DC943',alpha=1.0,linewidths=0.0)
                                    
                                    ##generate file name for the figure like in the HDF5
                                    pngfn = (PARAPROBE_SPATSTAT_SDM_RES + '/' + tsk + '/Cnts').replace('/','_') + '.pdf' #'.pdf' #'.png'
                                    
                                    plt.savefig(pngfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='pdf', 
                                        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)
                                    plt.close()
        return 'Finished SDM'
                            ####PLOT( ecdf, pngfn )
                            ####ADD SUBFIGURE( pngfn, targets, nbors, 0.5 )
                            ####plt.plot(ecdf[:,0],ecdf[:,1])
