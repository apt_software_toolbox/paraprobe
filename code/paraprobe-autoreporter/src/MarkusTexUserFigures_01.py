def pp_singlefigure( myfn, fgcaption, fglabel ):
    s = '\n' + r'''\begin{figure}[H]
    \centering
    \includegraphics[width=1.0\textwidth]{''' + myfn + r'''}
    \caption{''' + fgcaption + r'''}
    \label{''' + fglabel + r'''}''' + '\n'
    s = s + r'''\end{figure}'''
    return s