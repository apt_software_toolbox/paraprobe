//##MK::CODE

#include "CONFIG_Indexer.h"

E_ANALYSIS_MODE ConfigIndexer::AnalysisMode = PERFORM_INDEXING;
string ConfigIndexer::InputfileAraullo = "";
string ConfigIndexer::InputfileRefImages = "";
string ConfigIndexer::InputfileTestOris = "";

unsigned int ConfigIndexer::NumberOfS2Directions = 0;
unsigned int ConfigIndexer::IOUpToKthClosest = 1;
apt_real ConfigIndexer::IOSymmRedDisoriAngle = DEGREE2RADIANT(2.0);
bool ConfigIndexer::IOSolutionQuality = false;
bool ConfigIndexer::IODisoriMatrix = false;
bool ConfigIndexer::IOSymmReduction = false;
apt_real ConfigIndexer::IOSymmRedMaxDisoriAngle = DEGREE2RADIANT(2.0);
unsigned int ConfigIndexer::NumberOfSO3Candidates = 0;

int ConfigIndexer::GPUsPerNode = 0;
int ConfigIndexer::GPUWorkload = 10; //##MK::almost sure not optimal but sweep spot which number is optimal is case dependent, therefore here the possibility to do systematic studies


bool ConfigIndexer::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Input.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigIndexer")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "AnalysisMode" );
	switch (mode)
	{
		case EVALUATE_DISORIENTATIONS:
			AnalysisMode = EVALUATE_DISORIENTATIONS; break;
		case CREATE_REFERENCEIMAGE:
			AnalysisMode = CREATE_REFERENCEIMAGE; break;
		case PERFORM_INDEXING:
			AnalysisMode = PERFORM_INDEXING; break;
		default:
			AnalysisMode = E_NOTHING;
	}
	InputfileAraullo = read_xml_attribute_string( rootNode, "InputfileAraullo" );
	InputfileRefImages = read_xml_attribute_string( rootNode, "InputfileRefImages" );
	InputfileTestOris = read_xml_attribute_string( rootNode, "InputfileTestOris" );
	
	//NumberOfS2Directions will be read in-place while parsing InputfileAraullo
	
	if ( AnalysisMode == PERFORM_INDEXING ) {
		IOUpToKthClosest = read_xml_attribute_uint32( rootNode, "IOUpToKthClosest" );
		IOSymmRedDisoriAngle = DEGREE2RADIANT(read_xml_attribute_float( rootNode, "IOSymmRedDisoriAngle" ));
		IOSolutionQuality = read_xml_attribute_bool( rootNode, "IOSolutionQuality" );
		IODisoriMatrix = read_xml_attribute_bool( rootNode, "IODisoriMatrix" );
		IOSymmReduction = read_xml_attribute_bool( rootNode, "IOSymmReduction" );
		
		//IOSymmRedMaxDisoriAngle is a global parameter
		//NumberOfSO3Candidates will be set when parsing InputfileTestCands
	}
	
	GPUsPerNode = read_xml_attribute_int32( rootNode, "GPUsPerComputingNode" );
	GPUWorkload = read_xml_attribute_int32( rootNode, "GPUWorkload" );
	if ( GPUWorkload < 0 || GPUWorkload > 10000 ) {
		cout << "WARNING: The GPU workload has to be at least 1, the user input was errorneous, so I resetted to 10!" << "\n";
		GPUWorkload = 10;
	}
	
	return true;
}


bool ConfigIndexer::checkUserInput()
{
	cout << "ConfigIndexer::" << "\n";
	switch(AnalysisMode)
	{
		case EVALUATE_DISORIENTATIONS:
			cout << "We evaluate the disorientation of all TestOris against one another." << "\n";
			cout << "We report for which combinations the disorientation angle is below " << IOSymmRedMaxDisoriAngle << " radiants" << "\n";
			break;
		case CREATE_REFERENCEIMAGE:
			cerr << "CREATE_REFERENCEIMAGE is currently not implemented!" << "\n";
			return false;
		case PERFORM_INDEXING:
			cout << "We attempt an indexing of the APT specimen using the results from Araullo-Peters analysis." << "\n";
			break;
		default:
			cerr << "Unknown AnalysisMode chosen" << "\n";	return false;
	}
	
	if ( AnalysisMode == PERFORM_INDEXING ) {
		if ( IOUpToKthClosest < 1 ) {
			cerr << "IOUpToKthClosest has to be a positive integer at least as large as 1!" << "\n"; return false;
		}
		cout << "IOUpToKthClosest " << IOUpToKthClosest << "\n";
		if ( IOSymmRedDisoriAngle < EPSILON ) {
			cerr << "IOSymmRedDisoriAngle has to be positive and finite!" << "\n"; return false;
		}
		if ( IOSymmRedDisoriAngle > IOSymmRedDisoriAngle ) {
			cerr << "WARNING::IOSymmRedDisoriAngle was reset to IOSymmRedMaxDisoriAngle which is " << IOSymmRedMaxDisoriAngle << " radiants!" << "\n";
		}
		cout << "IOSymmRedDisoriAngle " << IOSymmRedDisoriAngle << "\n";
		cout << "IOSolutionQuality " << IOSolutionQuality << "\n";
		cout << "IODisoriMatrix " << IODisoriMatrix << "\n";
		cout << "IOSymmReduction " << IOSymmReduction << "\n";
	}
	cout << "IOSymmRedMaxDisoriAngle " << IOSymmRedMaxDisoriAngle << "\n";
	cout << "GPUsPerComputingNode " << GPUsPerNode << "\n";
	if ( GPUWorkload < 1 ) {
		cerr << "GPUWorkloadFactor needs to be integer and at least 1!" << "\n";
		cerr << "To delegate all work to CPUs instead set GPUsPerComputingNode to 0!" << "\n"; return false;
	}
	cout << "GPUWorkloadFactor " << GPUWorkload << "\n";
	
	cout << "InputfileAraullo read from " << InputfileAraullo << "\n";
	cout << "InputfileRefImages read from " << InputfileRefImages << "\n";
	cout << "InputfileTestOris read from " << InputfileTestOris << "\n";
	
	cout << "\n";
	return true;
}

