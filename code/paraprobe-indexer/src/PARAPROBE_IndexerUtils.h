//##MK::GPLV3


#ifndef __PARAPROBE_INDEXER_UTILS_H__
#define __PARAPROBE_INDEXER_UTILS_H__

//#include "PARAPROBE_VolumeSampler.h"
#include "../../paraprobe-utils/src/PARAPROBE_VolumeSampler.h"
#include "PARAPROBE_IndexerTicToc.h"

//MK::(de/)activate GPUs for easier development purposes, easier because will not force to have MPI+OMP+OPENACC capable compiler
#define UTILIZE_GPUS

#ifdef UTILIZE_GPUS
	//using OpenACC
	#include <openacc.h>
	#include <accelmath.h>
#endif


struct DisoriIJ
{
	apt_real q0;
	unsigned int i;
	unsigned int j;
	DisoriIJ() : q0(0.f), i(UINT32MX), j(UINT32MX) {}
	DisoriIJ( const apt_real _q0, const unsigned int _i, const unsigned int _j) :
		q0(_q0), i(_i), j(_j) {}
};



/*
#define NORESULTS_FOR_THIS_MATERIALPOINT	INT32MX
#define WAIT_UNTIL_GLOBAL_EXTREMUM_FOUND	INT32MX

struct hklval
{
	int mpid;
	int ijk;
	dft_real val;
	hklval() : mpid(NORESULTS_FOR_THIS_MATERIALPOINT), ijk(INT32MX), val(0.0) {}
	hklval( const int _mpid, const int _ijk, const dft_real _val ) :
		mpid(_mpid), ijk(_ijk), val(_val) {}
};

ostream& operator << (ostream& in, hklval const & val);



struct hklspace_res
{
	dft_real h;
	dft_real k;
	dft_real l;
	dft_real SQRIntensity;
	hklspace_res() : h(0.0), k(0.0), l(0.0), SQRIntensity(0.0) {}
	hklspace_res( const dft_real _h, const dft_real _k, const dft_real _l, const dft_real _sqr ) :
		h(_h), k(_k), l(_l), SQRIntensity(_sqr) {}
};

ostream& operator << (ostream& in, hklspace_res const & val);


class dft_cpu
{
	//the class is a utility tool it does the actual direct Fourier transform on the CPU
public:
	dft_cpu();
	dft_cpu( const size_t n );
	~dft_cpu();
	
	hklval execute( vector<dft_real> const & p );
	void reset();
		
private:
	dft_real* Fmagn;				//magnitude of the Fourier space intensity
	dft_real* ival;					//Fourier space grid position 1d of a 3d cube hkl <=> ijk
	int NI;							//Fourier space number of voxel 1d
	int NIJ;
	int NIJK;	
};
*/


#endif
