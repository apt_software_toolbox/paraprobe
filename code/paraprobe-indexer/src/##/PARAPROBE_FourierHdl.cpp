//##MK::CODESPLIT

#include "PARAPROBE_FourierHdl.h"


fourierHdl::fourierHdl()
{
	myrank = MASTER;
	nranks = 1;
}


fourierHdl::~fourierHdl()
{
}


bool fourierHdl::read_reconxyz_from_apth5()
{
	double tic = MPI_Wtime();

	if ( inputH5Hdl.read_xyz_from_apth5( ConfigFourier::InputfileReconstruction, xyz ) == true ) {
		cout << ConfigFourier::InputfileReconstruction << " read successfully" << "\n";
	}
	else {
		cerr << ConfigFourier::InputfileReconstruction << " read failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "ReadReconstructedXYZFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool fourierHdl::read_ranging_from_apth5()
{
/*
//load ranging information
	if ( inputReconH5Hdl.read_iontypes_from_apth5( ConfigSpatstat::InputfileReconstruction, rng.iontypes ) == true ) {
		cout << ConfigSpatstat::InputfileReconstruction << " read ranging information successfully" << "\n";
	}
	else {
		cerr << ConfigSpatstat::InputfileReconstruction << " read ranging information failed!" << "\n"; return false;
	}

	//create iontype_dictionary
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		size_t id = static_cast<size_t>(it->id);
		size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
	cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
		if ( id != 0 && hashval != 0 ) { //skip adding existent UNKNOWN_IONTYPE
			auto jt = rng.iontypes_dict.find( hashval );
			if ( jt == rng.iontypes_dict.end() ) { //really a new iontype for which not yet mqival were defined
	cout << "--->Adding new type " << id << "\t\t" << hashval << "\n";
				//add iontype in dictionary
				rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) );
			}
			else {
	cerr << "--->Detected replicated iontype which should not happen as iontypes have to be mutually exclusive!" << "\n"; return false;
			}
		}
		else {
	cout << "--->Me skipping the UNKNOWN_IONTYPE" << "\n";
		}
	}

	//load actual itypes per xyz position
	if ( inputReconH5Hdl.read_ityp_from_apth5( ConfigSpatstat::InputfileReconstruction, ityp_org ) == true ) {
		cout << ConfigSpatstat::InputfileReconstruction << " read itypes successfully" << "\n";
	}
	else {
		cerr << ConfigSpatstat::InputfileReconstruction << " read itypes failed!" << "\n"; return false;
	}
*/
	cerr << "read_ranging_from_apth5() not yet implemented!" << "\n";
	return true;
}


bool fourierHdl::read_triangle_hull_from_apth5()
{
	cerr << "read_triangle_hull_from_apth5 not implemented" << "\n";
/*
	double tic = MPI_Wtime();

	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	int status = 0;

	if ( ConfigFourier::InputfileHullAndDistances.substr(ConfigFourier::InputfileHullAndDistances.length()-6) != ".apth5" ) {
		cerr << "The InputfileHullAndDistances has not the proper apth5 file extension!" << "\n";
		return false;
	}

	//probe existence of xyz coordinates array in the input file and get dimensions
	inputH5Hdl.h5resultsfn = ConfigFourier::InputfileHullAndDistances;
	if ( inputH5Hdl.query_contiguous_matrix_dims( PARAPROBE_SURFRECON_ASHAPE_HULL_GEOM, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the triangle vertex data in the InputfileHullAndDistances" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_SURFRECON_ASHAPE_HULL_GEOM is a " << offs.nrmax << " rows " << offs.ncmax << " matrix" << "\n";
	}

	//read the content and transfer to working array
	ifo = h5iometa( PARAPROBE_SURFRECON_ASHAPE_HULL_GEOM, offs.nrows(), offs.ncols() );
	vector<float> rf32buf;
	status = inputH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs, rf32buf );
cout << "PARAPROBE_SURFRECON_ASHAPE_HULL_GEOM read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SURFRECON_ASHAPE_HULL_GEOM on InputfileHullAndDistances failed" << "\n"; return false;
	}

	try {
		triangle_hull.reserve( offs.nrows() );
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading triangle hull positions" << "\n";
	}
	for( size_t i = 0; i < rf32buf.size(); i += 9 ) {
		triangle_hull.push_back( tri3d(
				rf32buf[i+0], rf32buf[i+1], rf32buf[i+2],
				rf32buf[i+3], rf32buf[i+4], rf32buf[i+5],
				rf32buf[i+6], rf32buf[i+7], rf32buf[i+8] ) );
	}
	rf32buf = vector<float>();

	//read the distances
	if ( inputH5Hdl.query_contiguous_matrix_dims( PARAPROBE_SURFRECON_ASHAPE_ION2DIST, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the ion distance InputfileHullAndDistances" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_SURFRECON_ASHAPE_ION2DIST is a " << offs.nrmax << " rows " << offs.ncmax << " matrix" << "\n";
	}

	//read the content and transfer to working array
	ifo = h5iometa( PARAPROBE_SURFRECON_ASHAPE_ION2DIST, offs.nrows(), offs.ncols() );
	status = inputH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs, rf32buf );
cout << "PARAPROBE_SURFRECON_ASHAPE_ION2DIST read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_SURFRECON_ASHAPE_ION2DIST on InputfileHullAndDistances failed" << "\n"; return false;
	}

//	//##MK::ion_distances exists already on master and was filled with 0.0 as dummy values so just write into NO push_back !
//	try {
//		dist2hull.reserve( offs.nrows() );
//	}
//	catch (bad_alloc &croak) {
//		cerr << "Allocation error during reading triangle hull positions" << "\n";
//	}
//	for( size_t i = 0; i < rf32buf.size(); i += 1 ) {
//		dist2hull.push_back( rf32buf[i] );
//	}
//	rf32buf = vector<float>();

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "ReadTriangleHullAndDistancesFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
*/
	return true;
}


bool fourierHdl::broadcast_reconxyz()
{
	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( xyz.size() >= (INT32MX-1) ) {
			cerr << "Current implementation does not handle the case of larger than 2 billion ion dataset transfer!" << "\n"; localhealth = 0;
		}
		if ( xyz.size() == 0 ) {
			cerr << "Ion positions is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the dataset!" << "\n";
		return false;
	}

	//dataset on the master is okay, but do we need to broadcast at all to others, i.e. are there slaves?
	if ( get_nranks() < 2 ) {
		cout << "Skipping broadcast_reconxyz because single process execution" << "\n";
		return true;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<size_t>(xyz.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_IonPositions* buf = NULL;
	try {
		buf = new MPI_IonPositions[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf!" << "\n"; localhealth = 0;
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL; return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < xyz.size(); ++i ) {
			buf[i] = MPI_IonPositions( xyz[i] );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_IonPositions_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			xyz.reserve( static_cast<size_t>(nevt) );
			//dist2hull.reserve( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				xyz.push_back( p3d( buf[i].x, buf[i].y, buf[i].z ) );
				//dist2hull.push_back( buf1[i].d ); //##MK::fill with dummy values here!
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
		return false;
	}
	else {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
	}

	/*
	cout << "Rank " << get_myrank() << " listened to MASTER xyz.size() " << xyz.size() << "\n";
	cout << "Rank " << get_myrank() << " listened to MASTER dist2hull.size() " << dist2hull.size() << "\n";
	cout << "Rank " << get_myrank() << " listened to MASTER triangle_hull.size() " << triangle_hull.size() << "\n";
	*/

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "BroadcastReconXYZ", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool fourierHdl::broadcast_triangles()
{
cerr << "broadcast_triangles currently not implemented!" << "\n";
	double tic = MPI_Wtime();

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "BroadcastTriangles", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool fourierHdl::broadcast_distances()
{
cerr << "broadcast_distances currently not implemented, using dummy values at the moment!" << "\n";
	double tic = MPI_Wtime();

	try {
		dist2hull = vector<apt_real>( xyz.size(), F32MX );
	}
	catch (bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation of dist2hull failed!" << "\n";
		return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "BroadcastDistances", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}

/*
bool fourierHdl::broadcast_reconstruction_and_triangles()
{
	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far know state and size of the datasets!
		if ( xyz.size() >= (INT32MX-1) ) {
			cerr << "Current implementation does not handle the case of larger than 2 billion ion dataset transfer!" << "\n";
			localhealth = 0;
		}
		if ( xyz.size() != dist2hull.size() ) {
			cerr << "MASTER has a different number of ion positions than distance values!" << "\n";
			localhealth = 0;
		}
		if ( xyz.size() == 0 ) {
			cerr << "Ion positions is empty, so nothing to communicate!" << "\n";
			localhealth = 0;
		}
		if ( dist2hull.size() == 0 ) {
			cerr << "Ion distances is empty, so nothing to communicate!" << "\n";
			localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the dataset!" << "\n";
		return false;
	}

	//shortcut if only a single MPI process across MPI_COMM_WORLD
	if ( get_nranks() > 1 ) {

		//communicate size of the master's dataset
		int nevt = 0;
		if ( get_myrank() == MASTER ) {
			nevt = static_cast<size_t>(xyz.size());
		}
		MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

		//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
		localhealth = 1;
		MPI_IonWithDistance* buf1 = NULL;
		MPI_Triangle* buf2 = NULL;
		try {
			buf1 = new MPI_IonWithDistance[nevt];
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error for buf1 and/or buf2" << "\n";
			localhealth = 0;
		}
		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) {
			cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
			delete [] buf1; buf1 = NULL;
			delete [] buf2; buf2 = NULL;
			return false;
		}

		//master populates communication buffer
		if ( get_myrank() == MASTER ) {
			for( size_t i = 0; i < xyz.size(); ++i ) {
				buf1[i] = MPI_IonWithDistance( xyz[i], dist2hull[i] );
			}
		}

		//collective call MPI_Bcast, MASTER broadcasts, slaves listen
		MPI_Bcast( buf1, nevt, MPI_IonWithDistance_Type, MASTER, MPI_COMM_WORLD );

		//slave nodes pull from buffer
		if ( get_myrank() != MASTER ) {
			try {
				xyz.reserve( static_cast<size_t>(nevt) );
				dist2hull.reserve( static_cast<size_t>(nevt) );
				for( int i = 0; i < nevt; i++ ) {
					xyz.push_back( p3d( buf1[i].x, buf1[i].y, buf1[i].z ) );
					dist2hull.push_back( buf1[i].d ); //##MK::fill with dummy values here!
				}
			}
			catch(bad_alloc &mecroak) {
				cerr << "Rank " << get_myrank() << " allocation error during buf1 readout!" << "\n";
				localhealth = 0;
			}
		}

		//all release the buffer, delete on NULL pointer gets compiled as no op
		delete [] buf1; buf1 = NULL;
		delete [] buf2; buf2 = NULL;

		//final check whether all were successful so far
		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) {
			cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
			return false;
		}
	}
	else {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
	}

	//cout << "Rank " << get_myrank() << " listened to MASTER xyz.size() " << xyz.size() << "\n";
	//cout << "Rank " << get_myrank() << " listened to MASTER dist2hull.size() " << dist2hull.size() << "\n";
	//cout << "Rank " << get_myrank() << " listened to MASTER triangle_hull.size() " << triangle_hull.size() << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "BroadcastDataAcrossMPIProcesses", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}
*/


void fourierHdl::spatial_decomposition()
{
	double tic, toc;
	tic = MPI_Wtime();

	sp.tip_aabb_get_extrema( xyz_ityp );

	toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "SpecimenAABB3DGetExtrema", APT_XX, APT_IS_SEQ, mm, tic, toc);

	tic = MPI_Wtime();

	sp.loadpartitioning( xyz_ityp );

	toc = MPI_Wtime();
	mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "AABB3DLoadPartitioning", APT_XX, APT_IS_PAR, mm, tic, toc);
}


void fourierHdl::define_matpoint_volume_grid()
{
	double tic = MPI_Wtime();

	//##MK::to begin with use the entire tip as our reference window to analyze
	mp.define_probe_volume( sp.tip );

	switch (ConfigFourier::SamplingGridMode)
	{
		//##MK::case
		default:
			mp.define_regular_grid( ConfigFourier::SamplingGridBinWidthX, ConfigFourier::SamplingGridBinWidthY,
										ConfigFourier::SamplingGridBinWidthZ, ConfigFourier::ROIRadiusMax );
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "DefineMaterialPointVolumeGrid", APT_XX, APT_IS_SEQ, mm, tic, toc);
}


void fourierHdl::distribute_matpoints_on_processes()
{
	double tic = MPI_Wtime();

	//all processes have the same matpoints and agree on the same global round-robin work partitioning
	//##MK::error handling
	if ( mp.matpoints.size() >= static_cast<size_t>(INT32MX-1) ) {
		cerr << "More material sampling points were defined that current implementation can handle!" << "\n"; return;
	}

	mp2rk.reserve( mp.matpoints.size() );
	//globally orchestrated round robin partitioning
	int nmp = static_cast<int>(mp.matpoints.size());
	int nr = get_nranks();
	int mr = get_myrank();
	for( int mpid = 0; mpid < nmp; mpid++ ) {
		int thisrank = mpid % nr;
		mp2rk.push_back( thisrank );
		if ( thisrank != mr ) {
			continue;
		}
		else {
			myworkload.push_back( mp.matpoints[mpid] );
//cout << "Rank " << thisrank << " takes care of material point/ROI " << mpid << "\n";
		}
	}

	//per-allocate local result buffers
	myhklval = vector<hklval>( myworkload.size(), hklval( NORESULTS_FOR_THIS_MATERIALPOINT, INT32MX, 0.0 ) );

	double toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "DistributeMaterialpointsAcrossMPIProcesses", APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " will process " << myworkload.size() << " of a total of " << mp.matpoints.size() << " sampling points" << "\n";
}


gpu_cpu_max_workload fourierHdl::plan_max_per_epoch( const int ngpu, const int nthr, const int nmp )
{
	gpu_cpu_max_workload res = gpu_cpu_max_workload();
	if ( ngpu == 1 ) {
		if ( nthr > 1 ) { //master delegates work to GPUs, only if additional threads CPU also participates in FT
			if ( nmp <= nthr-1 ) { //##MK::small workload only on the GPU
				res.chunk_max_cpu = 0; //##MK::could be improved, but insignificant
				res.chunk_max_gpu = nmp;
			}
			else { //large workload distribute on GPUs and CPUs
				res.chunk_max_cpu = nthr-1;
				res.chunk_max_gpu = ngpu * ConfigFourier::GPUWorkload; //##MK::needs benchmarking, empirical!, per GPU!, assume multiple of the same accelerator e.g. 2x NVIDIA V100 as on TALOS
			}
		}
		else { //only a master which delegates work to GPUs but no own FT work scheduled to this master CPU !
			res.chunk_max_cpu = 0; //##MK::could be improved, but insignificant
			res.chunk_max_gpu = ngpu * ConfigFourier::GPUWorkload;
		}
	}
	else if ( ngpu == 0 ) { //no GPUs, everything on threads/CPU
		res.chunk_max_gpu = 0;
		res.chunk_max_cpu = nthr; //every thread gets an own FT to work on to maximize independent workload as such minimize sync
		//##MK::might be problematic for large NN, as memory consumption is at least nt*(NN^3)*3*8B, <=256 should no problem even for nt=40 for TALOS
	}
	else {
		cerr << "Rank " << get_myrank() << " attempt to use more than a single GPU per process is  invalid!" << "\n";
	}

	return res;
}


gpu_cpu_now_workload fourierHdl::plan_now_per_epoch(
		const int ngpu, const int nthr, const int nmp, const int mp, const gpu_cpu_max_workload mxl )
{
	gpu_cpu_now_workload res = gpu_cpu_now_workload();
	int rem = nmp - mp;

	if ( ngpu == 1 ) { //if we can use accelerators
		if ( nthr > 1 ) { //master delegates work to GPUs surplus additional threads CPUs also computing FTs
			if ( rem >= (mxl.chunk_max_gpu + mxl.chunk_max_cpu) ) { //still enough work for GPUs and CPUs
				res.chunk_now_gpu = mxl.chunk_max_gpu;
				res.chunk_now_cpu = mxl.chunk_max_cpu;
			}
			else if ( rem >= mxl.chunk_max_cpu ) { //more work than CPUs available, so GPUs assist
				res.chunk_now_gpu = rem - mxl.chunk_max_cpu; //GPUs assist
				res.chunk_now_cpu = mxl.chunk_max_cpu; //CPUs remainder
			}
			else {
				res.chunk_now_gpu = 0; //not even enough work to keep all CPUs busy, ##MK::can be improved...
				res.chunk_now_cpu = rem;
			}
		}
		else { //only master, so master delegates potentially to GPUs
			if ( rem >= mxl.chunk_max_gpu ) {
				res.chunk_now_gpu = mxl.chunk_max_gpu; //enough work to make a full gpu epoch
				res.chunk_now_cpu = 0;
			}
			else {
				res.chunk_now_gpu = rem; //not enough work for a complete gpu epoch but delegate then this less work still to the gpu
				res.chunk_now_cpu = 0;
			}
		}
	}
	else if ( ngpu == 0 ) { //we have no accelerators
		res.chunk_now_gpu = 0;
		if ( rem >= mxl.chunk_max_cpu ) { //more work than CPUs available all process full epoch
			res.chunk_now_cpu = mxl.chunk_max_cpu;
		}
		else { //not even enough work to keep all CPUs busy
			res.chunk_now_cpu = rem;
		}
	}
	else {
		cerr << "Rank " << get_myrank() << " attempt to use more than a single GPU per process is  invalid!" << "\n";
	}

	return res;
}


/*
void fourierHdl::generate_debug_roi( mt19937 & mydice, vector<dft_real> * out )
{
	//generate a pseudo lattice workload for testing purposes of the algorithm
	uniform_real_distribution<dft_real> unifrnd(0.f, 1.f); //no MT warmup
	int imi = -5;
	int imx = +5;
	dft_real a = 0.404; //lattice constant
	dft_real da = 0.10 * a; //some simple squared scatter about ideal lattice position
	for( int z = imi; z <= imx; z++ ) {
		for( int y = imi; y <= imx; y++ ) {
			for( int x = imi; x <= imx; x++ ) {
				//create dummy sc lattice
				dft_real xx = static_cast<dft_real>(x) * a * (2*unifrnd(mydice)-1.0) * 1.0 * da;
				dft_real yy = static_cast<dft_real>(y) * a * (2*unifrnd(mydice)-1.0) * 1.0 * da;
				dft_real zz = static_cast<dft_real>(z) * a * (2*unifrnd(mydice)-1.0) * 0.5 * da;
				//if ( unifrnd(mydice) < 1.0 ) { //random thinning
				out->push_back( xx );
				out->push_back( yy );
				out->push_back( zz );
				//}
			}
		}
	}
	//##MK::BEGIN OF DEBUGGING
	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " generated a dummy dataset with " << out->size() << " coordinates" << "\n";
	}
	//##MK::END OF DEBUGGING
}
*/


void fourierHdl::query_ions_within_roi( mp3d const & roicenter, vector<dft_real> * out )
{
	//CALLED FROM WITHIN PARALLEL REGION
	apt_xyz SearchRadiusSQR = SQR(roicenter.R);
	p3dm1 probesite = p3dm1( roicenter.x, roicenter.y, roicenter.z, DEFAULTTYPE );

	vector<p3dm1> neighbors3dm1;
	for( size_t thr = MASTER; thr < sp.db.size(); thr++ ) { //scan all regions O(lgN) to find neighboring candidates
		threadmemory* thisregion = sp.db.at(thr);
		if ( thisregion != NULL ) {
			vector<p3dm1> const & theseions = thisregion->ionpp3_kdtree;
			kd_tree* curr_kauri = thisregion->threadtree;
			if ( curr_kauri != NULL ) {
				curr_kauri->range_rball_noclear_nosort_p3d( probesite, theseions, SearchRadiusSQR, neighbors3dm1 );
			}
		}
	}

	//##MK::implement possible iontype filtering here!

	for( auto it = neighbors3dm1.begin(); it != neighbors3dm1.end(); it++ ) { //un-pack and layout into de-interleaved array of xyz distance differences
		out->push_back( it->x - probesite.x );
		out->push_back( it->y - probesite.y );
		out->push_back( it->z - probesite.z );
	}
}


void fourierHdl::debug_epoch_profiling( vector<epoch_log> const & in, vector<epoch_prof> const & timing )
{
	/*
	cout << "DEBUG reporting how threads spent time in the epochs" << "\n";
	cout << "ThreadID;NThreads;Epoch;WallClock(s);NChkuns;Ionsum" << "\n";
	for( auto it = in.begin(); it != in.end(); it++ ) {
		cout << *it << "\n";
	}
	*/

	//generate table with epochs as rows and columns detailing what the threads were doing
	//demands that number of threads did not change
	if ( in.size() < 1 ) {
		cerr << "Epoch diary is empty!" << "\n"; return;
	}
	size_t expected_nt = static_cast<size_t>(in.at(0).nthreads);
	if ( (in.size() % expected_nt) != 0 ) {
		cerr << in.size() << "\n";
		cerr << expected_nt << "\n";
		cerr << (in.size() % expected_nt) << "\n";
		cerr << "Total number of logs is not an integer multiple of the expected number of threads so data are faulty or data are missing!" << "\n"; return;
	}
	for( auto it = in.begin(); it != in.end(); it++ ) {
		if ( static_cast<size_t>(it->nthreads) == expected_nt ) {
			continue;
		}
		else {
			cerr << "The total number of threads was not the same for all epochs" << "\n"; return;
		}
	}
	size_t nepochs = in.size() / expected_nt;
	if ( timing.size() != nepochs ) {
		cerr << "The number of epoch_prof timing steps is inconsistent with the thread profiling data!" << "\n"; return;
	}

	//##MK::suboptimal... one file per rank
	string fn = "PARAPROBE.Fourier.SimID." + to_string(ConfigShared::SimID) + ".Rank." + to_string(get_myrank()) + ".EpochThreadProfiling.csv";

	ofstream csvlog;
	csvlog.open(fn.c_str(), ofstream::out | ofstream::trunc);
	if (csvlog.is_open() == true) {
		//header
		csvlog << "Epoch;MPITotalWallclock";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "OMPWallClock";
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "OMPIonTotal";
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "OMPChunkTotal";
		csvlog << "\n";
		csvlog << "Epoch;MPITotalWallClock";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "Thread" << thr;
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "Thread" << thr;
		//csvlog << ";";
		for( unsigned int thr = MASTER; thr < expected_nt; thr++ )
			csvlog << ";" << "Thread" << thr;
		csvlog << "\n";

		for( size_t i = 0; i < in.size(); i += expected_nt ) {
			csvlog << in.at(i).epoch << ";" << timing.at(i/expected_nt).dt;
			for( size_t thr = MASTER; thr < expected_nt; thr++ )
				csvlog << ";" << in.at(i+thr).dt;
			//csvlog << ";";
			for( size_t thr = MASTER; thr < expected_nt; thr++ )
				csvlog << ";" << in.at(i+thr).ionsum;
			//csvlog << ";";
			for( size_t thr = MASTER; thr < expected_nt; thr++ )
				csvlog << ";" << in.at(i+thr).nchunks;
			csvlog << "\n";
		}

		csvlog.flush();
		csvlog.close();
		cout << "Rank " << get_myrank() << " successful generation of epoch profiling protocol file" << "\n";
	}
	else {
		cerr << "Unable to write process-local epoch profiling protocol file" << "\n";
	}
}

/*
void fourierHdl::execute_local_workpackage()
{
	cout << "Testing OpenMP/OpenACC direct Fourier transform" << "\n";

	double global_tic = MPI_Wtime();

	//initialize the two NVIDIA GPUs on the TALOS node on which I, as get_myrank() am currently running
	int n_gpus = 0; //0 - no gpus, 1 or 2 gpus
	int devId = -1;
	int devCount = 0;

	#ifdef UTILIZE_GPUS
		devId = -1;
		devCount = acc_get_num_devices(acc_device_nvidia);
		cout << "Rank " << get_myrank() << " NVIDIA accelerator GPU initialization devCount " << devCount << "\n";
		//##MK::non-portable, i.e. system-specific accelerator initialization
		//here exemplified for a TALOS node with 2x20core + 2xNVIDIA V100 accelerators/GPGPUs/"GPUs"
		if ( devCount > 0 && devCount < 3 ) {
			n_gpus = devCount;
			devId = 0;
		}
		if ( n_gpus == 2 ) {
			// creates a context on the GPUs
			//excludes initialization time from computations
			acc_set_device_num(devId, acc_device_nvidia);
			acc_init(acc_device_nvidia);
			acc_set_device_num(devId+1, acc_device_nvidia); //(devID+1)%devCount
			acc_init(acc_device_nvidia);
		}

		acc_set_device_num(devId, acc_device_nvidia);
		devId = acc_get_device_num(acc_device_nvidia);
		cout << "Rank " << get_myrank() << " running on GPU with device ID " << devId << "\n";
		if ( n_gpus == 2 ) {
			acc_set_device_num(devId+1, acc_device_nvidia);
			devId = acc_get_device_num(acc_device_nvidia);
			cout << "Rank " << get_myrank() << " running on GPU with device ID " << devId << "\n";
		}
	#endif

	cout << "Rank " << get_myrank() << " GPU and CPU initialized now processing..." << "\n";

	//the passing of individual ROI for each process onto the CPU/GPUs works as follows:
	//the CPU OpenMP thread team identifies the ions within each ROI
	//ROIs are group into packets, a packet to work on for the CPU and a packet to work for the GPUs
	//the CPUs process their package in parallel, each thread one material point, i.e. one DFT
	//the GPUs process the GPU packet
	//the GPU get a heuristic larger fraction of ROIs
	//we work through the pairs of CPU/GPU ROI packages until all ROIs of the MPI process are completed
	//we such work piece on a package we call an epoch

	int local_nmp = myworkload.size();
	cout << "Rank " << get_myrank() << " myworkload.size() " << local_nmp << "\n";

	//we need to initialize first buffers to store the ion positions from all ROIs per epoch
	vector<vector<dft_real>*> buffer_gpu;
	vector<vector<dft_real>*> buffer_cpu;
	vector<int> buffer_local_mpid_gpu; //buffer to store which specific ROIs are currently processed, this is handled by the CPU
	vector<int> buffer_local_mpid_cpu; //MK::DO NOT JUMP UP these local IDs with the global material point/ROI ids!
	//the local IDs live on the ID interval [0, myworkload.size() )] BUT
	//the global IDs live on the ID interval [ 0, mp2rk.size() ) !

	//we go parallel with the threads but orchestrate/guide them through their workplan
	//we need a parallel region to have the allocation of the buffers in the master
	//##MK::TO DO performance benchmark, if the master thread gets pinned to core 0 it has on average faster access
	//to the PCI control busses, therefore we let the master delegate and execute work on the CPUs, the master therefore
	//does not contribute in solving own Fourier transforms ##MK::TO BE DISCUSSED but dont underestimate tricky to get orchestrated
	//because we have here heterogeneous parallelism with device (GPU) and host (CPU)
	gpu_cpu_max_workload wl = gpu_cpu_max_workload();
	cout << "Rank " << get_myrank() << " chk_max_gpu/chk_max_cpu " << wl.chunk_max_gpu << ";" << wl.chunk_max_cpu << "\n";

	#pragma omp parallel shared(n_gpus,local_nmp,buffer_gpu,buffer_cpu,buffer_local_mpid_gpu,buffer_local_mpid_cpu,wl)
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		#pragma omp master
		{
			//MK::idea is if GPUS+CPUs are used, in each iteration nt-1 FT are solved on the CPU, and an empirically identified number per GPU
			//this number, ##MK here just chosen as 4, will minimize CPU vs GPU async wait time if GPU runtime per epoch is about equal as per thread

			wl = plan_max_per_epoch( n_gpus, nt, local_nmp );
			cout << "Rank " << get_myrank() << " MASTER " << mt << " chk_max_gpu/chk_max_cpu " << wl.chunk_max_gpu << ";" << wl.chunk_max_cpu << "\n";

			//thread team shared global address space buffers get populated by master
			//the reason for using a vector of vector of pointer rather than a vector of coordinates
			//is to collate raw pointers to different addresses, specifically pointer to different thread-local mem virtual mem sections
			if ( wl.chunk_max_gpu > 0 ) {
				buffer_gpu = vector<vector<dft_real>*>( wl.chunk_max_gpu, NULL );
				buffer_local_mpid_gpu = vector<int>( wl.chunk_max_gpu, INT32MI ); //flagged as negative IDs to enable consistency tests
			}
			if ( wl.chunk_max_cpu > 0 ) {
				buffer_cpu = vector<vector<dft_real>*>( wl.chunk_max_cpu, NULL );
				buffer_local_mpid_cpu = vector<int>( wl.chunk_max_cpu, INT32MI ); //flagged as negative IDs to enable consistency tests
			}
		} //master initialized buffer memory
	}

	//start processing epochs
	vector<epoch_log> epoch_diary;
	vector<epoch_prof> epoch_wallclock;
	int epoch = 0;
	int mympid = 0;
	while ( mympid < local_nmp )
	{
		double tic = MPI_Wtime();
		gpu_cpu_now_workload wn = gpu_cpu_now_workload();

		//we define an epoch as a single chunk of FT for ROIs processed, the epoch contains at most chunk_plan_gpu+chunk_plan_cpu material points
		#pragma omp parallel shared(n_gpus,epoch_diary,epoch,mympid,local_nmp,wl,wn,buffer_gpu,buffer_cpu,buffer_local_mpid_gpu,buffer_local_mpid_cpu)
		{
			int mt = omp_get_thread_num();
			int nt = omp_get_num_threads();
			//we keep a thread-local buffer of results to avoid having to synchronize after each ROI transform a write-back of
			//results to the globally shared container which would choke parallel execution performance
			vector<hklval> myresults_buffer;
			double mytic = omp_get_wtime();
			epoch_log mystatus = epoch_log( static_cast<unsigned int>(mt), static_cast<unsigned int>(nt) );

			//buffer keeps pointers to ion position data for ROI of material points
			//we store the ROIs for the FT on the GPUs at [0,chunk_plan_gpu+chunk_plan_cpu-2] and
			//the ROI for the FT on the CPUs at [chunk_plan_gpu+chunk_plan_cpu-1]

			//thread local pseudo random number generator (PRNG)
			//##MK::############read from this->myworkload.at(mympid) to get info about specific ROI
			//##MK::############used to instantiate test cases only
//			mt19937 mydice;
//			mydice.seed( -1000 * mt ); //##MK::likely a poor seed choice but for testing purposes okay

			//once initialized the thread team processes through its material points using the GPU as accelerators,
			//meanwhile other processes do the same but on different material points/ROIs,
			//thereby using triple parallelism (hybrid MPI/OpenMP + OpenACC)
			#pragma omp single
			{
				//only a single thread must compute this, hence implicitly pragma omp critically writing on wn
				wn = plan_now_per_epoch( n_gpus, nt, local_nmp, mympid, wl );

				int id = mympid; //at which ID on ID interval [0, myworkload.size() ) where we started the current epoch
				for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
					buffer_local_mpid_gpu.at(chk_gpu) = id + chk_gpu;
				}
				id = mympid + wn.chunk_now_gpu;
				for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
					buffer_local_mpid_cpu.at(chk_cpu) = id + chk_cpu;
				}
				//now we know which global material point IDs are processed by the CPU/GPU team

			} //we need a barrier to and use the implicit of the single construct, sync chunk_now... and master has no implicit one
			#pragma omp barrier

//			#pragma omp critical
//			{
//				cout << "Current epoch " << epoch << " on thread " << mt << " chunk_now_gpu " << wn.chunk_now_gpu << " chunk_now_cpu " << wn.chunk_now_cpu << "\n";
//			}

			//firstly, we let the threads (all of the team), populate the buffers for GPU and CPUs
			//##MK::this is also useful to avoid very high memory consumption
			//e.g. lets assume 1000 mat points, each taking a ROI with 5000 ions, would require storing 5k * 1k points,
			//using epoch only 5k * as many as material points per epoch which is << local_nmp
			for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
				int whom = chk_gpu % nt;
				if ( whom == mt ) {
					vector<dft_real>* mybuf = NULL;
					try {
						mybuf = new vector<dft_real>;
					}
					catch (bad_alloc &croak) {
						#pragma omp critical
						{
							cerr << "Allocation error for mybuf in thread " << mt << "\n";
						}
					}

					int mpid = buffer_local_mpid_gpu.at(chk_gpu);
					mp3d thisroi = myworkload.at(mpid);
					query_ions_within_roi( thisroi, mybuf );
					thisroi.nions = mybuf->size() / 3;
					myworkload.at(mpid) = thisroi; //no pragma omp critical, mpids are disjoint across thread team!
//					#pragma omp critical
//					{
//						cout << "Thread " << omp_get_thread_num() << " mpid " << mpid << " thisroi " << myworkload.at(mpid).x << ";" << myworkload.at(mpid).y << ";"
//								<< myworkload.at(mpid).z << ";" << myworkload.at(mpid).R << "\t\t" << myworkload.at(mpid).nions << "\n";
//					}
					//generate_debug_roi( mydice, mybuf );

					//no pragma omp critical, necessary, enforced collision free through whom == mt
					buffer_gpu.at(chk_gpu) = mybuf;
				}
			}

			//next, the thread team populates the CPU buffer
			for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
				int whom = chk_cpu % nt;
				if ( whom == mt ) {
					vector<dft_real>* mybuf = NULL;
					try {
						mybuf = new vector<dft_real>;
					}
					catch (bad_alloc &croak) {
						#pragma omp critical
						{
							cerr << "Allocation error for mybuf in thread " << mt << "\n";
						}
					}

					int mpid = buffer_local_mpid_cpu.at(chk_cpu);
					mp3d thisroi = myworkload.at(mpid);
					query_ions_within_roi( thisroi, mybuf );
					thisroi.nions = mybuf->size() / 3;
					myworkload.at(mpid) = thisroi; //no pragma omp critical, mpids are disjoint across thread team!
//					#pragma omp critical
//					{
//						cout << "Thread " << omp_get_thread_num() << " mpid " << mpid << " thisroi " << myworkload.at(mpid).x << ";" << myworkload.at(mpid).y << ";"
//								<< myworkload.at(mpid).z << ";" << myworkload.at(mpid).R << "\t\t" << myworkload.at(mpid).nions << "\n";
//					}
					//generate_debug_roi( mydice, mybuf );

					//no pragma omp critical, necessary, enforced collision free through whom == mt
					buffer_cpu.at(chk_cpu) = mybuf;
				}
			} //buffer for all chunks of current epoch populated

			//process the MPI-process local ROI material point IDs critical and master only, threads get to know values because ID fields are shared across team
			#pragma omp master
			{

			} //barrier needed, as pragma omp master has no implicit one
			//MK::without spending days on debugging and twisting your mind, assume for now that the
			//barrier is necessary, because all buffer (cpus, gpus) have to be filled, before asynchronous processing on cpu/gpus starts...
			#pragma omp barrier

//			//##MK::BEGIN DEBUGGING, can later be deleted
//			#pragma omp master
//			{
//				cout << "Buffer for all chunks of current epoch " << epoch << " populated" << "\n";
//				size_t ntotal = 0;
//				size_t nactive = 0;
//				for(size_t ii = 0; ii < buffer_gpu.size(); ii++) {
//					if ( buffer_gpu.at(ii) != NULL ) { //allocated
//						if ( buffer_gpu.at(ii)->size() > 0 ) { //and at least one ion within ROI
//							nactive++;
//							ntotal += buffer_gpu.at(ii)->size();
//						}
//					}
//				}
//				for(size_t ii = 0; ii < buffer_cpu.size(); ii++) {
//					if ( buffer_cpu.at(ii) != NULL ) { //allocated
//						if ( buffer_cpu.at(ii)->size() > 0 ) { //and at least one ion within ROI
//							nactive++;
//							ntotal += buffer_cpu.at(ii)->size();
//						}
//					}
//				}
//				cout << "Average number of ions per point " << ntotal / (3*nactive) << "\n"; //3 because three coordinates values per ion
//			} //no implicit barrier
//			#pragma omp barrier
//			//##MK::END DEBUGGING


			//handle heterogeneously and possibly asynchronously executed direct Fourier transforms of current epoch on the GPUs through MASTER OMP_NESTED=true
#ifdef UTILIZE_GPUS
			if ( n_gpus > 0 ) { //if there are accelerators

				if ( mt == MASTER ) { //master directs scheduling of async GPU jobs
//					double mmytic = omp_get_wtime();
//					cout << "Current epoch " << epoch << " on thread MASTER processing GPU part" << "\n";

					//define 1d Fourier grid positions on [-2.0,+2.0]
					int ni = ConfigFourier::FourierGridResolution;
					int nij = SQR(ni);
					int nijk = CUBE(ni);
					//dft_real* IV = (dft_real*) malloc( ni*sizeof(dft_real) ); //restrict ... (dft_real*)
					dft_real* IV = new dft_real[ni];
					//dft_real* const IV = malloc( ni*sizeof(dft_real) );

					dft_real bound = 2.0;
					dft_real start = -bound; //-1.0;
					dft_real stop = +bound; //+1.0;
					dft_real num = static_cast<dft_real>(ni-1);
					dft_real step = (stop-start)/num;
					for( int i = 0; i < ni; i++ ) {
						IV[i] = TWO_PI * (start + static_cast<dft_real>(i)*step);
					}

					for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu;         ) {
//						double gtic = omp_get_wtime();

						//prepare processing on GPU0 and GPU1
						//efficiently solving the max, index search is involved, see here
						//https://www.pgroup.com/userforum/viewtopic.php?t=5096
						dft_real* A0 = NULL; ///restrict
						dft_real* A1 = NULL;
						dft_real* Fmagn0 = NULL;
						dft_real* Fmagn1 = NULL;
						//https://www.pgroup.com/resources/docs/18.7/x86/pgi-user-guide/index.htm

						//prepare asynchronous processing on GPU0 and possibly GPU1
						int thisone0 = chk_gpu+0;
						int thisone1 = chk_gpu+1;
						int n0 = 0;
						int n1 = 0;

						bool useboth = false;
						if ( n_gpus == 2 && (chk_gpu+1) < wn.chunk_now_gpu) {
							useboth = true;
						}

						bool gpu0valid = false;
						if ( buffer_gpu.at(thisone0) != NULL ) {
							if ( buffer_gpu.at(thisone0)->size() > 0 ) {
								gpu0valid = true;
							}
						}
						bool gpu1valid = false;
						if ( useboth == true ) {
							if ( buffer_gpu.at(thisone1) != NULL ) {
								if ( buffer_gpu.at(thisone1)->size() > 0 ) {
									gpu1valid = true;
								}
							}
						}

						//master delegates work on the GPUs, the master itself does not process work but does host interaction
						if ( gpu0valid == true ) {
							n0 = buffer_gpu.at(thisone0)->size();
							//A0 = (dft_real*) malloc( n0 * sizeof(dft_real) );
							A0 = new dft_real[n0];
							//if ( A0 == NULL ) {
							//	cerr << "Rank " << get_myrank() << " MASTER thread A0 allocation failed!" << "\n";
							//}
							//dft_real* const A0 = malloc( n0 * sizeof(dft_real) );
							//Fmagn0 = (dft_real*) calloc( nijk*1, sizeof(dft_real) );
							Fmagn0 = new dft_real[nijk];
							//if ( Fmagn0 == NULL ) {
							//	cerr << "Rank " << get_myrank() << " MASTER thread Fmagn0 allocation failed!" << "\n";
							//}
							for( int i = 0; i < nijk; i++ )
								Fmagn0[i] = 0.f;
							//dft_real* const Fmagn0 = calloc( nijk, sizeof(dft_real) );
							vector<dft_real> & here0 = *(buffer_gpu[thisone0]);
							for( int i = 0; i < n0; i++ )
								A0[i] = here0[i];

							acc_set_device_num(0, acc_device_nvidia);
							//do all on GPU0 ASYNCHRONOUSLY
							#pragma acc data copy(Fmagn0[0:nijk]) async(1)
							{
								int ijk, iion;
								//#pragma acc enter data copyin(Fmagn0[0:nijk]) async(1)
								#pragma acc parallel loop copyin(A0[0:n0],IV[0:ni]) present(Fmagn0[0:nijk]) async(1) private(iion,n0)
								for( ijk = 0; ijk < nijk; ijk++) {
									int i = ijk % ni;
									int j = (ijk / nij) % ni;
									int k = ijk / nij;
									dft_real fr = 0.0;
									dft_real fi = 0.0;
									for( iion = 0; iion < n0; iion += 3 ) {
										dft_real a = A0[iion+0]*IV[i] + A0[iion+1]*IV[j] + A0[iion+2]*IV[k];
										fr += cos(a);
										fi += sin(a);
									}
									Fmagn0[ijk] = sqrt( fr*fr + fi*fi );
								} //Fmagn0 continues to life happily on GPU0
								//#pragma acc update host(Fmagn0[0:nijk]) async(1)
								//#pragma acc exit data delete(Fmagn0[0:nijk]) async(1)
							}
						}

						//##MK::maybe bind the second, i.e. GPU1, through process 1 but this requires to handle the corner case
						//when there is only one master thread and will lock potential deadtime on thread=1
						if ( gpu1valid == true ) {
							n1 = buffer_gpu[thisone1]->size();
							//A1 = (dft_real*) malloc( n1 * sizeof(dft_real) ); //(dft_real*)
							A1 = new dft_real[n1];
							//if ( A1 == NULL ) {
							//	cerr << "Rank " << get_myrank() << " MASTER thread A0 allocation failed!" << "\n";
							//}
							//dft_real* const A1 = malloc( n1 * sizeof(dft_real));
							//Fmagn1 = (dft_real*) calloc( nijk*1, sizeof(dft_real) );
							Fmagn1 = new dft_real[nijk];
							//if ( Fmagn1 == NULL ) {
							//	cerr << "Rank " << get_myrank() << " MASTER thread Fmagn0 allocation failed!" << "\n";
							//}
							for ( int i = 0; i < nijk; i++ )
								Fmagn1[i] = 0.f;
							//dft_real* const Fmagn1 = calloc( nijk, sizeof(dft_real) );
							vector<dft_real> & here1 = *(buffer_gpu[thisone1]);
							for( int i = 0; i < n1; i++ )
								A1[i] = here1[i];

							acc_set_device_num(1, acc_device_nvidia);

							//do all on GPU1 ASYNCHRONOUSLY
							#pragma acc data copy(Fmagn1[0:nijk]) async(2)
							{
								int ijk, iion;
								//#pragma acc enter data copyin(Fmagn1[0:nijk]) async(2)
								#pragma acc parallel loop copyin(A1[0:n1],IV[0:ni]) present(Fmagn1[0:nijk]) async(2) private(iion,n1)
								for( ijk = 0; ijk < nijk; ijk++) {
									int i = ijk % ni;
									int j = (ijk / nij) % ni;
									int k = ijk / nij;
									dft_real fr = 0.0;
									dft_real fi = 0.0;
									for( iion = 0; iion < n1; iion += 3 ) {
										dft_real a = A1[iion+0]*IV[i] + A1[iion+1]*IV[j] + A1[iion+2]*IV[k];
										fr += cos(a);
										fi += sin(a);
									}
									Fmagn1[ijk] = sqrt( fr*fr + fi*fi );
								} //Fmagn1 continues to life happily on GPU1
								//#pragma acc update host(Fmagn1[0:nijk]) async(2)
								//#pragma acc exit data delete(Fmagn1[0:nijk]) async(2)
							}
						}

						//wait for both GPUs to complete all asynchronously executed tasks i.e. here async queues default, 1, and 2
						#pragma acc wait
						//only because now we know that both GPUs have populated/completed their asynchronous calculations we can search for the maximum value on Fmagn0/1

						hklval max0 = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
						if ( gpu0valid == true ) {
							for( int ijk = 0; ijk < nijk; ijk++ ) {
								if ( Fmagn0[ijk] < max0.val )
									continue;
								else
									max0 = hklval( WAIT_UNTIL_GLOBAL_EXTREMUM_FOUND, ijk, Fmagn0[ijk] );
							}
							max0.mpid = buffer_local_mpid_gpu.at(thisone0);
							//free(A0);
							delete [] A0; A0 = NULL;
							//free(Fmagn0);
							delete [] Fmagn0; Fmagn0 = NULL;
						}
						//this is a process-local material point ID !, i.e. there are possibly as many 0-th local material points as there are MPI processes
						//use myworkload.at(max0.mpid).mpid to resolve the global material point ID
						//if max0.mpid is flagged with NORESULTS_FOR_THIS_MATERIALPOINT we can filter this out!
						myresults_buffer.push_back( max0 );

						hklval max1 = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
						if ( gpu1valid == true ) {
							for( int ijk = 0; ijk < nijk; ijk++ ) {
								if ( Fmagn1[ijk] < max1.val )
									continue;
								else
									max1 = hklval( WAIT_UNTIL_GLOBAL_EXTREMUM_FOUND, ijk, Fmagn1[ijk] );
							}
							max1.mpid = buffer_local_mpid_gpu.at(thisone1);
							//free(A1);
							delete [] A1; A1 = NULL;
							//free(Fmagn1);
							delete [] Fmagn1; Fmagn1 = NULL;
						}
						myresults_buffer.push_back( max1 );

//						//##MK::BEGIN DEBUG
//						#pragma omp critical
//						{
//							cout << "Current epoch " << epoch << " on thread MASTER after pragma acc wait" << "\n";
//							cout << "max0 " << max0 << "\n";
//							if ( useboth == true )
//								cout << "max1 " << max1 << "\n";
//						}
//						double gtoc = omp_get_wtime();

//						#pragma omp critical
//						{
//							if ( useboth == true )
//								cout << "Current epoch " << epoch << " on thread MASTER chk_gpu+0, chk_gpu+1 " << chk_gpu+0 << ";" << chk_gpu+1 << " two FT took " << (gtoc-gtic) << " seconds" << "\n";
//							else
//								cout << "Current epoch " << epoch << " on thread MASTER chk_gpu+0, chk_gpu+1 " << chk_gpu+0 << ";" << "xxxx" << " one FT took " << (gtoc-gtic) << " seconds" << "\n";
//						}
//						//##MK::END DEBUG

						if ( useboth == true ) {
							chk_gpu += 2;
							mystatus.nchunks += 2;
						}
						else {
							chk_gpu += 1;
							mystatus.nchunks += 1;
						}

						mystatus.ionsum = mystatus.ionsum + (n0 + n1)/3;

					} //process the next singleton or pair of GPU workload

					//free(IV);
					delete [] IV; IV = NULL;

//					double mmytoc = omp_get_wtime();
//					#pragma omp critical
//					{
//						cout << "Current epoch " << epoch << " on thread MASTER async with " << wn.chunk_now_gpu << " on GPUs took " << (mmytoc-mmytic) << " seconds" << "\n";
//					}
				} //master done with its work
				else { //remainder non-master threads work asychronously on own direct Fourier transform
//					double mmytic = omp_get_wtime();
					//##MK::for now we can with this model execute only one chk on the CPUs per epoch
					//because as master execution path is different than of non-master threads we cannot invoke a #pragma omp barrier here
					//MK::we could dispatch as chks for each accelerator and proceed with OMP threading but typically accelerators are
					//faster and hence could process multiple chks while the thread team hasnt even complete one
					if ( (mt-1) < wn.chunk_now_cpu ) {
						//MK::the construct mt-1 works because the MASTER thread never enters this section so mt >= 1
						//MK::mt-1 possible because this section is only executed by non-master thread if such exist

						hklval cpumt1res = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
						size_t thisone = mt-1; //all threads contribute, if enough points left
						if ( buffer_cpu.at(thisone) != NULL ) {
							if ( buffer_cpu.at(thisone)->size() > 0 ) {

								dft_cpu solver = dft_cpu( ConfigFourier::FourierGridResolution );

								cpumt1res = solver.execute( *(buffer_cpu.at(thisone)) );

								cpumt1res.mpid = buffer_local_mpid_cpu.at(thisone); //process-local ID

								mystatus.ionsum = mystatus.ionsum + (buffer_cpu.at(thisone)->size() / 3);
								mystatus.nchunks++;
							}
						}
						myresults_buffer.push_back( cpumt1res );

//						double mmytoc = omp_get_wtime();
//						#pragma omp critical
//						{
//							cout << "Current epoch " << epoch << " on thread/chk " << mt << " FT took " << (mmytoc-mmytic) << " seconds" << "\n";
//						}

					} //non-master threads done with processing the only cpu part of the current epoch
				} //non-master thread part

			} //handled case of asynchronous GPU + CPU parallel execution
			else {
#endif
				//handle multithreaded fallback in case there are no accelerators
//				#pragma omp master
//				{
//					cout << "Multithreaded fallback current epoch chunk_now_cpu " << wn.chunk_now_cpu << "\n";
//				}
//				double mmytic = omp_get_wtime();
				if ( mt < wn.chunk_now_cpu ) { //possibly all threads contribute, if enough points left

					hklval cpumtres = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
					size_t thisone = mt; //all threads contribute, if enough points left
					if ( buffer_cpu.at(thisone) != NULL ) {
						if ( buffer_cpu.at(thisone)->size() > 0 ) { //there are coordinate values

							dft_cpu solver = dft_cpu( ConfigFourier::FourierGridResolution );

							cpumtres = solver.execute( *(buffer_cpu.at(thisone)) );

							cpumtres.mpid = buffer_local_mpid_cpu.at(thisone); //process-local ID

							mystatus.ionsum = mystatus.ionsum + (buffer_cpu.at(thisone)->size() / 3);
							mystatus.nchunks++;
						}
					}
					myresults_buffer.push_back( cpumtres );
				}

//				double mmytoc = omp_get_wtime();
//				#pragma omp critical
//				{
//					cout << "Current epoch " << epoch << " on thread/chk " << mt << " FT took " << (mmytoc-mmytic) << " seconds" << "\n";
//				}

#ifdef UTILIZE_GPUS
			} //handled case of multithreaded fallback
#endif

			//threads dump their results resulting in only as many criticals as threads rather than individual transforms per epoch
			#pragma omp critical
			{
				for( auto kyvl = myresults_buffer.begin(); kyvl != myresults_buffer.end(); kyvl++ ) {
					if ( kyvl->mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) {
						myhklval.at(kyvl->mpid) = *kyvl;
					}
				}
				myresults_buffer = vector<hklval>();
			}

			mystatus.epoch = epoch;
			double mytoc = omp_get_wtime();
			mystatus.dt = mytoc-mytic;
			#pragma omp critical
			{
				epoch_diary.push_back( mystatus );
			}

			//a barrier is necessary, because we dont want to avoid any memory invalidation of async stuff possibly still going on ?
			#pragma omp barrier

			//clear the buffer and reinitialize for next use
			#pragma omp master
			{
//				cout << "About to delete data buffers for current epoch " << epoch << "\n";
				for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
					if ( buffer_gpu.at(chk_gpu) != NULL ) {
						delete buffer_gpu.at(chk_gpu);
						buffer_gpu.at(chk_gpu) = NULL; //necessary to prepare for reusage
					}
				}
				for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
					if ( buffer_cpu.at(chk_cpu) != NULL ) {
						delete buffer_cpu.at(chk_cpu);
						buffer_cpu.at(chk_cpu) = NULL; //necessary to prepare for reusage
					}
				}
			} //no implicit barrier as were about to exit parallel region with implicit barrier anyway

		} //end of parallel region within the while loop for the current epoch

		mympid = mympid + wn.chunk_now_gpu + wn.chunk_now_cpu; //advance to begin a new epoch

		double toc = MPI_Wtime();
		epoch_wallclock.push_back( epoch_prof( epoch, toc-tic ) );

		cout << "Current epoch " << epoch << " with a total of " << wn.chunk_now_gpu + wn.chunk_now_cpu << " completed took " << (toc-tic) << " seconds" << "\n";
		epoch++;
	} //end of the while loop

	double global_toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "QueryROIsAndPerformDirectFourierTransforms", APT_XX, APT_IS_PAR, mm, global_tic, global_toc);

	cout << "The test program took " << (global_toc-global_tic) << " seconds" << "\n";

	sort( epoch_diary.begin(), epoch_diary.end(), SortEpochLogAsc );

	debug_epoch_profiling( epoch_diary, epoch_wallclock );
}
*/


void fourierHdl::execute_local_workpackage2()
{
	cout << "Rank " << get_myrank() << " performing OpenMP/OpenACC direct Fourier transforms..." << "\n";

	double global_tic = MPI_Wtime();

	//initialize one GPUthe two NVIDIA GPUs on the TALOS node on which I, as get_myrank() am currently running
	int n_gpus = 0; //0 - no gpus, 1 or 2 gpus
	int n_mygpus = 0; //either 0, or 1, rank either has a gpu and can use it or not
	int devId = -1;
	int devCount = 0;

#ifdef UTILIZE_GPUS
	if ( ConfigFourier::GPUsPerNode > 0 ) {
		devId = -1;
		devCount = acc_get_num_devices(acc_device_nvidia);
		if ( devCount > 0  ) {
			n_gpus = devCount;
			n_mygpus = 1;
			cout << "Rank " << get_myrank() << " NVIDIA accelerator GPU initialization devCount " << devCount << "\n";
		}
		else {
			n_mygpus = 0;
			cout << "Rank " << get_myrank() << " no accelerators existent " << devCount << "\n";
		}
	}
	else

	//##MK::not necessarily portable, here system-specific accelerator initialization exemplified
	//for the MPCDF system TALOS, specifically its computing nodes with 2x20core + 2xNVIDIA V100 accelerators/GPGPUs/"GPUs"
	if ( n_gpus > 0 ) {
		//MK::assume user has instructed model with two processes per node each getting one GPUs
		devId = get_myrank() % ConfigFourier::GPUsPerNode;
		//cout << "Rank " << get_myrank() << " my GPU devID is " << devId << "\n";
		// creates a context on the GPUs
		//excludes initialization time from computations
		acc_set_device_num(devId, acc_device_nvidia);
		acc_init(acc_device_nvidia);
cout << "Rank " << get_myrank() << " running on GPU with device ID " << devId << "\n";
	}
	else {
		cerr << "Rank " << get_myrank() << " will use multi-threaded CPU fallback n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";
	}
#endif
	//##cout <<  report use fallback multithreaded CPU only
	cout << "Rank " << get_myrank() << " GPU and CPU initialized now processing... n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";

	//the passing of individual ROIs for each process onto the CPU/GPUs works as follows:
	//the CPU OpenMP thread team identifies the ions within each ROI
	//ROIs are group into packets (epoch), a packet to work on for the CPU and a packet to work for the GPUs
	//the CPUs process their package in parallel, each thread one material point, i.e. one DFT
	//the GPUs process the GPU packet
	//the GPU get a heuristic fraction of ROIs: when a CPU processes 1 ROI the GPU processes ConfigFourier::GPUWorkload ROIs at the same time
	//##MK::to get more fine tuning in the future allow CPUs to have more than one package
	//we work through the pairs of CPU/GPU ROI packages until all ROIs of the MPI process are completed

	int local_nmp = myworkload.size();
	cout << "Rank " << get_myrank() << " myworkload.size() " << local_nmp << "\n";

	//we need to initialize buffers first to store the ion positions from all ROIs per epoch
	vector<vector<dft_real>*> buffer_gpu;
	vector<vector<dft_real>*> buffer_cpu;
	vector<int> buffer_local_mpid_gpu; //buffer to store which specific ROIs are currently processed, this is handled by the CPU
	vector<int> buffer_local_mpid_cpu; //MK::do not jumble up these local ROI IDs with the global material point/ROI ids!
	//the local IDs live on the ID interval [0, myworkload.size() )] BUT
	//the global IDs live on the ID interval [ 0, mp2rk.size() ) !

	//we go parallel with the threads but orchestrate/guide them through their workplan
	//we need a parallel region to have the allocation of the buffers in the master
	//##MK::TO DO performance benchmark, if the master thread gets pinned to core 0 it has on average faster access
	//to the PCI control busses, therefore we let the master thread delegate and execute work on the CPUs, the master therefore
	//does not contribute in solving own Fourier transforms ##MK::TO BE DISCUSSED but dont underestimate might get  tricky to get orchestrated
	//otherwise because we have here heterogeneous parallelism with device (GPU) and host (CPU) + asynchronous execution on the device
	//we call the gpu for now the device and the cpu the host

	gpu_cpu_max_workload wl = gpu_cpu_max_workload();
	//cout << "Rank " << get_myrank() << " chk_max_gpu/chk_max_cpu " << wl.chunk_max_gpu << ";" << wl.chunk_max_cpu << "\n";

	#pragma omp parallel shared(n_gpus,n_mygpus,local_nmp,buffer_gpu,buffer_cpu,buffer_local_mpid_gpu,buffer_local_mpid_cpu,wl)
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		#pragma omp master
		{
			//there is always a master thread
			//MK::idea is if GPUS+CPUs are used, in each iteration nt-1 FT are solved on the CPU, and an empirically identified number per GPU
			//this number, ##MK here just chosen as 4, will minimize CPU vs GPU async wait time if GPU runtime per epoch is about equal as per thread
			wl = plan_max_per_epoch( n_mygpus, nt, local_nmp );
			cout << "Rank " << get_myrank() << " MASTER " << mt << " chk_max_gpu/chk_max_cpu " << wl.chunk_max_gpu << ";" << wl.chunk_max_cpu << "\n";

			//thread team shared global address space buffers get populated by master
			//the reason for using a vector of vector of pointer rather than a vector of coordinates
			//is to collate raw pointers to different addresses, specifically pointer to different thread-local mem virtual mem sections
			if ( wl.chunk_max_gpu > 0 ) {
				buffer_gpu = vector<vector<dft_real>*>( wl.chunk_max_gpu, NULL );
				buffer_local_mpid_gpu = vector<int>( wl.chunk_max_gpu, INT32MI ); //flagged as negative IDs to enable consistency tests
			}
			if ( wl.chunk_max_cpu > 0 ) {
				buffer_cpu = vector<vector<dft_real>*>( wl.chunk_max_cpu, NULL );
				buffer_local_mpid_cpu = vector<int>( wl.chunk_max_cpu, INT32MI ); //flagged as negative IDs to enable consistency tests
			}
		} //master initialized buffer memory
	} //end of parallel region

	//start processing epochs
	vector<epoch_log> epoch_diary;
	vector<epoch_prof> epoch_wallclock;
	int epoch = 0;
	int mympid = 0;
	int ni = ConfigFourier::FourierGridResolution;
	while ( mympid < local_nmp )
	{
		double tic = MPI_Wtime();
		gpu_cpu_now_workload wn = gpu_cpu_now_workload();

		//we define an epoch as a single chunk of FT for ROIs processed, the epoch contains at most chunk_plan_gpu+chunk_plan_cpu material points
		#pragma omp parallel shared(n_gpus,n_mygpus,epoch_diary,epoch,mympid,local_nmp,wl,wn,buffer_gpu,buffer_cpu,buffer_local_mpid_gpu,buffer_local_mpid_cpu,ni)
		{
			int mt = omp_get_thread_num();
			int nt = omp_get_num_threads();
			//we keep a thread-local buffer of results to avoid having to synchronize after each ROI transform a write-back of
			//results to the globally shared container which would choke parallel execution performance
			vector<hklval> myresults_buffer;
			double mytic = omp_get_wtime();
			epoch_log mystatus = epoch_log( static_cast<unsigned int>(mt), static_cast<unsigned int>(nt) );

			//buffer keeps pointers to ion position data for ROI of material points
			//we store the ROIs for the FT on the GPUs at [0,chunk_plan_gpu+chunk_plan_cpu-2] and
			//the ROI for the FT on the CPUs at [chunk_plan_gpu+chunk_plan_cpu-1]

			//once initialized the thread team processes through its material points using the GPU as accelerators,
			//meanwhile other processes do the same but on different material points/ROIs,
			//thereby using triple parallelism (hybrid MPI/OpenMP + OpenACC)
			#pragma omp single
			{
				//only a single thread must compute this, hence implicitly pragma omp critically writing on wn
				wn = plan_now_per_epoch( n_mygpus, nt, local_nmp, mympid, wl );

				int id = mympid; //at which ID on ID interval [0, myworkload.size() ) where we started the current epoch
				for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
					buffer_local_mpid_gpu.at(chk_gpu) = id + chk_gpu;
				}
				id = mympid + wn.chunk_now_gpu;
				for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
					buffer_local_mpid_cpu.at(chk_cpu) = id + chk_cpu;
				}
				//now we know which global material point IDs are processed by the CPU/GPU team

			}
			//we need a barrier to assure that all threads see change of wn
			#pragma omp barrier

			#pragma omp master
			{
				cout << "Current epoch " << epoch << " on thread " << mt << " chunk_now_gpu " << wn.chunk_now_gpu << " chunk_now_cpu " << wn.chunk_now_cpu << "\n";
			}

			//firstly, we let the threads (all of the team), populate the buffers for GPU and CPUs
			//the concept of epochs is useful to avoid a very high memory consumption for storing ROI population with ions
			//e.g. lets assume 1000 mat points per process, each ROI having 5000 ions, this would require storing 5k * 1k points,
			//using epochs we need to buffer only 5k times as many as ROIs as processed per epoch which is typically << local_nmp
			for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
				int whom = chk_gpu % nt;
				if ( whom == mt ) { //threads share the allocation and querying load
					vector<dft_real>* mybuf = NULL;
					mybuf = new vector<dft_real>;
					if ( mybuf != NULL ) {
						int mpid = buffer_local_mpid_gpu.at(chk_gpu);
						mp3d thisroi = myworkload.at(mpid);

						query_ions_within_roi( thisroi, mybuf );

						thisroi.nions = mybuf->size() / 3;
						//##MK::false sharing?
						myworkload.at(mpid) = thisroi; //no pragma omp critical, mpids are disjoint across the thread team!
						/*#pragma omp critical
						{
							cout << "Thread " << omp_get_thread_num() << " mpid " << mpid << " thisroi " << myworkload.at(mpid).x << ";" << myworkload.at(mpid).y << ";"
									<< myworkload.at(mpid).z << ";" << myworkload.at(mpid).R << "\t\t" << myworkload.at(mpid).nions << "\n";
						}*/
						buffer_gpu.at(chk_gpu) = mybuf; //no pragma omp critical necessary, enforced collision free through whom == mt
					}
					else {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " allocation error for GPU mybuf!" << "\n";
						}
					}
				} //mt is done
			} //done with filling in GPU work

			//next, the thread team populates the CPU buffer
			for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
				int whom = chk_cpu % nt;
				if ( whom == mt ) {
					vector<dft_real>* mybuf = NULL;
					mybuf = new vector<dft_real>;
					if ( mybuf != NULL ) {
						int mpid = buffer_local_mpid_cpu.at(chk_cpu);
						mp3d thisroi = myworkload.at(mpid);

						query_ions_within_roi( thisroi, mybuf );

						thisroi.nions = mybuf->size() / 3;
						myworkload.at(mpid) = thisroi; //no pragma omp critical, mpids are disjoint across thread team!
						/*#pragma omp critical
						{
							cout << "Thread " << omp_get_thread_num() << " mpid " << mpid << " thisroi " << myworkload.at(mpid).x << ";" << myworkload.at(mpid).y << ";"
									<< myworkload.at(mpid).z << ";" << myworkload.at(mpid).R << "\t\t" << myworkload.at(mpid).nions << "\n";
						}*/
						buffer_cpu.at(chk_cpu) = mybuf;
					}
					else {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " allocation error for CPU mybuf!" << "\n";
						}
					}
				} //mt done
			} //done with filling CPU buffers

			//process the MPI-process local ROI material point IDs critical and master only, threads get to know values because ID fields are shared across team
			//barrier is necessary, because all buffer (cpus, gpus) have to be filled, before asynchronous processing on cpu/gpus starts...
			#pragma omp barrier

			/*//##MK::BEGIN DEBUGGING, can later be deleted
			#pragma omp master
			{
				cout << "Buffer for all chunks of current epoch " << epoch << " populated" << "\n";
				size_t ntotal = 0;
				size_t nactive = 0;
				for(size_t ii = 0; ii < buffer_gpu.size(); ii++) {
					if ( buffer_gpu.at(ii) != NULL ) { //allocated
						if ( buffer_gpu.at(ii)->size() > 0 ) { //and at least one ion within ROI
							nactive++;
							ntotal += buffer_gpu.at(ii)->size();
						}
					}
				}
				for(size_t ii = 0; ii < buffer_cpu.size(); ii++) {
					if ( buffer_cpu.at(ii) != NULL ) { //allocated
						if ( buffer_cpu.at(ii)->size() > 0 ) { //and at least one ion within ROI
							nactive++;
							ntotal += buffer_cpu.at(ii)->size();
						}
					}
				}
				cout << "Average number of ions per point " << ntotal / (3*nactive) << "\n"; //3 because three coordinates values per ion
			} //no implicit barrier
			#pragma omp barrier
			//##MK::END DEBUGGING*/

			//handle heterogeneously and possibly asynchronously executed direct Fourier transforms of current epoch on the GPUs through MASTER OMP_NESTED=true
			if ( n_mygpus > 0 ) { //if there are accelerators...
#ifdef UTILIZE_GPUS
				if ( mt == MASTER ) { //...the master thread always schedules work to GPUs only
					//we have only one GPU per process
					/*double mmytic = omp_get_wtime();
					cout << "Current epoch " << epoch << " on thread MASTER processing GPU part" << "\n";*/

					//define 1d Fourier grid positions on [-2.0,+2.0]
					int nij = SQR(ni);
					int nijk = CUBE(ni);
					//dft_real* IV = (dft_real*) malloc( ni*sizeof(dft_real) ); //restrict ... (dft_real*)
					dft_real* IV = new dft_real[ni];
					//dft_real* const IV = malloc( ni*sizeof(dft_real) );

					dft_real bound = 2.0;
					dft_real start = -bound; //-1.0;
					dft_real stop = +bound; //+1.0;
					dft_real num = static_cast<dft_real>(ni-1);
					dft_real step = (stop-start)/num;
					for( int i = 0; i < ni; i++ ) {
						IV[i] = TWO_PI * (start + static_cast<dft_real>(i)*step);
					}

					for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu;         ) {
						//prepare processing on the process's GPU devId

						//efficiently solving the max, index search is involved, for discussion see here
						//https://www.pgroup.com/userforum/viewtopic.php?t=5096
						dft_real* A0 = NULL; ///restrict
						dft_real* Fmagn0 = NULL;
						//https://www.pgroup.com/resources/docs/18.7/x86/pgi-user-guide/index.htm

						//prepare asynchronous processing on GPU0
						int n0 = 0;
						bool gpuvalid = false;
						if ( buffer_gpu.at(chk_gpu) != NULL ) {
							if ( buffer_gpu.at(chk_gpu)->size() >= 3 ) { //at least one ion within ROI!
								gpuvalid = true;
							}
						}

						//master delegates work on the GPUs, the master itself does not process work but does host interaction
						if ( gpuvalid == true ) {
							n0 = buffer_gpu.at(chk_gpu)->size();
							//A0 = (dft_real*) malloc( n0 * sizeof(dft_real) );
							A0 = new dft_real[n0];
							//dft_real* const A0 = malloc( n0 * sizeof(dft_real) );
							//Fmagn0 = (dft_real*) calloc( nijk*1, sizeof(dft_real) );
							Fmagn0 = new dft_real[nijk];
							for( int i = 0; i < nijk; i++ ) {
								Fmagn0[i] = 0.f;
							}
							//dft_real* const Fmagn0 = calloc( nijk, sizeof(dft_real) );
							vector<dft_real> & here0 = *(buffer_gpu[chk_gpu]);
							for( int i = 0; i < n0; i++ ) { //fill buffer for the device first with values on the host
								A0[i] = here0[i];
							}

							//acc_set_device_num(devId, acc_device_nvidia);
							//do all on GPU0 ASYNCHRONOUSLY, use the default async queue because we use only one GPU
							//create a Fmagn0 on the device
							#pragma acc data copy(Fmagn0[0:nijk]) async
							{
								int ijk, iion;
								//#pragma acc enter data copyin(Fmagn0[0:nijk]) async(1)
								#pragma acc parallel loop copyin(A0[0:n0],IV[0:ni]) present(Fmagn0[0:nijk]) async private(iion,n0)
								for( ijk = 0; ijk < nijk; ijk++) {
									int i = ijk % ni;
									int j = (ijk / nij) % ni;
									int k = ijk / nij;
									dft_real fr = 0.0;
									dft_real fi = 0.0;
									for( iion = 0; iion < n0; iion += 3 ) {
										dft_real a = A0[iion+0]*IV[i] + A0[iion+1]*IV[j] + A0[iion+2]*IV[k];
										fr += cos(a);
										fi += sin(a);
									}
									Fmagn0[ijk] = sqrt( fr*fr + fi*fi );
								} //Fmagn0 continues to live happily on GPU0
								//#pragma acc update host(Fmagn0[0:nijk]) async(1)
								//#pragma acc exit data delete(Fmagn0[0:nijk]) async(1)
							}
							//destroyed Fmagn0 on the device
							//pragma acc data copy allocates memory on GPU and copies data from host to GPU when entering
							//the region and copies the data to the host when exiting region
						}

						//necessary, because once the host has dispatched work to the GPU
						//it could do own work ##MK:: host needs to wait for the GPU to complete all asynchronously executed tasks on the default async queue
						#pragma acc wait
						//now that the host/master threads knows that the GPUs has populated Fmagn0 with the results we can search for the maximum value on Fmagn0

						hklval max0 = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
						if ( gpuvalid == true ) {
							for( int ijk = 0; ijk < nijk; ijk++ ) {
								if ( Fmagn0[ijk] < max0.val )
									continue;
								else
									max0 = hklval( WAIT_UNTIL_GLOBAL_EXTREMUM_FOUND, ijk, Fmagn0[ijk] );
							}
							max0.mpid = buffer_local_mpid_gpu.at(chk_gpu); //process-local material point ID, not global Material point ID!
							//free(A0);
							delete [] A0; A0 = NULL;
							//free(Fmagn0);
							delete [] Fmagn0; Fmagn0 = NULL;
						}
						//this is a process-local material point ID !, i.e. there are possibly as many 0-th local material points as there are MPI processes
						//use myworkload.at(max0.mpid).mpid to resolve the global material point ID
						//if max0.mpid is flagged with NORESULTS_FOR_THIS_MATERIALPOINT we can filter this out!
						myresults_buffer.push_back( max0 );

						/*//##MK::BEGIN DEBUG
						#pragma omp critical
						{
							cout << "Current epoch " << epoch << " on thread MASTER after pragma acc wait" << "\n";
							cout << "max0 " << max0 << "\n";
							if ( useboth == true )
								cout << "max1 " << max1 << "\n";
						}
						double gtoc = omp_get_wtime();
						#pragma omp critical
						{
							if ( useboth == true )
								cout << "Current epoch " << epoch << " on thread MASTER chk_gpu+0, chk_gpu+1 " << chk_gpu+0 << ";" << chk_gpu+1 << " two FT took " << (gtoc-gtic) << " seconds" << "\n";
							else
								cout << "Current epoch " << epoch << " on thread MASTER chk_gpu+0, chk_gpu+1 " << chk_gpu+0 << ";" << "xxxx" << " one FT took " << (gtoc-gtic) << " seconds" << "\n";
						}
						//##MK::END DEBUG*/

						chk_gpu++;

						//bookkeeping
						mystatus.nchunks++;
						mystatus.ionsum = mystatus.ionsum + n0/3;

					} //next chk_gpu

					//free(IV);
					delete [] IV; IV = NULL;

					/*	double mmytoc = omp_get_wtime();
					#pragma omp critical
					{
						cout << "Current epoch " << epoch << " on thread MASTER async with " << wn.chunk_now_gpu << " on GPUs took " << (mmytoc-mmytic) << " seconds" << "\n";
					}*/
				} //master done with its work
				else { //remainder non-master threads work asychronously on own direct Fourier transform
					/*double mmytic = omp_get_wtime();*/
					//##MK::for now we can with this model execute only one chk on the CPUs per epoch
					//because as master execution path is different than of non-master threads we cannot invoke a #pragma omp barrier here
					//MK::we could dispatch as chks for each accelerator and proceed with OMP threading but typically accelerators are
					//faster and hence could process multiple chks while the thread team hasnt even complete one
					if ( (mt-1) < wn.chunk_now_cpu ) {
						//MK::the construct mt-1 works because the MASTER thread never enters this section so mt is always >= 1
						//MK::mt-1 possible because this section is only executed by non-master thread if such exist

						hklval cpumt1res = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
						size_t thisone = mt-1; //all threads contribute, if enough points left
						if ( buffer_cpu.at(thisone) != NULL ) {
							if ( buffer_cpu.at(thisone)->size() > 0 ) {

								dft_cpu solver = dft_cpu( ni );

								cpumt1res = solver.execute( *(buffer_cpu.at(thisone)) );

								cpumt1res.mpid = buffer_local_mpid_cpu.at(thisone); //process-local ID

								mystatus.ionsum = mystatus.ionsum + (buffer_cpu.at(thisone)->size() / 3);
								mystatus.nchunks++;
							}
						}
						myresults_buffer.push_back( cpumt1res );

						/*double mmytoc = omp_get_wtime();
						#pragma omp critical
						{
							cout << "Current epoch " << epoch << " on thread/chk " << mt << " FT took " << (mmytoc-mmytic) << " seconds" << "\n";
						}*/
					} //non-master threads done with processing the only cpu part of the current epoch
				} //non-master thread part
#endif
			} //handled case of asynchronous GPU + CPU parallel execution
			else {
				//handle multithreaded fallback in case there are no accelerators
				/*#pragma omp critical
				{
					cout << "Thread " << mt << " participates in multi-threaded fallback current epoch chunk_now_cpu " << wn.chunk_now_cpu << "\n";
				}*/
				double mmytic = omp_get_wtime();

				if ( mt < wn.chunk_now_cpu ) { //possibly all threads contribute, if enough points left
					hklval cpumtres = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
					size_t thisone = mt; //all threads contribute, if enough points left
					if ( buffer_cpu.at(thisone) != NULL ) { //if the ROI was queried
						if ( buffer_cpu.at(thisone)->size() > 0 ) { //and it contains coordinate values

							dft_cpu solver = dft_cpu( ni );

							cpumtres = solver.execute( *(buffer_cpu.at(thisone)) );

							cpumtres.mpid = buffer_local_mpid_cpu.at(thisone); //process-local ID

							mystatus.ionsum = mystatus.ionsum + (buffer_cpu.at(thisone)->size() / 3);
							mystatus.nchunks++;
						}
					}
					myresults_buffer.push_back( cpumtres );
				}

				double mmytoc = omp_get_wtime();
				#pragma omp critical
				{
					cout << "Current epoch " << epoch << " on thread/chk " << mt << " FT took " << (mmytoc-mmytic) << " seconds" << "\n";
				}
			} //done for the CPU only multi-threaded fallback

			//threads dump their results now resulting in only as many criticals as there are threads
			//rather than as many individual criticals as FT transforms per epoch
			#pragma omp critical
			{
				for( auto kyvl = myresults_buffer.begin(); kyvl != myresults_buffer.end(); kyvl++ ) {
					if ( kyvl->mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) {
						myhklval.at(kyvl->mpid) = *kyvl;
					}
				}
				//release thread-local buffer of collected results
				myresults_buffer = vector<hklval>();
			}

			mystatus.epoch = epoch;
			double mytoc = omp_get_wtime();
			mystatus.dt = mytoc-mytic;
			#pragma omp critical
			{
				epoch_diary.push_back( mystatus );
			}

			//a barrier is necessary, because we need to avoid that masters starts release buffers already while threads may still accumulate results
			#pragma omp barrier

			//clear the buffer and reinitialize for next use
			#pragma omp master
			{
				for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
					if ( buffer_gpu.at(chk_gpu) != NULL ) {
						delete buffer_gpu.at(chk_gpu);
						buffer_gpu.at(chk_gpu) = NULL; //necessary to prepare for reusage
					}
				}
				for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
					if ( buffer_cpu.at(chk_cpu) != NULL ) {
						delete buffer_cpu.at(chk_cpu);
						buffer_cpu.at(chk_cpu) = NULL; //necessary to prepare for reusage
					}
				}
			} //no implicit barrier as were about to exit parallel region with implicit barrier anyway

		} //end of parallel region within the while loop for the current epoch

		mympid = mympid + wn.chunk_now_gpu + wn.chunk_now_cpu; //advance to begin a new epoch

		double toc = MPI_Wtime();
		epoch_wallclock.push_back( epoch_prof( epoch, toc-tic ) );

		cout << "Current epoch " << epoch << " with a total of " << wn.chunk_now_gpu + wn.chunk_now_cpu << " completed took " << (toc-tic) << " seconds" << "\n";
		epoch++;
	} //end of the while loop

	double global_toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "QueryROIsAndPerformDirectFourierTransforms", APT_XX, APT_IS_PAR, mm, global_tic, global_toc);

	cout << "Rank " << get_myrank() << " the direct FT took " << (global_toc-global_tic) << " seconds" << "\n";

	sort( epoch_diary.begin(), epoch_diary.end(), SortEpochLogAsc );

	debug_epoch_profiling( epoch_diary, epoch_wallclock );
}


bool fourierHdl::init_target_file()
{
	//##MK::generate group hierarchy of an open source IFES APTTC APTH5 HDF5 file
	string h5fn_out = "PARAPROBE.Fourier.Results.SimID." + to_string(ConfigShared::SimID) + ".apth5";
	cout << "Initializing target file " << h5fn_out << "\n";

	if ( debugh5Hdl.create_fourier_apth5( h5fn_out ) == WRAPPED_HDF5_SUCCESS ) {
		return true;
	}
	else {
		return false;
	}
}


bool fourierHdl::collect_results_on_masterprocess()
{
	double tic = MPI_Wtime();

	//then we have two options, the simplest, enforce sequential loop of individual slave sends, master receives
	//or leaner a MPI_Gatherv on the master
	//##MK::we opt first for the simpler strategy, because computing time for the fourier transform >> communication time

	//all processes first identify how many results they have to sent
	int localhealth = 1;
	int globalhealth = 0;
	if ( myhklval.size() >= UINT32MX ) {
		cerr << "Rank " << get_myrank() << " has too many results to send them using the currently implemented communcation protocol!";
		localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all processes are able to contribute results!" << "\n";
		return false;
	}

	unsigned int n_localresults = static_cast<unsigned int>(myhklval.size()); //we send also results on material points with no ions
	vector<unsigned int> rk2nres = vector<unsigned int>( get_nranks(), 0 );
	unsigned int* buf = NULL;
	if ( myrank == MASTER ) {
		buf = rk2nres.data(); //raw pointer to array ie &rk2nres[0]
	}
	MPI_Gather( &n_localresults, 1, MPI_UNSIGNED, buf, 1, MPI_UNSIGNED, MASTER, MPI_COMM_WORLD );

	size_t nrestotal = 0;
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < rk2nres.size(); i++ ) {
			cout << "Rank " << i << " contributes " << rk2nres[i] << " results" << "\n";
		}
		for( auto it = rk2nres.begin(); it != rk2nres.end(); it++ ) {
			nrestotal += static_cast<size_t>(*it);
		}
		cout << "Rank MASTER has counted " << nrestotal << " results and expects " << mp.matpoints.size() << "\n";

		//is this consistent with expectation?
		if ( nrestotal != mp.matpoints.size() ) {
			localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that the reported number of results is different from expectation!" << "\n";
		return false;
	}

	//no differences, so let master allocate sufficiently sized buffer
	//MK::we enforce here, at the cost of performance, and explicit writing of the result in order, such that regardless the number of processes used
	//the order of the results arrays in the HDF5 file is always the same, i.e. deterministic
	vector<MPI_Fourier_ROI> roisend;
	vector<MPI_Fourier_ROI> roirecv;
	vector<MPI_Fourier_HKLValue> hklsend;
	vector<MPI_Fourier_HKLValue> hklrecv;

	if ( get_myrank() == MASTER ) {
		//master allocates global results field, see above comments on strategy
		mproi = vector<MPI_Fourier_ROI>( nrestotal, MPI_Fourier_ROI() );
		mpres = vector<MPI_Fourier_HKLValue>( nrestotal, MPI_Fourier_HKLValue() );

		//master fills in all values
		//infos about the master's ROIs
		for( auto it = myworkload.begin(); it != myworkload.end(); it++ ) { //myworkload handling global material point IDs
			unsigned int global_mpid = it->mpid;
			mproi.at(global_mpid) = MPI_Fourier_ROI( it->x, it->y, it->z, it->R, it->mpid, it->nions ); //number of ions not yet known
		}
		//master also fills in all its actual results
		for( auto it = myhklval.begin(); it != myhklval.end(); it++ ) { //myhklval handling local material point IDs
			unsigned int local_mpid = it->mpid; //process-local ID
			if ( local_mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) {
				unsigned int global_mpid = myworkload.at(local_mpid).mpid;
				mpres.at(global_mpid) = MPI_Fourier_HKLValue( global_mpid, it->ijk, it->val ); //from explicit global_mpid to implicit IDs
				//##MPI_Fourier_HKLValue change mpid from int to unsigned int
			}
			//if no result leave MPI_Fourier_HKLValue with the default datatype constructor value
		}
	}
	else { //meanwhile the slaves prepare their MPI send buffers
		//infos about the slaves's ROIs
		roisend.reserve( myworkload.size() );
		for( auto it = myworkload.begin(); it != myworkload.end(); it++ ) { //myworkload handling global material point IDs
			roisend.push_back( MPI_Fourier_ROI(it->x, it->y, it->z, it->R, it->mpid, it->nions) );
		}
		//the actual results of the slave
		hklsend.reserve( myhklval.size() );
		for( auto it = myhklval.begin(); it != myhklval.end(); it++ ) { //myhklval handling local material point IDs
			unsigned int local_mpid = it->mpid; //process-local ID
			if ( local_mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) { //only if this is resolvable there is a result, else the ROI has no ion support
				unsigned int global_mpid = myworkload.at(local_mpid).mpid;
				hklsend.push_back( MPI_Fourier_HKLValue( global_mpid, it->ijk, it->val ) ); //from explicit global_mpid to implicit IDs
			}
			else {
				hklsend.push_back( MPI_Fourier_HKLValue( NORESULTS_FOR_THIS_MATERIALPOINT, INT32MX, 0.f ) );
			}
		}
	}

	//agglomeration stage, master collects results from slaves
	for( int rank = MASTER + 1; rank < get_nranks(); rank++ ) { //+1 because we have already the results from the master
		if ( get_myrank() == MASTER ) {

			roirecv = vector<MPI_Fourier_ROI>( rk2nres.at(rank), MPI_Fourier_ROI() );
			MPI_Recv( roirecv.data(), rk2nres.at(rank), MPI_Fourier_ROI_Type, rank, rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE );

			hklrecv = vector<MPI_Fourier_HKLValue>( rk2nres.at(rank), MPI_Fourier_HKLValue() );
			MPI_Recv( hklrecv.data(), rk2nres.at(rank), MPI_Fourier_HKLValue_Type, rank, 1000*rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			//once results are received fill them in
			for( auto it = roirecv.begin(); it != roirecv.end(); it++ ) {
				unsigned int global_mpid = it->mpid;
				mproi.at(global_mpid) = *it;
			}
			for( auto jt = hklrecv.begin(); jt != hklrecv.end(); jt++ ) {
				unsigned int global_mpid = jt->mpid;
				if ( global_mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) { //reset only those results values for which results exist
					mpres.at(global_mpid) = *jt;
				}
			}
		}
		else { //I am a slave to I have to send to MASTER
			if ( get_myrank() == rank ) {
				MPI_Send( roisend.data(), roisend.size(), MPI_Fourier_ROI_Type, MASTER, rank, MPI_COMM_WORLD );
				roisend = vector<MPI_Fourier_ROI>();

				MPI_Send( hklsend.data(), hklsend.size(), MPI_Fourier_HKLValue_Type, MASTER, 1000*rank, MPI_COMM_WORLD );
				hklsend = vector<MPI_Fourier_HKLValue>();
			}
			//else {} //nothing to do for me, I neither do I/O nor have the data for rank
		}
		MPI_Barrier(MPI_COMM_WORLD); //##MK::hyperphobic
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "CollectResultsOnMasterProcess", APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " participated successfully in results collection stage took " << (toc-tic) << " seconds" << "\n";
	return true;
}


bool fourierHdl::write_materialpoints_to_apth5()
{
	//##MK::error management
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER ) { //only the master performs I/O of metadata

		if ( mproi.size() < 1 ) {
			cout << "WARNING:: There are no material point ROI infos to report" << "\n"; return true;
		}
		if ( mpres.size() < 1 ) {
			cout << "WARNING:: There are no material point results to report" << "\n"; return true;
		}

		vector<unsigned int> u32;
		//material point topology for rendering in paraview
		u32 = vector<unsigned int>( (1+1+1)*mproi.size(), 1 );
		//+1 first index identifies geometric primitive, here single vertex
		//+1 second index tells how many elements to except type for XDMF is XDMF keyword what we see, here point
		//+1 the value vertex ID to retrieve the coordinates of the point from the PARAPROBE_FOURIER_MATPOINT_XYZ array in the HDF5 file through XDMF in Paraview/VisIt
		for( size_t i = 0; i < mproi.size(); i++ ) {
			u32.at(3*i+2) = (i < static_cast<size_t>(UINT32MX)) ? static_cast<unsigned int>(i) : UINT32MX;
		}

		int status = 0;
		h5iometa ifo = h5iometa( PARAPROBE_FOURIER_META_MP_TOPO, u32.size()/PARAPROBE_FOURIER_META_MP_TOPO_NCMAX,
				PARAPROBE_FOURIER_META_MP_TOPO_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_TOPO create failed! " << status << "\n"; return false;
		}
cout << "PARAPROBE_FOURIER_META_MP_TOPO create " << status << "\n";
		h5offsets offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_MP_TOPO_NCMAX, 0, PARAPROBE_FOURIER_META_MP_TOPO_NCMAX,
				u32.size()/PARAPROBE_FOURIER_META_MP_TOPO_NCMAX, PARAPROBE_FOURIER_META_MP_TOPO_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_TOPO write failed! " << status << "\n"; return false;
		}
cout << "PARAPROBE_FOURIER_META_MP_TOPO write " << status << "\n";
		u32 = vector<unsigned int>();

		//material point IDs
		u32.reserve( mproi.size() );
		if ( mproi.size() >= static_cast<size_t>(UINT32MX) ) {
			cerr << "Too many mproi.size() to export!" << "\n"; return false;
		}
		unsigned int ni = static_cast<unsigned int>(mproi.size()); //downcast now safe
		for( unsigned int i = 0; i < ni; i++ ) {
			u32.push_back( mproi.at(i).mpid );
		}
		ifo = h5iometa( PARAPROBE_FOURIER_META_MP_ID, u32.size()/PARAPROBE_FOURIER_META_MP_ID_NCMAX,
				PARAPROBE_FOURIER_META_MP_ID_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_ID create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_MP_ID create " << status << "\n";
		offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_MP_ID_NCMAX, 0, PARAPROBE_FOURIER_META_MP_ID_NCMAX,
				u32.size()/PARAPROBE_FOURIER_META_MP_ID_NCMAX, PARAPROBE_FOURIER_META_MP_ID_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_ID write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_MP_ID write " << status << "\n";
		u32 = vector<unsigned int>();

		//number of ions
		u32.reserve( mproi.size() );
		for( unsigned int i = 0; i < ni; i++ ) { //check for safe downcasting done before already
			u32.push_back( mproi.at(i).nions );
		}
		ifo = h5iometa( PARAPROBE_FOURIER_META_MP_NIONS, u32.size()/PARAPROBE_FOURIER_META_MP_NIONS_NCMAX,
				PARAPROBE_FOURIER_META_MP_NIONS_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_NIONS create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_MP_NIONS create " << status << "\n";
		offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_MP_NIONS_NCMAX, 0, PARAPROBE_FOURIER_META_MP_NIONS_NCMAX,
				u32.size()/PARAPROBE_FOURIER_META_MP_NIONS_NCMAX, PARAPROBE_FOURIER_META_MP_NIONS_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_NIONS write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_MP_NIONS write " << status << "\n";
		u32 = vector<unsigned int>();

		vector<float> f32;
		f32.reserve( 3*mproi.size() );
		for( auto it = mproi.begin(); it != mproi.end(); it++ ) {
			f32.push_back( it->x );
			f32.push_back( it->y );
			f32.push_back( it->z );
		}
		ifo = h5iometa( PARAPROBE_FOURIER_META_MP_XYZ, f32.size()/PARAPROBE_FOURIER_META_MP_XYZ_NCMAX, PARAPROBE_FOURIER_META_MP_XYZ_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS  ) {
			cerr << "PARAPROBE_FOURIER_META_MP_XYZ_NCMAX create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_MP_XYZ create " << status << "\n";
		offs = h5offsets( 0, f32.size()/PARAPROBE_FOURIER_META_MP_XYZ_NCMAX, 0, PARAPROBE_FOURIER_META_MP_XYZ_NCMAX,
				f32.size()/PARAPROBE_FOURIER_META_MP_XYZ_NCMAX, PARAPROBE_FOURIER_META_MP_XYZ_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_MP_XYZ write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_MP_XYZ write " << status << "\n";
		f32 = vector<float>();

		string xdmffn = "PARAPROBE.Fourier.Results.SimID." + to_string(ConfigShared::SimID) + ".xdmf";
		debugxdmf.create_materialpoint_file( xdmffn, mproi.size(), debugh5Hdl.h5resultsfn );

		//details about the grid used
		u32 = vector<unsigned int>( 3, ConfigFourier::FourierGridResolution );
		ifo = h5iometa( PARAPROBE_FOURIER_META_HKL_NIJK, u32.size()/PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX,
				PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_NIJK create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_NIJK create " << status << "\n";
		offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX, 0, PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX,
				u32.size()/PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX, PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_NIJK write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_NIJK write " << status << "\n";
		u32 = vector<unsigned int>();

		//##MK::make small function which computes this mapping taking only FourierGridResolution as input
		f32 = vector<float>();
		dft_real bound = 2.0;
		dft_real start = -bound; //-1.0;
		dft_real stop = +bound; //+1.0;
		dft_real num = static_cast<dft_real>(ConfigFourier::FourierGridResolution-1);
		dft_real step = (stop-start)/num;
		for( unsigned int i = 0; i < ConfigFourier::FourierGridResolution; ++i ) {
			dft_real v = TWO_PI * (start + static_cast<dft_real>(i)*step);
			f32.push_back( v );
		}
		ifo = h5iometa( PARAPROBE_FOURIER_META_HKL_IVAL, f32.size()/PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX,
				PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_IVAL create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_IVAL create " << status << "\n";
		offs = h5offsets( 0, f32.size()/PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX, 0, PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX,
				f32.size()/PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX, PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_META_HKL_IVAL write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_META_HKL_IVAL write " << status << "\n";
		f32 = vector<float>();
	}
	//implicit else

	MPI_Barrier( MPI_COMM_WORLD );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "MASTERWriteMetadataToHDF5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	if ( get_myrank() == MASTER ) {
		cout << "Rank " << get_myrank() << " writing of material infos into H5 file took " << (toc-tic) << " seconds" << "\n";
	}

	return true;
}


bool fourierHdl::write_results_to_apth5()
{
	//##MK::error management
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER ) { //only the master performs I/O

		if ( mpres.size() < 1 ) {
			cout << "WARNING:: There are no material point results to report" << "\n"; return true;
		}

		//hklvalues
		vector<unsigned int> u32;
		u32.reserve( mpres.size() );
		for( auto it = mpres.begin(); it != mpres.end(); it++ ) {
			u32.push_back( it->mpid );
		}
		h5iometa ifo = h5iometa( PARAPROBE_FOURIER_RES_HKL_MPID, u32.size()/PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX,
				PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX );
		int status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_RES_HKL_MPID create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_RES_HKL_MPID create " << status << "\n";
		h5offsets offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX, 0, PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX,
				u32.size()/PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX, PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_RES_HKL_MPID write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_RES_HKL_MPID write " << status << "\n";
		u32 = vector<unsigned int>();

		u32.reserve( mpres.size() );
		for( auto it = mpres.begin(); it != mpres.end(); it++ ) {
			u32.push_back( it->ijk );
		}
		ifo = h5iometa( PARAPROBE_FOURIER_RES_HKL_MAXIJK, u32.size()/PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX,
				PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_RES_HKL_MAXIJK create failed! " << status << "\n"; return false;
		}
cout << "PARAPROBE_FOURIER_RES_HKL_MAXIJK create " << status << "\n";
		offs = h5offsets( 0, u32.size()/PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX, 0, PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX,
				u32.size()/PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX, PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_RES_HKL_MAXIJK write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_RES_HKL_MAXIJK write " << status << "\n";
		u32 = vector<unsigned int>();

		vector<float> f32;
		f32.reserve( mpres.size() );
		for( auto it = mpres.begin(); it != mpres.end(); it++ ) {
			f32.push_back( it->value );
		}
		ifo = h5iometa( PARAPROBE_FOURIER_RES_HKL_MAXVAL, f32.size()/PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX,
				PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX );
		status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_RES_HKL_MAXVAL create failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_RES_HKL_MAXVAL create " << status << "\n";
		offs = h5offsets( 0, f32.size()/PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX, 0, PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX,
				f32.size()/PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX, PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX);
		status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
		if ( status != WRAPPED_HDF5_SUCCESS ) {
			cerr << "PARAPROBE_FOURIER_RES_HKL_MAXVAL write failed! " << status << "\n"; return false;
		}
	cout << "PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX write " << status << "\n";
		f32 = vector<float>();
	}
	//implicit else

	MPI_Barrier( MPI_COMM_WORLD );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier_tictoc.prof_elpsdtime_and_mem( "MASTERWriteResultsToHDF5", APT_XX, APT_IS_PAR, mm, tic, toc);

	if ( get_myrank() == MASTER ) {
		cout << "Rank " << get_myrank() << " writing of materials results into H5 file took " << (toc-tic) << " seconds" << "\n";
	}

	return true;
}



int fourierHdl::get_myrank()
{
	return this->myrank;
}


int fourierHdl::get_nranks()
{
	return this->nranks;
}


void fourierHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void fourierHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void fourierHdl::init_mpidatatypes()
{
	//define MPI datatype which assists us to logically structure the communicated data packages
	MPI_Type_contiguous(3, MPI_FLOAT, &MPI_IonPositions_Type);
	MPI_Type_commit(&MPI_IonPositions_Type);

	MPI_Type_contiguous(4, MPI_FLOAT, &MPI_IonWithDistance_Type);
	MPI_Type_commit(&MPI_IonWithDistance_Type);

	MPI_Type_contiguous(9, MPI_FLOAT, &MPI_Triangle_Type);
	MPI_Type_commit(&MPI_Triangle_Type);


	//commit utility data types to simplify and collate data transfer operations
	int cnts1[2] = {4, 2};
	MPI_Aint dsplc1[2] = {0, 4 * 4}; //4 B for a float
	MPI_Datatype otypes1[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, cnts1, dsplc1, otypes1, &MPI_Fourier_ROI_Type);
	MPI_Type_commit(&MPI_Fourier_ROI_Type);

	int cnts2[2] = {2, 1};
	MPI_Aint dsplc2[2] = {0, 2 * 4 }; //4 B for an int32
	MPI_Datatype otypes2[2] = {MPI_INT, MPI_FLOAT};
	MPI_Type_create_struct(2, cnts2, dsplc2, otypes2, &MPI_Fourier_HKLValue_Type);
	MPI_Type_commit(&MPI_Fourier_HKLValue_Type);
}
