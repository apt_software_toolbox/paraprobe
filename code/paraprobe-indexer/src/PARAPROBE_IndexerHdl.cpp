//##MK::CODESPLIT

#include "PARAPROBE_IndexerHdl.h"


indexerHdl::indexerHdl()
{
	myrank = MASTER;
	nranks = 1;
}


indexerHdl::~indexerHdl()
{
}


bool indexerHdl::read_testoris_from_h5()
{
	double tic = MPI_Wtime();

	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	int status = 0;

	if ( ConfigIndexer::InputfileTestOris.substr(ConfigIndexer::InputfileTestOris.length()-3) != ".h5" ) {
		cerr << "The InputfileTestOris has not the proper h5 file extension!" << "\n";
		return false;
	}

	//probe existence of xyz coordinates array in the input file and get dimensions
	inputTestOrisH5Hdl.h5resultsfn = ConfigIndexer::InputfileTestOris;
	if ( inputTestOrisH5Hdl.query_contiguous_matrix_dims( PARAPROBE_IDXR_META_ORI_QUAT, offs ) != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Unable access and parse dimensions of the test rotations/orientations in InputfileTestOris" << "\n";
		return false;
	}
	else {
		cout << "PARAPROBE_IDXR_META_ORI_QUAT is a " << offs.nrmax << " rows " << offs.ncmax << " matrix" << "\n";
	}

	//read the content and transfer to working array
	ifo = h5iometa( PARAPROBE_IDXR_META_ORI_QUAT, offs.nrows(), offs.ncols() );
	vector<float> f32;
	status = inputTestOrisH5Hdl.read_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
cout << "PARAPROBE_IDXR_META_ORI_QUAT read " << status << "\n";
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "Reading PARAPROBE_IDXR_META_ORI_QUAT on InputfileTestOris failed" << "\n"; return false;
	}

	try {
		testoris.reserve( offs.nrows() );
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error during reading test rotations/orientations!" << "\n";
	}
	for( size_t i = 0; i < f32.size(); i += 4 ) { //+4 because four quaternion components define a row of the matrix
		testoris.push_back( squat( f32[i+0], f32[i+1], f32[i+2], f32[i+3] ) );
	}
	f32 = vector<float>();

	//dont forget to set ConfigIndexer::NumberOfSO3Candidates once all values were broadcast across process team!

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "ReadTestOrientationsFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool indexerHdl::broadcast_testoris()
{
	double tic = MPI_Wtime();

	//MASTER communicates testorientations to all SLAVES
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( testoris.size() >= (INT32MX-1) ) {
			cerr << "Rank " << MASTER << " current implementation does not handle the case of larger than 2 billion testoris!" << "\n"; localhealth = 0;
		}
		if ( testoris.size() == 0 ) {
			cerr << "Rank " << MASTER << " testoris on MASTER is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the dataset!" << "\n";
		return false;
	}

	//dataset on the master is okay, but do we need to broadcast at all to others, i.e. are there slaves?
	if ( get_nranks() < 2 ) {
		cout << "Skipping broadcast_testoris because single process execution" << "\n";
		return true;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<size_t>(testoris.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Quaternion* buf = NULL;
	try {
		buf = new MPI_Quaternion[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf!" << "\n"; localhealth = 0;
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL; return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < testoris.size(); ++i ) {
			buf[i] = MPI_Quaternion( testoris[i].q0, testoris[i].q1, testoris[i].q2, testoris[i].q3 );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_Quaternion_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			testoris.reserve( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				testoris.push_back( squat( buf[i].q0, buf[i].q1, buf[i].q2, buf[i].q3 ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
		return false;
	}
	else {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
	}

	/*
	cout << "Rank " << get_myrank() << " listened to MASTER testoris.size() " << testoris.size() << "\n";
	*/

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	indexer_tictoc.prof_elpsdtime_and_mem( "BroadcastTestOris", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


void indexerHdl::evaluate_disorientations()
{
	double tic = MPI_Wtime();

	//upper ##MK::?? triangle matrix
	vector<DisoriIJ> disori_matrix;
	#pragma omp parallel
	{
		vector<DisoriIJ> myres;
		size_t ncand = testoris.size();
		apt_real thrshld = ConfigIndexer::IOSymmRedMaxDisoriAngle; // only <= allowed

		#pragma omp for schedule(dynamic,1) nowait
		for( size_t i = 0; i < ncand; i++ ) {
			double mytic = omp_get_wtime();
			squat ip = testoris.at(i);
			for( size_t j = i+1; j < ncand; j++ ) {
				squat iq = testoris.at(j);

				pair<bool,squat> ij = disoriquaternion_cubic_cubic( ip, iq );
				if ( ij.first == true ) { //most likely case
					//collect only disorientations with angle <= thrshld
					apt_real theta = 2.f*acos(ij.second.q0);
					if ( theta > thrshld ) { //most likely case
						continue;
					}
					else {
						//##MK::DEBUG
						#pragma omp critical
						{
							cout << "Found new one i/j " << i << "\t\t" << j << "\n";
						}
						//##MK::END DEBUG
						myres.push_back( DisoriIJ(i, j, theta) );
					}
				}
				/*
				else {
					#pragma omp critical
					{
						cerr << "i/j/invalid " << i << "\t\t" << j << "\n";
					}
				}*/
			} //next j

			double mytoc = omp_get_wtime();
			#pragma omp critical
			{
				cout << "i = " << i << " in " << (mytoc-mytic) << " seconds " << (ncand-(i+1)) << " cases!" << "\n";
			}
		} //next i

		#pragma omp critical
		{
			//concentrate potential reallocations
			disori_matrix.reserve( disori_matrix.size() + myres.size() );
			for( auto it = myres.begin(); it != myres.end(); ++it ) { //fill in thread-local results to master
				disori_matrix.push_back( *it );
			}
			myres = vector<DisoriIJ>();
		}
	}

	double toc = MPI_Wtime();
}


/*
void fourierHdl::execute_local_workpackage2()
{
	cout << "Rank " << get_myrank() << " performing OpenMP/OpenACC direct Fourier transforms..." << "\n";

	double global_tic = MPI_Wtime();

	//initialize one GPUthe two NVIDIA GPUs on the TALOS node on which I, as get_myrank() am currently running
	int n_gpus = 0; //0 - no gpus, 1 or 2 gpus
	int n_mygpus = 0; //either 0, or 1, rank either has a gpu and can use it or not
	int devId = -1;
	int devCount = 0;

#ifdef UTILIZE_GPUS
	if ( ConfigFourier::GPUsPerNode > 0 ) {
		devId = -1;
		devCount = acc_get_num_devices(acc_device_nvidia);
		if ( devCount > 0  ) {
			n_gpus = devCount;
			n_mygpus = 1;
			cout << "Rank " << get_myrank() << " NVIDIA accelerator GPU initialization devCount " << devCount << "\n";
		}
		else {
			n_mygpus = 0;
			cout << "Rank " << get_myrank() << " no accelerators existent " << devCount << "\n";
		}
	}
	else

	//##MK::not necessarily portable, here system-specific accelerator initialization exemplified
	//for the MPCDF system TALOS, specifically its computing nodes with 2x20core + 2xNVIDIA V100 accelerators/GPGPUs/"GPUs"
	if ( n_gpus > 0 ) {
		//MK::assume user has instructed model with two processes per node each getting one GPUs
		devId = get_myrank() % ConfigFourier::GPUsPerNode;
		//cout << "Rank " << get_myrank() << " my GPU devID is " << devId << "\n";
		// creates a context on the GPUs
		//excludes initialization time from computations
		acc_set_device_num(devId, acc_device_nvidia);
		acc_init(acc_device_nvidia);
cout << "Rank " << get_myrank() << " running on GPU with device ID " << devId << "\n";
	}
	else {
		cerr << "Rank " << get_myrank() << " will use multi-threaded CPU fallback n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";
	}
#endif
	//##cout <<  report use fallback multithreaded CPU only
	cout << "Rank " << get_myrank() << " GPU and CPU initialized now processing... n_gpus " << n_gpus << " n_mygpus " << n_mygpus << "\n";

	//the passing of individual ROIs for each process onto the CPU/GPUs works as follows:
	//the CPU OpenMP thread team identifies the ions within each ROI
	//ROIs are group into packets (epoch), a packet to work on for the CPU and a packet to work for the GPUs
	//the CPUs process their package in parallel, each thread one material point, i.e. one DFT
	//the GPUs process the GPU packet
	//the GPU get a heuristic fraction of ROIs: when a CPU processes 1 ROI the GPU processes ConfigFourier::GPUWorkload ROIs at the same time
	//##MK::to get more fine tuning in the future allow CPUs to have more than one package
	//we work through the pairs of CPU/GPU ROI packages until all ROIs of the MPI process are completed

	int local_nmp = myworkload.size();
	cout << "Rank " << get_myrank() << " myworkload.size() " << local_nmp << "\n";

	//we need to initialize buffers first to store the ion positions from all ROIs per epoch
	vector<vector<dft_real>*> buffer_gpu;
	vector<vector<dft_real>*> buffer_cpu;
	vector<int> buffer_local_mpid_gpu; //buffer to store which specific ROIs are currently processed, this is handled by the CPU
	vector<int> buffer_local_mpid_cpu; //MK::do not jumble up these local ROI IDs with the global material point/ROI ids!
	//the local IDs live on the ID interval [0, myworkload.size() )] BUT
	//the global IDs live on the ID interval [ 0, mp2rk.size() ) !

	//we go parallel with the threads but orchestrate/guide them through their workplan
	//we need a parallel region to have the allocation of the buffers in the master
	//##MK::TO DO performance benchmark, if the master thread gets pinned to core 0 it has on average faster access
	//to the PCI control busses, therefore we let the master thread delegate and execute work on the CPUs, the master therefore
	//does not contribute in solving own Fourier transforms ##MK::TO BE DISCUSSED but dont underestimate might get  tricky to get orchestrated
	//otherwise because we have here heterogeneous parallelism with device (GPU) and host (CPU) + asynchronous execution on the device
	//we call the gpu for now the device and the cpu the host

	gpu_cpu_max_workload wl = gpu_cpu_max_workload();
	//cout << "Rank " << get_myrank() << " chk_max_gpu/chk_max_cpu " << wl.chunk_max_gpu << ";" << wl.chunk_max_cpu << "\n";

	#pragma omp parallel shared(n_gpus,n_mygpus,local_nmp,buffer_gpu,buffer_cpu,buffer_local_mpid_gpu,buffer_local_mpid_cpu,wl)
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		#pragma omp master
		{
			//there is always a master thread
			//MK::idea is if GPUS+CPUs are used, in each iteration nt-1 FT are solved on the CPU, and an empirically identified number per GPU
			//this number, ##MK here just chosen as 4, will minimize CPU vs GPU async wait time if GPU runtime per epoch is about equal as per thread
			wl = plan_max_per_epoch( n_mygpus, nt, local_nmp );
			cout << "Rank " << get_myrank() << " MASTER " << mt << " chk_max_gpu/chk_max_cpu " << wl.chunk_max_gpu << ";" << wl.chunk_max_cpu << "\n";

			//thread team shared global address space buffers get populated by master
			//the reason for using a vector of vector of pointer rather than a vector of coordinates
			//is to collate raw pointers to different addresses, specifically pointer to different thread-local mem virtual mem sections
			if ( wl.chunk_max_gpu > 0 ) {
				buffer_gpu = vector<vector<dft_real>*>( wl.chunk_max_gpu, NULL );
				buffer_local_mpid_gpu = vector<int>( wl.chunk_max_gpu, INT32MI ); //flagged as negative IDs to enable consistency tests
			}
			if ( wl.chunk_max_cpu > 0 ) {
				buffer_cpu = vector<vector<dft_real>*>( wl.chunk_max_cpu, NULL );
				buffer_local_mpid_cpu = vector<int>( wl.chunk_max_cpu, INT32MI ); //flagged as negative IDs to enable consistency tests
			}
		} //master initialized buffer memory
	} //end of parallel region

	//start processing epochs
	vector<epoch_log> epoch_diary;
	vector<epoch_prof> epoch_wallclock;
	int epoch = 0;
	int mympid = 0;
	int ni = ConfigFourier::FourierGridResolution;
	while ( mympid < local_nmp )
	{
		double tic = MPI_Wtime();
		gpu_cpu_now_workload wn = gpu_cpu_now_workload();

		//we define an epoch as a single chunk of FT for ROIs processed, the epoch contains at most chunk_plan_gpu+chunk_plan_cpu material points
		#pragma omp parallel shared(n_gpus,n_mygpus,epoch_diary,epoch,mympid,local_nmp,wl,wn,buffer_gpu,buffer_cpu,buffer_local_mpid_gpu,buffer_local_mpid_cpu,ni)
		{
			int mt = omp_get_thread_num();
			int nt = omp_get_num_threads();
			//we keep a thread-local buffer of results to avoid having to synchronize after each ROI transform a write-back of
			//results to the globally shared container which would choke parallel execution performance
			vector<hklval> myresults_buffer;
			double mytic = omp_get_wtime();
			epoch_log mystatus = epoch_log( static_cast<unsigned int>(mt), static_cast<unsigned int>(nt) );

			//buffer keeps pointers to ion position data for ROI of material points
			//we store the ROIs for the FT on the GPUs at [0,chunk_plan_gpu+chunk_plan_cpu-2] and
			//the ROI for the FT on the CPUs at [chunk_plan_gpu+chunk_plan_cpu-1]

			//once initialized the thread team processes through its material points using the GPU as accelerators,
			//meanwhile other processes do the same but on different material points/ROIs,
			//thereby using triple parallelism (hybrid MPI/OpenMP + OpenACC)
			#pragma omp single
			{
				//only a single thread must compute this, hence implicitly pragma omp critically writing on wn
				wn = plan_now_per_epoch( n_mygpus, nt, local_nmp, mympid, wl );

				int id = mympid; //at which ID on ID interval [0, myworkload.size() ) where we started the current epoch
				for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
					buffer_local_mpid_gpu.at(chk_gpu) = id + chk_gpu;
				}
				id = mympid + wn.chunk_now_gpu;
				for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
					buffer_local_mpid_cpu.at(chk_cpu) = id + chk_cpu;
				}
				//now we know which global material point IDs are processed by the CPU/GPU team

			}
			//we need a barrier to assure that all threads see change of wn
			#pragma omp barrier

			#pragma omp master
			{
				cout << "Current epoch " << epoch << " on thread " << mt << " chunk_now_gpu " << wn.chunk_now_gpu << " chunk_now_cpu " << wn.chunk_now_cpu << "\n";
			}

			//firstly, we let the threads (all of the team), populate the buffers for GPU and CPUs
			//the concept of epochs is useful to avoid a very high memory consumption for storing ROI population with ions
			//e.g. lets assume 1000 mat points per process, each ROI having 5000 ions, this would require storing 5k * 1k points,
			//using epochs we need to buffer only 5k times as many as ROIs as processed per epoch which is typically << local_nmp
			for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
				int whom = chk_gpu % nt;
				if ( whom == mt ) { //threads share the allocation and querying load
					vector<dft_real>* mybuf = NULL;
					mybuf = new vector<dft_real>;
					if ( mybuf != NULL ) {
						int mpid = buffer_local_mpid_gpu.at(chk_gpu);
						mp3d thisroi = myworkload.at(mpid);

						query_ions_within_roi( thisroi, mybuf );

						thisroi.nions = mybuf->size() / 3;
						//##MK::false sharing?
						myworkload.at(mpid) = thisroi; //no pragma omp critical, mpids are disjoint across the thread team!
						//#pragma omp critical
						//{
						//	cout << "Thread " << omp_get_thread_num() << " mpid " << mpid << " thisroi " << myworkload.at(mpid).x << ";" << myworkload.at(mpid).y << ";"
									<< myworkload.at(mpid).z << ";" << myworkload.at(mpid).R << "\t\t" << myworkload.at(mpid).nions << "\n";
						//}
						buffer_gpu.at(chk_gpu) = mybuf; //no pragma omp critical necessary, enforced collision free through whom == mt
					}
					else {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " allocation error for GPU mybuf!" << "\n";
						}
					}
				} //mt is done
			} //done with filling in GPU work

			//next, the thread team populates the CPU buffer
			for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
				int whom = chk_cpu % nt;
				if ( whom == mt ) {
					vector<dft_real>* mybuf = NULL;
					mybuf = new vector<dft_real>;
					if ( mybuf != NULL ) {
						int mpid = buffer_local_mpid_cpu.at(chk_cpu);
						mp3d thisroi = myworkload.at(mpid);

						query_ions_within_roi( thisroi, mybuf );

						thisroi.nions = mybuf->size() / 3;
						myworkload.at(mpid) = thisroi; //no pragma omp critical, mpids are disjoint across thread team!
						//#pragma omp critical
						//{
						//	cout << "Thread " << omp_get_thread_num() << " mpid " << mpid << " thisroi " << myworkload.at(mpid).x << ";" << myworkload.at(mpid).y << ";"
									<< myworkload.at(mpid).z << ";" << myworkload.at(mpid).R << "\t\t" << myworkload.at(mpid).nions << "\n";
						//}
						buffer_cpu.at(chk_cpu) = mybuf;
					}
					else {
						#pragma omp critical
						{
							cerr << "Rank " << get_myrank() << " thread " << mt << " allocation error for CPU mybuf!" << "\n";
						}
					}
				} //mt done
			} //done with filling CPU buffers

			//process the MPI-process local ROI material point IDs critical and master only, threads get to know values because ID fields are shared across team
			//barrier is necessary, because all buffer (cpus, gpus) have to be filled, before asynchronous processing on cpu/gpus starts...
			#pragma omp barrier

			////##MK::BEGIN DEBUGGING, can later be deleted
			//#pragma omp master
			//{
			//	cout << "Buffer for all chunks of current epoch " << epoch << " populated" << "\n";
			//	size_t ntotal = 0;
			//	size_t nactive = 0;
			//	for(size_t ii = 0; ii < buffer_gpu.size(); ii++) {
			//		if ( buffer_gpu.at(ii) != NULL ) { //allocated
			//			if ( buffer_gpu.at(ii)->size() > 0 ) { //and at least one ion within ROI
			//				nactive++;
			//				ntotal += buffer_gpu.at(ii)->size();
			//			}
			//		}
			//	}
			//	for(size_t ii = 0; ii < buffer_cpu.size(); ii++) {
			//		if ( buffer_cpu.at(ii) != NULL ) { //allocated
			//			if ( buffer_cpu.at(ii)->size() > 0 ) { //and at least one ion within ROI
			//				nactive++;
			//				ntotal += buffer_cpu.at(ii)->size();
			//			}
			//		}
			//	}
			//	cout << "Average number of ions per point " << ntotal / (3*nactive) << "\n"; //3 because three coordinates values per ion
			//} //no implicit barrier
			//#pragma omp barrier
			//##MK::END DEBUGGING

			//handle heterogeneously and possibly asynchronously executed direct Fourier transforms of current epoch on the GPUs through MASTER OMP_NESTED=true
			if ( n_mygpus > 0 ) { //if there are accelerators...
#ifdef UTILIZE_GPUS
				if ( mt == MASTER ) { //...the master thread always schedules work to GPUs only
					//we have only one GPU per process
					//double mmytic = omp_get_wtime();
					//cout << "Current epoch " << epoch << " on thread MASTER processing GPU part" << "\n";

					//define 1d Fourier grid positions on [-2.0,+2.0]
					int nij = SQR(ni);
					int nijk = CUBE(ni);
					//dft_real* IV = (dft_real*) malloc( ni*sizeof(dft_real) ); //restrict ... (dft_real*)
					dft_real* IV = new dft_real[ni];
					//dft_real* const IV = malloc( ni*sizeof(dft_real) );

					dft_real bound = 2.0;
					dft_real start = -bound; //-1.0;
					dft_real stop = +bound; //+1.0;
					dft_real num = static_cast<dft_real>(ni-1);
					dft_real step = (stop-start)/num;
					for( int i = 0; i < ni; i++ ) {
						IV[i] = TWO_PI * (start + static_cast<dft_real>(i)*step);
					}

					for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu;         ) {
						//prepare processing on the process's GPU devId

						//efficiently solving the max, index search is involved, for discussion see here
						//https://www.pgroup.com/userforum/viewtopic.php?t=5096
						dft_real* A0 = NULL; ///restrict
						dft_real* Fmagn0 = NULL;
						//https://www.pgroup.com/resources/docs/18.7/x86/pgi-user-guide/index.htm

						//prepare asynchronous processing on GPU0
						int n0 = 0;
						bool gpuvalid = false;
						if ( buffer_gpu.at(chk_gpu) != NULL ) {
							if ( buffer_gpu.at(chk_gpu)->size() >= 3 ) { //at least one ion within ROI!
								gpuvalid = true;
							}
						}

						//master delegates work on the GPUs, the master itself does not process work but does host interaction
						if ( gpuvalid == true ) {
							n0 = buffer_gpu.at(chk_gpu)->size();
							//A0 = (dft_real*) malloc( n0 * sizeof(dft_real) );
							A0 = new dft_real[n0];
							//dft_real* const A0 = malloc( n0 * sizeof(dft_real) );
							//Fmagn0 = (dft_real*) calloc( nijk*1, sizeof(dft_real) );
							Fmagn0 = new dft_real[nijk];
							for( int i = 0; i < nijk; i++ ) {
								Fmagn0[i] = 0.f;
							}
							//dft_real* const Fmagn0 = calloc( nijk, sizeof(dft_real) );
							vector<dft_real> & here0 = *(buffer_gpu[chk_gpu]);
							for( int i = 0; i < n0; i++ ) { //fill buffer for the device first with values on the host
								A0[i] = here0[i];
							}

							//acc_set_device_num(devId, acc_device_nvidia);
							//do all on GPU0 ASYNCHRONOUSLY, use the default async queue because we use only one GPU
							//create a Fmagn0 on the device
							#pragma acc data copy(Fmagn0[0:nijk]) async
							{
								int ijk, iion;
								//#pragma acc enter data copyin(Fmagn0[0:nijk]) async(1)
								#pragma acc parallel loop copyin(A0[0:n0],IV[0:ni]) present(Fmagn0[0:nijk]) async private(iion,n0)
								for( ijk = 0; ijk < nijk; ijk++) {
									int i = ijk % ni;
									int j = (ijk / nij) % ni;
									int k = ijk / nij;
									dft_real fr = 0.0;
									dft_real fi = 0.0;
									for( iion = 0; iion < n0; iion += 3 ) {
										dft_real a = A0[iion+0]*IV[i] + A0[iion+1]*IV[j] + A0[iion+2]*IV[k];
										fr += cos(a);
										fi += sin(a);
									}
									Fmagn0[ijk] = sqrt( fr*fr + fi*fi );
								} //Fmagn0 continues to live happily on GPU0
								//#pragma acc update host(Fmagn0[0:nijk]) async(1)
								//#pragma acc exit data delete(Fmagn0[0:nijk]) async(1)
							}
							//destroyed Fmagn0 on the device
							//pragma acc data copy allocates memory on GPU and copies data from host to GPU when entering
							//the region and copies the data to the host when exiting region
						}

						//necessary, because once the host has dispatched work to the GPU
						//it could do own work ##MK:: host needs to wait for the GPU to complete all asynchronously executed tasks on the default async queue
						#pragma acc wait
						//now that the host/master threads knows that the GPUs has populated Fmagn0 with the results we can search for the maximum value on Fmagn0

						hklval max0 = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
						if ( gpuvalid == true ) {
							for( int ijk = 0; ijk < nijk; ijk++ ) {
								if ( Fmagn0[ijk] < max0.val )
									continue;
								else
									max0 = hklval( WAIT_UNTIL_GLOBAL_EXTREMUM_FOUND, ijk, Fmagn0[ijk] );
							}
							max0.mpid = buffer_local_mpid_gpu.at(chk_gpu); //process-local material point ID, not global Material point ID!
							//free(A0);
							delete [] A0; A0 = NULL;
							//free(Fmagn0);
							delete [] Fmagn0; Fmagn0 = NULL;
						}
						//this is a process-local material point ID !, i.e. there are possibly as many 0-th local material points as there are MPI processes
						//use myworkload.at(max0.mpid).mpid to resolve the global material point ID
						//if max0.mpid is flagged with NORESULTS_FOR_THIS_MATERIALPOINT we can filter this out!
						myresults_buffer.push_back( max0 );

						////##MK::BEGIN DEBUG
						//#pragma omp critical
						//{
						//	cout << "Current epoch " << epoch << " on thread MASTER after pragma acc wait" << "\n";
						//	cout << "max0 " << max0 << "\n";
						//	if ( useboth == true )
						//		cout << "max1 " << max1 << "\n";
						//}
						//double gtoc = omp_get_wtime();
						//#pragma omp critical
						//{
						//	if ( useboth == true )
						//		cout << "Current epoch " << epoch << " on thread MASTER chk_gpu+0, chk_gpu+1 " << chk_gpu+0 << ";" << chk_gpu+1 << " two FT took " << (gtoc-gtic) << " seconds" << "\n";
						//	else
						//		cout << "Current epoch " << epoch << " on thread MASTER chk_gpu+0, chk_gpu+1 " << chk_gpu+0 << ";" << "xxxx" << " one FT took " << (gtoc-gtic) << " seconds" << "\n";
						//}
						////##MK::END DEBUG

						chk_gpu++;

						//bookkeeping
						mystatus.nchunks++;
						mystatus.ionsum = mystatus.ionsum + n0/3;

					} //next chk_gpu

					//free(IV);
					delete [] IV; IV = NULL;

					//double mmytoc = omp_get_wtime();
					//#pragma omp critical
					//{
					//	cout << "Current epoch " << epoch << " on thread MASTER async with " << wn.chunk_now_gpu << " on GPUs took " << (mmytoc-mmytic) << " seconds" << "\n";
					//}
				} //master done with its work
				else { //remainder non-master threads work asychronously on own direct Fourier transform
					//double mmytic = omp_get_wtime();
					//##MK::for now we can with this model execute only one chk on the CPUs per epoch
					//because as master execution path is different than of non-master threads we cannot invoke a #pragma omp barrier here
					//MK::we could dispatch as chks for each accelerator and proceed with OMP threading but typically accelerators are
					//faster and hence could process multiple chks while the thread team hasnt even complete one
					if ( (mt-1) < wn.chunk_now_cpu ) {
						//MK::the construct mt-1 works because the MASTER thread never enters this section so mt is always >= 1
						//MK::mt-1 possible because this section is only executed by non-master thread if such exist

						hklval cpumt1res = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
						size_t thisone = mt-1; //all threads contribute, if enough points left
						if ( buffer_cpu.at(thisone) != NULL ) {
							if ( buffer_cpu.at(thisone)->size() > 0 ) {

								dft_cpu solver = dft_cpu( ni );

								cpumt1res = solver.execute( *(buffer_cpu.at(thisone)) );

								cpumt1res.mpid = buffer_local_mpid_cpu.at(thisone); //process-local ID

								mystatus.ionsum = mystatus.ionsum + (buffer_cpu.at(thisone)->size() / 3);
								mystatus.nchunks++;
							}
						}
						myresults_buffer.push_back( cpumt1res );

						//double mmytoc = omp_get_wtime();
						//#pragma omp critical
						//{
						//	cout << "Current epoch " << epoch << " on thread/chk " << mt << " FT took " << (mmytoc-mmytic) << " seconds" << "\n";
						//}
					} //non-master threads done with processing the only cpu part of the current epoch
				} //non-master thread part
#endif
			} //handled case of asynchronous GPU + CPU parallel execution
			else {
				//handle multithreaded fallback in case there are no accelerators
				//#pragma omp critical
				//{
				//	cout << "Thread " << mt << " participates in multi-threaded fallback current epoch chunk_now_cpu " << wn.chunk_now_cpu << "\n";
				//}
				double mmytic = omp_get_wtime();

				if ( mt < wn.chunk_now_cpu ) { //possibly all threads contribute, if enough points left
					hklval cpumtres = hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
					size_t thisone = mt; //all threads contribute, if enough points left
					if ( buffer_cpu.at(thisone) != NULL ) { //if the ROI was queried
						if ( buffer_cpu.at(thisone)->size() > 0 ) { //and it contains coordinate values

							dft_cpu solver = dft_cpu( ni );

							cpumtres = solver.execute( *(buffer_cpu.at(thisone)) );

							cpumtres.mpid = buffer_local_mpid_cpu.at(thisone); //process-local ID

							mystatus.ionsum = mystatus.ionsum + (buffer_cpu.at(thisone)->size() / 3);
							mystatus.nchunks++;
						}
					}
					myresults_buffer.push_back( cpumtres );
				}

				double mmytoc = omp_get_wtime();
				#pragma omp critical
				{
					cout << "Current epoch " << epoch << " on thread/chk " << mt << " FT took " << (mmytoc-mmytic) << " seconds" << "\n";
				}
			} //done for the CPU only multi-threaded fallback

			//threads dump their results now resulting in only as many criticals as there are threads
			//rather than as many individual criticals as FT transforms per epoch
			#pragma omp critical
			{
				for( auto kyvl = myresults_buffer.begin(); kyvl != myresults_buffer.end(); kyvl++ ) {
					if ( kyvl->mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) {
						myhklval.at(kyvl->mpid) = *kyvl;
					}
				}
				//release thread-local buffer of collected results
				myresults_buffer = vector<hklval>();
			}

			mystatus.epoch = epoch;
			double mytoc = omp_get_wtime();
			mystatus.dt = mytoc-mytic;
			#pragma omp critical
			{
				epoch_diary.push_back( mystatus );
			}

			//a barrier is necessary, because we need to avoid that masters starts release buffers already while threads may still accumulate results
			#pragma omp barrier

			//clear the buffer and reinitialize for next use
			#pragma omp master
			{
				for( int chk_gpu = 0; chk_gpu < wn.chunk_now_gpu; chk_gpu++ ) {
					if ( buffer_gpu.at(chk_gpu) != NULL ) {
						delete buffer_gpu.at(chk_gpu);
						buffer_gpu.at(chk_gpu) = NULL; //necessary to prepare for reusage
					}
				}
				for( int chk_cpu = 0; chk_cpu < wn.chunk_now_cpu; chk_cpu++ ) {
					if ( buffer_cpu.at(chk_cpu) != NULL ) {
						delete buffer_cpu.at(chk_cpu);
						buffer_cpu.at(chk_cpu) = NULL; //necessary to prepare for reusage
					}
				}
			} //no implicit barrier as were about to exit parallel region with implicit barrier anyway

		} //end of parallel region within the while loop for the current epoch

		mympid = mympid + wn.chunk_now_gpu + wn.chunk_now_cpu; //advance to begin a new epoch

		double toc = MPI_Wtime();
		epoch_wallclock.push_back( epoch_prof( epoch, toc-tic ) );

		cout << "Current epoch " << epoch << " with a total of " << wn.chunk_now_gpu + wn.chunk_now_cpu << " completed took " << (toc-tic) << " seconds" << "\n";
		epoch++;
	} //end of the while loop

	double global_toc = MPI_Wtime();
	memsnapshot mm = fourier_tictoc.get_memoryconsumption();
	fourier_tictoc.prof_elpsdtime_and_mem( "QueryROIsAndPerformDirectFourierTransforms", APT_XX, APT_IS_PAR, mm, global_tic, global_toc);

	cout << "Rank " << get_myrank() << " the direct FT took " << (global_toc-global_tic) << " seconds" << "\n";

	sort( epoch_diary.begin(), epoch_diary.end(), SortEpochLogAsc );

	debug_epoch_profiling( epoch_diary, epoch_wallclock );
}
*/


int indexerHdl::get_myrank()
{
	return this->myrank;
}


int indexerHdl::get_nranks()
{
	return this->nranks;
}


void indexerHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void indexerHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void indexerHdl::init_mpidatatypes()
{
	//define MPI datatype which assists us to logically structure the communicated data packages
	MPI_Type_contiguous(4, MPI_FLOAT, &MPI_Quaternion_Type);
	MPI_Type_commit(&MPI_Quaternion_Type);

	//commit utility data types to simplify and collate data transfer operations
	/*int cnts1[2] = {4, 2};
	MPI_Aint dsplc1[2] = {0, 4 * 4}; //4 B for a float
	MPI_Datatype otypes1[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, cnts1, dsplc1, otypes1, &MPI_Fourier_ROI_Type);
	MPI_Type_commit(&MPI_Fourier_ROI_Type);*/
}

