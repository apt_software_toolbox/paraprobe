//##MK::CODE

#ifndef __PARAPROBE_CONFIG_INDEXER_H__
#define __PARAPROBE_CONFIG_INDEXER_H__

#include "../../paraprobe-utils/src/CONFIG_Shared.h"
#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"


enum E_ANALYSIS_MODE {
	E_NOTHING,
	EVALUATE_DISORIENTATIONS,
	CREATE_REFERENCEIMAGE,
	PERFORM_INDEXING
};


class ConfigIndexer
{
public:
	
	static E_ANALYSIS_MODE AnalysisMode;
	static string InputfileAraullo;
	static string InputfileRefImages;
	static string InputfileTestOris;
	
	static unsigned int NumberOfS2Directions;

	static unsigned int IOUpToKthClosest;
	static apt_real IOSymmRedDisoriAngle;
	
	static bool IOSolutionQuality;
	static bool IODisoriMatrix;
	static bool IOSymmReduction;
	
	static apt_real IOSymmRedMaxDisoriAngle;
	static unsigned int NumberOfSO3Candidates;

	static int GPUsPerNode;
	static int GPUWorkload;
		
	static bool readXML( string filename = "" );
	static bool checkUserInput();
};

#endif
