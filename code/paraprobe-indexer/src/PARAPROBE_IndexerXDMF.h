//##MK::GPLV3

#ifndef __PARAPROBE_INDEXER_XDMF_H__
#define __PARAPROBE_INDEXER_XDMF_H__

#include "PARAPROBE_IndexerHDF5.h"

class indexer_xdmf : public xdmfHdl
{
	//tool-specific sub-class of a XDMF inheriting all methods of the base class h5Hdl
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	indexer_xdmf();
	~indexer_xdmf();

	/*
	int create_materialpoint_file( const string xmlfn, const size_t nmp, const string h5ref );
	*/

//private:
};

#endif
