//##MK::CODESPLIT


#ifndef __PARAPROBE_INDEXER_HDL_H__
#define __PARAPROBE_INDEXER_HDL_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_IndexerUtils.h"


class indexerHdl
{
	//process-level class which implements the worker instance which performs the indexing
	//using OpenMP and OpenACC i.e. CPU and GPUs
	//specimens result are written to a specific HDF5 files

public:
	indexerHdl();
	~indexerHdl();
	
	bool read_testoris_from_h5();

	bool broadcast_testoris();
	
	void evaluate_disorientations();

	/*
	void spatial_decomposition();
	void define_matpoint_volume_grid();
	void distribute_matpoints_on_processes();

	gpu_cpu_max_workload plan_max_per_epoch( const int ngpu, const int nthr, const int nmp );
	gpu_cpu_now_workload plan_now_per_epoch( const int ngpu, const int nthr, const int nmp, const int mp, const gpu_cpu_max_workload mxl );
	//void generate_debug_roi( mt19937 & mydice, vector<dft_real> * out );
	void query_ions_within_roi( mp3d const & roicenter, vector<dft_real> * out );
	void debug_epoch_profiling( vector<epoch_log> const & in, vector<epoch_prof> const & timing );
	void execute_local_workpackage();
	void execute_local_workpackage2();
		
	bool init_target_file();
	bool write_materialpoints_to_apth5();
	bool collect_results_on_masterprocess();
	bool write_results_to_apth5();
	*/
	
	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();

	/*
	decompositor sp;
	volsampler mp;
	vector<int> mp2rk;								//mapping of material points => to ranks,
	vector<MPI_Fourier_ROI> mproi;					//only significant for MASTER to organize results writing
	vector<MPI_Fourier_HKLValue> mpres;				//only significant for MASTER to organize results writing
	*/
	
	h5Hdl inputTestOrisH5Hdl;
	h5Hdl inputAraulloH5Hdl;
	h5Hdl inputRefImgsH5Hdl;
	indexer_h5 debugh5Hdl;
	//indexer_xdmf debugxdmf;

	vector<squat> testoris;					//test rotations, interpreted first as active rotations 
											//used to rotate #### measured against reference
	
	/*
	vector<p3d> xyz;
	vector<p3dm1> xyz_ityp;					//##MK::temporarily until proper ion type handling implemented
	vector<tri3d> triangle_hull;
	vector<apt_real> dist2hull;
	*/
	//rangeTable rng;
	/*
	vector<mp3d> myworkload;				//the material points of the process
	vector<hklval> myhklval;				//##MK::DEBUG for now the actual data of interest for science
	*/
	
	profiler indexer_tictoc;

private:
	//MPI related
	int myrank;								//my MPI ID in the MPI_COMM_WORLD
	int nranks;

	MPI_Datatype MPI_Quaternion_Type;
};


#endif

