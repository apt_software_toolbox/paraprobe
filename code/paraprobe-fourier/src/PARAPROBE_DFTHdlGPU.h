//##MK::CODESPLIT


#ifndef __PARAPROBE_FOURIER_DFTHDLGPU_H__
#define __PARAPROBE_FOURIER_DFTHDLGPU_H__

#include "PARAPROBE_DFTHdlCPU.h"

#define SINGLE_GPU_WORKLOAD					10

//MK::(de/)activate GPUs for easier development purposes, easier because will not force to have MPI+OMP+OPENACC capable compiler
#define UTILIZE_GPUS

#ifdef UTILIZE_GPUS

//using OpenACC
#include <openacc.h>
#include <accelmath.h>

//##MK::CONSIDER AS OBSOLETE
/*
class dft_gpu
{
	//the class is a utility tool it does the actual direct Fourier transform on the CURRENTLY ACTIVE GPU INSTANCE
public:
	dft_gpu();
	dft_gpu( const size_t n );
	~dft_gpu();
	
	hklspace_res execute( vector<dft_real> const & p );

	dft_real* ival;					//Fourier space grid position 1d of a 3d cube hkl <=> ijk
	int NI;							//Fourier space number of voxel 1d
	int NIJ;
	int NIJK;
	int DeviceID;					//logical ID of the GPU instance
	int ASyncQueue;
};
*/

#endif


#endif
