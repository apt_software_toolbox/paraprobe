//##MK::CODESPLIT

#include "PARAPROBE_DFTHdlCPU.h"

ostream& operator << (ostream& in, hklval const & val) {
	in << val.mpid << "\t\t" << val.ijk << "\t\t" << val.val;
	return in;
}


ostream& operator<<(ostream& in, hklspace_res const & val)
{
	//in << "\n" << val.h << ";" << val.k << ";" << val.l << "\t\t" << val.SQRIntensity << "\n";
	in << val.h << ";" << val.k << ";" << val.l << "\t\t" << val.SQRIntensity;
	return in;
}


dft_cpu::dft_cpu()
{
	Fmagn = NULL;
	ival = NULL;
	NI = 0;
	NIJ = 0;
	NIJK = 0;
}


dft_cpu::dft_cpu( const size_t n )
{
	NI = n;
	NIJ = n*n;
	NIJK = n*n*n;
	Fmagn = NULL;
	ival = NULL;
	try {
		//Fmagn = (dft_real*) calloc( NIJK, sizeof(dft_real) );
		Fmagn = new dft_real[NIJK];
		for( int ijk = 0; ijk < NIJK; ijk++ )
			Fmagn[ijk] = 0.f;

		//ival = (dft_real*) calloc( NI, sizeof(dft_real) );
		ival = new dft_real[NI];
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of ival and/or Fmagn failed" << "\n";
		return;
	}
	dft_real bound = 2.0;
	dft_real start = -bound; //-1.0;
	dft_real stop = +bound; //+1.0;
	dft_real num = static_cast<dft_real>(NI-1);
	dft_real step = (stop-start)/num;
	for( int i = 0; i < NI; ++i ) {
		ival[i] = TWO_PI * (start + static_cast<dft_real>(i)*step);
	}
}

dft_cpu::~dft_cpu()
{
	//free(Fmagn); Fmagn = NULL;
	delete [] Fmagn; Fmagn = NULL;
	//free(ival); ival = NULL;
	delete [] ival; ival = NULL;
	NI = 0;
	NIJ = 0;
	NIJK = 0;
}


hklval dft_cpu::execute( vector<dft_real> const & p )
{
	if ( p.size() > 0 ) {
		//MK::we can save time and not clear Fmagn values because we always start from fr and fi = 0.0 ## ?
		int iion = 0;
		int ni = static_cast<int>(p.size());
		hklval report = hklval();
		for( int k = 0; k < NI; k++ ) {
			dft_real ival_k = ival[k];
			for ( int j = 0; j < NI; j++ ) {
				dft_real ival_j = ival[j];
				for ( int i = 0; i < NI; i++ ) {
					//sliced clear
					//Fhkl_cpu[ijk+0] = 0.0;
					//Fhkl_cpu[ijk+1] = 0.0;
					//Fmagn_cpu[ijk] = 0.0;
					//reciprocal space coordinate
					dft_real ival_i = ival[i];
					dft_real fr = 0.0;
					dft_real fi = 0.0;

					for( iion = 0; iion < ni; iion += 3 ) { //most compute intensive part
						dft_real a = p[iion+0] * ival_i + p[iion+1] * ival_j + p[iion+2] * ival_k;
						fr += cos(a);
						fi += sin(a);
					}
					
					//sliced updating
					int ijk = i + j*NI + k*NIJ;
					Fmagn[ijk] = sqrt( fr*fr + fi*fi );
					if ( Fmagn[ijk] < report.val ) { //most likely not hitting the maximum
						continue;
					}
					else {
						report = hklval( WAIT_UNTIL_GLOBAL_EXTREMUM_FOUND, ijk, Fmagn[ijk] );
					}
				} //i
			} //l
		} //k

		//##MK::current way of reporting find position in Fourier space with highest intensity
		return report;
	}
	return hklval( NORESULTS_FOR_THIS_MATERIALPOINT, 0, 0.0 );
}


void dft_cpu::reset()
{
	for( int i = 0; i < NIJK; ++i ) {
		Fmagn[i] = 0.0;
	}
}
