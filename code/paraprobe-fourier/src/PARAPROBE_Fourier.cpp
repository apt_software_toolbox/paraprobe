//##MK::CODESPLIT

#include "PARAPROBE_FourierHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

//##MK::for querying system maximum physical memory see
//https://stackoverflow.com/questions/2513505/how-to-get-available-memory-c-g


bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigFourier::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigFourier::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void many_local_direct_fourier_transforms_on_reconstruction( const int r, const int nr )
{
	//allocate process-level instance of a fourierHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	fourierHdl* fourier = NULL;

	int localhealth = 1;
	int globalhealth = nr;
	try {
		fourier = new fourierHdl;
		fourier->set_myrank(r);
		fourier->set_nranks(nr);
		fourier->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " unable to allocate a sequential surfacerHdl instance" << "\n"; localhealth = 0;
	}

	//do we have all processes on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete fourier; fourier = NULL; return;
	}
	
	//we have all on board, read reconstruction and triangle hull
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast, we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( fourier->get_myrank() == MASTER ) {
		if ( fourier->read_reconxyz_from_apth5() == true ) {
			cout << "MASTER read successfully all ion coordinates of the specimen in reconstruction space" << "\n";
		}
		else {
			cerr << "MASTER was unable to read a specimen in reconstruction space!" << "\n"; localhealth = 0;
		}
		if ( ConfigFourier::SamplingGridRemoveBndPoints == true ) {
			if ( fourier->read_triangle_hull_from_apth5() == true ) {
				cout << "MASTER read successfully all triangles of the dataset boundary!" << "\n";
			}
			else {
				cerr << "MASTER was unable to read triangle hull!" << "\n"; localhealth = 0;
			}
		}
		else {
			cerr << "WARNING SamplingGridRemoveBndPoints is currently not implemented!" << "\n";
		}
	}
	//else slaves processes wait
	MPI_Barrier( MPI_COMM_WORLD );
	
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that all data have arrived!" << "\n";
		delete fourier; fourier = NULL; return;
	}
	
	if ( fourier->broadcast_reconxyz() == true ) {
		cout << "Rank " << r << " has synchronized reconxyz" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize reconxyz!" << "\n"; localhealth = 0;
	}
	if ( fourier->broadcast_triangles() == true ) {
		cout << "Rank " << r << " has synchronized triangles" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize triangle hull!" << "\n"; localhealth = 0;
	}
	if ( fourier->broadcast_distances() == true ) {
		cout << "Rank " << r << " has synchronized distances" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize distances!" << "\n"; localhealth = 0;
	}

	//##MK::for now we do not account for ion types but paraprobe-utils defines methods for p3dm1 type only so we need to
	//replace at least for now and p3d type xyz with p3dm1 type xyz array
	try {
		fourier->xyz_ityp.reserve( fourier->xyz.size() );
		for( auto it = fourier->xyz.begin(); it != fourier->xyz.end(); it++ ) {
			fourier->xyz_ityp.push_back( p3dm1( it->x, it->y, it->z, UNKNOWN_IONTYPE ) );
		}
		fourier->xyz = vector<p3d>();
		cout << "Rank " << r << " xyz_ityp.size() " << fourier->xyz_ityp.size() << "\n";
	}
	catch (bad_alloc &mecroak) {
		cerr << "Temporary replacement of xyz by xyz_ityp had allocation error!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized the reconstruction and triangle hull!" << "\n";
		delete fourier; fourier = NULL; return;
	}
	
	//now processes can proceed and process massively parallel and independently
	//MK::each process computes the same grid and sets the same deterministic a priori known work partitioning for now

	fourier->spatial_decomposition();

	fourier->define_matpoint_volume_grid();
	
	fourier->distribute_matpoints_on_processes();
	
	fourier->execute_local_workpackage2();
	
	cout << "Rank " << r << " has completed its workpackage" << "\n";
	MPI_Barrier(MPI_COMM_WORLD);
	
	
	/*//##MK::a simple outputting for debugging purposes
	for( int rk = MASTER; rk < nr; rk++ ) {
		if ( r == rk ) {
			cout << "DEBUG results from rank " << rk << "\n";
			for( auto it = fourier->myhklval.begin(); it != fourier->myhklval.end(); it++ ) {
				unsigned int local_mpid = it->mpid; //process-local ID
				if ( local_mpid != NORESULTS_FOR_THIS_MATERIALPOINT ) {
					unsigned int global_mpid = fourier->myworkload.at(local_mpid).mpid;
					unsigned int ions_in_roi = fourier->myworkload.at(local_mpid).nions;
					cout << global_mpid << "\t\t" << it->ijk << "\t\t" << it->val << "\t\t" << ions_in_roi << "\n";
				}
				else {
					cout << "Rank " << r << " there is a ROI with no results" << "\n";
				}
			}
		}
		MPI_Barrier( MPI_COMM_WORLD );
	}*/

	double ttic = MPI_Wtime();

	if ( fourier->get_myrank() == MASTER ) {
		if ( fourier->init_target_file() == true ) {
			cout << "Rank MASTER successfully initialized APTH5 results file" << "\n";
		}
		else {
			cerr << "Rank MASTER unable to initialize results APTH5 file" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized after generation of the target file!" << "\n";
		delete fourier; fourier = NULL;
		return;
	}

	//##MK::currently, in this proof-of-concept we just report the extremum value positions of results in Fourier space
	//this is small enough to collect on the master process and do sequential I/O
	//##MK::for really storing all Fourier grid results for every material point, which are crazy many, take e.g. 100^3 cube
	//already 4MB per material point, so if 10 million material points, 40 TB!
	//##MK::in such future use case we need to employ the parallel HDF5 library and let the process write the results in parallel
	double ttoc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	fourier->fourier_tictoc.prof_elpsdtime_and_mem( "InitializeAPTH5ResultsFile", APT_XX, APT_IS_SEQ, mm, ttic, ttoc);

	MPI_Barrier( MPI_COMM_WORLD ); //##MK::might be reduced to a single one see above barrier...

	if ( fourier->collect_results_on_masterprocess() == true ) {
		if ( r == MASTER ) {
			cout << "Rank MASTER successfully collected all proof-of-concept results on the MASTER MPI process" << "\n";
		}
	}
	else {
		cerr << "Rank " << r << " unable to collect proof-of-concept results on MASTER MPI process!" << "\n";
		delete fourier; return;
	}

	if ( fourier->write_materialpoints_to_apth5() == true ) { //does not require MPI communication all results already on the master
		if ( r == MASTER ) {
			cout << "Rank MASTER successfully wrote all materialpoints to an APTH5 file" << "\n";
		}
	}
	else {
		cerr << "Unable to write all materialpoints to the APTH5 file" << "\n";
		delete fourier; fourier = NULL; return;
	}

	if ( fourier->write_results_to_apth5() == true ) {
		if ( r == MASTER ) {
			cout << "Rank MASTER successfully wrote all materialpoint-related results to an APTH5 file" << "\n";
		}
	}
	else {
		cerr << "Rank " << r << " unable to write all materialpoint-related reults to the APTH5 file" << "\n";
		delete fourier; fourier = NULL; return;
	}

	fourier->fourier_tictoc.spit_profiling( "Fourier", ConfigShared::SimID, fourier->get_myrank() );

	//release resources
	delete fourier; fourier = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
	
//for this MPI/OMP/OpenACC, i.e. process/thread/accelerator heterogeneous program not every combination of mpiexec -n, OMP_NUM_THREADS and GPUs is admissible
	//##MK::if we dont want to use GPUs at all possible
	//##MK::current implementation initiates one MPI process per GPU on the node
	//##MK::problem is that the number of MPI processes per computing node is typically decided

	bool validsettings = true;
#ifdef UTILIZE_GPUS
	int devCount = acc_get_num_devices(acc_device_nvidia);
	if ( devCount < ConfigFourier::GPUsPerNode ) {
		cerr << "Rank " << r << " there are only " << devCount << " GPUs on the node but user wanted " << ConfigFourier::GPUsPerNode << "\n";
		validsettings = false;
	}
	else { //devCount >= GPUsPerNode
		//number of processes needs to be integer dividable through number of gpus
		if ( ConfigFourier::GPUsPerNode > 0 ) {
			//MPI_Get_processor name algorithm to check on how many disjoint nodes the program instances are distributed
			//if nr > ConfigFourier::GPUsPerNode*NumberOfNodesInUse == validsettings = false;
			if ( (nr % ConfigFourier::GPUsPerNode) == 0 ) {
				//MK::e.g. 1 rank but 2 gpus: forbidden, 3 ranks but 2 gpus: forbidden, 4 ranks 2 gpus: allowed
				//##MK::2 processes per node, each using one gpu, 2 two such CPU/GPU pairs across the team of two nodes used
				//requires batch queue system variables like slurm ntasks-per-node=2 to be set properly
				cout << "GPUs will be utilized and number of MPI processes is an integer multiple of amount of GPUs planned" << "\n";
			}
			else {
				cerr << "GPUs should be used but number of MPI processes is not an integer multiple of GPUsPerNode!" << "\n";
				validsettings = false;
			}
		}
		//else{} GPUs exist but should not be used, setting remain valid will use sequential fallback!
	}
#endif

	if ( validsettings == true ) {
//EXECUTE SPECIFIC TASK
		many_local_direct_fourier_transforms_on_reconstruction( r, nr );
	}
	
//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	double toc = omp_get_wtime();
	
	cout << "paraprobe-fourier took " << (toc-tic) << " seconds wall-clock time in total" << endl;
	return 0;
}
