//##MK::CODE

#include "CONFIG_Fourier.h"

E_SAMPLING_GRID_MODE ConfigFourier::SamplingGridMode = E_POINT_GRID_REGULAR;
string ConfigFourier::InputfileReconstruction;
string ConfigFourier::InputfileHullAndDistances;
//string ConfigFourier::Outputfile;

apt_real ConfigFourier::ROIRadiusMax = 0.f;
apt_real ConfigFourier::SamplingGridBinWidthX = 0.f;
apt_real ConfigFourier::SamplingGridBinWidthY = 0.f;
apt_real ConfigFourier::SamplingGridBinWidthZ = 0.f;
unsigned int ConfigFourier::FourierGridResolution = 256;
int ConfigFourier::GPUsPerNode = 0;
int ConfigFourier::GPUWorkload = 10; //##MK::almost sure not optimal but sweep spot which number is optimal is case dependent, therefore here the possibility to do systematic studies

bool ConfigFourier::SamplingGridRemoveBndPoints = false;


bool ConfigFourier::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Input.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigFourier")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "SamplingGridPointType" );
	switch (mode)
	{
		case E_POINT_GRID_REGULAR:
			SamplingGridMode = E_POINT_GRID_REGULAR; break;
		//##MK::
		default:
			SamplingGridMode = E_NOTHING;
	}
	InputfileReconstruction = read_xml_attribute_string( rootNode, "InputfileReconstruction" );
	InputfileHullAndDistances = read_xml_attribute_string( rootNode, "InputfileHullAndDistances" );
	//Outputfile = read_xml_attribute_string( rootNode, "Outputfile" );
	
	ROIRadiusMax = read_xml_attribute_float( rootNode, "CrystalloRadiusMax" );
	if ( SamplingGridMode == E_POINT_GRID_REGULAR ) {
		SamplingGridBinWidthX = read_xml_attribute_float( rootNode, "SamplingGridBinWidthX" );
		SamplingGridBinWidthY = read_xml_attribute_float( rootNode, "SamplingGridBinWidthY" );
		SamplingGridBinWidthZ = read_xml_attribute_float( rootNode, "SamplingGridBinWidthZ" );
	}

	FourierGridResolution = read_xml_attribute_uint32( rootNode, "FourierGridResolution" );

	GPUsPerNode = read_xml_attribute_int32( rootNode, "GPUsPerComputingNode" );
	GPUWorkload = read_xml_attribute_int32( rootNode, "GPUWorkload" );
	if ( GPUWorkload < 0 || GPUWorkload > 10000 ) {
		cout << "WARNING: The GPU workload has to be at least 1, the user input was errorneous, so I resetted to 10!" << "\n";
		GPUWorkload = 10;
	}
	
	SamplingGridRemoveBndPoints = false;
	return true;
}


bool ConfigFourier::checkUserInput()
{
	cout << "ConfigFourier::" << "\n";
	switch(SamplingGridMode)
	{
		case E_POINT_GRID_REGULAR:
			cout << "Building an axis-aligned z-axis centered cuboidal 3d grid of material sampling points" << "\n"; break;
		default:
			cerr << "Unknown SamplingGridPointType chosen" << "\n";	return false;
	}
	if ( ROIRadiusMax < EPSILON ) {
		cerr << "ROIRadiusMax must be positive and finite!" << "\n"; return false;
	}
	cout << "ROIRadiusMax " << ROIRadiusMax << "\n";
	if ( SamplingGridMode == E_POINT_GRID_REGULAR ) {
		if ( SamplingGridBinWidthX < EPSILON || 
				SamplingGridBinWidthY < EPSILON || 
					SamplingGridBinWidthZ < EPSILON ) {
			cerr << "All SamplingGridBinWidths need to be positive and finite!" << "\n"; return false;
		}
		cout << "SamplingGridBinWidthX " << SamplingGridBinWidthX << "\n";
		cout << "SamplingGridBinWidthY " << SamplingGridBinWidthY << "\n";
		cout << "SamplingGridBinWidthZ " << SamplingGridBinWidthZ << "\n";
	}
	if ( FourierGridResolution < 32 || FourierGridResolution > 1024 ) {
		//##MK::currently Fourier space resolution is limited by internal choice in DFTHdlCPU and DFTHdlGPU to use int32 as Fourier space position iterators
		cerr << "FourierGridResolution must be within interval [32, 4096] hkl grid points!" << "\n";
		return false;
	}
	cout << "FourierGridResolution " << FourierGridResolution << "\n";
	cout << "GPUsPerComputingNode " << GPUsPerNode << "\n";
	if ( GPUWorkload < 1 ) {
		cerr << "GPUWorkloadFactor needs to be integer and at least 1!" << "\n";
		cerr << "To delegate all work to CPUs instead set GPUsPerComputingNode to 0!" << "\n"; return false;
	}
	cout << "GPUWorkloadFactor " << GPUWorkload << "\n";
	
	cout << "InputReconstruction read from " << InputfileReconstruction << "\n";
	cout << "InputHullsAndDistances read from " << InputfileHullAndDistances << "\n";
	//cout << "Output written to " << Outputfile << "\n";
	
	if ( SamplingGridRemoveBndPoints == true ) {
		cout << "Material points outside the inside voxel or within closer than ROIRadiusMax to the triangle hull are discarded!" << "\n";
	}
	else {
		cout << "WARNING finite dataset boundary affects are not accounted for so some sampling points will have insignificant ion support!" << "\n";
	}
	
	cout << "\n";
	return true;
}

