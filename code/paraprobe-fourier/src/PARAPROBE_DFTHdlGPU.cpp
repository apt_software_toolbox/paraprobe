//##MK::CODESPLIT

#include "PARAPROBE_DFTHdlGPU.h"

#ifdef UTILIZE_GPUS

/*

dft_gpu::dft_gpu()
{
	ival = NULL;
	NI = 0;
	NIJ = 0;
	NIJK = 0;
	DeviceID = 0;
	ASyncQueue = 0;
}


dft_gpu::dft_gpu( const size_t n )
{
	NI = n;
	NIJ = n*n;
	NIJK = n*n*n;
	try {
		ival = (dft_real*) calloc( NI, sizeof(dft_real) );
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of ival failed" << "\n";
		//ival = NULL;
		return;
	}
	dft_real bound = 2.0;
	dft_real start = -bound; //-1.0;
	dft_real stop = +bound; //+1.0;
	dft_real num = static_cast<dft_real>(NI-1);
	dft_real step = (stop-start)/num;
	for( size_t i = 0; i < NI; ++i ) {
		ival[i] = TWO_PI * (start + static_cast<dft_real>(i)*step);
	}
	DeviceID = 0;
	ASyncQueue = 0; //default
}


dft_gpu::~dft_gpu()
{
	free(ival);
	NI = 0;
	NIJ = 0;
	NIJK = 0;
	DeviceID = 0;
	ASyncQueue = 0;
}


hklspace_res dft_gpu::execute( vector<dft_real> const & p )
{
	//##MK::DONT FORGET TO HAVE EXECUTED
	//MK::TALOS
	//acc_set_device_num(0, acc_device_nvidia); for GPU0 
	//acc_set_device_num(1, acc_device_nvidia); for GPU1 respectively
	
	if ( p.size() > 0 && p.size() < INT32MX ) {
		//MK::we can save time and not clear Fmagn values because we always start from fr and fi = 0.0 ## ?
		
		int n = static_cast<int>(p.size());
		int ijk = 0;
		int iion = 0;
		int ni = NI;
		int nij = NIJ;
		int nijk = NIJK;
		
		//layout coordinate values in p flat in memory before GPU transfer
		dft_real* restrict const A = (dft_real*) calloc( p.size(), sizeof(dft_real) );
		for( size_t i = 0; i < p.size(); ++i ) {
			A[i] = p[i];
		}

		//magnitude of the Fourier space intensity
		dft_real* restrict const Fmagn = (dft_real*) calloc( NIJK, sizeof(dft_real) );

		int hh = 0;
		int kk = 0;
		int ll = 0;
		dft_real vv = 0.0;
		//instantiate datastructures on the device and transfer data from allocated array onto device
		#pragma acc enter data create(hh)
		#pragma acc enter data create(kk)
		#pragma acc enter data create(ll)
		#pragma acc enter data create(vv)
		#pragma acc update device(hh)
		#pragma acc update device(kk)
		#pragma acc update device(ll)
		#pragma acc update device(vv)		
		#pragma acc enter data copyin(Fmagn[0:nijk*1]) async(DeviceID)
		#pragma acc parallel loop copyin(A[0:ni],ival[0:ni]) present(Fmagn[0:nijk*1]) async(DeviceID) private(iion,n)
		for( ijk = 0; ijk < nijk; ijk++) { //execute direct Fourier transform kernel on the accelerator device
			int i = ijk % ni;
			int j = (ijk / nij) % ni;
			int k = ijk / nij;
			dft_real fr = 0.0;
			dft_real fi = 0.0;
			for( iion = 0; iion < n; iion += 3 ) {
				dft_real a = A[iion+0]*ival[i] + A[iion+1]*ival[j] + A[iion+2]*ival[k];
				fr += cos(a);
				fi += sin(a);
			}
			Fmagn[ijk] = sqrt( fr*fr + fi*fi );
			if ( Fmagn[ijk] < vv ) {
				continue;
			}
			else { //found a local maximum, maybe also a global...
				hh = i;
				kk = j;
				ll = k;
				vv = Fmagn[ijk];
			}
		} //done on the accelerator device
		//update explicitly the global maximum and position on host with values on the device
		#pragma acc update host(hh)
		#pragma acc update host(kk)
		#pragma acc update host(ll)
		#pragma acc update host(vv)

		//release resources
		free(Fmagn);
		free(A);
		
		//##MK::current way of reporting find position in Fourier space with highest intensity
		return hklspace_res( hh, kk, ll, vv );
	}
	
	return hklspace_res(); 
}
*/

#endif
