//##MK::CODESPLIT


#ifndef __PARAPROBE_FOURIER_DFTHDLCPU_H__
#define __PARAPROBE_FOURIER_DFTHDLCPU_H__

//#include "PARAPROBE_VolumeSampler.h"
#include "../../paraprobe-utils/src/PARAPROBE_VolumeSampler.h"
#include "PARAPROBE_FourierTicToc.h"

typedef float dft_real;
//#define SINGLEGPUWORKLOAD					10

#define NORESULTS_FOR_THIS_MATERIALPOINT	INT32MX
#define WAIT_UNTIL_GLOBAL_EXTREMUM_FOUND	INT32MX

struct hklval
{
	int mpid;
	int ijk;
	dft_real val;
	hklval() : mpid(NORESULTS_FOR_THIS_MATERIALPOINT), ijk(INT32MX), val(0.0) {}
	hklval( const int _mpid, const int _ijk, const dft_real _val ) :
		mpid(_mpid), ijk(_ijk), val(_val) {}
};

ostream& operator << (ostream& in, hklval const & val);



struct hklspace_res
{
	dft_real h;
	dft_real k;
	dft_real l;
	dft_real SQRIntensity;
	hklspace_res() : h(0.0), k(0.0), l(0.0), SQRIntensity(0.0) {}
	hklspace_res( const dft_real _h, const dft_real _k, const dft_real _l, const dft_real _sqr ) :
		h(_h), k(_k), l(_l), SQRIntensity(_sqr) {}
};

ostream& operator << (ostream& in, hklspace_res const & val);


class dft_cpu
{
	//the class is a utility tool it does the actual direct Fourier transform on the CPU
public:
	dft_cpu();
	dft_cpu( const size_t n );
	~dft_cpu();
	
	hklval execute( vector<dft_real> const & p );
	void reset();
		
private:
	dft_real* Fmagn;				//magnitude of the Fourier space intensity
	dft_real* ival;					//Fourier space grid position 1d of a 3d cube hkl <=> ijk
	int NI;							//Fourier space number of voxel 1d
	int NIJ;
	int NIJK;	
};


#endif
