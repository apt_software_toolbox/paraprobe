//##MK::CODE

#ifndef __PARAPROBE_CONFIG_FOURIER_H__
#define __PARAPROBE_CONFIG_FOURIER_H__

#include "../../paraprobe-utils/src/CONFIG_Shared.h"
#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"


enum E_SAMPLING_GRID_MODE {
	E_NOTHING,
	E_POINT_GRID_REGULAR,
	E_POINT_RANDOM_DENSITY_BASED,
	E_POINT_RANDOM_COUNT_BASED,
	E_POINT_PREDEFINED_LIST
};


class ConfigFourier
{
public:
	
	static E_SAMPLING_GRID_MODE SamplingGridMode;
	static string InputfileReconstruction;
	static string InputfileHullAndDistances;
	//static string Outputfile;
	
	static apt_real ROIRadiusMax;
	static apt_real SamplingGridBinWidthX;
	static apt_real SamplingGridBinWidthY;
	static apt_real SamplingGridBinWidthZ;
	static unsigned int FourierGridResolution;
	static int GPUsPerNode;
	static int GPUWorkload;
	
	static bool SamplingGridRemoveBndPoints;

	
	//static unsigned int WorkloadPerProcess;
	//static unsigned int WorkloadPerThread;
		
	static bool readXML( string filename = "" );
	static bool checkUserInput();
};

#endif
