cmake_minimum_required(VERSION 3.10)

################################################################################################################################
##USER INTERACTION##############################################################################################################
##in this section modifications and local paths need to be modified by the user#################################################
################################################################################################################################
#PROJECTNAME
#please name your project accordingly
set(MYPROJECTNAME "paraprobe_fourier")

#tell the top directory where this local PARAPROBE version is stored
##MAWS30
set(MYPROJECTPATH "/home/m.kuehbach/GITHUB/MPIE_APTFIM_TOOLBOX/paraprobe/code/")
##TALOS
##set(MYPROJECTPATH "/talos/scratch/mkuehbac/MPIE_APTFIM_TOOLBOX/paraprobe/code/")

set(MYUTILSPATH "${MYPROJECTPATH}/paraprobe-utils/build/CMakeFiles/paraprobe-utils.dir/src/")
set(MYTOOLPATH "${MYPROJECTPATH}/paraprobe-fourier/")

#LIBRARIES
#necessary
set(EMPLOY_MYHDF5 ON)
##CUDA and PGI ##########################################################

#optional
set(EMPLOY_JEMALLOC OFF)

#locations
##MAWS30
set(MYHDFPATH "/home/m.kuehbach/APT3DOIM/src/thirdparty/HDF5/CMake-hdf5-1.10.2/build/HDF5-1.10.2-Linux/HDF_Group/HDF5/1.10.2")
##TALOS
##set(MYHDFPATH "/talos/u/mkuehbac/HDF5/CMake-hdf5-1.10.2/build/HDF5-1.10.2-Linux/HDF_Group/HDF5/1.10.2")

##set(MYJEMALLOCPATH "${MYPROJECTPATH}/paraprobe-utils/src/thirdparty/Jemalloc/jemalloc-5.1.0")

	
#COMPILER, choose which one to use
set(EMPLOY_INTELCOMPILER OFF)
set(EMPLOY_GNUCOMPILER OFF)
set(EMPLOY_PGICOMPILER ON) #only for GPU/CPU stuff

#PARALLELIZATION LAYERS
set(EMPLOY_PARALLELISM_MPI ON)
set(EMPLOY_PARALLELISM_OMP ON)
set(EMPLOY_PARALLELISM_OPENACC ON)


#choose optimization level
##-O0 nothing, debug purposes, -O1 moderate optimization, -O2 -O3 for production level up to aggressive architecture specific non-portable optimization
if(EMPLOY_INTELCOMPILER)
	set(MYOPTLEVEL "-O0 -g") #"-O3 -g -march=skylake") #"-O0 -g"
endif()
if(EMPLOY_GNUCOMPILER)
	set(MYOPTLEVEL "-O3 -march=native")
endif()
if(EMPLOY_PGICOMPILER)
	#set(MYOPTLEVEL "-O0 -pg")
	##set(MYOPTLEVEL "-O2 -pg -tp=haswell-64 -acc -ta=tesla:managed -Minfo=accel,mp") #-ta=nvidia,cuda9i2, tesla:managed helped to solve pure mpi acc mem issues but failed when more than one omp
	set(MYOPTLEVEL "-O2 -pg -tp=haswell-64 -acc -ta=nvidia -Minfo=accel,mp")
endif()


################################################################################################################################
#END OF INTERACTION FOR NON PRO USERS###########################################################################################
##in this section advanced users might want/need to make modifications if they use non default places for thirdparty libraries##
################################################################################################################################ 
#HDF5 local installation for advanced I/O, collecting metadata and bundle analysis results together
#download Unix tar archive for the HDF5-1.10.2 release from www.hdf5.org
#copy into local directory, do not use sudo to execute
#unpack via tar -xvf tarballname
#change into directory from now on this is referred to as the hdfroot folder
#execute ./build_unix.sh  this will configure the static libraries, sequential no fortran support and use deflate compression library
#when configure build and test has been completed
#take packed archive and extract into the automatically generated build folder within the hdfroot folder
#deep in these folders are the include directories and the static libraries (a files) we to understand the hdf5.cpp commands and 
#the libraries we need to link statically against to execute hdf5 functionalities

#build executable name
set(MYEXECNAME "${MYPROJECTNAME}")
if(EMPLOY_INTEL_COMPILER)
	set(MYCOMPILER_TAG "_itl")
	list(APPEND MYEXECNAME "_intel")
endif()
if(EMPLOY_GNU_COMPILER)
	set(MYCOMPILER_TAG "_gnu")
	list(APPEND MYEXECNAME "_gnu")
endif()
if(EMPLOY_PGI_COMPILER)
	set(MYCOMPILER_TAG "_pgi")
	list(APPEND MYEXECNAME "_pgi")
endif()

if(EMPLOY_PARALLELISM_MPI)
	set(MYPARALLELISM_MPI_TAG "_mpi")
	list(APPEND MYEXECNAME "_mpi")
elseif()
	set(MYPARALLELISM_MPI_TAG "_xxx")
	list(APPEND MYEXECNAME "_xxx")
endif()
if(EMPLOY_PARALLELISM_OMP)
	set(MYPARALLELISM_OMP_TAG "_omp")
	list(APPEND MYEXECNAME "_omp")
elseif()
	set(MYPARALLELISM_OMP_TAG "_xxx")
	list(APPEND MYEXECNAME "_xxx")
endif()
if(EMPLOY_PARALLELISM_OPENACC)
	set(MYPARALLELISM_ACC_TAG "_acc")
	list(APPEND MYEXECNAME "_acc")
elseif()
	set(MYPARALLELISM_ACC_TAG "_xxx")
	list(APPEND MYEXECNAME "_xxx")
endif()
##string( REPLACE ";" "" MYEXECNAME "${MYEXECNAME}" )
string(CONCAT MYEXECNAME "${MYPROJECTNAME} ${MYCOMPILER_TAG} ${MYPARALLELISM_MPI_TAG} ${MYPARALLELISM_OMP_TAG} ${MYPARALLELISM_ACC_TAG}")
message(STATUS "MYEXECNAME:" ${MYEXECNAME})

if(EMPLOY_MYHDF5)
	include_directories("${MYHDFPATH}/include")
	link_directories("${MYHDFPATH}/lib")
	##link against static libraries
	set(MYHDFLINKFLAGS "-L${MYHDFPATH}/lib/ ${MYHDFPATH}/lib/libhdf5_hl.a ${MYHDFPATH}/lib/libhdf5.a -lz -ldl")
endif()


##########################################################################################
#####PERFORMANCE RELATED##################################################################
##here settings for scalable vectorization and scalable allocator classes are made########
##so far these components are optional####################################################


##MK::#NumScale bSIMD scalable vectorization support
##MK::if (EMPLOY_BSIMDSUPPORT)
##MK::	include_directories("${MYPROJECTPATH}/src/thirdparty/bSIMD/home/m.kuehbach/SIMD/master/include")
##MK::	set(MYSIMDFLAGS "-msse4.2")
##MK::elseif()
##MK::	set(MYSIMDFLAGS "")
##MK::endif()

#Linux NUMA Non-uniform memory access (NUMA)
#might need installation on cluster
##needs necessarily linking into the program but not necessarily used
##MK::set(MYNUMA "-lnuma")
##MK::add_definitions(${MYNUMA})

#Scalable allocation using Jason Evans alternative malloc3 implementation##################
##alternatives are for instance tcmmalloc##################################################
##optional: if desired specify location of working jemalloc installation or local compiled version of the library
##use only in combination with pinned threads to reduce fragmentation and improve memory locality during allocation
##MK::if(EMPLOY_JEMALLOC)
##MK::	set(MYSCALABLEALLOC "-ljemalloc")
##MK::	add_definitions(${MYSCALABLEALLOC})
##MK::	include_directories("${MYJEMALLOCPATH}/include")
##MK::	link_directories("${MYJEMALLOCPATH}/lib")
##MK::elseif()
##MK::	set(MYSCALABLEALLOC "")
##MK::	add_definitions(${MYSCALABLEALLOC})
##MK::endif()


#############################################################################################################################
##AUTOMATIC SECTION##########################################################################################################
##normally no further changes should be required below unless for development################################################
#############################################################################################################################
set(VERBOSE_VECTORIZATION ON)

#user input sanity checks
if(EMPLOY_MYHDF5)
	message([STATUS] "We use the HDF5 library for advanced I/O")
endif()
if(EMPLOY_MYCGAL)
	message([STATUS] "We use the Computational Geometry Algorithms Library (CGAL)")
endif()
if(EMPLOY_MYBOOST)
	message([STATUS] "We use boost, the C++ STL template library extension")
endif()
if(EMPLOY_JEMALLOC)
	message([STATUS] "We use the Jemalloc multi-threading optimized allocator library")
endif()

#avoid multi compiler check
##MK::if(EMPLOY_INTELCOMPILER AND EMPLOY_GNUCOMPILER)
##MK::	message([FATAL_ERROR] "You cannot utilize two compiler at the same time!")
##MK::endif()

#automatically assign project name and compiler flags
project(${MYPROJECTNAME})
set(CMAKE_BUILD_DIR "build")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${MYOPTLEVEL}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${MYOPTLEVEL}")

#setting up compiler-specifics
#intel path
if(EMPLOY_INTELCOMPILER)
	message([STATUS] "Employing the Intel compiler!")
	if(VERBOSE_VECTORIZATION)
		set(MYVERBOSE "-qopt-report=5") #-phase=vec") #https://software.intel.com/en-us/node/522949/ "-qopt-report-phase=vec")
	elseif()
		set(MYVERBOSE "")
	endif()
	message([WARNING] "Currently not SIMD flags provided for Intel compile options")
	#omp
	if(EMPLOY_PARALLELISM_OMP)
		set(MYOMP "-qopenmp -lpthread")
		add_definitions(${MYOMP})
	elseif()
		set(MYOMP "")
	endif()
	add_definitions("${MYOPTLEVEL} ${MYOMP} ${MYVERBOSE} ${MYMKLCOMP}")
	#c2011 support
	add_definitions("-std=c++0x")
	#further definitions
	##add_definitions("-Warray-bounds -Wchar-subscripts -Wcomment -Wenum-compare -Wformat 
	##	-Wuninitialized -Wmaybe-uninitialized -Wmain -Wnarrowing -Wnonnull 
	##	-Wparentheses -Wpointer-sign -Wreorder -Wreturn-type -Wsign-compare 
	##	-Wsequence-point -Wtrigraphs -Wunused-variable")
	add_definitions("-Wall -Warray-bounds -Wchar-subscripts -Wcomment -Wenum-compare -Wformat -Wuninitialized -Wmaybe-uninitialized -Wmain 
		-Wnarrowing -Wnonnull -Wparentheses -Wpointer-sign -Wreorder -Wreturn-type -Wsign-compare -Wsequence-point -Wtrigraphs -Wunused-function -Wunused-but-set-variable -Wunused-variable -Wwrite-strings -Wint-to-pointer-cast")
#gcc path
##MK::elseif(EMPLOY_GNUCOMPILER)
##MK::	message([STATUS] "Employing the GNU compiler!")
##MK::	if(VERBOSE_VECTORIZATION)
##MK::		set(MYVERBOSE "-fopt-info")
##MK::	endif()
##MK::	add_definitions("${MYOPTLEVEL} ${MYVERBOSE} ${MYSIMDFLAGS}")
##MK::	set(MYOMP "-fopenmp -lpthread")
##MK::	add_definitions(${MYOMP})
##MK::	add_definitions("-std=c++11")
##MK::	add_definitions("-Wall -Warray-bounds -Wchar-subscripts -Wcomment -Wenum-compare -Wformat 
##MK::		-Wuninitialized -Wmaybe-uninitialized -Wmain -Wnonnull 
##MK::		-Wparentheses -Wreorder -Wreturn-type -Wsign-compare -Wsequence-point 
##MK::		-Wtrigraphs -Wunused-function -Wunused-but-set-variable -Wunused-variable") #-Wnarrowing
#pgi path
elseif(EMPLOY_PGICOMPILER)
	message([STATUS] "Employing the PGI compiler!")
	##verbose vectorization reporting
	set(MYOMP "-mp -ldl")
	add_definitions("${MYOPTLEVEL} ${MYOMP}")
	set(MYOPENACCLINK "-acc -ta=nvidia") #host
	add_definitions("-std=c++11")
	##additional defs and large file support
else()
	message([FATAL_ERROR] "You have to utilize a compiler!")
endif()

message([STATUS] "Projectname is ${MYPROJECTNAME}")
message([STATUS] "We utilize optimization level ${MYOPTLEVEL}")

#parallelization - MPI process-level
#query location of MPI library
if(EMPLOY_PARALLELISM_MPI)
	find_package(MPI REQUIRED)
	include_directories(${MPI_INCLUDE_PATH})
endif()

#specific paths of dependencies for this tool
set(MYTOOLSRCPATH "${MYTOOLPATH}/src/")

#list firstly the precompiled shared aka utils, secondly the tool-specific components, lastly the tool-specific main
add_executable(${MYPROJECTNAME}
	${MYUTILSPATH}/PARAPROBE_Verbose.cpp.o
	${MYUTILSPATH}/PARAPROBE_Profiling.cpp.o
	${MYUTILSPATH}/PARAPROBE_EPOSEndianness.cpp.o
	${MYUTILSPATH}/PARAPROBE_Datatypes.cpp.o
	${MYUTILSPATH}/PARAPROBE_OriMath.cpp.o
	${MYUTILSPATH}/PARAPROBE_Math.cpp.o
	${MYUTILSPATH}/PARAPROBE_PeriodicTable.cpp.o
	${MYUTILSPATH}/PARAPROBE_Composition.cpp.o	
	${MYUTILSPATH}/PARAPROBE_Histogram.cpp.o
	${MYUTILSPATH}/PARAPROBE_SDM3D.cpp.o
	${MYUTILSPATH}/PARAPROBE_AABBTree.cpp.o
	${MYUTILSPATH}/PARAPROBE_SpaceBucketing.cpp.o
	${MYUTILSPATH}/PARAPROBE_KDTree.cpp.o
	${MYUTILSPATH}/CONFIG_Shared.cpp.o
	${MYUTILSPATH}/PARAPROBE_HDF5.cpp.o
	${MYUTILSPATH}/PARAPROBE_XDMF.cpp.o
	${MYUTILSPATH}/PARAPROBE_Threadmemory.cpp.o
	${MYUTILSPATH}/PARAPROBE_Decompositor.cpp.o
	${MYUTILSPATH}/PARAPROBE_VolumeSampler.cpp.o
	${MYUTILSPATH}/PARAPROBE_CPUGPUWorkloadStructs.cpp.o
	${MYUTILSPATH}/PARAPROBE_FFT.cpp.o	
	
	${MYTOOLSRCPATH}/CONFIG_Fourier.cpp
##	${MYTOOLSRCPATH}/PARAPROBE_Threadmemory.cpp
##	${MYTOOLSRCPATH}/PARAPROBE_Decompositor.cpp
#	${MYTOOLSRCPATH}/PARAPROBE_Voxelizer.cpp
#	${MYTOOLSRCPATH}/PARAPROBE_TriangleTree.cpp
##	${MYTOOLSRCPATH}/PARAPROBE_VolumeSampler.cpp
	${MYTOOLSRCPATH}/PARAPROBE_DFTHdlCPU.cpp
	${MYTOOLSRCPATH}/PARAPROBE_DFTHdlGPU.cpp
	${MYTOOLSRCPATH}/PARAPROBE_FourierHDF5.cpp
	${MYTOOLSRCPATH}/PARAPROBE_FourierXDMF.cpp
	${MYTOOLSRCPATH}/PARAPROBE_FourierHdl.cpp
	
	${MYTOOLSRCPATH}/PARAPROBE_Fourier.cpp
)

##assure the Boost package is there to allow and ease the reading of files and folders from the filesystem
##message([STATUS] "We need Boost as well...")
##include_directories(${Boost_INCLUDE_DIR})
##link_directories(${Boost_LIBRARY_DIR})
##find_package(Boost COMPONENTS system filesystem REQUIRED)


#linking process, the target link libraries command is specific for each tool of the toolbox
target_link_libraries(${MYPROJECTNAME} ${MYOMP} ${MPI_LIBRARIES} ${MYHDFLINKFLAGS} ${MYOPENACCLINK})

#MPI compilation settings
if(MPI_COMPILE_FLAGS)
  set_target_properties(${MYPROJECTNAME} PROPERTIES COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif()

if(MPI_LINK_FLAGS)
  set_target_properties(${MYPROJECTNAME} PROPERTIES LINK_FLAGS "${MPI_LINK_FLAGS}")
endif()
