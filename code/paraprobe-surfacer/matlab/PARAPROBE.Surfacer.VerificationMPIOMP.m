% PARAPROBE.Surfacer
% Markus K\"uhbach, 2019/10/22
tic
    clear;
    clc;
    format long;
    
% load sequentially computed reference result
    dirnm = 'Y:\GITHUB\MPIE_APTFIM_TOOLBOX\paraprobe\code\paraprobe-surfacer\run\';
    prefix = 'PARAPROBE.Surfacer.';
    h5fnref = [dirnm prefix 'SimID.1.h5'];
    dsnm = '/SurfaceRecon/Ion2Surface/Results/Distance';
    DREF = h5read(h5fnref, dsnm);
    
% compare to in-parallel computed results
    for simid = 2:1:2
        h5fn = [dirnm prefix 'SimID.' num2str(simid) '.h5'];
        D = h5read(h5fn, dsnm);
    end
    ID = 1:1:length(DREF(1,:));
    DIFF = DREF(1,:) - D(1,:);
    max(DIFF(1,:))
    plot(ID(1,:),DIFF(1,:))
    %ecdf(DREF(1,:))
    %hold on
    %ecdf(D(1,:))
    DIFF(DIFF < eps) = nan;
    plot(DREF(1,:),log10(DIFF(1,:)),'.');
    
toc

%% DEBUG
% max(DAT(1,:));
% N = length(DAT(1,:));
% CDF(1,:) = sort(DAT(1,:));
% CDF(2,:) = (1:1:N)./N.*100.0;
% plot(CDF(1,:),CDF(2,:),'.');
% save('PARAPROBE.Araullo.CreateGeodesicSphere.40962.mat','-v7.3');
% h5create(h5fn, dsnm, size(DAT));
% h5write(h5fn, dsnm, DAT);
