h5wkdir='H:/BIGMAX_RELATED/Conferences/WorkshopNRWAPT_Nov2019/Content/paraprobe-surfacer/run'

import os, sys, glob, subprocess
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy import stats
import csv
import pandas as pd
import seaborn
import probscale
clear_bkgd = {'axes.facecolor':'none', 'figure.facecolor':'none'}
seaborn.set(style='ticks', context='talk', color_codes=True, rc=clear_bkgd)

# load up some example data from the seaborn package
##tips = seaborn.load_dataset("tips")
##iris = seaborn.load_dataset("iris")

#MPIE colors
#poster section background MPIE logo foreground green-blue #0073AA
#MPIE logo background light-blue #77B2D2
#MPIE 100year logo orange #EF8321
#Parula green #9DC943
#Gray Markus MMM2018, #DAD5CB
#Black #000000

sys.path.append(h5wkdir)
##sys.path.append(idxdir)
#be determinsitic
np.random.seed(0)
import h5py # if it is in Cstyle stored in the HDF5 file it will remain Cstyle ordered in numpy so no implicit transformation

np.set_printoptions(precision=3,suppress=False)


MYTOOLPATH='H:/BIGMAX_RELATED/MPIE-APTFIM-TOOLBOX/paraprobe/code/paraprobe-autoreporter'
sys.path.append(MYTOOLPATH + '/' + 'src')
sys.path.append(MYTOOLPATH + '/' + 'src' + '/' + 'metadata')

np.random.seed(0) #to be determinsitic
#if it is in Cstyle stored in the HDF5 file it will remain Cstyle ordered in numpy so no implicit transformation
#http://christopherlovell.co.uk/blog/2016/04/27/h5py-intro.html
import h5py

from PARAPROBE_Numerics import *
from PARAPROBE_CorporateDesign import *
from PARAPROBE_MetadataSpatstatDefsH5 import * 
from PARAPROBE_MetadataSurfacerDefsH5 import *

#define numpy quantile values to speed up plotting of only the relevant values
nquant = np.zeros([0,0],np.float64)
tmp = np.linspace(0,0.001,10,endpoint=False)
nquant = np.append( nquant, tmp)
tmp = np.linspace(0.001,0.01,9, endpoint=False)
nquant = np.append( nquant, tmp )
tmp = np.linspace(0.01,0.1, 9, endpoint=False)
nquant = np.append( nquant, tmp )
tmp = np.linspace(0.1,0.9, 80, endpoint=False)
nquant = np.append( nquant, tmp )
tmp = np.linspace(0.9,0.99, 9, endpoint=False)
nquant = np.append( nquant, tmp )
tmp = np.linspace(0.99,0.999, 9, endpoint=False)
nquant = np.append( nquant, tmp )
tmp = np.linspace(0.999, 1.0, 10, endpoint=True)
nquant = np.append( nquant, tmp )


#https://stackoverflow.com/questions/10138085/python-pylab-plot-normal-distribution
#http://www.cplusplus.com/reference/random/normal_distribution/

##Markus K\"uhbach, November 4, 2019
##script to visualize cumulative distance distribution from PARAPROBE.Surfacer.Results.h5

simid = 2001000
resfn = 'PARAPROBE.Surfacer.SimID.' + str(simid) + '.h5'
h5fn = h5wkdir + '/' + resfn
hf = h5py.File(h5fn, 'r')
surfacer=list(hf['SurfaceRecon'])
if 'Ion2Surface' in surfacer:
    res = list(hf['SurfaceRecon/Ion2Surface'])
    if 'Results' in res:
        thisone = list(hf['SurfaceRecon/Ion2Surface/Results'])
        if 'Distance' in thisone:
            dist2hull = np.array(hf[PARAPROBE_SURFACER_RES_DIST]).flatten()
            
            #nl = len(dist2hull)
            #ecdf = np.zeros([nl,2],np.float64)
            #ecdf[:,0] = np.sort(dist2hull)
            #ecdf[:,1] = np.linspace(1,nl,nl,endpoint=True)/nl;
            nl = len(nquant)
            ecdf = np.zeros([nl,2],np.float64)
            ecdf[:,1] = nquant
            for i in range(nl):
                ecdf[i,0] = np.quantile( dist2hull[:], nquant[i])
                print(str(nquant[i]) + ' i = ' + str(i) + ' quantile computed')
                
            #plot diagram
            plt.plot(ecdf[:,0], ecdf[:,1], color=myparula[0], alpha=myalpha, linewidth=mylinewidth )
            plt.legend([r'Distance to $\alpha$-shape triangle hull'])
            plt.title(resfn) #r'Distance ' + ' SimID.' + str(simid))
            plt.xlabel(r'Distance to $\alpha$-shape (nm)')
            plt.ylabel(r'Cumulative distribution')
            #plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
            #plt.yscale('log')
            xmi = min(ecdf[:,0])
            xmx = max(ecdf[:,0])
            plt.xlim([-0.05*(xmx-xmi)+xmi, +0.05*(xmx-xmi)+xmx])
            ymi = min(ecdf[:,1])
            ymx = max(ecdf[:,1])
            plt.ylim([-0.05*(ymx-ymi)+ymi, +0.05*(ymx-ymi)+ymx])
            #ymi = 0.0
            #ymx = 1.0
            #plt.ylim([-0.05, +1.05])
            #plt.yticks([0.0:0.1:1.0]) #, ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
            fig = plt.gcf()
            fig.set_size_inches(myfigure_width, myfigure_height)
            #plt.scatter(qq[:,0],qq[:,1],s=30,color='#9DC943',alpha=1.0,linewidths=0.0)
            #generate file name for the figure like in the HDF5
            #self.h5res + '/' + 
            pngfn = h5wkdir + '/' + resfn + '.Distance.png' #(PARAPROBE_SURFACER_RES_DIST).replace('/','_') + '.png' #'.png'
            fig.savefig(pngfn, dpi=myfigure_dpi, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png', 
                        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None) #format='png'
            fig.clf()
            plt.close()
