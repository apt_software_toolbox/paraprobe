//##MK::CODESPLIT


#ifndef __PARAPROBE_SURFACERHDL_H__
#define __PARAPROBE_SURFACERHDL_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_TriangleTree.h"


struct MPI_DistAndID
{
	float d;
	unsigned int id;
	//unsigned int wkload;
	MPI_DistAndID() : d(0.0), id(UINT32MX) {} //, wkload(0) {}
	MPI_DistAndID( const float _d, const unsigned int _id ) : d(_d), id(_id) {} //, const unsigned int _wkld) :
		//d(_d), id(_id), wkload(_wkld) {}
};


struct MPI_WorkAndID
{
	unsigned int ntriangles;
	unsigned int id;
	MPI_WorkAndID() : ntriangles(0), id(UINT32MX) {}
	MPI_WorkAndID( const unsigned int _ntri, const unsigned int _id) : ntriangles(_ntri), id(_id) {}
};


class surfacerHdl
{
	//process-level class which implements the worker instance which synthesizes different virtual APT
	//specimens result are written to a specific HDF5 file

public:
	surfacerHdl();
	~surfacerHdl();

	bool read_reconxyz_from_apth5();
	
	bool broadcast_reconxyz();

	bool spatial_decomposition();
	bool prune_ions();
	bool build_triangle_hull();
	bool compute_distances1(); //no adaptive requerying of tree, no circumsphere pre-checks
	bool compute_distances2(); //adaptive requerying of trees

	
	bool init_target_file();
	bool communicate_distances1();
	bool communicate_distances2();
	bool write_triangles_to_apth5();
	bool write_distances_to_apth5();

	void init_mpidatatypes();
	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );

	decompositor sp;
	vxlizer ca;
	mesher tr;
	tritree bvh;

	//xdmfHdl debugxdmfHdl;
	h5Hdl inputH5Hdl;
	surfacer_h5 debugh5Hdl;
	surfacer_xdmf debugxdmf;

	vector<p3d> recon_positions;
	vector<apt_real> distances;							//only significant at MASTER
	vector<unsigned int> workload;						//only significant at master, how many triangles were tested to get shortest distance per ion

	vector<MPI_DistAndID> mydist;						//temporary where process holds his local data
	vector<MPI_WorkAndID> mywork;

	vector<p3d> cand_positions;

	size_t myIonWorkload;								//how many ions has this process solved?

	profiler surf_tictoc;

private:
	//MPI related
	int myrank;											//my MPI ID in the MPI_COMM_WORLD
	int nranks;
	MPI_Datatype MPI_DistAndID_Type;
	MPI_Datatype MPI_WorkAndID_Type;
	MPI_Datatype MPI_Ion_Type;
	MPI_Datatype MPI_IonPositions_Type;
};


#endif

