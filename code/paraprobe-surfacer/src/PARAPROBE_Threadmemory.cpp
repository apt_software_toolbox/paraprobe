//##MK::CODE

#include "PARAPROBE_Threadmemory.h"

threadmemory::threadmemory()
{
	//owner = NULL;
	//threadtree = NULL;
	//tessmii = p6d64();
	//tesshalowidth = p3d64();
	zmi = F32MX;
	zmx = F32MI;
	melast = false;
}


threadmemory::~threadmemory()
{
	//MK::do not delete owner only backreference to decompositor who owns me!
	/*if ( threadtree != NULL ) {
		delete threadtree; threadtree = NULL;
	}*/
}


void threadmemory::init_localmemory( p6d64 const & mybounds, p3d64 const & myhalo, const bool mlast )
{
	//MK::CALLED FROM WITHIN PARALLEL REGION
	//tessmii = mybounds;
	//tesshalowidth = myhalo;
	zmi = mybounds.zmi;
	zmx = mybounds.zmx;
	melast = mlast;
}


void threadmemory::init_localions( vector<p3d> const & p )
{
	//MK::CALLED FROM WITHIN PARALLEL REGION
	//picks from reconstructed volume all positions inside me into local structure

	//bool TessellationYesOrNo = (Settings::VolumeTessellation != E_NOTESS) ? true : false;
/*
	apt_real local_zmi = tessmii.zmi; //by construction the same as zmi
	apt_real local_zmx = tessmii.zmx; //by construction the same as zmx
	apt_real voro_zmi = tessmii.zmi - tesshalowidth.z;
	apt_real voro_zmx = tessmii.zmx + tesshalowidth.z;
*/
	apt_real local_zmi = zmi;
	apt_real local_zmx = zmx;

	//when we need to have a guard zone for the tessellation we have a different strategy
	//if ( TessellationYesOrNo == true ) {} //see implementation in main code

	unsigned int nions = p.size();
	for( unsigned int ionID = 0; ionID < nions; ionID++ ) {
		if ( p[ionID].z < zmi )
			continue;
		if ( p[ionID].z >= zmx )
			continue;

		//not continued ion is within or on boundary of interval [zmi, zmx)
		ionpp3.push_back( p3dm3( p[ionID].x, p[ionID].y, p[ionID].z, ionID, VALIDZONE_ION, DEFAULTTYPE ) );
	}

	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " init_localions ionpp3.size() << " << ionpp3.size() << "\n";
	}
}

/*
inline size_t threadmemory::rectangular_transfer( const apt_xyz pos, const apt_xyz mi, const apt_xyz w, const apt_xyz mx, const size_t nvxl )
{
	//maps from position in continuous space to position in discrete space
	apt_xyz tmpd = (pos - mi) / w;
	size_t tmpsz = static_cast<size_t>(tmpd);
	size_t loc = (tmpsz != nvxl-1) ? 1+tmpsz : nvxl-1;
	//because total bin grid width is nx include left guard zone 1+ .......  +1 right guard zone at nx-1 last addressible bin ix nx-2
	return loc;
}


void threadmemory::binning( sqb const & vxlgrid )
{
	//MK::CALLED FROM WITHIN PARALLEL REGION

	//bins all local ions according to instructed vxlgrid
	ion2bin.clear();

	//##MK::potentially also local reallocate beneficial ?

	size_t ni = ionpp3.size();
	aabb3d limits = vxlgrid.box;
	apt_xyz binwidth = vxlgrid.width;

	for(size_t i = 0; i < ni; ++i) {
		size_t thisbin = -1;

		size_t x = rectangular_transfer( ionpp3.at(i).x, limits.xmi, binwidth, limits.xmx, vxlgrid.nx );
		size_t y = rectangular_transfer( ionpp3.at(i).y, limits.ymi, binwidth, limits.ymx, vxlgrid.ny );
		size_t z = rectangular_transfer( ionpp3.at(i).z, limits.zmi, binwidth, limits.zmx, vxlgrid.nz );

		thisbin = x + y*vxlgrid.nx + z*vxlgrid.nxy;

//cout << i << "\t\t" << thisbin << endl;

		ion2bin.push_back( thisbin );
	}
}


void threadmemory::ion2surfdistance_bvh()
{
	//MK::CALLED FROM WITHIN PARALLEL REGION
	//MK::hint on boost::dynamic_bitset
	//http://www.boost.org/doc/libs/1_64_0/libs/dynamic_bitset/
	// An example of setting and reading some bits. Note that operator[] goes from the least-significant bit at 0 to the most significant
	// bit at size()-1. The operator<< for dynamic_bitset prints the bitset from most-significant to least-significant, since that is
	// the format most people are used to reading. flag all ions close to the boundary
	double tic, toc;
	tic = omp_get_wtime();

	apt_real R = 0.f; //##MK::was Settings::MaximumKernelRadius = 2.0 nm;
	apt_real FarFromSurface = owner->owner->sp->tip.diag() * 2.0;
	apt_real SQRFarFromSurface = SQR(FarFromSurface);
	//SQR to avoid sqrt computations, threadmath.closestPointOnTriangle returns squared distance!

	ion2surf.clear();

	size_t ni = ionpp3.size();
	Tree* thisbvh = owner->owner->surf->bvh;
	vector<tri3d>* rbucket = &owner->owner->surf->tipsurface;
	vector<unsigned int> candidates;
	size_t myworkload = 0;
	for(size_t i = 0; i < ni; ++i) {
		//get my position
		p3d me = p3d( ionpp3.at(i).x, ionpp3.at(i).y, ionpp3.at(i).z );

		//instead of naive O(N^2) approach test for all ions against all triangles
		//prune most triangles by utilizing bounded volume hierarchy (BVH) Rtree
		hedgesAABB e_aabb( trpl(me.x-R, me.y-R, me.z-R), trpl(me.x+R, me.y+R, me.z+R) );

		candidates.clear();
		candidates = thisbvh->query( e_aabb ); //MK::assure that this is read only ##MK future improve for better locality multiple Rtrees

		myworkload += candidates.size();

		//if axis-aligned bounding box about triangle does not intersect this probe volume triangle must be farther away than RCutoff
		if ( candidates.size() < 1 ) { //no triangle close, most likely case
			ion2surf.push_back( FarFromSurface );
		}
		else {
			apt_real CurrentBest = SQRFarFromSurface;
			apt_real CurrentValue = SQRFarFromSurface;
			size_t nc = candidates.size();
			for ( size_t c = 0; c < nc; ++c ) {
				CurrentValue = threadmath.closestPointOnTriangle( rbucket->at(candidates.at(c)), me ); //##MK::far field fetch but much fewer candidates in total

				if ( CurrentValue > CurrentBest ) {
					continue; //most likely
				}
				else {
					CurrentBest = CurrentValue;
//cout << "i/c/nc/best " << i << "\t\t" << c << "\t\t" << nc << "\t\t" << CurrentBest << endl;
				}
			}
			//final decision on dSQR against minimum distance required to probe that sphere of radius at least dSQRCutoff does not protrude trihull
			//if dSQR > RSQR, deepinside the tip volume
			//else, ion lays too close to the tipsurface therefore attempting probing a sphere would protrude into vacuum and yield fewer neighbors i.e unbiased higher order statistics
			ion2surf.push_back( CurrentBest );
		}
	} //for each ion

	toc = omp_get_wtime();
	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " processed " << ionpp3.size() << " probed " << myworkload << " took " << (toc-tic) << " seconds" << endl;
	}
}
*/

void threadmemory::init_localdistances( const apt_real val )
{
	//CALLED FROM WITHIN PARALLEL REGION
	size_t ni = ionpp3.size();
	try {
		ion2surf = vector<apt_real>( ni, val );
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error in thread " << omp_get_thread_num() << " during initialization of ion2surf distances" << "\n";
	}
}

/*
bool threadmemory::build_local_kdtree()
{
	//MK::called from within parallel region

	//MK::there is a principal conflict, the input heavy data on owner->owner->owner->rawdata are stored only approximately in order along increasing depth, i.e. negative z direction
	//within an arbitrary plane however the sequence probes almost randomly over the detector space, hence the order in
	//owner->owner->sp->db[] is not expected in any reasonable way memory efficiently organized
	//for all so far conducted tasks this is irrelevant as processing operations such as ranging, finding bounding boxes are essential single time trivial parallel reads
	//spatial range queries though require an as best as possible balance grouping of ion data in logical groups (i.e. tree structures) to remain efficient.
	//MK::hence when building a tree the structure on ionpp3 will be reordered, and should afterwards be once also physically restructured to place ions in the same leaf of the tree
	//adjacent in memory to improve locality once probing the pruned search space
	//MK::we do not store an entire copy of the data in the KDTree but require a one time twice allocation to restructure however
	//if we then were to release the original ionpp3 all previous functions face now a different logical sequence of ions when machining off ionpp3
	//##MK:: as of now: build tree, restructure and keep two copies, future improvement, build tree already in spatial partitioning phase and implement leaner via for instance vector<vector*>

	//##MK::if sufficient threads are used also unsigned int will be sufficient

	double tic, toc;
	tic = omp_get_wtime();

	size_t ioncnt = ionpp3.size();
	vector<p3d> ioninput;
	vector<size_t> permutations;
	try {
		ioninput.reserve(ioncnt);
		permutations.reserve(ioncnt);
		ionpp3_kdtree.reserve(ioncnt);
		ion2surf_kdtree.reserve(ioncnt);
	}
	catch (bad_alloc &threadcroak) {
		string mess = "Allocation error during building threadlocal KDTree in " + to_string(omp_get_thread_num());
		stopping(mess);
		return false;
	}

	for(size_t i = 0; i < ioncnt; ++i) {
		ioninput.push_back( p3d( ionpp3.at(i).x, ionpp3.at(i).y, ionpp3.at(i).z) );
	}

	threadtree = new kd_tree; //this calls the default constructor

	//this really constructs the nodes of the tree
	threadtree->build( ioninput, permutations, 256 );

	#pragma omp critical
	{
		//cout << "Thread-local KDTree on " << to_string(omp_get_thread_num()) << " has [" << threadtree->min.x << ";" << threadtree->min.y << ";" << threadtree->min.z << "----" << threadtree->max.x << "] with " << threadtree->nodes.size() << " nodes in total"  << endl;
		//cout << "Thread-local KDTree on " << to_string(omp_get_thread_num()) << " has [" << threadtree->min << ";" << threadtree->max.x << "] with " << threadtree->nodes.size() << " nodes in total"  << endl;
		cout << "Thread-local KDTree on " << to_string(omp_get_thread_num()) << " has " << threadtree->nodes.size() << " nodes in total consuming at least " << threadtree->get_treememory_consumption() << endl;
	}

	threadtree->pack_p3dm1_dist( permutations, ionpp3, ion2surf, ionpp3_kdtree, ion2surf_kdtree );

	//permutation array no longer necessary
	//ioninput as well, both are temporaries will be freed automatically upon exiting subroutine

	if ( threadtree->verify( ionpp3_kdtree ) == false ) {
		string mess = "Threadlocal KDTree for " + to_string(omp_get_thread_num()) + " has overlapping indices!";
		stopping( mess );
		return false;
	}

	toc = omp_get_wtime();
	#pragma omp critical
	{
		cout << "Threadlocal KDTree build on " << to_string(omp_get_thread_num()) << " completed in " << (toc-tic) << " seconds" << endl;
	}

	return true;
}


inline apt_real threadmemory::get_zmi() const
{
	return this->zmi;
}

inline apt_real threadmemory::get_zmx() const
{
	return this->zmx;
}

inline bool threadmemory::me_last() const
{
	return this->melast;
}
*/


/*
inline size_t threadmemory::get_memory_consumption()
{
	size_t bytes = 0;
	bytes += ionpp3.size() * sizeof(p3dm1);
	bytes += ion2bin.size() * sizeof(size_t);
	bytes += ion2surf.size() * sizeof(apt_xyz);

	bytes += ionpp3_kdtree.size() * sizeof(p3dm1);
	bytes += ion2surf_kdtree.size() * sizeof(apt_xyz);

	bytes += threadtree->nodes.size()*sizeof(node);
	//do not account for elementar type locals

	return bytes;
}
*/
