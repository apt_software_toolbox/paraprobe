//##MK::CODESPLIT


#ifndef __PARAPROBE_SURFACER_THREADMEMORY_H__
#define __PARAPROBE_SURFACER_THREADMEMORY_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_HoshenKopelman.h"


class threadmemory
{
public:
	threadmemory();
	~threadmemory();

	void init_localmemory( p6d64 const & mybounds, p3d64 const & myhalo, const bool mlast );
	void init_localions( vector<p3d> const & p );
	/*inline size_t rectangular_transfer( const apt_xyz pos, const apt_xyz mi, const apt_xyz w, const apt_xyz mx, const size_t nvxl );
	void binning( sqb const & vxlgrid );
	void ion2surfdistance_bvh();
	*/
	void init_localdistances( const apt_real val );
	/*
	bool build_local_kdtree();
	*/

	inline apt_real get_zmi() const;
	inline apt_real get_zmx() const;
	inline bool me_last() const;

	vector<p3dm3> ionpp3;						//x,y,z, world ionID in reconstruction, VALIDZONE or GUARDZONE, iontype
	vector<size_t> ion2bin;						//temporary in which bin does each ion end up?
	vector<apt_real> ion2surf;					//temporary the distance of the ion to the surface

	//##MK::optimize
	//vector<p3dm3> ionpp3_tess;
	//vector<unsigned int> ionpp3_ionpp3idx;
	/*vector<unsigned int> distinfo_tess;
	//MK::SAME LENGTH THEN ionpp3_tess one-to-one correspondence
	//MK::stores an array index for ionpp3 which allows to identify which ion the respective entry in ionpp3_tess represents!
	//MK::avoids to store for every ion always an ID and thereby always to carry for every analysis task
	//larger array cache length than necessary, remind: only when the distancing info is to be used again
	//outside of the threadmemory scope, for instance in the Voro tess to identify how close an ion is
	//to the tip surface and use this information to eliminate cells with bias incorrect topology and geometry
	//MK::IN LATTER CASE ALLOWS ONLY to QUERY DISTANCE FOR VALIDZONE_IONS as their order of
	//populating ionpp3_tess during read_localions is instructed to proceed order preserving!*/

	//##MK::optimize, idea for instance, set up KDTree immediately would replace ionpp3 by ionpp3_kdtree and ion2surf by ion2surf_kdtree
	/*vector<p3dm1> ionpp3_kdtree;
	vector<apt_xyz> ion2surf_kdtree;

	kd_tree* threadtree;*/

	//inline size_t get_memory_consumption();

private:
	//p6d64 tessmii;
	//p3d64 tesshalowidth;
	apt_real zmi;
	apt_real zmx;
	bool melast;
};

#endif
