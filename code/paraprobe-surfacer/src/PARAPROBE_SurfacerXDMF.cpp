//##MK::GPLV3

#include "PARAPROBE_SurfacerXDMF.h"


surfacer_xdmf::surfacer_xdmf()
{
	
}
	
surfacer_xdmf::~surfacer_xdmf()
{
	
}


int surfacer_xdmf::create_alphashape_file( const string xmlfn, const size_t topo_nelements,
		const size_t topo_dims, const size_t geom_dims, const string h5hull )
{
	xdmfout.open( xmlfn.c_str() );
	if ( xdmfout.is_open() == true ) {
		xdmfout << XDMF_HEADER_LINE1 << "\n";
		xdmfout << XDMF_HEADER_LINE2 << "\n";
		xdmfout << XDMF_HEADER_LINE3 << "\n";

		xdmfout << "  <Domain>" << "\n";
		xdmfout << "    <Grid Name=\"alphashape\" GridType=\"Uniform\">" << "\n";
		xdmfout << "      <Topology TopologyType=\"Mixed\" NumberOfElements=\"" << topo_nelements << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << topo_dims << "\" NumberType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5hull << ":" << PARAPROBE_SURF_ASHAPE_RES_HULL_TOPO << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Topology>" << "\n";
		xdmfout << "      <Geometry GeometryType=\"XYZ\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << geom_dims << "\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5hull << ":" <<  PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Geometry>" << "\n";
		xdmfout << "    </Grid>" << "\n";
		xdmfout << "  </Domain>" << "\n";
		xdmfout << "</Xdmf>" << "\n";

		xdmfout.flush();
		xdmfout.close();

		return WRAPPED_XDMF_SUCCESS;
	}
	else {
		return WRAPPED_XDMF_IOFAILED;
	}
}


int surfacer_xdmf::create_distance_file( const string xmlfn, const size_t nions, const string h5recon, const string h5dist )
{
	xdmfout.open( xmlfn.c_str() );
	if ( xdmfout.is_open() == true ) {
		xdmfout << XDMF_HEADER_LINE1 << "\n";
		xdmfout << XDMF_HEADER_LINE2 << "\n";
		xdmfout << XDMF_HEADER_LINE3 << "\n";

		xdmfout << "  <Domain>" << "\n";
		xdmfout << "    <Grid Name=\"distancer\" GridType=\"Uniform\">" << "\n";
		xdmfout << "      <Topology TopologyType=\"Mixed\" NumberOfElements=\"" << nions << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << 3*nions << "\" NumberType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5recon << ":" << PARAPROBE_SYNTH_VOLRECON_RES_TOPO << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Topology>" << "\n";
		xdmfout << "      <Geometry GeometryType=\"XYZ\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nions << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5recon << ":" << PARAPROBE_SYNTH_VOLRECON_RES_XYZ << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Geometry>" << "\n";

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"Iontype\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nions << " 1\" DataType=\"UInt\" Precision=\"1\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5recon << ":" << PARAPROBE_RANGER_RES_TYPID << "\n"; //MK::because we write ranging info into reconstruction file
		xdmfout << "       </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";

		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"Dist2Hull\">" << "\n";
		xdmfout << "	    <DataItem Dimensions=\"" << nions << " 1\" DataType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5dist << ":" << PARAPROBE_SURF_DIST_RES_D << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";
		
		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"DistWorkload\">" << "\n";
		xdmfout << "	    <DataItem Dimensions=\"" << nions << " 1\" DataType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5dist << ":" << PARAPROBE_SURF_DIST_META_NTRI << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";

		xdmfout << "    </Grid>" << "\n";
		xdmfout << "  </Domain>" << "\n";
		xdmfout << "</Xdmf>" << "\n";

		xdmfout.flush();
		xdmfout.close();

		return WRAPPED_XDMF_SUCCESS;
	}
	else {
		return WRAPPED_XDMF_IOFAILED;
	}
}


