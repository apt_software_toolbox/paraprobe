//##MK::CODE

#include "PARAPROBE_Mesher.h"

mesher::mesher()
{
	hullifo = ashape_info();
}


mesher::~mesher()
{
}


bool mesher::alphashape_core( vector<p3d> const & p )
{
	//utilize the CGAL library to compute the alpha shapes
	if ( p.size() < 1 ) {
		cerr << "There are no candidates to construct an alpha shape from!" << "\n";
		return false;
	}
	if ( p.size() >= UINT32MX ) {
		cerr << "Computing alpha shapes for more than UINT32MX i.e. 4.2 billion ions not implemented!" << "\n";
		return false;
	}

	cout << "About to compute an alpha shape from " << p.size() << " candidate points using the CGAL library" << "\n";

	/*
	double tic = MPI_Wtime();
	*/

	hullifo.ncand = p.size();

	Delaunay dt;
	unsigned int ncand = static_cast<unsigned int>(p.size());
	for ( unsigned int c = 0; c < ncand; ++c ) {
		dt.insert( Point_3( p[c].x, p[c].y, p[c].z) );
	}

	/*
	double toc = MPI_Wtime();
	memsnapshot mm1 = owner->owner->tictoc.get_memoryconsumption();
	owner->owner->tictoc.prof_elpsdtime_and_mem( "SurfTriangulationDelaunayConstruct", APT_GEO, APT_IS_SEQ, mm1, tic, toc);
	cout << "CGAL::Delaunay data structure with total of " << ncand << " points initialized in " << (toc-tic) << endl;
	tic = MPI_Wtime();
	*/

	//compute alpha shape
	Alpha_shape_3 as(dt);
	cout << "Alpha shape will be computed in REGULARIZED mode by default"  << "\n";

	//find optimal alpha values
	Alpha_shape_3::NT alpha_solid = as.find_alpha_solid();
	Alpha_iterator opt = as.find_optimal_alpha(1);
	cout << "Smallest alpha value to get a solid through data points is " << alpha_solid << "\n";
	cout << "Optimal alpha value to get one connected component is " << *opt << "\n";

	/*
	memsnapshot mm2 = owner->owner->tictoc.get_memoryconsumption();
	*/

	//set shape to automatically determined optimum
	if ( ConfigSurfacer::AlphaShapeAlphaValue == E_ASHAPE_CGAL_OPTIMAL) {
		cout << "Taking the CGAL optimal value to get a solid through data points is " << *opt << " for triangulation" << "\n";
		as.set_alpha(*opt);
		hullifo.alph = *opt;
	}
	else {
		cout << "Taking the smallest alpha value to get a solid through data points is " << alpha_solid << " for triangulation" << "\n";
		as.set_alpha(alpha_solid);
		hullifo.alph = alpha_solid;
	}

	cout << "The number of solid components of the CGAL alpha shape is " << as.number_of_solid_components() << "\n";

	/*
	toc = MPI_Wtime();
	memsnapshot mm3 = owner->owner->tictoc.get_memoryconsumption();
	owner->owner->tictoc.prof_elpsdtime_and_mem( "SurfTriangulationAlphaShapeConstruct", APT_GEO, APT_IS_SEQ, mm3, tic, toc);
	cout << "CGAL::Delaunay-based alpha shape generated and assessed in " << (toc-tic) << " seconds" << endl;

	//extract triangulation
	tic = MPI_Wtime();
	*/

	//output triangulation of (optimal) alpha shape
	//in CGAL-4.11 the "outer" surface is what we are looking for, i.e. all facets of kind REGULAR
	//https://stackoverflow.com/questions/15905833/saving-cgal-alpha-shape-surface-mesh
	vector<Alpha_shape_3::Facet> facets;
	as.get_alpha_shape_facets(back_inserter(facets), Alpha_shape_3::REGULAR);
	size_t nf = facets.size();

	//##MK::DEBUG
	map<int, int> vunique;
	map<int, int>::iterator which;
	vector<p3d> vuni;
	vector<triref3d> tri;

	for ( size_t f = 0; f < nf; ++f ) {
		//To have a consistent orientation of the facet, always consider an exterior cell
		if ( as.classify( facets[f].first ) != Alpha_shape_3::EXTERIOR ) {
			facets[f] = as.mirror_facet( facets[f] );
		}
		CGAL_assertion( as.classify(facets[f].first) == Alpha_shape_3::EXTERIOR );

		int indices[3] = { (facets[f].second+1)%4, (facets[f].second+2)%4, (facets[f].second+3)%4 };

		//according to the encoding of vertex indices, this is needed to get a consistent orientation
		if ( facets[f].second%2 != 0 ) {
			//collect on the fly disjoint vertices
			for ( unsigned int i = 0; i < 3; ++i ) {
				if ( vunique.find(indices[i]) == vunique.end() ) {
					vuni.push_back( p3d(facets[f].first->vertex(indices[i])->point().x(), facets[f].first->vertex(indices[i])->point().y(), facets[f].first->vertex(indices[i])->point().z()) );
					vunique[indices[i]] = vuni.size()-1;
				}
			}

			tipsurface.push_back( tri3d(
					facets[f].first->vertex(indices[0])->point().x(), facets[f].first->vertex(indices[0])->point().y(), facets[f].first->vertex(indices[0])->point().z(),
					facets[f].first->vertex(indices[1])->point().x(), facets[f].first->vertex(indices[1])->point().y(), facets[f].first->vertex(indices[1])->point().z(),
					facets[f].first->vertex(indices[2])->point().x(), facets[f].first->vertex(indices[2])->point().y(), facets[f].first->vertex(indices[2])->point().z() ) );
			//std::cout << facets[f].first->vertex(indices[0])->point() << ";" << facets[f].first->vertex(indices[1])->point() << ";" << facets[f].first->vertex(indices[2])->point() << std::endl;

			tri.push_back( triref3d(indices[0], indices[1], indices[2]) ); //key maps to position in vuni holding the coordinates
		}
		else {
			swap(indices[0], indices[1]);

			//collect on the fly disjoint vertices
			for ( unsigned int i = 0; i < 3; ++i ) {
				if ( vunique.find(indices[i]) == vunique.end() ) {
					vuni.push_back( p3d(facets[f].first->vertex(indices[i])->point().x(), facets[f].first->vertex(indices[i])->point().y(), facets[f].first->vertex(indices[i])->point().z()) );
					vunique[indices[i]] = vuni.size()-1;
				}
			}

			tipsurface.push_back( tri3d(
				facets[f].first->vertex(indices[0])->point().x(), facets[f].first->vertex(indices[0])->point().y(), facets[f].first->vertex(indices[0])->point().z(),
				facets[f].first->vertex(indices[1])->point().x(), facets[f].first->vertex(indices[1])->point().y(), facets[f].first->vertex(indices[1])->point().z(),
				facets[f].first->vertex(indices[2])->point().x(), facets[f].first->vertex(indices[2])->point().y(), facets[f].first->vertex(indices[2])->point().z() ) );

			tri.push_back( triref3d(indices[0], indices[1], indices[2]) );
		}
	}

//cout << "VUni/Tri.size() " << vuni.size() << "\t\t" << tri.size() << endl;
	/*
	toc = MPI_Wtime();
	memsnapshot mm4 = owner->owner->tictoc.get_memoryconsumption();
	owner->owner->tictoc.prof_elpsdtime_and_mem( "SurfTriangulationAlphaShapeTriExtract", APT_GEO, APT_IS_SEQ, mm4, tic, toc);
	*/

	cout << "Extracted triangularized tip surface with " << nf << " nfacets and " << tipsurface.size() << " triangles in total" << "\n"; //) in " << (toc-tic) << " seconds" << endl;

	return true;
}
