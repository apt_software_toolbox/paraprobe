//##MK::CODE

#ifndef __PARAPROBE_SURFACER_TRITREE_H__
#define __PARAPROBE_SURFACER_TRITREE_H__

#include "PARAPROBE_Mesher.h"

class tritree
{
public:
	tritree();
	~tritree();

	bool build_rtree( vector<tri3d> const & tri );
	bool build_circumspheres( vector<tri3d> const & tri );
	
	apt_real compute_sqrdist_p3d_circumsphere( p3d const & p, const unsigned int i );
	apt_real compute_sqrdist_p3d_triangle( p3d const & p, const unsigned int i );

	Tree* kauri;

	vector<tri3d> triangles;				//temporary buffer to improve locality
	vector<circumsphere> cspheres;			//the circumspheres about each triangle
	//MK::of course we precompute the circumspheres to reduce numerical complexity
	//MK::we do not build a datastructure where the triangle with its coordinates and its circumsphere is
	//in one datatype, in fact this would improve memory locality if the circumsphere collision test finds collision and the triangle
	//has to be inspected. However, in more cases only the circumspheres have to be inspected and thus we better be
	//memory local on their coordinate values
};

#endif
