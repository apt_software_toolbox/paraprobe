//##MK::CODE

#include "PARAPROBE_TriangleTree.h"

tritree::tritree()
{
	kauri = NULL;
}


tritree::~tritree()
{
	delete kauri; kauri = NULL;
}


bool tritree::build_rtree( vector<tri3d> const & tri )
{
	//builds an AABBTree / RTree of triangles with respect to the global space the triangles occupy in $\mathcal{R}^3$
	if ( tri.size() < 1 ) {
		cout << "WARNING There are no triangles" << "\n";
		return false;
	}
	if ( tri.size() >= static_cast<size_t>(UINT32MX-1) ) {
		cerr << "Implementation of triangle hull supports currently at most UINT32MX individuals!" << "\n";
		return false;
	}
	unsigned int ntriangles = static_cast<unsigned int>(tri.size());
	if ( kauri != NULL ) {
		cerr << "A BVH tree already exists" << "\n";
		return false;
	}
	
	try {
		kauri = new class Tree( ntriangles );
	}
	catch (bad_alloc &surfexc) {
		cerr << "Unable to allocate memory for the tree of the triangle BVH!" << "\n";
		return false;
	}
	
	try {
		triangles.reserve( ntriangles );
	}
	catch(bad_alloc &croak) {
		cerr << "Unable to allocate memory for the triangles of the BVH!" << "\n";
		return false;
	}

	apt_real xmi = F32MX;
	apt_real xmx = F32MI;
	apt_real ymi = F32MX;
	apt_real ymx = F32MI;
	apt_real zmi = F32MX;
	apt_real zmx = F32MI;
	unsigned int triid = 0;
	for ( auto it = tri.begin(); it != tri.end(); ++it ) {
		triangles.push_back( *it );
		//##MK::fattening in lohedges/aabbcc code only for purpose of improving rebalancing, however this tree is static,
		//hence no fattening required... xmi -= Settings::MaximumKernelRadius

		xmi = fmin( fmin(it->x1, it->x2), it->x3 );
		xmx = fmax( fmax(it->x1, it->x2), it->x3 );
		ymi = fmin( fmin(it->y1, it->y2), it->y3 );
		ymx = fmax( fmax(it->y1, it->y2), it->y3 );
		zmi = fmin( fmin(it->z1, it->z2), it->z3 );
		zmx = fmax( fmax(it->z1, it->z2), it->z3 );

		//https://github.com/lohedges/aabbcc
		//Here index is a key that is used to create a map between particles and nodes in the AABB tree.
		//The key should be unique to the particle and can take any value between 0 and std::numeric_limits<unsigned int>::max() - 1

		kauri->insertParticle( triid, trpl(xmi, ymi, zmi), trpl(xmx, ymx, zmx) );
		triid++;
	}

	/*
	if ( Settings::IOTriangulationBVH == true ) {
		//report all nodes of the resulting tree
		string csvfn = "PARAPROBE.SimID." + to_string(Settings::SimID) + ".AABBTree.csv";
		bvh->report_tree( csvfn );
	}
	*/
	cout << "BVH of triangles build successfully" << "\n";
	return true;
}


bool tritree::build_circumspheres( vector<tri3d> const & tri )
{
	//builds an AABBTree / RTree of triangles with respect to the global space the triangles occupy in $\mathcal{R}^3$
	if ( tri.size() < 1 ) {
		cerr << "WARNING There are no triangles in input!" << "\n"; return false;
	}
	if ( tri.size() >= static_cast<size_t>(UINT32MX) ) {
		cerr << "Implementation of triangle hull supports currently at most UINT32MX individuals!" << "\n"; return false;
	}
	
	try {
		cspheres = vector<circumsphere>( tri.size(), circumsphere() );
	}
	catch(bad_alloc &croak) {
		cerr << "Unable to allocate memory for the circumspheres of the triangles!" << "\n"; return false;
	}
	
	for( size_t i = 0; i < tri.size(); i++ ) {
		cspheres.at(i) = circumsphere_about_triangle( tri.at(i) );
	}
	
	cout << "Circumspheres successfully computed" << "\n";
	return true;	
}




apt_real tritree::compute_sqrdist_p3d_circumsphere( p3d const & p, const unsigned int i )
{
	circumsphere sph = cspheres.at(i); //##MK::.at(i) to [i]

	return SQR(p.x-sph.x) + SQR(p.y-sph.y) + SQR(p.z-sph.z);
}


apt_real tritree::compute_sqrdist_p3d_triangle( p3d const & p, const unsigned int i )
{
	p3d point = p;
	tri3d tgl = triangles.at(i);
	return closestPointOnTriangle( tgl, point );
}
