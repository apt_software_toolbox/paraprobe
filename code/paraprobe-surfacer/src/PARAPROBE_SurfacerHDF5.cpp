//##MK::CODE

#include "PARAPROBE_SurfacerHDF5.h"
//https://www.learncpp.com/cpp-tutorial/114-constructors-and-initialization-of-derived-classes/


surfacer_h5::surfacer_h5()
{
}


surfacer_h5::~surfacer_h5()
{
}


int surfacer_h5::create_surfacer_apth5( const string h5fn )
{
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

	//generate a PARAPROBE APTH5 HDF5 file
	//##MK::catch errors

	//domain specific HDF5 keywords and data fields
	groupid = H5Gcreate2(fileid,  PARAPROBE_SURF, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	status = H5Gclose(groupid);
cout << " PARAPROBE_SURF " << status << "\n";

	if ( ConfigSurfacer::SurfacingMode == E_SURFACE_ALPHASHAPE ) {
		groupid = H5Gcreate2(fileid,  PARAPROBE_SURF_ASHAPE, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);
cout << " PARAPROBE_SURF_ASHAPE " << status << "\n";
		groupid = H5Gcreate2(fileid,  PARAPROBE_SURF_ASHAPE_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);
cout << " PARAPROBE_SURF_ASHAPE_META " << status << "\n";
		groupid = H5Gcreate2(fileid,  PARAPROBE_SURF_ASHAPE_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);
cout << " PARAPROBE_SURF_ASHAPE " << status << "\n";
	}

	if ( ConfigSurfacer::DistancingMode != E_DISTANCING_NOTHING ) {
		groupid = H5Gcreate2(fileid,  PARAPROBE_SURF_DIST, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);
cout << " PARAPROBE_SURF_DIST " << status << "\n";
		groupid = H5Gcreate2(fileid,  PARAPROBE_SURF_DIST_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);
cout << " PARAPROBE_SURF_DIST_META " << status << "\n";
		groupid = H5Gcreate2(fileid,  PARAPROBE_SURF_DIST_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		status = H5Gclose(groupid);
cout << " PARAPROBE_SURF_DIST_RES " << status << "\n";
	}

	status = H5Fclose(fileid);
cout << "Closing APTH5 file" << "\n";

	return WRAPPED_HDF5_SUCCESS;
}
