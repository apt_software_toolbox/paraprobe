//##MK::CODESPLIT

#include "PARAPROBE_SurfacerHdl.h"


surfacerHdl::surfacerHdl()
{
	myrank = MASTER;
	nranks = SINGLEPROCESS;
	myIonWorkload = 0;
}


surfacerHdl::~surfacerHdl()
{
}


bool surfacerHdl::read_reconxyz_from_apth5()
{
    double tic = MPI_Wtime();

	if ( inputH5Hdl.read_xyz_from_apth5( ConfigSurfacer::Inputfile, recon_positions ) == true ) {
		cout << ConfigSurfacer::Inputfile << " read successfully" << "\n";
	}
	else {
		cerr << ConfigSurfacer::Inputfile << " read failed!" << "\n"; return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	//surfacer_tictoc.prof_elpsdtime_and_mem( "ReadReconstructedXYZFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
    return true;
}


bool surfacerHdl::broadcast_reconxyz()
{
	double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( recon_positions.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ion reconstructions!" << "\n"; localhealth = 0;
		}
		if ( recon_positions.size() < 1 ) {
			cerr << "Ion positions is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the size and state of the datasets!" << "\n";
		return false;
	}

	if ( get_nranks() > 1 ) { //shortcut if only a single MPI process across MPI_COMM_WORLD

		//define MPI datatype which assists us to logically structure the communicated data packages
		//##MK::use MPI_Ion_Type; user compound type MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_UNSIGNED

		//communicate size of the master's dataset
		int nevt = 0;
		if ( get_myrank() == MASTER ) {
			nevt = static_cast<int>(recon_positions.size());
		}
		MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

		//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
		localhealth = 1;
		MPI_IonPositions* buf = NULL;
		try {
			buf = new MPI_IonPositions[nevt];
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error for MPI_Ion buffer!" << "\n"; localhealth = 0;
		}

		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) {
			cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
			delete [] buf; buf = NULL; return false;
		}

		//master populates communication buffer
		if ( get_myrank() == MASTER ) {
			for( size_t i = 0; i < recon_positions.size(); i++ ) {
				buf[i] = MPI_IonPositions( recon_positions[i].x, recon_positions[i].y, recon_positions[i].z ); //, static_cast<unsigned int>(i) );
			}
		}

		//collective call MPI_Bcast, MASTER broadcasts, slaves listen
		MPI_Bcast( buf, nevt, MPI_IonPositions_Type, MASTER, MPI_COMM_WORLD );

		//slave nodes pull from buffer
		if ( get_myrank() != MASTER ) {
			try {
				recon_positions.reserve( static_cast<size_t>(nevt) );
				for( int i = 0; i < nevt; i++ ) {
					recon_positions.push_back( p3d( buf[i].x, buf[i].y, buf[i].z ) );
				}
			}
			catch(bad_alloc &mecroak) {
				cerr << "Rank " << get_myrank() << " allocation error during buf1 readout!" << "\n"; localhealth = 0;
			}
		}

		//all release the buffer, delete on NULL pointer gets compiled as no op
		delete [] buf; buf = NULL;

		//final check whether all were successful so far
		globalhealth = 0;
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) {
			cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";
			return false;
		}
	}
	else {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	//surfacer_tictoc.prof_elpsdtime_and_mem( "BroadcastReconstructionAcrossMPIProcesses", APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool surfacerHdl::spatial_decomposition()
{
	//build a point count balanced spatial partitioning to distribute storing across thread
	double tic = MPI_Wtime();

	sp.tip_aabb_get_extrema( recon_positions );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	surf_tictoc.prof_elpsdtime_and_mem( "SpecimenAABB3DGetExtrema", APT_XX, APT_IS_SEQ, mm, tic, toc );
	tic = MPI_Wtime();

	sp.loadpartitioning( recon_positions );

	toc = MPI_Wtime();
	mm = surf_tictoc.get_memoryconsumption();
	surf_tictoc.prof_elpsdtime_and_mem( "AABB3DLoadPartitioning", APT_XX, APT_IS_PAR, mm, tic, toc );

	return true;
}


bool surfacerHdl::prune_ions()
{
	//##execute CA functionalities
	//thread-local binning of ion positions into global vxlgrid
	double tic = MPI_Wtime();

	//define the binning used for pruning vxlgrid
	ca.define_rectangular_binning( 0.5*(ConfigSurfacer::AdvIonPruneBinWidthMin + ConfigSurfacer::AdvIonPruneBinWidthMax), sp.tip );

	if ( ca.allocate_binaries() == false ) {
		cerr << "Allocation of temporaries for binning was unsuccessful!" << "\n";
		return false;
	}

	//thread-local binning
	ca.initialize_bins();
	#pragma omp parallel
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();

		if ( sp.db.at(mt) != NULL ) {
			//allocate memory to store binning
			//##MK::error management
			vector<p3dm3> & here = sp.db.at(mt)->ionpp3;
			vector<size_t> & there = sp.db.at(mt)->ion2bin;
			for( auto it = here.begin(); it != here.end(); it++ ) {
				there.push_back( ca.rectangular_binning( *it ) );
			}
		}
	}

	//sequential binarization of ion position bin occupancy to prepare the pruning
	for( size_t mt = 0; mt < sp.db.size(); mt++ ) {
		if ( sp.db.at(mt) != NULL ) {
			vector<size_t> & these = sp.db.at(mt)->ion2bin;
			for( auto it = these.begin(); it != these.end(); it++ ) {
				if ( ca.IsVacuum[*it] == true ) //visited already, most likely case
					continue;
				else
					ca.IsVacuum[*it] = true;
			}
		}
	}
	//##MK::it seems somewhat counterintuitive to set here the vacuum as true
	//what in fact we do though is an inverted HK finding of a percolating cluster
	//that is infact constituting vacuum using HK algorithm

	if ( ca.identify_vacuum_bins() == false ) {
		cerr << "HK identification of vacuum bin percolating cluster failed" << "\n";
		return false;
	}

	ca.identify_surface_adjacent_bins();
	ca.identify_inside_bins();

	//thread-local filtering of candidate positions best to pass to triangulation library to reduce formal complexity
	#pragma omp parallel
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();

		vector<p3d> mycand;
		if ( sp.db.at(mt) != NULL ) {
			size_t ni = sp.db.at(mt)->ion2bin.size();
			vector<size_t> & bid = sp.db.at(mt)->ion2bin;
			vector<p3dm3> & pid = sp.db.at(mt)->ionpp3;
			for( size_t i = 0; i < ni; i++ ) {
				size_t binid = bid[i];
				if ( ca.IsSurface[bid[i]] == false ) { //most likely ion is not adjacent to surface for large datasets
					continue;
				}
				else {
					mycand.push_back( p3d( pid[i].x, pid[i].y, pid[i].z ) );
				}
			}

			//local binning information no longer necessary, so clear and enforce memory release
			sp.db.at(mt)->ion2bin = vector<size_t>();
		}

		//enforce a barrier to have a deterministic order of the pruning results on cand!
		#pragma omp barrier

		for( size_t thr = 0; thr < sp.db.size(); thr++ ) {
			if ( static_cast<size_t>(mt) == thr ) {
				for( auto it = mycand.begin(); it != mycand.end(); it++ ) {
					cand_positions.push_back( *it );
				}
				mycand = vector<p3d>();
			}
			else {
				//others threads wait in front the barrier
			}
			#pragma omp barrier
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = surf_tictoc.get_memoryconsumption();
	surf_tictoc.prof_elpsdtime_and_mem( "PruningPriorToHullConstruction", APT_XX, APT_IS_PAR, mm, tic, toc );

	cout << "Pruning of candidates was successful " << cand_positions.size() << " remaining in total" << "\n";
	return true;
}


bool surfacerHdl::build_triangle_hull()
{
	double tic = MPI_Wtime();

	if ( ConfigSurfacer::SurfacingMode == E_SURFACE_ALPHASHAPE ) {

		tr.alphashape_core( cand_positions );

		//##MK::cand_positions no longer needed, enforce memory release
		cand_positions = vector<p3d>();
	}

	double toc = MPI_Wtime();
	memsnapshot mm = surf_tictoc.get_memoryconsumption();
	surf_tictoc.prof_elpsdtime_and_mem( "AlphaShapeHullConstruction", APT_XX, APT_IS_SEQ, mm, tic, toc );

	return true;
}


bool surfacerHdl::compute_distances1()
{
	//##MK::we want to compute the distance of each ion to the triangle hull
	//there are challenges however: the ions are balanced across the threads spatially but the triangles are not and
	//because the specimen hull shape narrows, different average number of triangle tests for same number of ions
	//per unit volume are required in base versus top of the specimen, this creates load imbalance
	//especial because ions are loosely ordered wrt to the height in the reconstruction
	//we use therefore a round-robin modulo partitioning of the ions across the processes and dynamic work partitioning
	//of the processes' local workload across the thread team rather than a fixed spatial decompositing and block partitioning
	//##MK::key idea of this algorithm here, build global Rtree of triangles, threads filling with maximum probed distance
	//then sequentialized processing of thread regions by all members of thread team
	//i.e. coorperative distancing per region by all thread team members
	//threads collect results locally than update distances for close to the tip ions in critical region and
	//proceed with next region, despite these optimization tricks there is much more to optimize in the future potentially, namely:
	//MK::an additional modification to the proof-of-concept code is here that the individual costly analytical
	//closestPointOnTriangle tests are reduced by probing first with a simplified collision test against circumspheres
	//so for each ion, we query first an RTree for triangle candidates
	//for all those triangle we probe their circumspheres, if a distance to a circumsphere center versus the
	//current shortest distances is larger, we reject the triangle, if not we go for the more involved test
	//good is that such we reduce branching per ion because fewer analytical triangle tests are done (which have branching)
	//good is that the point versus sphere collision test is trivial
	//bad is that we have on average more memory traffic as we jump between pulling triangle coordinates and spheres
	//##MK::we test first empirically the improvement, one could also pack triangle coordinates and circumspheres
	//##MK::a further improvement could be to make an adaptive requerying to successively prune candidates, like done in
	//M. K\"uhbach and F. Roters MSMSE 2019, however this comes at branch cost and tree traversal, so no clue whether this will
	//in the end turn out to be faster, for this reason, we stick for now with our pruning/sphere/analytical test protocol
	double tic = MPI_Wtime();

	if ( tr.tipsurface.size() < 1 ) {
		cout << "Rank " << get_myrank() << " WARNING: there are no triangles against which to distance" << "\n"; return true;
	}

	try {
		if ( get_myrank() == MASTER ) {
			//distances are significant only at root
			distances = vector<apt_real>( recon_positions.size(), F32MX ); //set all distances to a default value F32MX
			workload = vector<unsigned int>( recon_positions.size(), 0 );
			mydist = vector<MPI_DistAndID>(); //mydist is not required for MASTER, because MASTER writes his results directly into distances
			mywork = vector<MPI_WorkAndID>(); //mywork like mydist
		}
		else { //SLAVES
			//mydist is significant for slaves and non-root processes
			distances = vector<apt_real>();
			workload = vector<unsigned int>();
		}
	}
	catch (bad_alloc &croak) {
		cerr << "Rank " << get_myrank() << " allocation of distances failed!" << "\n"; return false;
	}

	//build shared triangle tree
	if ( bvh.build_rtree( tr.tipsurface ) == true ) {
		cout << "Rank " << get_myrank() << " successful BVH triangle pruning structure build" << "\n";
	}
	else {
		cerr << "Rank " << get_myrank() << " BVH triangle pruning structure build failed!" << "\n"; return false;
	}

	if ( bvh.build_circumspheres( tr.tipsurface ) == true ) {
		cout << "Rank " << get_myrank() << " successful building of circumspheres" << "\n";
	}
	else {
		cerr << "Rank " << get_myrank() << " circumsphere building failed!" << "\n"; return false;
	}

//	toc = MPI_Wtime();
//	mm = owner->tictoc.get_memoryconsumption();
//	owner->tictoc.prof_elpsdtime_and_mem( "SurfTriangleHullRTreeConstruct", APT_BVH, APT_IS_SEQ, mm, tic, toc);
//	cout << "Building triangle tree BVH " << (toc-tic) << " seconds" << endl;

	cout << "Rank " << get_myrank() << " now executing surface distancing ..." << "\n";

	//cooperative computation of distances region by region
//	tic = MPI_Wtime();


	#pragma omp parallel
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		unsigned int NProcesses = get_nranks();
		unsigned int MyProcessID = get_myrank();

		//threadlocal buffering of results to reduce time spent in pragma omp criticals
		//use half the shortest distance to bounding box
		apt_real R = min( min((sp.tip.xmx - sp.tip.xmi), (sp.tip.ymx - sp.tip.ymi) ), (sp.tip.zmx - sp.tip.zmi) ) / 2;

		//MK::if we set here instead a shorter radius and the query does not bring candidates we can be sure that the ion is at least R from the hull
		if ( ConfigSurfacer::DistancingMode == E_DISTANCING_ANALYTICAL_SKIN ) {
			R = ConfigSurfacer::DistancingRadiusMax;
		}

		apt_real RSQR = SQR(R);
		//size_t myworkload = 0;
		//size_t nochecks = 0;

		//MK::the global IDs of the ions are stored interleaved within the ionpp3 object, this is the beauty
		//we will have no further cache-misses associated with finding what is the global ion while processing geometry
		for(int thr = MASTER; thr < nt; thr++) { //everybody executes enforced one after another processing of data for thread thr by
			if ( sp.db.at(thr) != NULL ) {
				vector<p3dm3> const & theseions = sp.db.at(thr)->ionpp3;
				size_t ni = theseions.size();

				vector<unsigned int> mycand;
				vector<MPI_DistAndID> myres;
				vector<MPI_WorkAndID> mywkl;

				//MK::experiences shows that schedule(static,1) caused substantial load imbalance
				#pragma omp for schedule(dynamic,1) nowait
				for( size_t i = 0; i < ni; ++i ) { //all members of the thread team grab ions from current region and do distancing
					//MK::work partitioning across MPI processes
					//get the global ionID, global, i.e. entire specimen which has duplicates in each process
					unsigned int IonID = theseions[i].id;
					//round-robin processing of each nranks-th ion within this process
					unsigned int WhichProcess = IonID % NProcesses;
					if ( WhichProcess != MyProcessID ) { //most likely case, ##MK::yes little bit extra control flow, but analysis of a single ion thousands of flops more costly than this!
						continue;
					}
					else { //a random thread processes this ion
						p3d me = p3d( theseions[i].x, theseions[i].y, theseions[i].z ); //referring to memory owned by potentially different thread than me

						//step one:: prune most triangles of the surface by utilizing bounded volume hierarchy (BVH) Rtree
						hedgesAABB e_aabb( trpl(me.x-R, me.y-R, me.z-R), trpl(me.x+R, me.y+R, me.z+R) );
						mycand.clear();
						mycand = bvh.kauri->query( e_aabb );
						//myworkload += mycand.size();
						if ( mycand.size() > 0 ) { //no triangle close nothing to do distance value was already set, most likely case
							apt_real CurrentBest = RSQR;
							apt_real CurrentValue = RSQR;
							//auto CurrentBestJT = mycand.begin();
							for ( auto jt = mycand.begin(); jt != mycand.end(); jt++ ) {
								unsigned int triid = *jt;
								/*
								//step two::lean collision test against sphere
								CurrentValue = bvh.compute_sqrdist_p3d_circumsphere( me, triid );
								if ( CurrentValue > CurrentBest ) { //most likely not resetting the minimum
									continue;
								}
								else {
								*/
									//step three::costly collision test triangle
									CurrentValue = bvh.compute_sqrdist_p3d_triangle( me, triid );
									if ( CurrentValue > CurrentBest ) { //most likely not resetting the minimum
										continue;
									}
									else {
										CurrentBest = CurrentValue;
										//CurrentBestJT = jt;
									}
								/*
								}
								*/
				//cout << "i/c/nc/best " << i << "\t\t" << c << "\t\t" << nc << "\t\t" << CurrentBest << endl;
							}
							myres.push_back( MPI_DistAndID(CurrentBest, IonID) ); //storage in thread-local memory, without immediate sync

							//unsigned int wkld = ( static_cast<size_t>(CurrentBestJT - mycand.begin()) < static_cast<size_t>(UINT32MX) )
							//		? static_cast<unsigned int>(CurrentBestJT - mycand.begin()) : UINT32MX;
							unsigned int wkld = ( static_cast<size_t>(mycand.size()) <  static_cast<size_t>(UINT32MX) ) ?
									static_cast<unsigned int>(mycand.size()) : UINT32MX;
							mywkl.push_back( MPI_WorkAndID( wkld, IonID) );
//cout << i << "\t\t" << IonID << "\t\t" << CurrentBest << "\n";
						} //all candidate triangles probed
						else {
							//this should not occur we have the entire dataset enclosed because we compute the distance for all ions!
							//++nochecks;
							myres.push_back( MPI_DistAndID( F32MX, IonID) );
							mywkl.push_back( MPI_WorkAndID( 0, IonID) );
						}
					} //thread takes next ion
				} //MK::we use a nowait clause to avoid the implicit barrier of the pragma-omp-for construct
				//thereby completed threads can already fill in the values into MPI process level results buffers

				//done with all thread regions, compute square root only once in parallel and fuse results into process buffer
				#pragma omp critical
				{
					myIonWorkload += myres.size();
					if ( get_myrank() == MASTER ) {
						//threads of Master process store distances immediately in order of I/O within distances
						for( auto jt = myres.begin(); jt != myres.end(); jt++ ) {
							distances.at(jt->id) = sqrt(jt->d); //finally here we go from the more efficient handling of SQR(d) to d
						}
						for( auto wt = mywkl.begin(); wt != mywkl.end(); wt++ ) {
							workload.at(wt->id) = wt->ntriangles;
						}
					}
					else { //== SLAVES by contrast collect data in mydist without a particular order
						mydist.reserve( mydist.size() + myres.size() ); //concentrate all possible reallocations
						for( auto jt = myres.begin(); jt != myres.end(); jt++ ) {
							mydist.push_back( MPI_DistAndID( sqrt(jt->d), jt->id ) );
						}
						myres = vector<MPI_DistAndID>();
						mywork.reserve( mywork.size() + mywkl.size() );
						for( auto wt = mywkl.begin(); wt != mywkl.end(); wt++ ) {
							mywork.push_back( MPI_WorkAndID( wt->ntriangles, wt->id ) );
						}
						mywkl = vector<MPI_WorkAndID>();
					}
	//cout << "Rank " << get_myrank() << " thread " << mt << " processed " << myres.size() << "/" << nochecks << " with/without checks @" << myworkload << " triangles for region " << thr << "\n"; //" took " << (mytoc-mytic) << " seconds" << endl;
				}
			}

			//the barrier is necessary to avoid that a thr once having passed the nowait critical region reenters the pragma-omp-for construct and control flow diverges
			#pragma omp barrier

			//threads work cooperatively on all ions of region
			//##MK::assuming regions are large and potentially billions of triangle distance to be computed load
			//should be much better distributed, making it at least for now admissible to spend some time at barrier

			#pragma omp master
			{
				cout << "Rank " << get_myrank() << " cooperative distancing for ions on thread " << thr << " successful" << "\n"; // took " << (mytoc-mytic) << "/" << (omp_get_wtime()-mytic) << " without/with barrier" << endl;
			} //no barrier at the end of omp master

		} //next thr

	} //implicit barrier of pragma-omp-parallel end of parallel region

	//##MK::technically from now on BVH no longer required

	double toc = MPI_Wtime();
	memsnapshot mm = surf_tictoc.get_memoryconsumption();
	surf_tictoc.prof_elpsdtime_and_mem( "Ion2SurfDistancing", APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " computing parallelized distance to surface completed in " << (toc-tic) << " seconds" << "\n";
	return true;
}


bool surfacerHdl::compute_distances2()
{
	//see comments for compute_distances1 instead here we perform a adaptive requerying to successively prune candidates while narrowing down the dist
	//M. K\"uhbach and F. Roters MSMSE 2019, however this comes at branch cost and tree traversal
	double tic = MPI_Wtime();

	if ( tr.tipsurface.size() < 1 ) {
		cout << "Rank " << get_myrank() << " WARNING: there are no triangles against which to distance" << "\n"; return true;
	}

	try {
		if ( get_myrank() == MASTER ) {
			//distances are significant only at root
			distances = vector<apt_real>( recon_positions.size(), F32MX ); //set all distances to a default value F32MX
			workload = vector<unsigned int>( recon_positions.size(), 0 );
			//mydist and mywork not required for MASTER, it writes directly into distances
		}
		else { //SLAVES, mydist is significant for slaves and non-root processes
			distances = vector<apt_real>();
			workload = vector<unsigned int>();
		}
	}
	catch (bad_alloc &croak) {
		cerr << "Rank " << get_myrank() << " allocation of distances failed!" << "\n"; return false;
	}

	//build shared triangle tree
	if ( bvh.build_rtree( tr.tipsurface ) == true ) {
		cout << "Rank " << get_myrank() << " successful BVH triangle pruning structure build" << "\n";
	}
	else {
		cerr << "Rank " << get_myrank() << " BVH triangle pruning structure build failed!" << "\n"; return false;
	}

	/*if ( bvh.build_circumspheres( tr.tipsurface ) == true ) {
		cout << "Rank " << get_myrank() << " successful building of circumspheres" << "\n";
	}
	else {
		cerr << "Rank " << get_myrank() << " circumsphere building failed!" << "\n"; return false;
	}*/

	cout << "Rank " << get_myrank() << " now executing surface distancing ..." << "\n";

	//cooperative computation of distances region by region
	#pragma omp parallel
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		unsigned int NProcesses = get_nranks();
		unsigned int MyProcessID = get_myrank();

		//threadlocal buffering of results to reduce time spent in pragma omp criticals
		//start with half the shortest distance to bounding box
		apt_real R = min( min((sp.tip.xmx - sp.tip.xmi), (sp.tip.ymx - sp.tip.ymi) ), (sp.tip.zmx - sp.tip.zmi) ) / 2;
		//MK::if we set here instead a shorter radius and the query does not bring candidates we can be sure that the ion is at least R from the hull
		if ( ConfigSurfacer::DistancingMode == E_DISTANCING_ANALYTICAL_SKIN ) {
			R = ConfigSurfacer::DistancingRadiusMax;
		}
		apt_real RequeryThrshld = ConfigSurfacer::RequeryingThreshold;
		apt_real RSQR = SQR(R);

		//MK::the global IDs of the ions are stored interleaved within the ionpp3 object, this is the beauty
		//we will have no further cache-misses associated with finding what is the global ion while processing geometry
		for(int thr = MASTER; thr < nt; thr++) { //everybody executes enforced one after another processing of data for thread thr by
			if ( sp.db.at(thr) != NULL ) {
				vector<p3dm3> const & theseions = sp.db.at(thr)->ionpp3;
				size_t ni = theseions.size();

				vector<unsigned int> mycand;
				vector<MPI_DistAndID> myres;
				vector<MPI_WorkAndID> mywkl;

				//MK::experiences shows that schedule(static,1) caused substantial load imbalance
				#pragma omp for schedule(dynamic,1) nowait
				for( size_t i = 0; i < ni; i++ ) { //all members of the thread team grab ions from current region and do distancing
					//MK::work partitioning across MPI processes
					//get the global ionID, global, i.e. entire specimen which has duplicates in each process
					unsigned int IonID = theseions[i].id;
					//round-robin processing of each nranks-th ion within this process
					unsigned int WhichProcess = IonID % NProcesses;
					if ( WhichProcess != MyProcessID ) { //most likely case, ##MK::yes little bit extra control flow, but analysis of a single ion thousands of flops more costly than this!
						continue;
					}
					else { //a random thread processes this ion
						p3d me = p3d( theseions[i].x, theseions[i].y, theseions[i].z ); //referring to memory owned by potentially different thread than me

						//before requerying step one:: prune most triangles of the surface by utilizing bounded volume hierarchy (BVH) Rtree
						hedgesAABB e_aabb( trpl(me.x-R, me.y-R, me.z-R), trpl(me.x+R, me.y+R, me.z+R) );
						mycand.clear();
						mycand = bvh.kauri->query( e_aabb );

						if ( mycand.size() > 0 ) { //most likely case
							size_t ntests = 0;
							size_t ntri = mycand.size();
							size_t tr = 0;
							apt_real CurrentBest = RSQR;
							apt_real CurrentValue = RSQR;
							while ( tr < ntri ) { //head controlled, if tr == 0 and ntri == 0 will not execute
								unsigned int triid = mycand.at(tr);
								CurrentValue = bvh.compute_sqrdist_p3d_triangle( me, triid );
								if ( CurrentValue > CurrentBest ) { //most likely not resetting the minimum
									tr++;
									continue;
								}
								else {
									apt_real OldBest = CurrentBest;
									//update best, we found a shorter one!
									CurrentBest = CurrentValue;
									if ( OldBest > EPSILON ) { //is also a numerical stable requerying possible eventually?
										if ( (CurrentValue / OldBest) > RequeryThrshld ) { //insufficient distance reduction, do not requery
											tr++;
											continue;
										}
										else { //substantial distance reduction achieved, it is likely useful to requery now
											apt_real Rnew = sqrt(CurrentBest) + EPSILON; //better a little bit larger R to catch case of exact overlap
											hedgesAABB e_aabb( trpl(me.x-Rnew, me.y-Rnew, me.z-Rnew),
													trpl(me.x+Rnew, me.y+Rnew, me.z+Rnew) );
											mycand.clear();
											ntests += tr; //so many tests were performed before this requerying
											mycand = bvh.kauri->query( e_aabb );
											tr = 0;
											ntri = mycand.size();
											continue;
										} //back to while
									} //case stable requerying done
									else { //querying might not be stable because float sqrt(eps) is ill-posed then do not requery and instead tests remaining candidates
										tr++;
										continue;
									}
								}
							} //eventually all candidates tested
							ntests += tr;
							myres.push_back( MPI_DistAndID(CurrentBest, IonID) ); //storage in thread-local memory, without immediate sync
							unsigned int wkld = ( static_cast<size_t>(ntests) < static_cast<size_t>(UINT32MX) ) ? static_cast<unsigned int>(ntests) : UINT32MX;
							mywkl.push_back( MPI_WorkAndID( wkld, IonID) );
//cout << i << "\t\t" << IonID << "\t\t" << CurrentBest << "\n";
						} //all candidate triangles probed
						else {
							//this should not occur we have the entire dataset enclosed because we compute the distance for all ions
							myres.push_back( MPI_DistAndID( F32MX, IonID) );
							mywkl.push_back( MPI_WorkAndID( 0, IonID) );
						}
					} //thread takes next ion
				} //MK::we use a nowait clause to avoid the implicit barrier of the pragma-omp-for construct
				//thereby completed threads can already fill in the values into MPI process level results buffers

				//done with all thread regions, compute square root only once in parallel and fuse results into process buffer
				#pragma omp critical
				{
					myIonWorkload += myres.size();
					if ( get_myrank() == MASTER ) {
						//threads of Master process store distances immediately in order of I/O within distances
						for( auto jt = myres.begin(); jt != myres.end(); jt++ ) {
							distances.at(jt->id) = sqrt(jt->d); //finally here we go from the more efficient handling of SQR(d) to d
						}
						for( auto wt = mywkl.begin(); wt != mywkl.end(); wt++ ) {
							workload.at(wt->id) = wt->ntriangles;
						}
					}
					else { //== SLAVES by contrast collect data in mydist without a particular order
						mydist.reserve( mydist.size() + myres.size() ); //concentrate all possible reallocations
						for( auto jt = myres.begin(); jt != myres.end(); jt++ ) {
							mydist.push_back( MPI_DistAndID( sqrt(jt->d), jt->id ) );
						}
						myres = vector<MPI_DistAndID>();
						mywork.reserve( mywork.size() + mywkl.size() );
						for( auto wt = mywkl.begin(); wt != mywkl.end(); wt++ ) {
							mywork.push_back( MPI_WorkAndID( wt->ntriangles, wt->id ) );
						}
						mywkl = vector<MPI_WorkAndID>();
					}
	//cout << "Rank " << get_myrank() << " thread " << mt << " processed " << myres.size() << "/" << nochecks << " with/without checks @" << myworkload << " triangles for region " << thr << "\n"; //" took " << (mytoc-mytic) << " seconds" << endl;
				}
			}

			//the barrier is necessary to avoid that a thr once having passed the nowait critical region reenters the pragma-omp-for construct and control flow diverges
			#pragma omp barrier

			//threads work cooperatively on all ions of region
			//##MK::assuming regions are large and potentially billions of triangle distance to be computed load
			//should be much better distributed, making it at least for now admissible to spend some time at barrier

			#pragma omp master
			{
				cout << "Rank " << get_myrank() << " cooperative distancing for ions on thread " << thr << " successful" << "\n"; // took " << (mytoc-mytic) << "/" << (omp_get_wtime()-mytic) << " without/with barrier" << endl;
			} //no barrier at the end of omp master

		} //next thr

	} //implicit barrier of pragma-omp-parallel end of parallel region

	//##MK::technically from now on BVH no longer required

	double toc = MPI_Wtime();
	memsnapshot mm = surf_tictoc.get_memoryconsumption();
	surf_tictoc.prof_elpsdtime_and_mem( "Ion2SurfDistancing", APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " computing parallelized distance to surface completed in " << (toc-tic) << " seconds" << "\n";
	return true;
}


bool surfacerHdl::init_target_file()
{
	//##MK::generate group hierarchy of an open source IFES APTTC APTH5 HDF5 file
	string h5fn_out = "PARAPROBE.Surfacer.SimID." + to_string(ConfigShared::SimID) + ".h5";
cout << "Initializing target file " << h5fn_out << "\n";

	if ( debugh5Hdl.create_surfacer_apth5( h5fn_out ) == WRAPPED_HDF5_SUCCESS ) {
		return true;
	}
	else {
		return false;
	}
}


bool surfacerHdl::communicate_distances1()
{
	double tic = MPI_Wtime();

	//then we have two options, the simplest, enforce sequential loop of individual slave sends, master receives
	//or leaner a MPI_Gatherv on the master
	//##MK::we opt first for the simpler strategy, because computing time for the distances >> communication time
	if ( get_nranks() < 2 ) {
		cout << "Rank MASTER identified he is alone, so no need to communicate data across MPI_COMM_WORLD" << "\n"; return true;
	}

	int localhealth = 1;
	int globalhealth = 0;
	if ( mydist.size() >= static_cast<size_t>(INT32MX) ) {
		cerr << "Rank " << get_myrank() << " local mydist results buffer is too large for current implementation!" << "\n"; localhealth = 0;
	}
	if ( mywork.size() != mydist.size() ) {
		cerr << "Rank " << get_myrank() << " local mydist.size() != mywork.size() which is inconsistent!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all processes are able to contribute results!" << "\n";
		return false;
	}

	unsigned int nlocal_results = 0;
	if ( get_myrank() != MASTER ) { //master has already written his results in correct output buffer
		nlocal_results = static_cast<unsigned int>(mydist.size());
	}
	vector<unsigned int> rk2nres = vector<unsigned int>( get_nranks(), 0 );
	unsigned int* buf = NULL;
	if ( myrank == MASTER ) {
		buf = rk2nres.data(); //raw pointer to array ie &rk2nres[0]
	}
	MPI_Gather( &nlocal_results, 1, MPI_UNSIGNED, buf, 1, MPI_UNSIGNED, MASTER, MPI_COMM_WORLD );

	size_t nrestotal = 0;
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < rk2nres.size(); i++ ) {
			cout << "Rank " << i << " contributes " << rk2nres.at(i) << " results" << "\n";
		}
		for( auto it = rk2nres.begin(); it != rk2nres.end(); it++ ) {
			nrestotal += static_cast<size_t>(*it);
		}
		cout << "Rank MASTER has counted " << nrestotal << " results from slaves and expects " << recon_positions.size() << " in total " << "\n";

		//is this consistent with expectation?
		/*if ( nrestotal != recon_positions.size() ) {
			localhealth = 0;
		}*/
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that the reported number of results is different from expectation!" << "\n";
		return false;
	}

	//no differences, master has already allocated its results buffer in distances and workload
	//so missing is now only the snd and rcv buffers i.e. mydist and mywork from the slaves
	//we enforce here, at the costs of performance, that the distance values end up correctly
	//in the same order as the reconstructed ions positions

	vector<MPI_DistAndID> rcv1; //only significant at root
	vector<MPI_WorkAndID> rcv2;
	//MPI_Status status = MPI_SUCCESS;
	int ierr = 0;
	for( int rk = MASTER+1; rk < get_nranks(); rk++ ) {
		if ( get_myrank() == rk) {
			//if ( get_myrank() != MASTER ) { //applies to only one process at a time, and for sure not to the MASTER
cout << "rk " << rk << " Rank " << rk << " local results " << nlocal_results << "\n";
			if ( nlocal_results > 0 ) {
				ierr = MPI_Send( mydist.data(), nlocal_results, MPI_DistAndID_Type, MASTER, get_nranks()+rk+0, MPI_COMM_WORLD );
				if ( ierr != MPI_SUCCESS ) {
					cerr << "Rank " << rk << " MPI_Send mydist.data() failed!" << "\n"; localhealth = 0;
				}
 				ierr = MPI_Send( mywork.data(), nlocal_results, MPI_WorkAndID_Type, MASTER, get_nranks()+rk+1, MPI_COMM_WORLD );
				if ( ierr != MPI_SUCCESS ) {
					cerr << "Rank " << rk << " MPI_Send mywork.data() failed!" << "\n"; localhealth = 0;
				}

				globalhealth = 0;
				MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
				if ( globalhealth != get_nranks() ) {
					cerr << "Rank " << get_myrank() << " has recognized a communication error for " << rk << "!" << "\n";
					return false;
				}
			}
			else {
				cerr << "Rank " << rk << " nlocal_results == 0 for rank " << rk << "\n";
			}
		}
		else {
			if ( get_myrank() == MASTER ) {
				nlocal_results = rk2nres.at(rk);
cout << "rk " << rk << " Rank " << MASTER << " local results " << nlocal_results << "\n";
				if ( nlocal_results > 0 ) {
					rcv1 = vector<MPI_DistAndID>( nlocal_results, MPI_DistAndID() );
					rcv2 = vector<MPI_WorkAndID>( nlocal_results, MPI_WorkAndID() );
					MPI_Recv( rcv1.data(), nlocal_results, MPI_DistAndID_Type, rk, get_nranks()+rk+0, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
					/*if ( status != MPI_SUCCESS ) {
						cerr << "Rank " << MASTER << " MPI_Recv from " << rk << " for mydist.data() failed!" << "\n"; localhealth = 0;
					}*/
					MPI_Recv( rcv2.data(), nlocal_results, MPI_WorkAndID_Type, rk, get_nranks()+rk+1, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
					/*if ( status != MPI_SUCCESS ) {
						cerr << "Rank " << MASTER << " MPI_Recv from " << rk << " for mywork.data() failed!" << "\n"; localhealth = 0;
					}*/

					globalhealth = 0;
					MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
					if ( globalhealth != get_nranks() ) {
						cerr << "Rank " << get_myrank() << " MASTER has recognized a communication error for " << rk << "!" << "\n";
						return false;
					}

					//write back to target buffer
					for ( auto it = rcv1.begin(); it != rcv1.end(); it++ ) {
						distances.at(it->id) = it->d;
					}
					rcv1 = vector<MPI_DistAndID>();
					for( auto jt = rcv2.begin(); jt != rcv2.end(); jt++ ) {
						workload.at(jt->id) = jt->ntriangles;
					}
					rcv2 = vector<MPI_WorkAndID>();
				}
				else {
					cerr << "Rank " << MASTER << " nlocal_results == 0 for rank " << rk << "\n";
				}
			}
		}
		//should not be necessary, ##optimize to use MPI_Gatherv in the future
		MPI_Barrier( MPI_COMM_WORLD );
	}

	//free local temporaries
	mydist = vector<MPI_DistAndID>();
	mywork = vector<MPI_WorkAndID>();

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	//surfacer_tictoc.prof_elpsdtime_and_mem( "CollectResultsOnMasterProcess", APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " participated successfully in results collection stage took " << (toc-tic) << " seconds" << "\n";
	return true;
}


bool surfacerHdl::communicate_distances2()
{
	double tic = MPI_Wtime();

	//then we have two options, the simplest, enforce sequential loop of individual slave sends, master receives
	//or leaner a MPI_Gatherv on the master
	//##MK::we opt first for the simpler strategy, because computing time for the distances >> communication time
	if ( get_nranks() < 2 ) {
		cout << "Rank MASTER identified he is alone, so no need to communicate data across MPI_COMM_WORLD" << "\n"; return true;
	}

	int localhealth = 1;
	int globalhealth = 0;
	if ( mydist.size() >= static_cast<size_t>(INT32MX) ) {
		cerr << "Rank " << get_myrank() << " local mydist results buffer is too large for current implementation!" << "\n"; localhealth = 0;
	}
	if ( mydist.size() != mywork.size() ) {
		cerr << "Rank " << get_myrank() << " local mydist.size() != mywork.size() which is inconsistent!" << "\n"; localhealth = 0;
	}
	if ( myIonWorkload >= static_cast<size_t>(INT32MX) ) {
		cerr << "Rank " << get_myrank() << " myIonWorkload is too large for current implementation!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all processes are able to contribute results!" << "\n";
		return false;
	}

	int nlocal_result = static_cast<int>(myIonWorkload);
	vector<int> rk2nres = vector<int>( get_nranks(), 0 );
	int* buf = NULL;
	buf = rk2nres.data(); //raw pointer to array ie &rk2nres[0]

	MPI_Allgather( &nlocal_result, 1, MPI_INT, buf, 1, MPI_INT, MPI_COMM_WORLD );

	unsigned int nrestotal = 0;
	for( auto it = rk2nres.begin(); it != rk2nres.end(); it++ ) {
		nrestotal = nrestotal + static_cast<unsigned int>(*it);
		if ( get_myrank() == MASTER ) {
			cout << "Rank " << (it - rk2nres.begin()) << " contributes " << *it << " results" << "\n";
		}
	}
	cout << "Rank " << get_myrank() << " has counted " << nrestotal << " results from all and expects " << recon_positions.size() << " in total " << "\n";

	//is this consistent with expectation?
	if ( static_cast<size_t>(nrestotal) != recon_positions.size() ) {
		localhealth = 0;
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that the reported number of results is different from expectation!" << "\n";
		return false;
	}

	//no differences, master has already allocated its results buffer in distances and workload
	//so missing is now only the snd and rcv buffers i.e. mydist and mywork from the slaves
	//we enforce here, at the costs of performance, that the distance values end up correctly
	//in the same order as the reconstructed ions positions

	vector<MPI_DistAndID> rcv1; //only significant at root
	vector<MPI_WorkAndID> rcv2;
	int ierr = 0;
	for( int rk = MASTER+1; rk < get_nranks(); rk++ ) {
		//everybody now know how much to send if any results from rk
		if ( rk2nres.at(rk) > 0 ) {
			if ( get_myrank() != MASTER ) { //most likely case
				if ( get_myrank() == rk ) { //only one sends at a time
					ierr = MPI_Send( mydist.data(), rk2nres.at(rk), MPI_DistAndID_Type, MASTER, get_nranks()+rk+0, MPI_COMM_WORLD );
					if ( ierr != MPI_SUCCESS ) {
						cerr << "Rank " << rk << " MPI_Send mydist.data() failed!" << "\n"; localhealth = 0;
					}
					ierr = MPI_Send( mywork.data(), rk2nres.at(rk), MPI_WorkAndID_Type, MASTER, get_nranks()+rk+1, MPI_COMM_WORLD );
					if ( ierr != MPI_SUCCESS ) {
						cerr << "Rank " << rk << " MPI_Send mywork.data() failed!" << "\n"; localhealth = 0;
					}
				}
				//else non-rk slaves idle
			}
			else { //== MASTER
				rcv1 = vector<MPI_DistAndID>( rk2nres.at(rk), MPI_DistAndID() );
				rcv2 = vector<MPI_WorkAndID>( rk2nres.at(rk), MPI_WorkAndID() );

				MPI_Recv( rcv1.data(), rk2nres.at(rk), MPI_DistAndID_Type, rk, get_nranks()+rk+0, MPI_COMM_WORLD, MPI_STATUS_IGNORE );

				MPI_Recv( rcv2.data(), rk2nres.at(rk), MPI_WorkAndID_Type, rk, get_nranks()+rk+1, MPI_COMM_WORLD, MPI_STATUS_IGNORE );

				//write back to target buffer
				for ( auto it = rcv1.begin(); it != rcv1.end(); it++ ) {
					distances.at(it->id) = it->d;
				}
				rcv1 = vector<MPI_DistAndID>();
				for( auto jt = rcv2.begin(); jt != rcv2.end(); jt++ ) {
					workload.at(jt->id) = jt->ntriangles;
				}
				rcv2 = vector<MPI_WorkAndID>();
			}

			globalhealth = 0;
			MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
			if ( globalhealth != get_nranks() ) {
				cerr << "Rank " << get_myrank() << " MASTER has recognized a communication error for " << rk << "!" << "\n";
				return false;
			}
		}
		//should not be necessary, ##optimize to use MPI_Gatherv in the future
		MPI_Barrier( MPI_COMM_WORLD );
	}

	//free local temporaries
	mydist = vector<MPI_DistAndID>();
	mywork = vector<MPI_WorkAndID>();

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	//surfacer_tictoc.prof_elpsdtime_and_mem( "CollectResultsOnMasterProcess", APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " participated successfully in results collection stage took " << (toc-tic) << " seconds" << "\n";
	return true;
}


bool surfacerHdl::write_triangles_to_apth5()
{
	/*
	double tic = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	*/
	if ( tr.tipsurface.size()*3 >= static_cast<size_t>(UINT32MX) ) {
		cerr << "The total number of triangle vertices to write out is larger than currently supported by the implementation" << "\n"; return false;
		//##MK::change wuibuf to a 64-bit type , e.g. ui64 size_t
	}
	if ( tr.tipsurface.size() < 1 ) {
		cout << "WARNING:: There are no triangles to report" << "\n"; return true;
	}

	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();

	//write metadata pertaining to the alpha shape construction to the file
	//which a value was used?
	vector<float> f32;
	f32.push_back( tr.hullifo.alph );
	ifo = h5iometa( PARAPROBE_SURF_ASHAPE_META_AVALUE, 1, 1 );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_ASHAPE_META_AVALUE create failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_SURF_ASHAPE_META_AVALUE create " << status << "\n";
	offs = h5offsets( 0, 1, 0, 1, 1, 1);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_ASHAPE_META_AVALUE write failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_SURF_ASHAPE_META_AVALUE write " << status << "\n";
	f32 = vector<float>();

	//how many candidates
	vector<unsigned int> u32;
	u32.push_back( static_cast<unsigned int>(tr.hullifo.ncand) );
	ifo = h5iometa( PARAPROBE_SURF_ASHAPE_META_CAND_N, 1, 1 );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_ASHAPE_META_CAND_N create failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_SURF_ASHAPE_META_CAND_N create " << status << "\n";
	offs = h5offsets( 0, 1, 0, 1, 1, 1);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_ASHAPE_META_CAND_N write failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_SURF_ASHAPE_META_CAND_N write " << status << "\n";
	u32 = vector<unsigned int>();

	//which bin width chosen for the voxelization in nanometer?
	f32.push_back( ca.vxlgrid.width );
	ifo = h5iometa( PARAPROBE_SURF_ASHAPE_META_VXL_WIDTH, 1, 1 );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_ASHAPE_META_VXL_WIDTH create failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_SURF_ASHAPE_META_VXL_WIDTH create " << status << "\n";
	offs = h5offsets( 0, 1, 0, 1, 1, 1);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_ASHAPE_META_VXL_WIDTH write failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_SURF_ASHAPE_META_VXL_WIDTH write " << status << "\n";
	f32 = vector<float>();

	//write auxiliary topological information to visualize triangle hull using XDMF and e.g. Paraview
	u32.resize((1+1+3)*tr.tipsurface.size()); //total number of elements 5E6*5*4B so <=100MB
	//+1 first index identifies geometric primitive, here triangle polygon
	//+1 second index tells how many elements to excepttypefor XDMF is XDMF keyword what we see
	//+3 the third, fourth, and fifth value gives the IDs of the vertices that define the triangles
	size_t i = 0;
	size_t j = 0;
	unsigned int v = 0;
	for( i = 0; i < tr.tipsurface.size(); i++ ) {
		u32[j+0] = 3; //primitive key
		u32[j+1] = 3; //number of vertices to form the primitive
		u32[j+2] = v+0; //vertex IDs
		u32[j+3] = v+1;
		u32[j+4] = v+2;
		j = j + 5;
		v = v + 3; //##MK::multiple counting
	}

	ifo = h5iometa( PARAPROBE_SURF_ASHAPE_RES_HULL_TOPO, (1+1+3)*tr.tipsurface.size(), 1 );
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_ASHAPE_RES_HULL_TOPO create failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_SURF_ASHAPE_RES_HULL_TOPO create " << status << "\n";
	offs = h5offsets( 0, (1+1+3)*tr.tipsurface.size(), 0, 1, (1+1+3)*tr.tipsurface.size(), 1);
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_ASHAPE_RES_HULL_TOPO write failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_SURF_ASHAPE_RES_HULL_TOPO write " << status << "\n";
	u32 = vector<unsigned int>();

	//store actual vertex coordinates, ##MK::here we could remove redundancies and save space,
	//but at the cost of more involved pre-processing during visualization
	f32.resize(PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ_NCMAX*tr.tipsurface.size()); //total number of elements 5E6*9*4B so <=180MB
	i = 0;
	j = 0;
	for( i = 0; i < tr.tipsurface.size(); i++ ) {
		f32[j+0] = tr.tipsurface.at(i).x1; //v1, reinterleaved storage
		f32[j+1] = tr.tipsurface[i].y1;
		f32[j+2] = tr.tipsurface[i].z1;
		f32[j+3] = tr.tipsurface[i].x2; //v2,
		f32[j+4] = tr.tipsurface[i].y2;
		f32[j+5] = tr.tipsurface[i].z2;
		f32[j+6] = tr.tipsurface[i].x3; //v3,
		f32[j+7] = tr.tipsurface[i].y3;
		f32[j+8] = tr.tipsurface[i].z3;
		j = j + 9;
	}
	ifo = h5iometa( PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ, tr.tipsurface.size(), PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ create failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ create " << status << "\n";
	offs = h5offsets( 0, tr.tipsurface.size(), 0, PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ_NCMAX, tr.tipsurface.size(), PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ write failed! " << status << "\n";
	}
cout << "PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ write " << status << "\n";

	/*
	//peak memory consumption
	mm = owner->owner->tictoc.get_memoryconsumption();
	*/
	f32 = vector<float>();

	string xdmffn = "PARAPROBE.Surfacer.SimID." + to_string(ConfigShared::SimID) + ".h5" + ".AlphaShape.xdmf";
	debugxdmf.create_alphashape_file( xdmffn, 1*tr.tipsurface.size(),
			5*tr.tipsurface.size(), 9*tr.tipsurface.size(), debugh5Hdl.h5resultsfn );
	//5 because two XDMF geometric primitive type keys + 3 vertex ID
	//9 because 3*3 float vertex coordinate values

	/*
	double toc = MPI_Wtime();
	owner->owner->tictoc.prof_elpsdtime_and_mem( "HDF5XDMFReportTriangleHull", APT_IO, APT_IS_SEQ, mm, tic, toc);
	*/

	return true;
	//##MK::vector<pair<string,string>> attrbuf;
	//attrbuf.push_back( make_pair("SIUnit_DetX","mm") );
	//attrbuf.push_back( make_pair("SIUnit_DetY","mm") );
	//status = debugh5Hdl.write_attribute_string( APTH5_ACQ_02_DETECTOR_SIZE, attrbuf );
}


bool surfacerHdl::write_distances_to_apth5()
{
	//use distances on MASTER in-place
	double tic = MPI_Wtime();

	int status = 0;
	h5iometa ifo = h5iometa( PARAPROBE_SURF_DIST_RES_D, distances.size()/PARAPROBE_SURF_DIST_RES_D_NCMAX,
			PARAPROBE_SURF_DIST_RES_D_NCMAX );
	status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_DIST_RES_D create failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_SURF_DIST_RES_D create " << status << "\n";
	h5offsets offs = h5offsets( 0, distances.size()/PARAPROBE_SURF_DIST_RES_D_NCMAX, 0, PARAPROBE_SURF_DIST_RES_D_NCMAX,
				distances.size()/PARAPROBE_SURF_DIST_RES_D_NCMAX, PARAPROBE_SURF_DIST_RES_D_NCMAX);
	status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, distances );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_DIST_RES_D write failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_SURF_DIST_RES_D write " << status << "\n";

	ifo = h5iometa( PARAPROBE_SURF_DIST_META_NTRI, this->workload.size()/PARAPROBE_SURF_DIST_META_NTRI_NCMAX,
			PARAPROBE_SURF_DIST_META_NTRI_NCMAX);
	status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_DIST_META_NTRI create failed! " << status << "\n"; return false;
	}
cout << "PARAPROBE_SURF_DIST_META_NTRI_NCMAX create " << status << "\n";
	offs = h5offsets( 0, workload.size()/PARAPROBE_SURF_DIST_META_NTRI_NCMAX, 0, PARAPROBE_SURF_DIST_META_NTRI_NCMAX,
			workload.size()/PARAPROBE_SURF_DIST_META_NTRI_NCMAX, PARAPROBE_SURF_DIST_META_NTRI_NCMAX );
	status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, workload );
	if ( status != WRAPPED_HDF5_SUCCESS ) {
		cerr << "PARAPROBE_SURF_DIST_META_NTRI write failed! " << status << "\n"; return false;
	}

	string xdmffn = "PARAPROBE.Surfacer.SimID." + to_string(ConfigShared::SimID) + ".h5" + ".Dist2Hull.xdmf";
	debugxdmf.create_distance_file( xdmffn, distances.size(), inputH5Hdl.h5resultsfn, debugh5Hdl.h5resultsfn );

	distances = vector<apt_real>();
	workload = vector<unsigned int>();

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	surf_tictoc.prof_elpsdtime_and_mem( "WriteDistancesToHDF5", APT_XX, APT_IS_SEQ, mm, tic, toc );

	return true;
}


void surfacerHdl::init_mpidatatypes()
{
	int elementCounts1[2] = {1, 1}; //2};
	MPI_Aint displacements1[2] = {0, 1 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes1[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, elementCounts1, displacements1, oldTypes1, &MPI_DistAndID_Type);
	MPI_Type_commit(&MPI_DistAndID_Type);

    MPI_Type_contiguous( 2, MPI_UNSIGNED, &MPI_WorkAndID_Type );
    MPI_Type_commit(&MPI_WorkAndID_Type);

	int elementCounts2[2] = {3, 1};
	MPI_Aint displacements2[2] = {0, 3 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes2[2] = {MPI_FLOAT, MPI_UNSIGNED};
	MPI_Type_create_struct(2, elementCounts2, displacements2, oldTypes2, &MPI_Ion_Type);
	MPI_Type_commit(&MPI_Ion_Type);

    MPI_Type_contiguous( 3, MPI_FLOAT, &MPI_IonPositions_Type );
    MPI_Type_commit(&MPI_IonPositions_Type);
}


int surfacerHdl::get_myrank()
{
	return this->myrank;
}


int surfacerHdl::get_nranks()
{
	return this->nranks;
}


void surfacerHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void surfacerHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}
