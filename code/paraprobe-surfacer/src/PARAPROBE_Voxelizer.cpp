//##MK::CODE

#include "PARAPROBE_Voxelizer.h"

vxlizer::vxlizer()
{
	vxlgrid = sqb();

	IsVacuum = NULL;
	IsSurface = NULL;
	IsInside = NULL;
}


vxlizer::~vxlizer()
{
	delete [] IsVacuum; IsVacuum = NULL;
	delete [] IsSurface; IsSurface = NULL;
	delete [] IsInside; IsInside = NULL;
}


void vxlizer::define_rectangular_binning( const apt_real bwidth, const aabb3d rve )
{
	//expand by one voxel guardzone of edgelen bwidth
	vxlgrid = sqb();

	aabb3d ti = rve;
	ti.blowup( bwidth );
	
	//define cuboidal grid of bwidth sizes cubes, one layer guardzone on each side
	vxlgrid.nx = static_cast<size_t>( ceil((ti.xmx - ti.xmi) / bwidth) ); vxlgrid.nx += 2;
	vxlgrid.ny = static_cast<size_t>( ceil((ti.ymx - ti.ymi) / bwidth) ); vxlgrid.ny += 2;
	vxlgrid.nz = static_cast<size_t>( ceil((ti.zmx - ti.zmi) / bwidth) ); vxlgrid.nz += 2;

	vxlgrid.nxy = vxlgrid.nx * vxlgrid.ny;
	vxlgrid.nxyz = vxlgrid.nxy * vxlgrid.nz;

	vxlgrid.width = bwidth;
	vxlgrid.box = ti;
}


bool vxlizer::allocate_binaries()
{
	if ( IsVacuum != NULL ) {
		delete [] IsVacuum; IsVacuum = NULL;
	}
	if ( IsSurface != NULL ) {
		delete [] IsSurface; IsSurface = NULL;
	}
	if ( IsInside != NULL ) {
		delete [] IsInside; IsInside = NULL;
	}
	
	try {
		IsVacuum = new bool[vxlgrid.nxyz];
	}
	catch(bad_alloc &croak) {
		cerr << "Allocation of IsVacuum binary container failed!" << "\n";
		return false;
	}
	try {
		IsSurface = new bool[vxlgrid.nxyz];
	}
	catch(bad_alloc &croak) {
		cerr << "Allocation of IsSurface binary container failed!" << "\n";
		return false;
	}
	try {
		IsInside = new bool[vxlgrid.nxyz];
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of IsInside binary container failed!" << "\n";
		return false;
	}
	
	return true;	
}


void vxlizer::initialize_bins()
{
	for( size_t i = 0; i < vxlgrid.nxyz; i++ ) {
		IsVacuum[i] = false;
		IsSurface[i] = false;
		IsInside[i] = false;
	}
}


size_t vxlizer::rectangular_binning( const p3dm3 p )
{
	size_t res = -1;

	apt_real tmpd = (p.x - vxlgrid.box.xmi) / vxlgrid.width;
	size_t tmpsz = static_cast<size_t>(tmpd);
	size_t x = (tmpsz != vxlgrid.nx-1) ? 1+tmpsz : vxlgrid.nx-1;

	tmpd = (p.y - vxlgrid.box.ymi) / vxlgrid.width;
	tmpsz = static_cast<size_t>(tmpd);
	size_t y = (tmpsz != vxlgrid.ny-1) ? 1+tmpsz : vxlgrid.ny-1;

	tmpd = (p.z - vxlgrid.box.zmi) / vxlgrid.width;
	tmpsz = static_cast<size_t>(tmpd);
	size_t z = (tmpsz != vxlgrid.nz-1) ? 1+tmpsz : vxlgrid.nz-1;

	res = x + y*vxlgrid.nx + z*vxlgrid.nxy;

	return res;
}


bool vxlizer::identify_vacuum_bins()
{
	//MK::pruning strategy, hole filling strategy
	//now we perform a HoshenKopelman percolation analysis on IsVacuum to find the outer percolating shell of false's culling the tip and at the same time
	//identify any potential percolating cluster or individual arbitrarily arranged flicker noise of individual bins physically inside the tip but
	//with the given resolution too small a bin size that it is likely to find any ion inside (lateral resolution, detector efficiency)
	//knowing that the AABB about the tip was extended by a guardzone surplus the fact that APT tips do not touch the upper corner voxel at the AABB in particular
	//not the one in the guardzone we can now include all embedded flicker and separate from the non-connected culling outer vacuum cluster with id as that of the
	//upper corner guardzone voxel label ID and update bitmap1 excluding the guardzone
	//when we are sure that there are no isolated arbitrarily false voxel cluster inside the tip and we can safely now the Moore based surface patch identification

	//##MK::NUMA memory issues! this analyzer is at the moment running in the memory of the master thread only!
	//##empirical evidence so far even for 0.5nm binning and 1 billion ions though showed that total core time taken
	//is too insignificant to be a prime target for code optimization...

	percAnalyzer* hk = NULL;
	try { hk = new percAnalyzer; }
	catch(bad_alloc &croak) {
		cerr << "Unable to allocate HoshenKopelman analyzer instance!" << "\n"; return false;
	}

	if ( vxlgrid.nx >= UINT32MX || vxlgrid.ny >= UINT32MX || vxlgrid.nz >= UINT32MX || vxlgrid.nxyz >= UINT32MX ) {
		cerr << "Number of bins exceeds UINT32MX current maximum implement size_t HK!" << "\n";
		return false;
	}

	//now the cast from size_t to unsigned int is safe
	unsigned int nx = static_cast<unsigned int>(vxlgrid.nx);
	unsigned int ny = static_cast<unsigned int>(vxlgrid.ny);
	unsigned int nz = static_cast<unsigned int>(vxlgrid.nz);
	unsigned int nxy = static_cast<unsigned int>(vxlgrid.nxy);
	unsigned int nxyz = static_cast<unsigned int>(vxlgrid.nxyz);

	//pass binarized ion occupancy bin bitfield to the HK analyzer to perform clustering analysis as one would do for percolation studies
	if ( hk->initialize( IsVacuum, nx, ny, nz) == false ) {
		cerr << "HoshenKopelman initialization failed!" << "\n";
		delete hk; return false;
	}

	if ( hk->hoshen_kopelman() == false ) {
		cerr << "HoshenKopelman run failed during binning attempt!" << "\n";
		delete hk; return false;
	}

	if ( hk->compactify() == false ) {
		cerr << "HoskenKopelman label compactification failed during binning attempt!" << "\n";
		delete hk; return false;
	}

	/*if ( hk->checkLabeling() == false ) {
		stopping( "HoshenKopelman found labeling inconsistency");
		delete hk; return false;
	}*/

	if ( hk->determine_clustersize_distr() == false ) {
		cerr << "HoshenKopelman cluster size distribution failed during binning attempt!" << "\n";
		delete hk; return false;
	}

	//if not yet returned everything worked out so far so
	//MK::find HK label of frontmost top left voxel which is guaranteed to be in the guardzone because we added +1 guard on each side
	//and hence has no ion we can use the ID of this bin to reassign which bins are vacuum and which not
	unsigned int tfl_corner_b = 0 + 0*nx + (nz-1)*nxy;

	if ( hk->rebinarize( tfl_corner_b, nxyz, IsVacuum ) == false ) {
		cerr << "HoshenKopelman rebinarization attempt failed!" << "\n";
		delete hk; return false;
		//upon exit bitmap1 is true for bins in vacuum and false for tip
	}

	cout << "HoshenKopelman clustering analysis was successful" << "\n";
	delete hk; hk = NULL;
	return true;
}



void vxlizer::identify_surface_adjacent_bins()
{
	//IsVacuum is true if bin is part of the vacuum cluster enclosing the tip, these are bins that do not containing an ion
	unsigned int nx = static_cast<unsigned int>(vxlgrid.nx);
	unsigned int ny = static_cast<unsigned int>(vxlgrid.ny);
	unsigned int nz = static_cast<unsigned int>(vxlgrid.nz);
	unsigned int nxy = static_cast<unsigned int>(vxlgrid.nxy);
	unsigned int nxyz = static_cast<unsigned int>(vxlgrid.nxyz);

	//MK::assume first each bin is not part of the surface, i.e. initialize...
	for ( unsigned int b = 0; b < nxyz; ++b )
		IsSurface[b] = false;

	//now reset IsSurface to true only if any of the bins' Moore neighbors IsVacuum[i] == false...
	//query in Moore environment which bins have not all Moore neighbors trues ie. are not deep in tip (occupied with points)
	//MK::remember that IsVacuum is true if this is a bin belonging to the large cluster contacted to vacuum and false only if it is some bin in the tip even if isolated and empty inside the tip volume this is the trick!
	//start querying at 1 because guardzone is definately not in tip surplus we need to probe non-periodic Moore neighbors
	for ( unsigned int bz = 1; bz < nz-1; ++bz ) {
		for ( unsigned int by = 1; by < ny-1; ++by ) {
			for ( unsigned int bx = 1; bx < nx-1; ++bx ) {
				unsigned int here = bx + by*nx + bz*nxy;
				unsigned int there = 0;

				if( IsVacuum[here] == true ) { //MK::two bitmaps to avoid successive overwriting and data invalidating image buffer1 as input and buffer2 as output

					//a bin in vacuum --- is it surrounded by nonempty bins i.e. bins that obviously intrude the tip volume?

					//MK::mind that access order such to improve cache temporal and spatial locality
					there = (bx-1)	+	(by-1)	*nx	+	(bz-1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by-1)	*nx	+	(bz-1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true; //MK::order optimized for cache reutilization using the implicit indexing x+y*nx+z*nxy
					there = (bx+1)	+	(by-1)	*nx	+	(bz-1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx-1)	+	(by+0)	*nx	+	(bz-1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by+0)	*nx	+	(bz-1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by+0)	*nx	+	(bz-1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx-1)	+	(by+1)	*nx	+	(bz-1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by+1)	*nx	+	(bz-1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by+1)	*nx	+	(bz-1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;

					there = (bx-1)	+	(by-1)	*nx	+	(bz+0)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by-1)	*nx	+	(bz+0)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by-1)	*nx	+	(bz+0)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx-1)	+	(by+0)	*nx	+	(bz+0)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					//exclude myself
					there = (bx+1)	+	(by+0)	*nx	+	(bz+0)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx-1)	+	(by+1)	*nx	+	(bz+0)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by+1)	*nx	+	(bz+0)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by+1)	*nx	+	(bz+0)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;

					there = (bx-1)	+	(by-1)	*nx	+	(bz+1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by-1)	*nx	+	(bz+1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by-1)	*nx	+	(bz+1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx-1)	+	(by+0)	*nx	+	(bz+1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by+0)	*nx	+	(bz+1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by+0)	*nx	+	(bz+1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx-1)	+	(by+1)	*nx	+	(bz+1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by+1)	*nx	+	(bz+1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by+1)	*nx	+	(bz+1)	*nxy;		if(	IsVacuum[there] == false ) IsSurface[there] = true;
				}
			} //next bin along +x
		} //next xline along +y
	} //next xyslab along +z

	cout << "Surface adjacent bins identified" << "\n";	
}


void vxlizer::identify_inside_bins()
{
	//get volume and count of bins entirely inside
	unsigned int nxyz = static_cast<unsigned int>(vxlgrid.nxyz);

	//MK::IsVacuum is true if bin belonging to the large cluster contacted to vacuum and false if some bin in the tip even if isolated and empty inside the tip volume this is the trick!
	//MK::IsSurface by now is true if a bin to consider containing candidate ions to use in tip surface reconstruction
	//hence if IsVacuum == false && (in tip potentially surfacecontact) and IsSurface == false (excluding surface contact) inside
	for ( unsigned int b = 0; b < nxyz; ++b ) {
		if (IsVacuum[b] == false && IsSurface[b] == false)
			IsInside[b] = true;
		else
			IsInside[b] = false;
	}

	cout << "Inside bins identified" << "\n";
}

