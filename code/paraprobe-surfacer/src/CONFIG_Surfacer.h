/*
	Copyright Max-Planck-Institut f\"ur Eisenforschung, GmbH, D\"sseldorf
//##MK::CODE
 *
 */

#ifndef __PARAPROBE_CONFIG_SURFACER_H__
#define __PARAPROBE_CONFIG_SURFACER_H__

#include "../../paraprobe-utils/src/CONFIG_Shared.h"

enum E_SURFACING_MODE {
	E_SURFACE_NOTHING,
	E_SURFACE_ALPHASHAPE					//needs the CGAL library
};


enum E_ALPHASHAPE_ALPHAVALUE_CHOICE {
	E_ASHAPE_SMALLEST_SOLID,				//which alpha to use for alpha shape
	E_ASHAPE_CGAL_OPTIMAL
};


enum E_DISTANCING_MODE {
	E_DISTANCING_NOTHING,					
	E_DISTANCING_ANALYTICAL_COMPLETE,				//analytical ion to closest triangle all ions
	E_DISTANCING_ANALYTICAL_SKIN					//within skin to triangle hull up to rmax, rest not ranged

};


class ConfigSurfacer
{
public:
	
	static E_SURFACING_MODE SurfacingMode;
	static E_ALPHASHAPE_ALPHAVALUE_CHOICE AlphaShapeAlphaValue;
	static string Inputfile;
	//static string Outputfile;
	
	static apt_real AdvIonPruneBinWidthMin;
	static apt_real AdvIonPruneBinWidthIncr;
	static apt_real AdvIonPruneBinWidthMax;
	
	static E_DISTANCING_MODE DistancingMode;
	static apt_real DistancingRadiusMax;
	
	static apt_real RequeryingThreshold;

	static bool readXML( string filename = "" );
	static bool checkUserInput();
};

#endif
