//##MK::CODE

#ifndef __PARAPROBE_SURFACER_MESHER_H__
#define __PARAPROBE_SURFACER_MESHER_H__

#include "PARAPROBE_Voxelizer.h"

struct ashape_info
{
	size_t ncand;			//number of candidate points
	double alph;			//actual alpha value used for building triangle hull
	//double vxlwidth;		//width used to voxelize the structure
	ashape_info() : ncand(0), alph(0.0) {} //, vxlwidth(0.0) {}
	ashape_info( const size_t _nc, const double _alph ) : ncand(_nc), alph(_alph) {} //const double _width ) :	, vxlwidth(_width) {}
};


class mesher
{
public:
	mesher();
	~mesher();

	bool alphashape_core( vector<p3d> const & p );
	
	vector<tri3d> tipsurface;
	ashape_info hullifo;
};

#endif
