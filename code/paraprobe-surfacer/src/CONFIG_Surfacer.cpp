//##MK::CODE

#include "CONFIG_Surfacer.h"

E_SURFACING_MODE ConfigSurfacer::SurfacingMode = E_SURFACE_NOTHING;
E_ALPHASHAPE_ALPHAVALUE_CHOICE ConfigSurfacer::AlphaShapeAlphaValue = E_ASHAPE_SMALLEST_SOLID;
string ConfigSurfacer::Inputfile = "";
//string ConfigSurfacer::Outputfile = "";
apt_real ConfigSurfacer::AdvIonPruneBinWidthMin = 1.f; //1nm best practice guess useful for many cases
apt_real ConfigSurfacer::AdvIonPruneBinWidthIncr = 1.f;
apt_real ConfigSurfacer::AdvIonPruneBinWidthMax = 1.f;

E_DISTANCING_MODE ConfigSurfacer::DistancingMode = E_DISTANCING_NOTHING;
apt_real ConfigSurfacer::DistancingRadiusMax = 0.f;
apt_real ConfigSurfacer::RequeryingThreshold = 0.9;


bool ConfigSurfacer::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Input.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigSurfacing")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "SurfacingMode" );
	switch (mode)
	{
		case E_SURFACE_ALPHASHAPE:
			SurfacingMode = E_SURFACE_ALPHASHAPE; break;
		default:
			SurfacingMode = E_SURFACE_NOTHING;
	}
	Inputfile = read_xml_attribute_string( rootNode, "Inputfile" );
	//Outputfile = read_xml_attribute_string( rootNode, "Outputfile" );
	
	mode = read_xml_attribute_uint32( rootNode, "AlphaShapeAlphaValue" );
	switch (mode)
	{
		case E_ASHAPE_SMALLEST_SOLID:
			AlphaShapeAlphaValue = E_ASHAPE_SMALLEST_SOLID; break;
		case E_ASHAPE_CGAL_OPTIMAL:
			AlphaShapeAlphaValue = E_ASHAPE_CGAL_OPTIMAL; break;
		default:
			AlphaShapeAlphaValue = E_ASHAPE_SMALLEST_SOLID;
	}
	
	AdvIonPruneBinWidthMin = read_xml_attribute_float( rootNode, "AdvIonPruneBinWidthMin" );
	AdvIonPruneBinWidthIncr = read_xml_attribute_float( rootNode, "AdvIonPruneBinWidthIncr" );
	AdvIonPruneBinWidthMax = read_xml_attribute_float( rootNode, "AdvIonPruneBinWidthMin" );
	
	mode = read_xml_attribute_uint32( rootNode, "DistancingMode" );
	switch (mode)
	{
		case E_DISTANCING_NOTHING:
			DistancingMode = E_DISTANCING_NOTHING; break;
		case E_DISTANCING_ANALYTICAL_SKIN:
			DistancingMode = E_DISTANCING_ANALYTICAL_SKIN; break;
		default:
			DistancingMode = E_DISTANCING_ANALYTICAL_COMPLETE;

	}

	if ( DistancingMode == E_DISTANCING_ANALYTICAL_SKIN ) {
		DistancingRadiusMax = read_xml_attribute_float( rootNode, "DistancingRadiusMax" );
	}
	
	if ( DistancingMode != E_DISTANCING_NOTHING ) {
		RequeryingThreshold = read_xml_attribute_float( rootNode, "RequeryingThreshold" );
	}
	return true;
}


bool ConfigSurfacer::checkUserInput()
{
	cout << "ConfigSurfacer::" << "\n";
	switch(SurfacingMode)
	{
		case E_SURFACE_ALPHASHAPE:
			cout << "Building an alpha shape of the entire specimen" << "\n"; break;
		case E_SURFACE_NOTHING:
			cerr << "No surfacing choosen, about to quit" << "\n"; return false;
	}
	if( SurfacingMode != E_SURFACE_NOTHING ) {
		switch(AlphaShapeAlphaValue)
		{
			case E_ASHAPE_SMALLEST_SOLID:
				cout << "Building the alpha-shape with the smallest alpha value to get a solid" << "\n"; break;
			case E_ASHAPE_CGAL_OPTIMAL:
				cout << "Building the alpha-shape with the according to the CGAL optimal alpha value" << "\n"; break;
			default:
				cerr << "Using an unknown alpha-shape alpha value" << "\n";
		}
		if( AdvIonPruneBinWidthMin < EPSILON ) {
			cout << "AdvIonPruneBinWidthMin must be positive and finite!" << "\n"; return false;
		}
		cout << "AdvIonPruneBinWidthMin " << AdvIonPruneBinWidthMin << "\n";
		if( AdvIonPruneBinWidthIncr < EPSILON ) {
			cout << "AdvIonPruneBinWidthIncr must be positive and finite!" << "\n"; return false;
		}
		cout << "AdvIonPruneBinWidthIncr " << AdvIonPruneBinWidthIncr << "\n";
		if( AdvIonPruneBinWidthMin > AdvIonPruneBinWidthMax ) {
			cout << "AdvIonPruneBinWidthMax should be larger than AdvIonPruneBinWidthMin!" << "\n"; return false;
		}
		cout << "AdvIonPruneBinWidthMax " << AdvIonPruneBinWidthMax << "\n";
	}
	
	switch(DistancingMode)
	{
		case E_DISTANCING_ANALYTICAL_COMPLETE:
			cout << "Computing analytical distance to closest triangle for each ion within dataset, costly" << "\n"; break;
		case E_DISTANCING_ANALYTICAL_SKIN:
			cout << "Computing analytical distance to closest triangle for each ion closer than DistancingRadiusMax" << "\n"; break;
		default:
			cout << "No distance values to the hull are computed" << "\n";
	}
	
	if ( DistancingMode == E_DISTANCING_ANALYTICAL_SKIN ) {
		if( DistancingRadiusMax < EPSILON ) {
			cerr << "DistancingRadiusMax should be positive and finite!" << "\n"; return false;
		}
		cout << "DistancingRadiusMax " << DistancingRadiusMax << "\n";
	}
	if ( DistancingMode != E_DISTANCING_NOTHING ) {
		if ( RequeryingThreshold > 0.9 ) { //must not be 1.0 !
			cerr << "RequeryingThreshold should be on interval [0.0, 0.9]!" << "\n"; return false;
		}
		cout << "RequeryingThreshold " << RequeryingThreshold << "\n"; //can be set to 0.0 then one will never requery
	}
	cout << "Input read from " << Inputfile << "\n";
	//cout << "Output written to " << Outputfile << "\n";

	cout << "\n";
	return true;
}

