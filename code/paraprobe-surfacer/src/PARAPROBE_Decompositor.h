//##MK::CODESPLIT


#ifndef __PARAPROBE_SURFACER_DECOMPOSITOR_H__
#define __PARAPROBE_SURFACER_DECOMPOSITOR_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_Threadmemory.h"

/* ##MK::####
struct tip_portion
{
	apt_real core_zmi;
	apt_real core_zmx;
	apt_real halo_zmi;
	apt_real halo_zmx;
	unsigned int tid;		//logical ID
};
*/


class decompositor
{
public:
	decompositor();
	~decompositor();

	void tip_aabb_get_extrema( vector<p3d> const & p );
	void loadpartitioning( vector<p3d> const & p );
	//void reportpartitioning();

	vector<threadmemory*> db;				//the spatially partitioned dataset layout out as a collection of threadmemory class objects, one for each thread, each of which allocating pinned thread-local memory

	vector<p6d64> spatialsplits;			//where is the point cloud split
	//sqb rve;								//spatial bin geometry
	aabb3d tip;								//rigorous tip AABB bounds
	p3d64 halothickness;					//how much halo should be provided if tessellation desired
	bool kdtree_success;

private:
	bool healthy;
};


#endif
