//##MK::CODE

#ifndef __PARAPROBE_SURFACER_HDF_H__
#define __PARAPROBE_SURFACER_HDF_H__

//shared headers
#include "../../paraprobe-utils/src/PARAPROBE_APTH5.h"
#include "../../paraprobe-utils/src/PARAPROBE_XDMF.h" //HDF5.h"

#include "../../paraprobe-utils/src/metadata/PARAPROBE_SurfacerMetadataDefsH5.h"

//tool-specific headers
#include "PARAPROBE_CGALInterface.h"

/*
#include "METADATA_SurfacerDefsHDF5.h"
*/

class surfacer_h5 : public h5Hdl
{
	//tool-specific sub-class of a HDF5 inheriting all methods of the base class h5Hdl
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	surfacer_h5();
	~surfacer_h5();

	int create_surfacer_apth5( const string h5fn );

//private:
};

#endif
