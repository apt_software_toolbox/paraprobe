//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_MATH_H__
#define __PARAPROBE_UTILS_MATH_H__

#include "PARAPROBE_OriMath.h"

struct circumsphere
{
	apt_real x;
	apt_real y;
	apt_real z;
	apt_real R;
	circumsphere() : x(0.f), y(0.f), z(0.f), R(0.f) {}
	circumsphere(const apt_real _x, const apt_real _y, const apt_real _z, const apt_real _R) :
		x(_x), y(_y), z(_z), R(_R) {}
};

ostream& operator<<(ostream& in, circumsphere const & val);


apt_real lerp(const apt_real v0, const apt_real v1, const apt_real t);
vector<apt_real> quantiles_nosort( vector<apt_real> const & in, vector<apt_real> const & q );

apt_real closestPointOnTriangle( const tri3d face, const p3d src );

circumsphere circumsphere_about_triangle( const tri3d tri );


#endif
