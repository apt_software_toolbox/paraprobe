//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_CONFIG_SHARED_H__
#define __PARAPROBE_UTILS_CONFIG_SHARED_H__

#include "PARAPROBE_KDTree.h"

string read_xml_attribute_string( xml_node<> const * in, const string keyword );
apt_real read_xml_attribute_float( xml_node<> const * in, const string keyword );
unsigned int read_xml_attribute_uint32( xml_node<> const * in, const string keyword );
int read_xml_attribute_int32( xml_node<> const * in, const string keyword );
bool read_xml_attribute_bool( xml_node<> const * in, const string keyword );
unsigned long read_xml_attribute_uint64( xml_node<> const * in, const string keyword );


class ConfigShared
{
public:

	static unsigned int SimID;
	static unsigned int RndDescrStatsPRNGSeed;
	static unsigned int RndDescrStatsPRNGDiscard;

	//static void readXML( string filename = "" );
	//static bool checkUserInput();
};

#endif
