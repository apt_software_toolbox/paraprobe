//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_DECOMPOSITOR_H__
#define __PARAPROBE_UTILS_DECOMPOSITOR_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_Threadmemory.h"


class decompositor
{
public:
	decompositor();
	~decompositor();

	void tip_aabb_get_extrema( vector<p3d> const & p );
	void tip_aabb_get_extrema( vector<p3dm1> const & p );
	void loadpartitioning_subkdtree_per_ityp( vector<p3d> const & p, vector<apt_real> const & d,
			vector<unsigned char> const & lblorg, vector<unsigned char> const & lblrnd, const unsigned char ityp_max );
	//void loadpartitioning( vector<p3d> const & p );
	void loadpartitioning( vector<p3dm1> const & p );

	//void reportpartitioning();

	vector<threadmemory*> db;				//the spatially partitioned dataset layout out as a collection of threadmemory class objects, one for each thread, each of which allocating pinned thread-local memory

	vector<p6d64> spatialsplits;			//where is the point cloud split
	//sqb rve;								//spatial bin geometry
	aabb3d tip;								//rigorous tip AABB bounds
	p3d64 halothickness;					//how much halo should be provided if tessellation desired
	bool kdtree_success;

private:
	bool healthy;
};


#endif
