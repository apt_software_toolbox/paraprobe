//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_CRYSTALLOGRAPHY_H__
#define __PARAPROBE_UTILS_CRYSTALLOGRAPHY_H__

//crystal lattice
#define ATOMS_PER_UNITCELL_FCC			(4)

#define FCC								(0)
#define AL3SC							(1)
#define MMM								(2)

#endif
