//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_THREADMEMORY_H__
#define __PARAPROBE_UTILS_THREADMEMORY_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_XDMF.h"


class threadmemory
{
public:
	threadmemory();
	~threadmemory();

	void init_localmemory( p6d64 const & mybounds, p3d64 const & myhalo, const bool mlast );
	//void init_localions( vector<p3d> const & p );
	void init_localions( vector<p3dm1> const & p );
	void init_localions_subkdtree_per_ityp( vector<p3d> const & p, vector<apt_real> const & d,
			vector<unsigned char> const & lorg, vector<unsigned char> const & lrnd );
	void init_localdistances( const apt_real val );
	void init_localkdtree();
	void init_localkdtree_subkdtree_per_ityp( const unsigned char ityp_max );

	apt_real get_zmi();
	apt_real get_zmx();
	//inline bool me_last() const;

	vector<p3d> ionpp1;
	vector<p3difo> ionifo;
	//vector<p3dm2> ionpp2;						//x,y,z, org and shuffled label
	vector<p3dm3> ionpp3;						//x,y,z, world ionID in reconstruction, VALIDZONE or GUARDZONE, iontype
	vector<size_t> ion2bin;						//temporary in which bin does each ion end up?
	vector<apt_real> ion2surf;					//temporary the distance of the ion to the surface

	//##MK::optimize
	//vector<p3dm3> ionpp3_tess;
	//vector<unsigned int> ionpp3_ionpp3idx;
	//vector<unsigned int> distinfo_tess;
	//MK::SAME LENGTH THEN ionpp3_tess one-to-one correspondence
	//MK::stores an array index for ionpp3 which allows to identify which ion the respective entry in ionpp3_tess represents!
	//MK::avoids to store for every ion always an ID and thereby always to carry for every analysis task
	//larger array cache length than necessary, remind: only when the distancing info is to be used again
	//outside of the threadmemory scope, for instance in the Voro tess to identify how close an ion is
	//to the tip surface and use this information to eliminate cells with bias incorrect topology and geometry
	//MK::IN LATTER CASE ALLOWS ONLY to QUERY DISTANCE FOR VALIDZONE_IONS as their order of
	//populating ionpp3_tess during read_localions is instructed to proceed order preserving!

	//##MK::optimize, idea for instance, set up KDTree immediately would replace ionpp3 by ionpp3_kdtree and ion2surf by ion2surf_kdtree
	vector<p3dm1> ionpp3_kdtree;
	//vector<apt_xyz> ion2surf_kdtree;

	kd_tree* threadtree;

	vector<kd_tree*> ityp_kdtree_org;	//ion-type specific trees
	vector<kd_tree*> ityp_kdtree_rnd;
	//vector<vector<p3d>*> ionpp2_kdtree_org;
	//vector<vector<p3d>*> ionpp2_kdtree_rnd;
	vector<vector<p3d>*> ionpp1_kdtree_org;		//ionpp1 sub-vectors have same length as ionifo sub-vectors and represent coupled information
	//however we do not store this all in one array, because ionpp1 is much more frequently and cache-hot during querying stage than is ionifo
	//therefore splitting the two pieces of information packs data on average denser in the cachelines when scanning ionpp1
	vector<vector<p3d>*> ionpp1_kdtree_rnd;
	vector<vector<p3difo>*> ionifo_kdtree_org;
	vector<vector<p3difo>*> ionifo_kdtree_rnd;

	//inline size_t get_memory_consumption();

private:
	//p6d64 tessmii;
	//p3d64 tesshalowidth;
	apt_real zmi;
	apt_real zmx;
	bool melast;
};


#endif
