//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_SDM3D_H__
#define __PARAPROBE_UTILS_SDM3D_H__

#include "PARAPROBE_Histogram.h"


struct d3dd
{
	apt_real dx;	//components of the difference vector
	apt_real dy;
	apt_real dz;
	apt_real dSQR;
	d3dd() : dx(0.0), dy(0.0), dz(0.0), dSQR(0.0) {}
	d3dd(const apt_real _dx, const apt_real _dy, const apt_real _dz ) :
		dx(_dx), dy(_dy), dz(_dz), dSQR( SQR(dx)+SQR(dy)+SQR(dz) ) {}
};

ostream& operator<<(ostream& in, d3dd const & val);


inline bool SortDiffForAscDistance(const d3dd & a, const d3dd & b)
{
	return a.dSQR < b.dSQR;
}


struct vxlcube
{
	int NX;
	int NXY;
	int NXYZ;
	apt_real width;
	apt_real xmi;
	apt_real xmx;
	apt_real ymi;
	apt_real ymx;
	apt_real zmi;
	apt_real zmx;
	vxlcube() : NX(0), NXY(0*0), NXYZ(0*0*0), width(0.0),
		xmi(0.0), xmx(0.0), ymi(0.0), ymx(0.0), zmi(0.0), zmx(0.0) {}
	vxlcube( const int _N, const apt_real _width, const apt_real _xmi, const apt_real _xmx, 
				const apt_real _ymi, const apt_real _ymx, const apt_real _zmi, const apt_real _zmx ) :
					NX(_N), NXY(SQR(_N)), NXYZ(CUBE(_N)), width(_width),
						xmi(_xmi), xmx(_xmx), ymi(_ymi), ymx(_ymx), zmi(_zmi), zmx(_zmx) {}
};

ostream& operator<<(ostream& in, vxlcube const & val);


class sdm3d
{
public :
	sdm3d();
	sdm3d( const apt_real rmax, const apt_real width, const int nmax );
	~sdm3d();

	void add( d3dd const & where );
	//sqb get_support();
	p3d get_bincenter( const unsigned int ix, const unsigned int iy, const unsigned int iz );
	unsigned int get_nx();
	unsigned int get_nxyz();

	//##MK::if PCF is sparsely populated maybe a std::map is a better option
	vector<unsigned int> cnts;

private:
	vxlcube box;
	apt_real SQRRmax;
	int Offset; //min edge of bin cnts[0] is at support.box.imi which is offset by (nx-1)/2
	unsigned int dump;
};

#endif
