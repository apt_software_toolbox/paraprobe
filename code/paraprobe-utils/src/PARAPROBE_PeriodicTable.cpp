//##MK::GPLV3

#include "PARAPROBE_PeriodicTable.h"

ostream& operator << (ostream& in, evapion const & val)
{
	in << "X,Y,Z\t" << val.x << "\t" << val.y << "\t" << val.z << "\n";
	in << "Z1\t" << (int) val.Z1 << " N1\t" << (int) val.N1 << "\n";
	in << "Z2\t" << (int) val.Z2 << " N2\t" << (int) val.N2 << "\n";
	in << "Z3\t" << (int) val.Z3 << " N3\t" << (int) val.N3 << "\n";
	if ( val.sign == POSITIVE_SIGN ) {
		in << "Ch\t+"; 
	}
	else { 
		in << "Ch\t-";
	}
	in << (int) val.charge << "\n";
	return in;
}


ostream& operator << (ostream& in, evapion3 const & val)
{
	in << "Z1\t" << (int) val.Z1 << " N1\t" << (int) val.N1 << "\n";
	in << "Z2\t" << (int) val.Z2 << " N2\t" << (int) val.N2 << "\n";
	in << "Z3\t" << (int) val.Z3 << " N3\t" << (int) val.N3 << "\n";
	if ( val.sign == POSITIVE_SIGN ) {
		in << "Ch\t+"; 
	}
	else { 
		in << "Ch\t-";
	}
	in << (int) val.charge << "\n";
	return in;
}


ostream& operator << (ostream& in, evapcandidate const & val)
{
	in << "Z1\t" << (int) val.Z1 << " N1\t" << (int) val.N1 << "\n";
	in << "Z2\t" << (int) val.Z2 << " N2\t" << (int) val.N2 << "\n";
	in << "Z3\t" << (int) val.Z3 << " N3\t" << (int) val.N3 << "\n";
	if ( val.sign == POSITIVE_SIGN ) {
		in << "Ch\t+"; 
	}
	else { 
		in << "Ch\t-";
	}
	in << (int) val.charge << "\n";
	in << "Ms\t" << val.mass << "\n";
	in << "ABScore\t" << val.abundance_score << "\n";
	in << "ABCScore\t" << val.abunncomp_score << "\n";
	in << "CompScore\t" << val.nomincomp_score << "\n";
	return in;
}


inline bool SortEvapionDecrsScore( const evapcandidate &aa1, const evapcandidate &aa2)
{
	return aa1.abundance_score > aa2.abundance_score;
}


ostream& operator << (ostream& in, evapion_help const & val)
{
	in << "n = " << val.n << " [" << val.s << ", " << val.e << ")" << "\n";
	return in;
}


ostream& operator << (ostream& in, isotope_bucket const & val)
{
	in << "Z\t" << val.Zoffset << " N\t" << val.Noffset << "\t\t" << "[" << val.s << ";" << val.e << "]" << "\n";
	return in;
}


ostream& operator << (ostream& in, isotope_props const & val)
{
	in << "Mass\t" << val.mass << " Da (error " << val.mass_error << "), natural abundance " << val.abundance << " (error " << val.abun_error << " )" << "\n";
	return in;
}


isotope::isotope( const string _nm, const int _z, const int _n,
			const apt_real _ma, const apt_real _ab,
			const apt_real _merr, const apt_real _aerr )
{
	this->nm1 = _nm.at(0);
	this->nm2 = _nm.at(1);
	this->Z = static_cast<unsigned char>(abs(_z));
	this->N = static_cast<unsigned char>(abs(_n));
	this->mass = _ma;
	this->abun = _ab;
	this->mass_error = _merr;
	this->abun_error = _aerr;
}


std::ostream& operator << (std::ostream& in, isotope const & val)
{
	in << "Isotope of element " << val.nm1 << val.nm2 << "\n";
	in << "Z\t" << (int) val.Z << " N\t" << (int) val.N << "\n";
	in << "mass\t" << val.mass << " Da (error " << val.mass_error << ")\n";
	in << "abun\t" << val.abun << " (error " << val.abun_error << " )" << endl;
	return in;
}


chemicalelement::chemicalelement( const string _nm, const int _z, const apt_real _atperc, const apt_real _cerr )
{
	this->nm1 = _nm.at(0);
	this->nm2 = _nm.at(1);
	this->Z = static_cast<unsigned char>(abs(_z));
	this->N = UNOCCUPIED;
	this->atpercent = _atperc;
	this->comp_error = 0.f; //##MK::not taken into account so far _cerr;
}


ostream& operator << (ostream& in, chemicalelement const & val)
{
	in << "Chemical element " << val.nm1 << val.nm2 << "\n";
	in << "Z\t" << (int) val.Z << "\n";
	in << "at%\t" << val.atpercent << " at.-% (error " << val.comp_error << ")" << "\n";
	return in;
}



apt_real mqival::width()
{
	return (this->hi - this->lo);
}


bool mqival::inrange(const apt_real val)
{
	if ( val < this->lo ) //too low
		return false;
	if ( val > this->hi ) //too high
		return false;
	//else
	return true; //within [lo,hi]
}


ostream& operator<<(ostream& in, mqival const & val)
{
	in << "[ " << val.lo << ", " << val.hi << " ]" << "\n";
	return in;
}


inline bool SortMQRangeAscending( const mqival &aa1, const mqival &aa2)
{
	//promoting sorting in descending order
	return aa1.hi < aa2.lo;
}


ostream& operator<<(ostream& in, nuclid const & val)
{
	in << val.id << "\n";
	return in;
}



nuclidTable::nuclidTable()
{
}


nuclidTable::~nuclidTable()
{
}


bool nuclidTable::load_isotope_library( const string fn )
{
	for( unsigned char Z = 0; Z < STABLE_ELEMENTS_MAXIMUM; Z++ ) {
		isotopes.push_back( map<size_t,isotope>() );
	}
	//https://gist.github.com/JSchaenzle/2726944
	cout << "Parsing natural abundance nuclid table..." << "\n";
	xml_document<> doc;
	xml_node<> * root_node;

	//read the xml file into a vector
	ifstream theFile( fn );
	vector<char> buffer((istreambuf_iterator<char>(theFile)), istreambuf_iterator<char>());
	buffer.push_back('\0');
	//parse the buffer using the xml file parsing library into doc
	doc.parse<0>(&buffer[0]);
	//find our root node
	root_node = doc.first_node("atomic-mass-table");

	//iterate over the elements and their isotopes
	for (xml_node<> * element_node = root_node->first_node("entry"); element_node; element_node = element_node->next_sibling() ) {
		string elementnm = element_node->first_attribute("symbol")->value();
		string elementZ = element_node->first_attribute("atomic-number")->value();
		int Z = (stoi(elementZ) <= static_cast<int>(UCHARMX)) ? stoi(elementZ) : static_cast<int>(UCHARMX);

//		cout << "\t___" << elementnm << "___" << Z << endl;

		//interate over the natural abundance
		for(xml_node<> * ab_node = element_node->first_node("natural-abundance"); ab_node; ab_node = ab_node->next_sibling() ) {

			xml_node<> * ms_node = ab_node->first_node("mass");
			apt_real massval = stof(ms_node->first_attribute("value")->value());
			apt_real masserr = stof(ms_node->first_attribute("error")->value());
//			cout << "\t\t" << setprecision(32) << massval << ";" << setprecision(32) << masserr << endl;

			isotope istp = isotope();
			if ( elementnm.size() >= 2 ) {
				istp.nm1 = elementnm.at(0);
				istp.nm2 = elementnm.at(1);
			}
			else if ( elementnm.size() == 1 ) {
				istp.nm1 = elementnm.at(0);
				istp.nm2 = SPACECHARACTER;
			}
			else {
				cerr << "Unable to decipher name of element while reading PeriodicTableOfElements" << "\n";
				return false;
			}

			istp.Z = static_cast<unsigned char>(Z);

			//parse the individual isotopes of an element
			for(xml_node<> * isotop_node = ms_node->next_sibling(); isotop_node; isotop_node = isotop_node->next_sibling() ) {

				string isotopN = isotop_node->first_attribute("mass-number")->value();
				int N = (stoi(isotopN) <= static_cast<int>(UCHARMX)) ? stoi(isotopN) : static_cast<int>(UCHARMX);
//				cout << "\t\t\t" << "___" << N << "___" << endl;

				xml_node<> * ims = isotop_node->first_node("mass");
				apt_real imsval = stof(ims->first_attribute("value")->value());
				apt_real imserr = stof(ims->first_attribute("error")->value());
//				cout << "\t\t\t" << setprecision(32) << imsval << "\t\t" << setprecision(32) << imserr << endl;

				xml_node<> * iab = isotop_node->first_node("abundance");
				apt_real iabval = stof(iab->first_attribute("value")->value());
				apt_real iaberr = stof(iab->first_attribute("error")->value());
//				cout << "\t\t\t" << setprecision(32) << iabval << "\t\t" << setprecision(32) << iaberr << endl;

				istp.N = N;
				istp.mass = imsval;
				istp.mass_error = imserr;
				istp.abun = iabval;
				istp.abun_error = iaberr;

				isotopes.at(istp.Z).insert( pair<size_t,isotope>( istp.N, istp) );
			}
		}

		//##MK::add isotopes
	}

	/*
	cout << "Reporting isotopes..." << "\n";
	for( unsigned char Z = 0; Z < STABLE_ELEMENTS_MAXIMUM; Z++ ) {
		if ( isotopes.at(Z).size() > 0 ) {
			for( auto jt = isotopes.at(Z).begin(); jt != isotopes.at(Z).end(); ++jt ) {
				cout << "\n" << jt->first << "\t\t" << (int) jt->second.nm1 << ";" << (int) jt->second.nm2 << ";" <<
						(int) jt->second.Z << ";" << (int) jt->second.N << ";" << jt->second.mass << ";" << jt->second.abun << ";" <<
						jt->second.mass_error << ";" << jt->second.abun_error << "\n";
			}
			cout << "Element " << (int) Z << " has " << isotopes.at(Z).size() << " isotopes" << "\n";
		}
	}
	*/

//cout << "In total there are " << isotopes.size() << " isotopes " << endl;
//cout << "In total there are " << isotopes.size() << " elements " << "\n";

	//consistence of each element
	/*for( unsigned char Z = 0; Z < UCHARMX; Z++ ) {

	}*/
	/*
	for( size_t ZZ = 0; ZZ < isotopes.size(); ZZ++ ) {
		unordered_set<isotope> & thisone = isotopes.at(ZZ);
		for( auto it = thisone.begin(); it != thisone.end(); ++it ) {
			cout << endl << *it << endl;
		}
	}
	*/

	return true;
}


void rangedIontype::add_range( const mqival in )
{
	this->rng.push_back( in );
}


string rangedIontype::get_name()
{
	//##MK::
	return "";
}


ostream& operator<<(ostream& in, rangedIontype & val)
{
	in << "Ranged ion ID = " << val.id << " " << val.get_name() << "\n";
	in << "Structure " << val.strct << "\n";
	for ( auto it = val.rng.begin(); it != val.rng.end(); it++ ) {
		in << "\t" << it->lo << ";" << it->hi << "\n";
	}
	return in;
}


rangeTable::rangeTable()
{
	//add the zero-th element
	iontypes.push_back( rangedIontype() );
	iontypes.back().rng.push_back( mqival( 0.0, 0.001 ) );
cout << "RangeTable initialized with " << iontypes.size() << " types and " << iontypes.at(0).rng.size() << " mqivals on the 0-th type" << "\n";
	evapion3 zero = evapion3();
	iontypes_dict.insert( pair<size_t,size_t>( hash_molecular_ion(zero.Z1, zero.Z2, zero.Z3), UNKNOWN_IONTYPE ) );
}


rangeTable::~rangeTable()
{
	
}


size_t rangeTable::hash_molecular_ion( const unsigned char Z1, const unsigned char N1,
							const unsigned char Z2, const unsigned char N2,
								const unsigned char Z3, const unsigned char N3,
									const unsigned char charge )
{
	size_t hash =  	static_cast<size_t>(Z3)*static_cast<size_t>(FIRST_BYTE) +
					static_cast<size_t>(N3)*static_cast<size_t>(SECOND_BYTE) +
					static_cast<size_t>(Z2)*static_cast<size_t>(THIRD_BYTE) +
					static_cast<size_t>(N2)*static_cast<size_t>(FOURTH_BYTE) +
					static_cast<size_t>(Z1)*static_cast<size_t>(FIFTH_BYTE) +
					static_cast<size_t>(N1)*static_cast<size_t>(SIXTH_BYTE) +
					static_cast<size_t>( 0)*static_cast<size_t>(SEVENTH_BYTE) +
					static_cast<size_t>(charge); //the seventh byte is padding and we must not multiply by the EIGTH_BYTE as it would invalidate the value
	return hash;
}


size_t rangeTable::hash_molecular_ion( const unsigned char Z1, const unsigned char Z2, const unsigned char Z3 )
{
	size_t hash =  	static_cast<size_t>(Z3)*static_cast<size_t>(FIRST_BYTE) +
					static_cast<size_t>( 0)*static_cast<size_t>(SECOND_BYTE) +
					static_cast<size_t>(Z2)*static_cast<size_t>(THIRD_BYTE) +
					static_cast<size_t>( 0)*static_cast<size_t>(FOURTH_BYTE) +
					static_cast<size_t>(Z1)*static_cast<size_t>(FIFTH_BYTE) +
					static_cast<size_t>( 0)*static_cast<size_t>(SIXTH_BYTE) +
					static_cast<size_t>( 0)*static_cast<size_t>(SEVENTH_BYTE) +
					static_cast<size_t>( 0); //the seventh byte is padding and we must not multiply by the EIGTH_BYTE as it would invalidate the value
	return hash;
}


evapion3 rangeTable::unhash_molecular_ion( const size_t hash )
{
	evapion3 tmp = evapion3();
	size_t hsh = hash;
	size_t val = 0;

	val = hsh / FIRST_BYTE;		hsh = hsh - (val*FIRST_BYTE);
//cout << "Z3.) " << val << endl;
	tmp.Z3 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

	val = hsh / SECOND_BYTE; 	hsh = hsh - (val*SECOND_BYTE);
//cout << "N3.) " << val << endl;
	tmp.N3 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

	val = hsh / THIRD_BYTE; 	hsh = hsh - (val*THIRD_BYTE);
//cout << "Z2.) " << val << endl;
	tmp.Z2 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

	val = hsh / FOURTH_BYTE; 	hsh = hsh - (val*FOURTH_BYTE);
//cout << "N2.) " << val << endl;
	tmp.N2 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

	val = hsh / FIFTH_BYTE; 	hsh = hsh - (val*FIFTH_BYTE);
//cout << "Z1.) " << val << endl;
	tmp.Z1 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

	val = hsh / SIXTH_BYTE; 	hsh = hsh - (val*SIXTH_BYTE);
//cout << "N1.) " << val << endl;
	tmp.N1 = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

	val = hsh / SEVENTH_BYTE; 	hsh = hsh - (val*SEVENTH_BYTE);
//cout << "Pa.) " << val << endl;
	
	//must not divide by EIGTH_BYTE because would be division by zero!
	val = hsh;
//cout << "Ch.) " << val << endl;
	tmp.charge = ( val < 256 ) ? static_cast<unsigned char>(val) : FAULTY;

cout << (int) tmp.N1 << "\t" << (int) tmp.Z1 << "\t" << (int) tmp.N2 << "\t" << (int) tmp.Z2 << "\t" << (int) tmp.N3 << "\t" << (int) tmp.Z3 << "\t\t" << (int) tmp.charge << endl;
	return tmp;
}


void rangeTable::add_evapion3( string const & s, vector<evapion3> & out )
{
	//s is a string holding user input with comma-separated single or molecular ion in human-readable form
	//e.g. "AlH:,Cu", i.e. probe everything which is either a AlH molecular ion or a Cu single ion
/*
#ifdef UTILIZE_BOOST
	vector<string> tokens;
	boost::split( tokens, s, boost::is_any_of(",")); //can handle multiple delimiter different than getline
#else
    string input{s};
    regex re(","); //" [\\|,:]");
    sregex_token_iterator first{input.begin(), input.end(), re, -1}, last; //the '-1' is what makes the regex split (-1 := what was not matched)
    vector<string> tokens{first, last};
    for (auto t : tokens) {
    	cout << "regex __" << t << "__" << "\n";
    }
#endif
*/
	vector<string> tokens;
#ifdef UTILIZE_BOOST
	boost::split( tokens, s, boost::is_any_of(",")); //can handle multiple delimiter different than getline
#else
	string input{s};
	regex re("[,]+"); //" [\\|,:]");
	//sregex_token_iterator first{input.begin(), input.end(), re, -1};
	sregex_token_iterator it(input.begin(), input.end(), re, -1);
	sregex_token_iterator last; //the '-1' is what makes the regex split (-1 := what was not matched)
	for( ; it != last; it++) {
		cout << "regex __" << it->str() << "__" << "\n";
		tokens.push_back( it->str() );
	}
#endif
	unordered_set<size_t> unique_hashval;
	for( auto tk = tokens.begin(); tk != tokens.end(); tk++ ) {
		evapion3 tmp = evapion3();
		unsigned short nelements = 0;
		string thisone = *tk;

		 //if ( thisone.find("XX") != string::npos ) { cout << "found joker token which is to take all ions" << "\n"; }
		//find all elementnames in the string token via scanning the PSE
		for ( size_t Z = 1; Z < nuclides.isotopes.size(); Z++ ) { //scan periodic table for elements with names matching this one
			for ( auto jt = nuclides.isotopes.at(Z).begin(); jt != nuclides.isotopes.at(Z).end(); jt++ ) { //elements of the same Z
				char spacechar = SPACECHARACTER;
				bool dualchar_name = (jt->second.nm2 != spacechar) ? true : false;
				if ( dualchar_name == true ) {
					string elementname = "";
					elementname += jt->second.nm1;
					elementname += jt->second.nm2;
					//does an element with this name exist in the token?
//cout << "dualchar_name " << dualchar_name << " true elementname __" << elementname << "__" << thisone << "__" << "\n";
					if ( thisone.find( elementname ) != string::npos ) {
						switch(nelements)
						{
							case 0:
								tmp.Z1 = jt->second.Z; nelements++; break;  //##MK::ADD TREATMENT OF MULTIPLICITY!
							case 1:
								tmp.Z2 = jt->second.Z; nelements++; break;
							case 2:
								tmp.Z3 = jt->second.Z; nelements++; break;
							default:
								break; //ignoring more than two components
						}
//cout << "\t\t" << nelements << "\n";
					}
				}
				else { //singlechar molecules need special treatment because searching for "C" naivley would also hit "Co" which is incorrect
					//we use a semicolon to mask these special element i.e. H --> H:, C --> C: and so forth
					//thus, we know that in the XML user input a semicolon immediately after the elementname string so C: we can use this
					string elementname = "";
					elementname += jt->second.nm1;
					elementname += ':'; //here the masking happens
//cout << "dualchar_name " << dualchar_name << " true elementname __" << elementname << "__" << thisone << "__" << "\n";
					if ( thisone.find( elementname ) != string::npos ) {
						switch(nelements)
						{
							case 0:
								tmp.Z1 = jt->second.Z; nelements++; break;
							case 1:
								tmp.Z2 = jt->second.Z; nelements++; break;
							case 2:
								tmp.Z3 = jt->second.Z; nelements++; break;
							default:
								break; //ignoring more than two components
						}
//cout << "\t\t" << nelements << "\n";
					}
				}
				break; //test only one isotope, because all others are from the same element
			}
		} //probe next possible element

		if ( nelements >= 3 || nelements < 1 ) { //we do not parse molecular ions with more than two different components, element names are mutually exclusive for a rrrng file and in paraprobe in general
			continue;
		}

//cout << "\t\tDone with all tokens nelements " << nelements << " tmp.Z123 = " << (int) tmp.Z1 << ";" << (int) tmp.Z2 << ";" << (int) tmp.Z3 << "\n";
		vector<int> sortZ;
		sortZ.push_back( (int) tmp.Z1 );
		sortZ.push_back( (int) tmp.Z2 );
		sortZ.push_back( (int) tmp.Z3 );
		sort( sortZ.begin(), sortZ.end() ); //naturally will sort ascendingly

		//make unique order object and transform into size_t count hashed molecular ion composition object
		evapion3 cand = evapion3();
		size_t hashval = 0;
		switch(nelements)
		{
			case 1:
				cand.Z1 = static_cast<unsigned char>(sortZ.at(2));
				cand.Z2 = UNOCCUPIED;
				cand.Z3 = UNOCCUPIED;
				hashval = hash_molecular_ion( cand.Z1, UNOCCUPIED, UNOCCUPIED ); break;
			case 2:
				cand.Z1 = static_cast<unsigned char>(sortZ.at(2));
				cand.Z2 = static_cast<unsigned char>(sortZ.at(1));
				cand.Z3 = UNOCCUPIED;
				hashval = hash_molecular_ion( cand.Z1, cand.Z2, UNOCCUPIED ); break;
			case 3:
				cand.Z1 = static_cast<unsigned char>(sortZ.at(2));
				cand.Z2 = static_cast<unsigned char>(sortZ.at(1));
				cand.Z3 = static_cast<unsigned char>(sortZ.at(0));
				hashval = hash_molecular_ion( cand.Z1, cand.Z2, cand.Z3 ); break;
			default:
				break;
		}
//cout << "\t\tcandZ123 = " << (int) cand.Z1 << ";" << (int) cand.Z2 << ";" << (int) cand.Z3 << "\t\t" << hashval << "\n";
		//find if this parsed iontype already exists or not?
		if ( unique_hashval.find( hashval) == unique_hashval.end() ) {
//cout << "\t\tAccept a unique evapion3" << "\n";
			unique_hashval.insert( hashval );
			out.push_back( cand );
		}
		else {
//cout << "\t\tReject a duplicate ion!" << "\n";
		}
	} //process the next token
}


void rangeTable::add_iontype( vector<string> const & tks, mqival const & iv )
{
	//evaluate which of the token contains physical elements
	//number of token of a valid range line tells whether it is a multiple or not
	//Range34=3.0110 3.0220 Vol:0.00000 H:3 Color:00FF00
	//Range35=52.9200 53.0800 Vol:0.05211 Zr:1 O:1 Color:660099
	size_t ntks = tks.size();
	//less than five range line is likely corrupted
	if ( ntks < 5 ) {
		cerr << "RRNG range file line contains less than 5 tokens, so is likely corrupted!" << "\n"; return;
	}
	
	evapion3 tmp = evapion3();
	unsigned short nelements = 0;
	for( auto tk = tks.begin(); tk != tks.end(); tk++ ) {
		string thisone = *tk;
		//some tokens need to be discarded given specific format of a rangefile
		//MK::so do not analyze the 0,1,2 and last token ! otherwise Radon and Cobalt will be added as molecular ions =)
//cout << "Processing token __" << thisone << "__" << "\n";
		if ( thisone.find( "Range" ) != string::npos && thisone.find("=") != string::npos ) { //discard the Range = token
cerr << "\t\t__" << thisone << "__ out because of is Range" << "\n";
			continue;
		}
		if ( thisone.find( "Vol:" ) != string::npos ) { //discard the Vol: token
cerr << "\t\t__" << thisone << "__ out because of is Vol:" << "\n";
			continue;
		}
		if ( thisone.find( "Color:" ) != string::npos ) { //discard the Color: token
cerr << "\t\t__" << thisone << "__ out because of is Color:" << "\n";
			continue;
		}
		if ( thisone.find( ":" ) == string::npos ) { //discard range max token
cerr << "\t\t__" << thisone << "__ out because of is MaxRange" << "\n";
			continue;
		}
		//remaining is either a single or a multiple, element names are exclusive capitalized, and case sensitivity is assumed
//cout << "Zmax = " << nuclides.isotopes.size() << "\n";

		for ( size_t Z = 1; Z < nuclides.isotopes.size(); Z++ ) { //scan periodic table for elements with names matching this one
			for ( auto jt = nuclides.isotopes.at(Z).begin(); jt != nuclides.isotopes.at(Z).end(); jt++ ) { //elements of the same Z
				char spacechar = SPACECHARACTER;
				bool dualchar_name = (jt->second.nm2 != spacechar) ? true : false;
				if ( dualchar_name == true ) {
					//get element name
					string elementname = "";
					elementname += jt->second.nm1;
					elementname += jt->second.nm2;
					//does this name exist in the token?
//cout << "dualchar_name " << dualchar_name << " true elementname __" << elementname << "__" << thisone << "__" << "\n";
					if ( thisone.find( elementname ) != string::npos ) {
						switch(nelements)
						{
							case 0:
								tmp.Z1 = jt->second.Z; nelements++; break;  //##MK::ADD TREATMENT OF MULTIPLICITY!
							case 1:
								tmp.Z2 = jt->second.Z; nelements++; break;
							case 2:
								tmp.Z3 = jt->second.Z; nelements++; break;
							default:
								break; //ignoring more than two components
						}
//cout << "\t\t" << nelements << "\n";
					}
				}
				else { //singlechar molecules need special treatment because searching for "C" naivley would also hit "Co" which is incorrect
					//but we know that range file place a semicolon immediately after the elementname string so C: we can use this
					string elementname = "";
					elementname += jt->second.nm1;
					elementname += ':';
//cout << "dualchar_name " << dualchar_name << " true elementname __" << elementname << "__" << thisone << "__" << "\n";
					if ( thisone.find( elementname ) != string::npos ) {
						switch(nelements)
						{
							case 0:
								tmp.Z1 = jt->second.Z; nelements++; break;
							case 1:
								tmp.Z2 = jt->second.Z; nelements++; break;
							case 2:
								tmp.Z3 = jt->second.Z; nelements++; break;
							default:
								break; //ignoring more than two components
						}
//cout << "\t\t" << nelements << "\n";
					}
				}
				break; //test only one isotope, because all others are from the same element
			}
		}
		
		if ( nelements >= 3 ) { //we do not parse molecular ions with more than two different components, element names are mutually exclusive for a rrrng file
			break; 
		}
	}
	
	if ( nelements > 0 ) { //at least one place was populated
//cout << "\t\tDone with all tokens nelements " << nelements << " tmp.Z123 = " << (int) tmp.Z1 << ";" << (int) tmp.Z2 << ";" << (int) tmp.Z3 << "\n";
		vector<int> sortZ;
		sortZ.push_back( (int) tmp.Z1 );
		sortZ.push_back( (int) tmp.Z2 );
		sortZ.push_back( (int) tmp.Z3 );
		sort( sortZ.begin(), sortZ.end() ); //naturally will sort ascendingly
				
		//make unique order object and transform into size_t count hashed molecular ion composition object
		evapion3 cand = evapion3();
		size_t hashval = 0;
		switch(nelements)
		{
			case 1:
				cand.Z1 = static_cast<unsigned char>(sortZ.at(2));
				hashval = hash_molecular_ion( cand.Z1, UNOCCUPIED, UNOCCUPIED ); break;
			case 2:
				cand.Z1 = static_cast<unsigned char>(sortZ.at(2));
				cand.Z2 = static_cast<unsigned char>(sortZ.at(1));
				hashval = hash_molecular_ion( cand.Z1, cand.Z2, UNOCCUPIED ); break;
			case 3:
				cand.Z1 = static_cast<unsigned char>(sortZ.at(2));
				cand.Z2 = static_cast<unsigned char>(sortZ.at(1));
				cand.Z3 = static_cast<unsigned char>(sortZ.at(0));
				hashval = hash_molecular_ion( cand.Z1, cand.Z2, cand.Z3 ); break;
			default:
				break;
		}
//cout << "\t\tcandZ123 = " << (int) cand.Z1 << ";" << (int) cand.Z2 << ";" << (int) cand.Z3 << "\t\t" << hashval << "\n";
		//find if this parsed iontype already exists or not?
		auto it = iontypes_dict.find( hashval );
		if ( it == iontypes_dict.end() ) { //really a new iontype for which not yet mqival were defined
//cout << "--->Adding new type " << iv.lo << "\t\t" << iv.hi << "\n";
			//add iontype in dictionary
			iontypes_dict.insert( pair<size_t,size_t>( hashval, iontypes.size() ) );
			//create an instance of rangedIontype to hold the mqival ranges
			iontypes.push_back( rangedIontype( static_cast<unsigned int>(iontypes.size()), cand ) );
			iontypes.back().rng.push_back( iv );
		}
		else { //an already existent combination, just add the new mqival, overlaps will be tested later
//cout << "--->Adding already existent " << iv.lo << "\t\t" << iv.hi << " it->second " << it->second << "\n";
			iontypes.at(it->second).rng.push_back( iv );
		}
	}
}


bool rangeTable::read_rrng_file( const string ascii_io_fn )
{
	//automatic detection of single and molecular ions from an RRNG file
	//we skip the "[Ions]" section and read instead immediately the ranges these
	ifstream rrngfile;
	string rrngline;
	istringstream line;
	string datapiece;

	rrngfile.open( ascii_io_fn.c_str(), ifstream::in );
	if ( rrngfile.is_open() == true ) {
		//automatic identification of single and molecular ions given the periodic table of elements
		//so first part of rangefile [Ions] can be skipped lines until eof or keyword "[Ranges]" is found whatever next
		string key_word = "[Ranges]";
		bool key_found = false;
		while ( rrngfile.good() == true && key_found == false ) {
			//read a line
			getline( rrngfile, rrngline );
			//erase potential trailing newline
			if (!rrngline.empty() && rrngline[rrngline.length()-1] == '\n') {
			    rrngline.erase(rrngline.length()-1);
			}
cout << "__" << rrngline << "__ __" << key_word << "__" << "\n";
			//does it contain the keyword?
												//string is = rrngline.c_str();
			if ( rrngline != key_word ) { 		//is.find(key_word) == string::npos )
				continue; //may cycle up to eof if file is not a proper rrng
			}
			else {
				key_found = true;
				break; //line contains the keyword so break cycling, rrngline remains current
			}
		}

		if ( key_found == true ) {

			//discard [Ranges] header line
			if ( rrngfile.good() == true ) {
				getline( rrngfile, rrngline );
			}

			while ( rrngfile.good() == true ) { //head controlled, so if previous loop reached eof eofbit is set and will not enter
				//if eofbit not yet set will now filter every line that contains relevant ranging pieces of information
				getline( rrngfile, rrngline );
				//split the line into its tokens using boost
				vector<string> tokens;
#ifdef UTILIZE_BOOST
				boost::split( tokens, rrngline, boost::is_any_of(" \t")); //can handle multiple delimiter different than getline
#else
				string input{rrngline};
				regex re("[\\s\t]+"); //" [\\|,:]");
				//sregex_token_iterator first{input.begin(), input.end(), re, -1};
				sregex_token_iterator it(input.begin(), input.end(), re, -1);
				sregex_token_iterator last; //the '-1' is what makes the regex split (-1 := what was not matched)
				for( ; it != last; it++) {
					cout << "regex __" << it->str() << "__" << "\n";
					tokens.push_back( it->str() );
				}
#endif
				//vector<string> tokens;
				//boost::split(tokens, rrngline, boost::is_any_of(" \t")); //can handle multiple delimiter different than getline

				bool any_empty_token = false;
				for( auto it = tokens.begin(); it != tokens.end(); it++ ) {
					cout << "\t\t__" << *it << "__" << "\n";
					if ( it->size() == 0 )
						any_empty_token = true;
				}

				if ( any_empty_token == true ) {
					continue; //skip this line
				}

				if ( tokens.size() > (4+3) ) {
					cerr << "Formatting of the rrng file contains more than triple component molecular ions which is not accepted!" << "\n";
					rrngfile.close(); return false;
				}

				if ( tokens.size() < 1 ) { //in case of no tokens, interpret as end of rrng file, instead we could also evaluate Range=cnt topline of rrng file
					break; //end of file
				}

				bool contains_range_tk = false;
				bool contains_vol_tk = false;
				bool contains_color_tk = false;
				for( auto kt = tokens.begin(); kt != tokens.end(); kt++ ) {
					if ( kt->find("Range") != string::npos )
						contains_range_tk = true;
					if ( kt->find("Vol") != string::npos )
						contains_vol_tk = true;
					if ( kt->find("Color") != string::npos )
						contains_color_tk = true;
				}
				if ( contains_range_tk == false || contains_vol_tk == false || contains_color_tk == false ) { //not all key tokens contained in line, go out
					//probe formatting of the line given predefined token string values expected for rrng
					cerr << "Formatting of the rrng file line seems corrupted wrt to token values!" << "\n"; 
					rrngfile.close(); return false;
				}
				
				//get range m/q interval
				istringstream line( rrngline );
				getline( line, datapiece, '=');
				getline( line, datapiece, ' ');
	//#ifdef EMPLOY_SINGLEPRECISION
				apt_real low = stof( datapiece.c_str() ); //##MK::check consistence lo < hi etc...
	//#else
	//			apt_real low = stod( datapiece.c_str() );
	//#endif
				getline( line, datapiece, ' ');
	//#ifdef EMPLOY_SINGLEPRECISION
				apt_real high = stof( datapiece.c_str() );
	//#else
	//			apt_real high = stod( datapiece.c_str() );
	//#endif
				if ( low >= high ) {
					cerr << "RRNG line mqival bounds seem corrupted low must be < high!" << "\n"; 
					rrngfile.close(); return false;
				}
					
				mqival currentrange = mqival(low, high);
				
				//now we can use a !case sensitive! searching of element string names in the tokens
				add_iontype( tokens, currentrange );
	
			} //whether or not there is range information keep on parsing...
		}
		else {
			cerr << "Did not found section [Ranges]!" << "\n";
			rrngfile.close(); return false;
		}

		//sort individual ranges for a given key
		for ( auto it = iontypes.begin(); it != iontypes.end(); it++ ) {
			sort( it->rng.begin(), it->rng.end(), SortMQRangeAscending );
		}

cout << "The maximum IontypeID is " << iontypes.size() << "\n";
cout << "RRNGFILE " << ascii_io_fn << " was loaded successfully with a total of now " << iontypes.size() << " ion types" << "\n";

		cout << "I know the following elements" << "\n";
		/*
		//######################################
		for ( auto it = NuclidTable.begin(); it != NuclidTable.end(); ++it ) {
			cout << " " << it->first;
		}
		*/
		
		cout << "\n";
		cout << "I have identified the following single / molecular ions from the rrng file" << "\n";
		for( auto it = iontypes.begin(); it != iontypes.end(); it++ ) {
			cout << "\t\t" << it->id << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\t\t" << it->get_name() << "\n";
			for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++ ) {
				cout << "\t\t\t\t" << jt->lo << ";" << jt->hi << "\n";
			}
		}
		cout << "\n";
	
		rrngfile.close();
		//if no rangeinformation was presented no failure but every ion is considered UNKNOWN_IONTYPE default type
	}
	else {
		cerr << "Unable to load RRNG file " << ascii_io_fn << "\n";
		rrngfile.close();
		return false;
	}
	
	return true;
}


bool rangeTable::read_rng_file( const string ascii_io_fn )
{
	cerr << "Reading RNG formatted range files is currently not implemented!" << "\n";
	return false;
}


bool rangeTable::validity_check_rangemax()
{
	if ( this->iontypes.size() <= static_cast<size_t>(UCHARMX) ) {
		return true;
	}
	cerr << "validity_check_rangemax not passed, current implementation of PARAPROBE can handle 256 ranges max!" << "\n";
	return false;
}


bool rangeTable::validity_check_noemptyness()
{
	cerr << "validity_check_noemptyness needs implementation!" << "\n";
	return true;
}


bool rangeTable::validity_check_nonoverlapping()
{
	cerr << "validity_check_nonoverlapping needs implementation!" << "\n";
	return true;
}


bool rangeTable::validity_check()
{
	//run the range against a number of validity checks for consistence
	if ( validity_check_rangemax() == false )
		return false;
	if ( validity_check_noemptyness() == false )
		return false;
	if ( validity_check_nonoverlapping() == false )
		return false;

	cout << "rangeTable has passed the validity checks" << "\n";
	return true;
}

/*
PeriodicTable::PeriodicTable()
{
	MaximumTypID = 0;
	rangefile_loaded = false; //flag to prevent multiple reloads
	load_periodictable();
}


PeriodicTable::~PeriodicTable()
{
}


unsigned int PeriodicTable::mq2ionname( const apt_real mq )
{
	//MK::maps mass-to-charge to ion type according to known parts of the periodic table
	if ( rangefile_loaded == true ) { //maps a mass to charge ratio to an ionname
		for ( size_t i = 0; i < MQ2IonName.size(); ++i ) {
			if ( MQ2IonName.at(i).size() > 0 ) { //if at all known mass to charge values for this ion speci
				//##MK::earliest possible reject is when completely out of range
				//if ( mq < MQ2IonName.at(i).at(0).lo )
				//	continue;
				//if ( mq > MQ2IonName.at(i).at(MQ2IonName.at(i).size()-1).hi )
				//	continue;

				//no early reject, so there are candidates in mass-to-charge but which
				for ( size_t j = 0; j < MQ2IonName.at(i).size(); ++j ) {
					if (MQ2IonName.at(i).at(j).inrange( mq ) == false ) //try to reject as early as possible...
						continue;
					else
						return i;
				}
			}
		}
	}
	//not returned yet
	//complaining( "PeriodicTable no rangefile was loaded, assigning UNKNOWNTYPEs instead");
	return UNKNOWNTYPE;
}

unsigned int PeriodicTable::ion_name2type( const string name )
{
	map<string, unsigned int>::iterator it = IonTypes.find( name );
	if ( it != IonTypes.end() )
		return it->second;
	else {
		//MK::referring to a completely unknown type
		return UINT32MX;
	}
}

string PeriodicTable::ion_type2name( const unsigned int type )
{
	map<unsigned int, string>::iterator it = IonNames.find( type );
	if ( it != IonNames.end() )
		return it->second;
	else {
		return "COMPLETELYUNKNOWNTYPE";
	}
}

unsigned int PeriodicTable::get_maxtypeid()
{
	return MaximumTypID;
}


void PeriodicTable::add_iontype_single_or_molecular( vector<string> const & in, mqival const & ival )
{
	//use PeriodicTable of elements to find whether in contains useful info to build an iontype single or molecular
	//element names are exclusive capitalized case sensitive assumed
	string cand = "";
cout << "adding new iontype" << endl;
	for ( auto it = NuclidTable.begin(); it != NuclidTable.end(); ++it ) { //NuclidTable construction was in order strings in ascending order so
		//does any token of in contain the NuclidName (it->first)
		//format of a valid rrng range file line is Range1=44.8 50.0 Vol:0.01661 Sc:1 Color:33FFFF
		//MK::so do not analyze the 0,1,2 and last token ! otherwise Radon and Cobalt will be added as molecular ions =)
		string nuclidkeyword = it->first;
		bool handle_singlechar_keys = ( nuclidkeyword.size() == 1 ) ? true : false;
		for ( auto tk = in.begin()+3; tk != in.end()-1; ++tk) {
			//special case handling necessary for elements with only one character
			//Fe:1 F:1
			string test = *tk;
			if ( handle_singlechar_keys == true ) { //H, C,
				string keyword_in_rrng = nuclidkeyword + ":";
				if ( test.find(keyword_in_rrng) == string::npos ) {
					continue;
				}
				else {
					cand = cand + nuclidkeyword;
cout << "__" << cand << "__" << endl;
					break; //do not inspect further token to avoid multiple counting of ion
				}
			}
			else {
				if ( test.find( nuclidkeyword ) == string::npos ) { //token does not contain a string like "H" or "Yb"
					continue;
				}
				else {
					cand = cand + it->first;
cout << "__" << cand << "__" << endl;
					break; //do not inspect further token to avoid multiple counting of ion
				}
			}
		}
	}

	//check if an IonType with keyword cand exists already
	if ( IonTypes.find( cand ) == IonTypes.end() ) { //no it doesnt so create
cout << "Single/molecular ion type __" << cand << "__ is new and added" << endl;
		unsigned int typid = static_cast<unsigned int>(IonTypes.size());
		//execute after setting typid
		IonTypes[cand] = typid; //automatic incrementing given that UNKNOWNTYPE was added upon PeriodicTable construction!
		IonNames[typid] = cand;
		vector<mqival> tmp;
		MQ2IonName.push_back( tmp );
		MQ2IonName.at(typid).push_back( ival );
	}
	else { //yes it does, such add range //##MK::if not overlapping
cout << "Single/molecular ion type __" << cand << "__ exists already just adding range" << endl;
		unsigned int typid = IonTypes[cand];
		MQ2IonName.at(typid).push_back( ival);
	}
}


string PeriodicTable::parse_iontype_single_or_molecular( const string in )
{
	//use PeriodicTable of elements to find whether in contains useful info to build an iontype single or molecular
	//element names are exclusive capitalized case sensitive assumed
	string cand = "";
	cout << "adding new iontype" << endl;
	for ( auto it = NuclidTable.begin(); it != NuclidTable.end(); ++it ) { //NuclidTable construction was in order strings in ascending order so
		//does in identify any valid NuclidName (it->first)
		string nuclidkeyword = it->first;
		bool handle_singlechar_keys = ( nuclidkeyword.size() == 1 ) ? true : false;
		if ( handle_singlechar_keys == true ) { //H, C,
			if ( in.find( nuclidkeyword ) == string::npos ) {
				continue;
			}
			else {
				cand = cand + nuclidkeyword;
	cout << "__" << cand << "__" << endl;
				break; //do not inspect further token to avoid multiple counting of ion
			}
		}
		else {
			if ( in.find( nuclidkeyword ) == string::npos ) { //token does not contain a string like "H" or "Yb"
				continue;
			}
			else {
				cand = cand + it->first;
	cout << "__" << cand << "__" << endl;
				break; //do not inspect further token to avoid multiple counting of ion
			}
		}
	}

	return cand;
}


bool PeriodicTable::read_rangefile2( string ascii_io_fn )
{
	//automatic detection of single and molecular ions from a RRNG file
	//we skip the "Ions" section and read instead immediately the ranges these
	ifstream rrngfile;
	string rrngline;
	istringstream line;
	string datapiece;

	//the zeroth/null element is always the unknown type
	IonTypes["Unknown"] = UNKNOWNTYPE;
	IonNames[UNKNOWNTYPE] = "Unknown";
	//MK::typ numeral identification starts at UNKNOWNTYPE+1 !
	vector<mqival> tmp;
	MQ2IonName.push_back( tmp );
	MQ2IonName.at(UNKNOWNTYPE).push_back( mqival(0.0,0.001) ); //small dummy range

	//in PARAPROBE_Numerics.h we define UNKNOWNTYPE to be 0 resulting in Fortran indexing of ion types

	rrngfile.open( ascii_io_fn.c_str(), ifstream::in );
	if ( rrngfile.is_open() == true ) {
		//automatic identification of single and molecular ions given the periodic table of elements
		//so first part of rangefile [Ions] can be skipped lines until eof or keyword "[Ranges]" is found whatever next
		string keyword = "[Ranges]";
		while ( rrngfile.good() == true ) {
			//read a line
			getline( rrngfile, rrngline );
			//does it contain the keyword?
			string is = rrngline.c_str();
			if ( is.find(keyword) == string::npos )
				continue; //may cycle up to eof if file is not a proper rrng
			else
				break; //line contains the keyword so break cycling, rrngline remains current
		}

		while ( rrngfile.good() == true ) { //head controlled, so if previous loop reached eof eofbit is set and will not enter
			//if eofbit not yet set will now filter every line that contains relevant ranging pieces of information
			getline( rrngfile, rrngline );
			//split it into its tokens using boost
			vector<string> tokens;
			boost::split(tokens, rrngline, boost::is_any_of(" \t")); //can handle multiple delimiter different than getline
			if ( 	tokens.size() >= 5 &&
					tokens.at(0).find("Range") != string::npos &&
					tokens.at(2).find("Vol") != string::npos ) { //there is range information available
				//get range m/q interval
				istringstream line( rrngline );
				getline( line, datapiece, '=');
				getline( line, datapiece, ' ');
#ifdef EMPLOY_SINGLEPRECISION
				apt_real low = stof( datapiece.c_str() ); //##MK::check consistence lo < hi etc...
#else
				apt_real low = stod( datapiece.c_str() );
#endif
				getline( line, datapiece, ' ');
#ifdef EMPLOY_SINGLEPRECISION
				apt_real high = stof( datapiece.c_str() );
#else
				apt_real high = stod( datapiece.c_str() );
#endif
				mqival currentrange = mqival(low, high);
				//now we can use a !case sensitive! searching of element string names in the tokens
				add_iontype_single_or_molecular( tokens, currentrange );
			}
			//whether or not there is range information keep on parsing...
		}

		//sort individual ranges for a given key
		for ( size_t i = 0; i < MQ2IonName.size(); ++i ) {
			sort( MQ2IonName.at(i).begin(), MQ2IonName.at(i).end(), SortMQRangeAscending );
		}

		//store maximum typid
		MaximumTypID = IonTypes.size();
		cout << "The maximum typid is " << MaximumTypID << endl;

		cout << "RRNGFILE " << ascii_io_fn << " was loaded successfully with a total of now " << MaximumTypID << " ion types" << "\n";
		cout << "I know the following elements" << "\n";
		for ( auto it = NuclidTable.begin(); it != NuclidTable.end(); ++it ) {
			cout << " " << it->first;
		}
		cout << endl;
		cout << "I have identified the following single/molecular ions from the rrng file" << "\n";
		for( auto it = IonTypes.begin(); it != IonTypes.end(); it++ ) {
			cout << "\t\t" << it->first << "\t\tmapped to TypeID\t\t" << it->second << "\t\tcross-check name\t\t" << IonNames[it->second] << "\n";
		}
		cout << endl;

		rrngfile.close(); rangefile_loaded = true;
		return true;
		//if no rangeinformation was presented no failure but every ion is considered UNKNOWN debug type
	}
	else {
		complaining( "Unable to load RRNG file " + ascii_io_fn );
		rrngfile.close(); rangefile_loaded = false;
		return false;
	}
}


TypeSpecDescrStat::TypeSpecDescrStat()
{
	//target = make_pair("UNKNOWNTYPE", UNKNOWNTYPE);
}


TypeSpecDescrStat::~TypeSpecDescrStat()
{
}


bool TypeSpecDescrStat::define_iontask3(const string command, PeriodicTable const & pse )
{
	//new type identification scheme:
	//Central1,Central2,...,CentralN-NBor1,NBor2,...,NBorM  i.e.
	//minus sign is central-neighbor block separator
	//comma separates individual elements of centrals or targets

	//reject when command is empty?
	if ( command.empty() == true )
		return false;
	//reject when command has no or more than one minus
	//is there a single comma only in the command string so that the string can specify at all a valid command?
	if ( std::count(command.begin(), command.end(), '-') != 1 )
		return false;

	//split at the central nbor
	istringstream line( command );
	string centers;
	getline( line, centers, '-');
	string nbors;
	getline( line, nbors, '-');

	//how many elements of either centers and nbors?
	size_t ncenters = std::count(centers.begin(), centers.end(), ',') + 1; //##MK +1 because if none there is at least potentially one
	istringstream left( centers );
	string datapiece;
	for( size_t i = 0; i < ncenters; ++i ) {
		getline( left, datapiece, ',');
		for (auto jt = pse.IonTypes.begin(); jt != pse.IonTypes.end(); jt++ ) { //check if keyword is exactly one of the existent
			if ( datapiece.compare(jt->first) != 0 )
				continue;
			else
				trgcandidates.insert( make_pair(jt->first, jt->second) );
		}
	}
	//at least one element as central ion?
	if ( trgcandidates.size() < 1 )
		return false;


	size_t nnbors = std::count(nbors.begin(), nbors.end(), ',') + 1;
	istringstream right( nbors );
	for( size_t i = 0; i < nnbors; ++i ) {
		getline( right, datapiece, ',');
		for (auto jt = pse.IonTypes.begin(); jt != pse.IonTypes.end(); jt++ ) { //check if keyword is exactly one of the existent
			if ( datapiece.compare(jt->first) != 0 )
				continue;
			else
				envcandidates.insert( make_pair(jt->first, jt->second) );
		}
	}
	//at least one element as env ion?
	if ( envcandidates.size() < 1 )
		return false;

	//seems now to be a valid task
	return true;
}


bool TypeSpecDescrStat::define_iontask4(const string command, PeriodicTable const & pse )
{
	//new type identification scheme:
	//Central1,Central2,...,CentralN-NBor1,NBor2,...,NBorM  i.e.
	//minus sign is central-neighbor block separator
	//comma separates individual elements of centrals or targets

	//reformulates userdefined type single/molecular ions into standard internal format
	//for instance consider a rrng file with a Range specification "Al:2 O:1 H:1" a common molecular ion
	//the automatic iontype identification during in read_rangefile will identify from this a molecular ion
	//named AlOoHh, now given that the order in which the elements are found from the range file line
	//is defined but dependent on the order in the for loop over the nuclid table the same molecular ion
	//could be named HhAlOo or all permutations
	//the user should not have to worry about this, ie enter AlOH only now throwing this to the identification
	//algorithm will not find O or H because their keys in the NuclidTable are Hh and Oo respectively
	//this is why we have to internally convert the user input into a consistent representation using the
	//internal format

	//reject when command is empty?
	string mess = "";
	if ( command.empty() == true ) {
		mess = "DefineIontask4::Command __" + command + "__is empty";
		reporting( mess );
		return false;
	}
	//reject when command has no or more than one minus
	//is there a single minus separator only in the command string so that the string can specify at all a valid command?
	if ( std::count(command.begin(), command.end(), '-') != 1 ) {
		mess = "DefineIontask4::Command has not only one minus separator __" + command + "__is empty";
		reporting( mess );
		return false;
	}

	//split at the central nbor
	istringstream line( command );
	string centers;
	getline( line, centers, '-');
	string nbors;
	getline( line, nbors, '-');

	//how many elements of either centers and nbors?
	size_t ncenters = std::count(centers.begin(), centers.end(), ',') + 1; //##MK +1 because if none there is at least potentially one
	istringstream left( centers );
	string datapiece;
	vector<string> temp;
	for( size_t i = 0; i < ncenters; ++i ) {
		getline( left, datapiece, ',');
		cout << "___" << datapiece.c_str() << "___" << datapiece.size() << endl;
		string cand = "";
		temp.clear();
		//parsing of single character element name molecular ions requires additional testing logic
		//take for instance Sc, doing only an accept at first find would find sulphur, some thing for He and hydrogen...
		for ( auto it = pse.NuclidTable.begin(); it != pse.NuclidTable.end(); ++it ) {
			//MK::cycling through the NuclidTable in the same order than upon reading the rangefile
			//such molecular ion iontype name identification is consistent
			//cout << "NuclidTable___" << it->first << "___" << datapiece << "___" << datapiece.c_str() << endl;
			if ( datapiece.find( it->first ) == string::npos ) { //datapiece does not contain a string like "Hh" or "Yb"
				continue;
			}
			else {
				temp.push_back( it->first );
			}
		}
		//now that we know all possible elements of the (molecular)ion we need to build a consistent type string
		//and figure out which they actually are example SSc we would have found S, and Sc as temporaries
		//now test if the given molecular ion name "SSc" is exactly only on of the intermediates
		cout << "Temporary elementname candidates for targets are " << endl;
		for( auto kt = temp.begin(); kt != temp.end(); ++kt )
			cout << " __" << *kt << "__";
		cout << endl;
		bool IsItASingleElementIon = false;
		for( auto kt = temp.begin(); kt != temp.end(); ++kt ) { //take for example "SSc" temp will contain "S" and "Sc"
			if ( datapiece.compare( *kt ) != 0 ) { //"S" != "SSc"
				continue;
			}
			else {
				IsItASingleElementIon = true; //consider as a single ion
				cand = cand + *kt;
				break;
			}
		} //neither "Sc" == "SSc" nor "S" == "SSc" so SSc is a molecular ion
		if ( IsItASingleElementIon == false ) { //consider as a molecular ion
			for( auto kt = temp.begin(); kt != temp.end(); ++kt ) {
				cand = cand + *kt;
			}
		}
		auto jt = pse.IonTypes.find( cand );
		if ( jt != pse.IonTypes.end() ) { //molecular ion does not yet exist
			trgcandidates.insert( make_pair(jt->first, jt->second) );
		}
	}
	//at least one element as central ion?
	if ( trgcandidates.size() < 1 ) {
		mess = "DefineIontask4::trgcandidates.size() < 1";
		reporting( mess );
		return false;
	}


	size_t nnbors = std::count(nbors.begin(), nbors.end(), ',') + 1;
	istringstream right( nbors );
	for( size_t i = 0; i < nnbors; ++i ) {
		getline( right, datapiece, ',');
		string cand = "";
		temp.clear();
		for ( auto it = pse.NuclidTable.begin(); it != pse.NuclidTable.end(); ++it ) {
			//MK::cycling through the NuclidTable in the same order than upon reading the rangefile
			//such molecular ion iontype name identification is consistent
			if ( datapiece.find( it->first ) == string::npos ) { //datapiece does not contain a string like "Hh" or "Yb"
				continue;
			}
			else {
				temp.push_back( it->first );
			}
		}
		cout << "Temporary elementname candidates for neighbors are " << endl;
		for( auto kt = temp.begin(); kt != temp.end(); ++kt )
			cout << " __" << *kt << "__";
		cout << endl;
		//single or molecular ion oracle
		bool IsItASingleElementIon = false;
		for( auto kt = temp.begin(); kt != temp.end(); ++kt ) {
			if ( datapiece.compare( *kt ) != 0 ) {
				continue;
			}
			else {
				IsItASingleElementIon = true;
				cand = cand + *kt;
				break;
			}
		} //neither "Sc" == "SSc" nor "S" == "SSc" so SSc is a molecular ion
		if ( IsItASingleElementIon == false ) { //consider as a molecular ion
			for( auto kt = temp.begin(); kt != temp.end(); ++kt ) {
				cand = cand + *kt;
			}
		}

		auto jt = pse.IonTypes.find( cand );
		if ( jt != pse.IonTypes.end() ) { //molecular ion does not yet exist
			envcandidates.insert( make_pair(jt->first, jt->second) );
		}
	}
	//at least one element as env ion?
	if ( envcandidates.size() < 1 ) {
		mess = "DefineIontask4::envcandidates.size() < 1";
		reporting( mess );
		return false;
	}

	//seems now to be a valid task
	return true;
}



void parse_tasks( const string command, vector<TypeSpecDescrStat> & these, PeriodicTable const & thispse )
{
	//parses a single command string into a vector of individual task descriptives
	if ( command.empty() == true ) { //get out if no command at all
cout << "Parsing no tasks at all" << endl;
		return;
	}

	int numtasks = std::count( command.begin(), command.end(), ';') + 1;
	stringstream parsethis;
	parsethis << command;
	string datapiece;
	for( int tsk = 0; tsk < numtasks; tsk++ ) {
		getline( parsethis, datapiece, ';');

		cout << "Task " << tsk << "____" << datapiece << "____" << endl;

		TypeSpecDescrStat tmp; //speculative accepting of that new task
		these.push_back( tmp );
		TypeSpecDescrStat & thistask = these.back();
		//if ( thistask.define_iontask3( datapiece, thispse ) == true ) { //a valid task
		if ( thistask.define_iontask4( datapiece, thispse ) == true ) {
			//##MK::deprecated
			//cout << endl;
			//cout << "\t\tCentral ion type--->" << thistask.target.first << "/" << thistask.target.second << endl;
			for ( auto it = thistask.trgcandidates.begin(); it != thistask.trgcandidates.end(); it++) {
				cout << "\t\tCentral ion type--->" << it->first << "/" << it->second << endl;
			}
			for ( auto it = thistask.envcandidates.begin(); it != thistask.envcandidates.end(); it++) {
				cout << "\t\tEnvironment type--->" << it->first << "/" << it->second << endl;
			}
		}
		else { //an invalid task
			cout << "Kicking an invalid task" << endl;
			these.pop_back();
		}
	}
}


void PeriodicTable::load_periodictable()
{
	//MK::so far defines the periodic table of elements only
	//is used for reading rangefiles to allow for automatic detection of single and molecular ion species
	//##MK::can be extended to include nuclides and natural abundances
	nuclid dummy = nuclid();
	//Keyword must be two character string the first capital the second low-case
	//this is why elements with a single character name such as "H" hydrogen require a modified
	//key otherwise a string.find on a an iontype string such as "He" would identify
	//"H" and "He" an form HHe which is incorrect, using Hh prevents this and identifies only He for this example
	//correspondingly molecular ions like AlH become AlHh
	NuclidTable.insert( pair<string,nuclid>("H", dummy) );
	NuclidTable.insert( pair<string,nuclid>("He", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Li", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Be", dummy) );
	NuclidTable.insert( pair<string,nuclid>("B", dummy) );
	NuclidTable.insert( pair<string,nuclid>("C", dummy) );
	NuclidTable.insert( pair<string,nuclid>("N", dummy) );
	NuclidTable.insert( pair<string,nuclid>("O", dummy) );
	NuclidTable.insert( pair<string,nuclid>("F", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ne", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Na", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Mg", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Al", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Si", dummy) );
	NuclidTable.insert( pair<string,nuclid>("P", dummy) );
	NuclidTable.insert( pair<string,nuclid>("S", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Cl", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ar", dummy) );
	NuclidTable.insert( pair<string,nuclid>("K", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ca", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Sc", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ti", dummy) );
	NuclidTable.insert( pair<string,nuclid>("V", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Cr", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Mn", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Fe", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Co", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ni", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Cu", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Zn", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ga", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ge", dummy) );
	NuclidTable.insert( pair<string,nuclid>("As", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Se", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Br", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Kr", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Rb", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Sr", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Y", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Zr", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Nb", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Mo", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Tc", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ru", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Rh", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Pd", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ag", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Cd", dummy) );
	NuclidTable.insert( pair<string,nuclid>("In", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Sn", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Sb", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Te", dummy) );
	NuclidTable.insert( pair<string,nuclid>("I", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Xe", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Cs", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ba", dummy) );
	NuclidTable.insert( pair<string,nuclid>("La", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ce", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Pr", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Nd", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Pm", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Sm", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Eu", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Gd", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Tb", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Dy", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ho", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Er", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Tm", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Yb", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Lu", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Hf", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ta", dummy) );
	NuclidTable.insert( pair<string,nuclid>("W", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Re", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Os", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ir", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Pt", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Au", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Hg", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Tl", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Pb", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Bi", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Po", dummy) );
	NuclidTable.insert( pair<string,nuclid>("At", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Rn", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Fr", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ra", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Ac", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Th", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Pa", dummy) );
	NuclidTable.insert( pair<string,nuclid>("U", dummy) );
	NuclidTable.insert( pair<string,nuclid>("Pu", dummy) );
}

*/


