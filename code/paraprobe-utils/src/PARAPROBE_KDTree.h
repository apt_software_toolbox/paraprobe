//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_KDTREE_H__
#define __PARAPROBE_UTILS_KDTREE_H__

#include "PARAPROBE_SpaceBucketing.h"


//##MK::implement KDTree

//using namespace std;
//MK::SINGLE_PRECISION for APT data suffices
//typedef float apt_xyz;


/*//##MK::name clash with overlap
inline apt_xyz pi()
{
	return static_cast<apt_xyz>(MYPI);
}
*/

inline apt_xyz mysqr(apt_xyz v)
{
	return v*v;
}


inline apt_xyz euclidean_sqrd(const p3dm1 & a, const p3dm1 & b) {

	return mysqr(a.x-b.x) + mysqr(a.y-b.y) + mysqr(a.z-b.z);
}


inline apt_xyz euclidean_sqrd(const p3d & a, const p3d & b) {

	return mysqr(a.x-b.x) + mysqr(a.y-b.y) + mysqr(a.z-b.z);
}




struct cuboid
{
	p3d min;
	p3d max;
	cuboid() : min(RMAX,RMAX,RMAX), max(RMIN,RMIN,RMIN) {}
	cuboid(const p3d & thiscorner, const p3d & thatcorner) :
		min(thiscorner), max(thatcorner) {}
	apt_xyz outside_proximity(const p3dm1 & p ) const
	{
		apt_xyz res = 0.0; //assuming we are inside
		if ( p.x < this->min.x )
			res += mysqr(this->min.x - p.x);
		else if (p.x > this->max.x )
			res += mysqr(p.x - this->max.x);

		if ( p.y < this->min.y )
			res += mysqr(this->min.y - p.y);
		else if (p.y > this->max.y )
			res += mysqr(p.y - this->max.y);

		if ( p.z < this->min.z )
			res += mysqr(this->min.z - p.z);
		else if (p.z > this->max.z )
			res += mysqr(p.z - this->max.z);
		return res;
	}
	apt_xyz outside_proximity(const p3d & p ) const
	{
		apt_xyz res = 0.0; //assuming we are inside
		if ( p.x < this->min.x )
			res += mysqr(this->min.x - p.x);
		else if (p.x > this->max.x )
			res += mysqr(p.x - this->max.x);

		if ( p.y < this->min.y )
			res += mysqr(this->min.y - p.y);
		else if (p.y > this->max.y )
			res += mysqr(p.y - this->max.y);

		if ( p.z < this->min.z )
			res += mysqr(this->min.z - p.z);
		else if (p.z > this->max.z )
			res += mysqr(p.z - this->max.z);
		return res;
	}
};

//MK::https://programmizm.sourceforge.io/blog/2011/a-practical-implementation-of-kd-trees
struct node
{
	size_t i0, i1, split; //, dimdebug;dimdebug(-1),
	apt_xyz splitpos;

	node() : i0(-1), i1(-1), split(-1), splitpos(numeric_limits<apt_xyz>::max()) {}

	friend bool is_leaf(const node & n) {
		return n.split == size_t(-1);
	}
	friend size_t npoints(const node & n) {
		return (n.i1 - n.i0) + 1;
	}
};


struct build_task
{
	size_t first, last, node, dim;
};


struct traverse_task
{
	cuboid bx;
	size_t node;
	size_t dim;
	apt_xyz d;
};


struct scan_task
{
	cuboid bx;
	size_t node;
	size_t dim;
};


struct kd_tree
{
	p3d min;
	p3d max;
	vector<node> nodes;

	kd_tree() : min(p3d()), max(p3d()) {}
	void build(const vector<p3d> & points, vector<size_t> & permutate, const size_t leaf_size );

	void pack_p3dm1_d( const vector<size_t> & permutate, const vector<p3dm1> & apt1, const vector<apt_xyz> & dist1, vector<p3dm1> & apt2, vector<apt_xyz> & dist2 );
	void pack_p3dm1( const vector<size_t> & permutate, const vector<p3d> & aptpos1, const vector<unsigned int> & aptlabel1, vector<p3dm1> & apt2 );
	void pack_p3dm1_dist( vector<size_t> const & permutate, vector<p3dm1> const & in1, vector<apt_xyz> const & in2, vector<p3dm1> & out1, vector<apt_xyz> & out2 );
	void pack_p3dm1( vector<size_t> const & permutate, vector<p3dm3> const & in, vector<p3dm1> & out );
	void pack( vector<size_t> const & permutate, vector<p3d> const & in, vector<p3d> & out );
	void pack( vector<size_t> const & permutate, vector<p3d> const & in_ppp, vector<p3difo> const & in_ifo,
			vector<p3d> & out_ppp, vector<p3difo> & out_ifo );

	//void pack_p3d( vector<size_t> const & permutate, vector<p3d> const & in, vector<p3d> const & out );

	p3dm1 nearest_external(const p3dm1 target, const vector<p3dm1> & sortedpoints, apt_xyz epsball ) const;
	p3dm1 nearest(const size_t idx, const vector<p3dm1> & sortedpoints, apt_xyz epsball ) const;
	p3dm1 nearest(const p3d targ, const vector<p3dm1> & sortedpoints, apt_xyz epsball ) const;

	void range_rball_noclear_nosort_external(const p3dm1 target, const vector<p3dm1> & sortedpoints, const apt_xyz radius_sqrd, vector<nbor> & result );
	void range_rball_noclear_nosort(const size_t idx, const vector<p3dm1> & sortedpoints, const apt_xyz radius_sqrd, vector<nbor> & result );
	void range_rball_noclear_nosort_external_p3dm1(const p3dm1 target, const vector<p3dm1> & sortedpoints, const apt_xyz radius_sqrd, vector<p3dm1> & result );
	void range_rball_noclear_nosort_p3dm1(const size_t idx, const vector<p3dm1> & sortedpoints, const apt_xyz radius_sqrd, vector<p3dm1> & result );
	void range_rball_noclear_nosort_p3d( const p3dm1 target, vector<p3dm1> const & sortedpoints, const apt_xyz radius_sqrd, vector<p3dm1> & result );

	void range_rball_append_exclude_myself( const size_t idx, vector<p3d> const & sortedpoints, const apt_xyz radius_sqrd, vector<p3d> & out );
	void range_rball_append_external( const p3d target, vector<p3d> const & sortedpoints, const apt_xyz radius_sqrd, vector<p3d> & out );


	void range_rball_noclear_nosort_indices(const size_t idx, const vector<p3dm1> & sortedpoints, const apt_xyz radius_sqrd, vector<size_t> & result );

	inline p3d get_min();

	inline p3d get_max();

	bool verify(const vector<p3dm1> & sortedpoints);

	bool verify_p3d(vector<p3d> const & sortedpoints);


	void get_allboundingboxes( vector<scan_task> & out );

	void display_nodes();

	size_t get_treememory_consumption();
};


inline void expand(p3d & min, p3d & max, const p3dm1 & p)
{
	if (p.x < min.x)	min.x = p.x;
	if (p.x > max.x) 	max.x = p.x;
	if (p.y	< min.y) 	min.y = p.y;
	if (p.y > max.y) 	max.y = p.y;
	if (p.z	< min.z) 	min.z = p.z;
	if (p.z > max.z) 	max.z = p.z;
}

inline void expand(p3d & min, p3d & max, const p3d & p)
{
	if (p.x < min.x)	min.x = p.x;
	if (p.x > max.x) 	max.x = p.x;
	if (p.y	< min.y) 	min.y = p.y;
	if (p.y > max.y) 	max.y = p.y;
	if (p.z	< min.z) 	min.z = p.z;
	if (p.z > max.z) 	max.z = p.z;
}


struct lower_x
{
	const vector<p3d> & points;

	lower_x(const vector<p3d> & p) : points(p) {}

	bool operator()(size_t i1, size_t i2) const
	{
		return points[i1].x < points[i2].x;
	}
};

struct lower_y
{
	const vector<p3d> & points;

	lower_y(const vector<p3d> & p) : points(p) {}

	bool operator()(size_t i1, size_t i2) const
	{
		return points[i1].y < points[i2].y;
	}
};

struct lower_z
{
	const vector<p3d> & points;

	lower_z(const vector<p3d> & p) : points(p) {}

	bool operator()(size_t i1, size_t i2) const
	{
		return points[i1].z < points[i2].z;
	}
};

#endif
