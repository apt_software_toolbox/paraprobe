//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_PERIODICTABLE_H__
#define __PARAPROBE_UTILS_PERIODICTABLE_H__

#include "PARAPROBE_Math.h"

#define UNKNOWN_IONTYPE 			0

#define UNCHARGED					0
#define UNOCCUPIED					0x00	//there is no element with ID 0 because humans give elements Fortran style indexing ie begin from 1, so we can use 0x00 as a flag
#define FAULTY						0xFF	//there is no stable natural isotope with Z or N 255, so we can use 0xFF as a flag for faulty assignment
#define STABLE_ELEMENTS_MAXIMUM		93		//+1 because last is uranium with Z = 92
#define SPACECHARACTER				0x20

//bitpacking ions and compound ions saves space during excluding types and removing duplicates
//as an size_t is better cache aligned than a collection of eight individually queried unsigned chars
//avoids multiple syntax for different ion moleculars and singles
#define FIRST_BYTE		72057594037927936		//2^(64-8)
#define SECOND_BYTE		281474976710656			//2^(64-16)
#define THIRD_BYTE		1099511627776			//2^(64-24)
#define FOURTH_BYTE		4294967296				//2^(64-32)
#define FIFTH_BYTE		16777216				//2^(64-40)
#define SIXTH_BYTE		65536					//2^(64-48)
#define SEVENTH_BYTE	256						//2^(64-56)
#define EIGTH_BYTE		0						//2^(64-64)

#define POSITIVE_SIGN	0x01
#define NEGATIVE_SIGN	0xFF

#define FAULTY						0xFF	//there is no stable natural isotope with Z or N 255, so we can use 0xFF as a flag for faulty assignment


//the charge states
#define	ONE_ELECTRON_CHARGE			1
#define TWO_ELECTRON_CHARGE			2
#define THR_ELECTRON_CHARGE			3
#define FOU_ELECTRON_CHARGE			4

//#define ELECTRON_MASS
#define ELECTRON_MASS				5.48579909070e-4 //u //##MK::check and give better reference than wikipedia
#define ONE_ELECTRON_MASSES			((1)*(ELECTRON_MASS))	//does not take nucleon interaction into account
#define TWO_ELECTRON_MASSES			((2)*(ELECTRON_MASS))
#define THR_ELECTRON_MASSES			((3)*(ELECTRON_MASS))
#define FOU_ELECTRON_MASSES			((4)*(ELECTRON_MASS))


struct evapion
{
    apt_real x;
    apt_real y;
    apt_real z;
    unsigned char Z1;
    unsigned char N1;
    unsigned char Z2;
    unsigned char N2;
    unsigned char Z3;
    unsigned char N3;
    unsigned char sign;
    unsigned char charge;
    evapion() : x(0.f), y(0.f), z(0.f),
        Z1(UNOCCUPIED), N1(UNOCCUPIED),
            Z2(UNOCCUPIED), N2(UNOCCUPIED),
                Z3(UNOCCUPIED), N3(UNOCCUPIED),
                    sign(UNOCCUPIED), charge(UNOCCUPIED) {}
    evapion( p3d const & _pos, evapion const & _ion ) :
        x(_pos.x), y(_pos.y), z(_pos.z),
            Z1(_ion.Z1), N1(_ion.N1),
                Z2(_ion.Z2), N2(_ion.N2),
                    Z3(_ion.Z3), N3(_ion.N3),
                        sign(_ion.sign), charge(_ion.charge) {}
};

ostream& operator << (ostream& in, evapion const & val);


struct evapion3
{
	//an evaporated molecular ion cation with +charge composed of elements Z1,N1/Z2,N2/Z1,N2
	unsigned char Z1; 		//for single ions Z1, N1 is always occupied
	unsigned char N1;
	unsigned char Z2; 		//for dual ions Z2 and N2 gets populated additionally
	unsigned char N2;
	unsigned char Z3; 		//for molecular ions with three isotopes (possible duplicates) also Z3 and N3 on top
	unsigned char N3;

	unsigned char sign;		//sign of the charge of the complex, e.g. [AlFe]^2+ gets sign = POSITIVE_SIGN and charge = 2, Z1, N1, Z2, N2 will be occupied
	unsigned char charge;	//always used

	evapion3() : Z1(UNOCCUPIED), N1(UNOCCUPIED),
					Z2(UNOCCUPIED), N2(UNOCCUPIED),
						Z3(UNOCCUPIED), N3(UNOCCUPIED),
							sign(UNOCCUPIED), charge(UNCHARGED) {}
	evapion3( const unsigned char _z1, const unsigned char _n1,
				const unsigned char _z2, const unsigned char _n2,
					const unsigned char _z3, const unsigned char _n3, 
						const unsigned char _sgn, const unsigned char _q ) : 
						Z1(_z1), N1(_n1), Z2(_z2), N2(_n2), Z3(_z3), N3(_n3), 
							sign(_sgn), charge(_q) {}
};

ostream& operator << (ostream& in, evapion3 const & val);


struct evapcandidate
{
	//an evaporated molecular ion cation with +charge composed of elements Z1,N1/Z2,N2/Z1,N2
	unsigned char Z1; //for single ions Z1, N1 is always occupied
	unsigned char N1;
	unsigned char Z2; //for dual ions Z2 and N2 gets populated additionally
	unsigned char N2;
	unsigned char Z3; //for molecular ions with three isotopes (possible duplicates) also Z3 and N3 on top
	unsigned char N3;
	unsigned char sign;
	unsigned char charge;

	apt_real mass;
	apt_real abundance_score;	//product of natural isotope abundance
	apt_real abunncomp_score;	//product of the natural isotope abundance times nominal composition
	apt_real nomincomp_score;	//product of the compositions and expected contaminant chamber/sample levels

	evapcandidate() : Z1(UNOCCUPIED), N1(UNOCCUPIED),
						Z2(UNOCCUPIED), N2(UNOCCUPIED),
							Z3(UNOCCUPIED), N3(UNOCCUPIED),
								sign(UNOCCUPIED), charge(UNCHARGED), 
								mass(0.f), abundance_score(0.f), abunncomp_score(0.f), nomincomp_score(0.f) {}
	evapcandidate( const unsigned char _z1, const unsigned char _n1,
					const unsigned char _z2, const unsigned char _n2,
						const unsigned char _z3, const unsigned char _n3,
							const unsigned int _sgn, const unsigned char _q, const apt_real _m,
								const apt_real _absc, const apt_real _abcpsc, const apt_real _ncpsc ) :
						Z1(_z1), N1(_n1), Z2(_z2), N2(_n2), Z3(_z3), N3(_n3), sign(_sgn), charge(_q),
							mass(_m), abundance_score(_absc), abunncomp_score(_abcpsc),
								nomincomp_score(_ncpsc) {}
	evapcandidate( evapion3 const & in ) : Z1(in.Z1), N1(in.N1), Z2(in.Z2), N2(in.N2), Z3(in.Z3), N3(in.N3),
			sign(in.sign), charge(in.charge), mass(0.f), abundance_score(0.f),
				abunncomp_score(0.f), nomincomp_score(0.f) {}
};

ostream& operator << (ostream& in, evapcandidate const & val);


inline bool SortEvapionDecrsScore( const evapcandidate &aa1, const evapcandidate &aa2);


struct evapion_help
{
	size_t n;		//one past the end
	size_t s;		//contiguous array start position
	size_t e;		//one past contiguous array end position
	evapion_help() : n(0), s(-1), e(-1) {}
	evapion_help( const size_t _s, const size_t _e ) :
		n(_e - _s), s(_s), e(_e) {}
};

ostream& operator << (ostream& in, evapion_help const & val);


struct unique_order
{
	//enforcing a unique order to allow that hash values will get mapped on same size_t value and hence eliminatable by set STL containers
	unsigned char z1;
	unsigned char z2;
	unsigned char z3;
	unsigned char n1;
	unsigned char n2;
	unsigned char n3;
	bool pad1;
	bool pad2;
	unique_order() : z1(UNOCCUPIED), z2(UNOCCUPIED), z3(UNOCCUPIED),
			n1(UNOCCUPIED), n2(UNOCCUPIED), n3(UNOCCUPIED), pad1(false), pad2(false) {}
	unique_order( const unsigned char _z1, const unsigned char _z2, const unsigned char _z3,
			const unsigned char _n1, const unsigned char _n2, const unsigned char _n3 ) :
				z1(_z1), z2(_z2), z3(_z3), n1(_n1), n2(_n2), n3(_n3), pad1(false), pad2(false) {}
};


struct isotope_bucket
{
	size_t Zoffset;		//to translate an Zi-Zoffset into a position to specify an element
	size_t Noffset;		//to translate an Ni-Noffset into a position to specify an isotope of that element
	size_t s;			//total range on a contiguous array which the bucket spans
	size_t e;
	isotope_bucket() : Zoffset(0), Noffset(0), s(0), e(0) {}
	isotope_bucket(const size_t _zoff, const size_t _noff, const size_t _s, const size_t _e) :
		Zoffset(_zoff), Noffset(_noff), s(_s), e(_e) {}
};

ostream& operator << (ostream& in, isotope_bucket const & val);


struct isotope_props
{
	apt_real mass;
	apt_real abundance;		//MK::natural abundance!
	apt_real mass_error;
	apt_real abun_error;
	isotope_props() : mass(0.f), abundance(0.f), mass_error(0.f), abun_error(0.f) {}
	isotope_props( const apt_real _ma, const apt_real _ab,
			const apt_real _merr, const apt_real _aerr ) : mass(_ma), abundance(_ab),
					mass_error(_merr), abun_error(_aerr) {}
};

ostream& operator << (ostream& in, isotope_props const & val);


struct isotope
{
	char nm1;				//first character of name
	char nm2;				//second character of element name
	unsigned char Z;		//atomic number
	unsigned char N;		//neutron number

	apt_real mass;			//atomic mass
	apt_real abun;			//MK::natural abundance!

	apt_real mass_error;
	apt_real abun_error;

	isotope() : nm1(SPACECHARACTER), nm2(SPACECHARACTER), Z(UNOCCUPIED), N(UNOCCUPIED),
			mass(0.f), abun(0.f), mass_error(0.f), abun_error(0.f) {}
	isotope( const string _nm, const int _z, const int _n,
				const apt_real _ma, const apt_real _ab, const apt_real _merr, const apt_real _aerr );

	//##MK::implement constant time access for isotopes!
};

ostream& operator << (ostream& in, isotope const & val);


struct chemicalelement
{
	char nm1;				//first character of name
	char nm2;				//second character of element name
	unsigned char Z;		//atomic number
	unsigned char N;		//neutron number, for now padded with 0x00

	apt_real atpercent;		//molar fraction
	apt_real comp_error;

	chemicalelement() : nm1(SPACECHARACTER), nm2(SPACECHARACTER), Z(UNOCCUPIED), N(UNOCCUPIED), atpercent(0.f), comp_error(0.f) {}
	chemicalelement( const string _nm, const int _z, const apt_real _atperc, const apt_real _cerr );
};

ostream& operator << (ostream& in, chemicalelement const & val);


struct mqival
{
	apt_real lo;
	apt_real hi;
	mqival() :
		lo(0.f), hi(0.f) {}
	mqival( const apt_real _lo, const apt_real _hi) : lo(_lo), hi(_hi) {}

	apt_real width();
	bool inrange(const apt_real val);
};

ostream& operator<<(ostream& in, mqival const & val);


inline bool SortMQRangeAscending( const mqival &aa1, const mqival &aa2);
/*{
	//promoting sorting in descending order
	return aa1.hi < aa2.lo;
}*/


struct nuclid
{
	unsigned int id; //##MK::so far a dummy only but can be extended by adding pieces of information by user
	nuclid() : id(UNKNOWN_IONTYPE) {}
	nuclid(const unsigned int _id) : id(_id) {}
};

ostream& operator<<(ostream& in, nuclid const & val);


class nuclidTable
{
public:
	nuclidTable();
	~nuclidTable();

	bool load_isotope_library( const string fn );

	//isotope is a bucket list of constant time access of all isotopes for a given nucleon number Z and subsequent logarithmic access of a particular isotope, i.e. lg time
	vector<map<size_t,isotope>> isotopes;
	//##MK::implement for constant time access of both Z and isotope
};


struct rangedIontype
{
	unsigned int id;
	evapion3 strct;
	vector<mqival> rng;
	rangedIontype() : id(UNKNOWN_IONTYPE), strct(evapion3()), rng(vector<mqival>()) {}
	rangedIontype( const unsigned int _id, evapion3 const & _strct ) :
		id(_id), strct(_strct), rng(vector<mqival>()) {}
	void add_range( const mqival in );
	string get_name();	
};

ostream& operator<<(ostream& in, rangedIontype & val);


class rangeTable
{
public:
	rangeTable();
	~rangeTable();
	
	size_t hash_molecular_ion( const unsigned char Z1, const unsigned char N1, 	const unsigned char Z2, const unsigned char N2,
		const unsigned char Z3, const unsigned char N3, const unsigned char charge );
	size_t hash_molecular_ion( const unsigned char Z1, const unsigned char Z2, const unsigned char Z3 );
	evapion3 unhash_molecular_ion( const size_t hash );
	
	void add_evapion3( string const & s, vector<evapion3> & out );
	void add_iontype( vector<string> const & tks, mqival const & iv );
	bool read_rrng_file( const string ascii_io_fn );
	bool read_rng_file( const string ascii_io_fn );
	
	bool validity_check_rangemax();
	bool validity_check_noemptyness();
	bool validity_check_nonoverlapping();
	bool validity_check();

	vector<rangedIontype> iontypes;
	nuclidTable nuclides;
	map<size_t,size_t> iontypes_dict; //first, i.e. key tells me evapion3 hash, second, i.e. value tells me iontypes.at(look here)
};

/*
class PeriodicTable
{
	//MK::implements a smart periodic table with which to handle range files and ion type identification
public:
	PeriodicTable();
	~PeriodicTable();

	void load_periodictable();
	unsigned int mq2ionname( const apt_real mq );
	unsigned int ion_name2type( const string name );
	string ion_type2name( const unsigned int type );
	unsigned int get_maxtypeid();

	void add_iontype_single_or_molecular( vector<string> const & in, mqival const & ival );
	string parse_iontype_single_or_molecular( const string in );


	//##MK::deprecated bool read_rangefile( string ascii_io_fn );
	bool read_rangefile2( string ascii_io_fn ); //using an RRNG file
	bool read_rangefile3( string ascii_io_fn ); //using an RNG file

	map<string, nuclid> NuclidTable;
	map<string, unsigned int> IonTypes; //two way referencing to ease ranging
	map<unsigned int, string> IonNames;
	unsigned int MaximumTypID;

	//vector<string> IonName;
	vector<vector<mqival>> MQ2IonName;
	bool rangefile_loaded;
};
*/

/*
class TypeSpecDescrStat
{
	//class object to ease the processing of generic iontype-specific descriptive statistics characterization tasks
	//key idea: take for instance nearest neighbor distributions, APT folks are interested in studying not only how
	//an ion clusters is so and so far from another of any type but specific type
	//hence we should be able to process with the same function and code queries like
	//NN Al against all types
	//NN Al against only Sc
	//NN Al against only Sc and Zr
	//now lets abbreviate these queries via shorthand codes
	// Al,
	// Al,Sc
	// Al,Sc,Zr
	//##MK::add also negations as such Al,!Sc meaning Al against every type excluding Sc


public:
	TypeSpecDescrStat();
	~TypeSpecDescrStat();

	bool define_iontask3(const string command, PeriodicTable const & pse );
	bool define_iontask4(const string command, PeriodicTable const & pse );


	//pair<string, unsigned int> target;	 	 //##MK::deprecated, the typeID of the specimen for which do perform spatial statistics, i.e. Al e.g. 0
	map<string, unsigned int> trgcandidates; //the typeIDs of the central ions for which to perform spatial statistics
	map<string, unsigned int> envcandidates; //the corresponding typeIDs to test against in the environment of the central ions
};


void parse_tasks( const string command, vector<TypeSpecDescrStat> & these, PeriodicTable const & thispse ); //parses the command into individual iontype related tasks storing results as task class objects in these
*/

#endif

