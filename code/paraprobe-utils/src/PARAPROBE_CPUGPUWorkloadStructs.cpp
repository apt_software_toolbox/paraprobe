//##MK::GPLV3

#include "PARAPROBE_CPUGPUWorkloadStructs.h"

ostream& operator<<(ostream& in, epoch_log const & val)
{
	in << val.threadid << ";" << val.nthreads << ";" << val.epoch << ";" << val.dt << ";" << val.nchunks << ";" << val.ionsum << "\n";
	return in;
}


bool SortEpochLogAsc( const epoch_log & a, const epoch_log & b )
{
    return ( a.epoch < b.epoch || ((a.epoch == b.epoch) && (a.threadid < b.threadid) ) );
}
