//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_VERBOSE_H__
#define __PARAPROBE_UTILS_VERBOSE_H__

#include "PARAPROBE_STLIncludes.h"
#include "PARAPROBE_Numerics.h"
#include "PARAPROBE_PhysicalConstants.h"
#include "PARAPROBE_Crystallography.h"
#include "PARAPROBE_UnitConversions.h"
#include "PARAPROBE_Parallelization.h"

#include "PARAPROBE_BoostInterface.h"
#include "PARAPROBE_RapidXMLInterface.h"

//the GITSHA hash will be passed upon compilation using -DGITSHA

/*
#define VERSION_MAJOR				0
#define VERSION_MINOR				1
#define VERSION_REVISION			0
#define VERSION_BETASTAGE			1
bool helloworld( int pargc, char** pargv );
*/

//genering global functions to report state, warnings and erros
void reporting( const int rank, const string what );
void reporting( const string what );
void complaining( const int rank, const string what );
void complaining( const string what );
void stopping( const int rank, const string what );
void stopping( const string what );

#endif
