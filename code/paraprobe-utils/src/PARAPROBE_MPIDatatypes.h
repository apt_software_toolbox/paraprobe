//##MK::CODE


#ifndef __PARAPROBE_UTILS_MPIDATATYPES_H__
#define __PARAPROBE_UTILS_MPIDATATYPES_H__

#include "PARAPROBE_Datatypes.h"


#define MPIIO_OFFSET_INCR_F32			4
#define MPIIO_OFFSET_INCR_INT32			4
#define MPIIO_OFFSET_INCR_UINT32		4


struct MPI_Crystallo_MatPoint
{
	float x;
	float y;
	float z;
	float t;
	unsigned int mpid;
	unsigned int thrid;
	unsigned int cnts;
	MPI_Crystallo_MatPoint() : x(0.f), y(0.f), z(0.f), t(0.f),
			mpid(0), thrid(0), cnts(0) {}
	MPI_Crystallo_MatPoint( const float _x, const float _y, const float _z, const float _t,
			const unsigned int _mpid, const unsigned int _thrid, const unsigned int _c ) :
				x(_x), y(_y), z(_z), t(_t), mpid(_mpid), thrid(_thrid), cnts(_c) {}
};



struct MPI_Crystallo_S2G
{
	float el;
	float az;
	MPI_Crystallo_S2G() : el(0.f), az(0.f) {}
	MPI_Crystallo_S2G( const float _el, const float _az ) : el(_el), az(_az) {}
};


struct MPI_Crystallo_SO3G
{
	float q0;
	float q1;
	float q2;
	float q3;
	MPI_Crystallo_SO3G() : q0(1.f), q1(0.f), q2(0.f), q3(0.f) {}
	MPI_Crystallo_SO3G( const float _q0, const float _q1, const float _q2, const float _q3 ) :
		q0(_q0), q1(_q1), q2(_q2), q3(_q3) {}
};


struct MPI_VicBreenIO
{
	unsigned int mpid;
	float el;
	float az;
	float freqbin;
	float freqval;
	MPI_VicBreenIO() : mpid(UINT32MX), el(F32MX), az(F32MX), freqbin(F32MX), freqval(F32MX) {}
	MPI_VicBreenIO( const unsigned int _mpid, const float _el, const float _az, const float _fb, const float _fv ) :
		mpid(_mpid), el(_el), az(_az), freqbin(_fb), freqval(_fv) {}
};


struct MPI_IonPositions
{
	float x;
	float y;
	float z;
	MPI_IonPositions() : x(0.f), y(0.f), z(0.f) {}
	MPI_IonPositions( const float _x, const float _y, const float _z ) :
		x(_x), y(_y), z(_z) {}
	MPI_IonPositions( p3d const & _p ) :
		x(_p.x), y(_p.y), z(_p.z) {}
};


struct MPI_ElevAzim
{
	float elevation;
	float azimuth;
	MPI_ElevAzim() : elevation(0.f), azimuth(0.f) {}
	MPI_ElevAzim( const float _el, const float _az ) :
		elevation(_el), azimuth(_az) {}
};


struct MPI_ElevAzimXYZ
{
	float x;
	float y;
	float z;
	MPI_ElevAzimXYZ() : x(0.f), y(0.f), z(0.f) {}
	MPI_ElevAzimXYZ( const float _x, const float _y, const float _z ) :
		x(_x), y(_y), z(_z) {}
};




struct MPI_IonWithDistance
{
	float x;
	float y;
	float z;
	float d;
	MPI_IonWithDistance() : x(0.f), y(0.f), z(0.f), d(0.f) {}
	MPI_IonWithDistance( const float _x, const float _y, const float _z, const float _d ) :
		x(_x), y(_y), z(_z), d(_d) {}
	MPI_IonWithDistance( p3d const & _p, const float _d) :
		x(_p.x), y(_p.y), z(_p.z), d(_d) {}
};


struct MPI_Ion
{
	float x;
	float y;
	float z;
	unsigned int m;
	MPI_Ion() : x(0.f), y(0.f), z(0.f), m(0) {}
	MPI_Ion( const float _x, const float _y, const float _z, const unsigned int _m ) :
		x(_x), y(_y), z(_z), m(_m) {}
};


struct MPI_Triangle
{
	float x1;
	float y1;
	float z1;
	float x2;
	float y2;
	float z2;
	float x3;
	float y3;
	float z3;
	MPI_Triangle() : x1(0.f), y1(0.f), z1(0.f), x2(0.f), y2(0.f), z2(0.f), x3(0.f), y3(0.f), z3(0.f) {}
	MPI_Triangle( tri3d const & _tri ) :
		x1(_tri.x1), y1(_tri.y1), z1(_tri.z1),
		x2(_tri.x2), y2(_tri.y2), z2(_tri.z2),
		x3(_tri.x3), y3(_tri.y3), z3(_tri.z3) {}
};


struct MPI_Fourier_ROI
{
	float x;
	float y;
	float z;
	float r;
	unsigned int mpid;
	unsigned int nions;
	MPI_Fourier_ROI() : x(0.f), y(0.f), z(0.f), r(0.f), mpid(-1), nions(0) {}
	MPI_Fourier_ROI( const float _x, const float _y, const float _z, const float _r, const unsigned int _mpid, const unsigned int _nions ) :
		x(_x), y(_y), z(_z), r(_r), mpid(_mpid), nions(_nions) {}
	/*MPI_Fourier_ROI() : mpid(-1), nions(0) {}
	MPI_Fourier_ROI( const unsigned int _mpid, const unsigned int _nions ) :
			mpid(_mpid), nions(_nions) {}*/
};


struct MPI_Fourier_HKLValue
{
	int mpid;					//global ID
	int ijk;					//leave the coordinates in packed form to reduce data copy time because the message is smaller, works because all processes use the same ConfigFourier::FourierGridResolution
	float value;
	MPI_Fourier_HKLValue() : mpid(-1), ijk(-1), value(0.f) {}
	MPI_Fourier_HKLValue( const int _mpid, const int _ijk, const float _val ) :
		mpid(_mpid), ijk(_ijk), value(_val) {}
};


struct MPI_Ranger_Iontype
{
	unsigned char id;
	unsigned char Z1;
	unsigned char N1;
	unsigned char Z2;
	unsigned char N2;
	unsigned char Z3;
	unsigned char N3;
	unsigned char sign;
	unsigned char charge;
	MPI_Ranger_Iontype() : id(0x00), Z1(0x00), N1(0x00),
			Z2(0x00), N2(0x00), Z3(0x00), N3(0x00), sign(0x00), charge(0x00) {}
	MPI_Ranger_Iontype( const unsigned int _id, const unsigned int _z1, const unsigned int _n1,
			const unsigned int _z2, const unsigned int _n2,
			const unsigned int _z3, const unsigned int _n3,
			const unsigned int _sgn, const unsigned int _chg ) :
				id(_id), Z1(_z1), N1(_n1), Z2(_z2), N2(_n2),
				Z3(_z3), N3(_n3), sign(_sgn), charge(_chg) {}
};


struct MPI_Ranger_MQIval
{
	float low;
	float high;
	unsigned char id;
	unsigned char pad1;
	unsigned char pad2;
	unsigned char pad3;
	MPI_Ranger_MQIval() : low(0.0), high(0.0), id(0x00), pad1(0x00), pad2(0x00), pad3(0x00) {}
	MPI_Ranger_MQIval( const float _low, const float _high, const unsigned char _id ) :
		low(_low), high(_high), id(_id), pad1(0x00), pad2(0x00), pad3(0x00) {}
};


struct MPI_Ranger_DictKeyVal
{
	//https://stackoverflow.com/questions/15637228/what-is-the-downside-of-replacing-size-t-with-unsigned-long
	unsigned long long key;
	unsigned long long val;
	MPI_Ranger_DictKeyVal() : key(0), val(0) {}
	MPI_Ranger_DictKeyVal( const size_t _key, const size_t _val ) : key(_key), val(_val) {}
};


struct MPI_Synth_XYZ
{
	float x;
	float y;
	float z;
	MPI_Synth_XYZ() : x(0.0), y(0.0), z(0.0) {}
	MPI_Synth_XYZ( p3d const & p ) : x(p.x), y(p.y), z(p.z) {}
};


struct MPI_Spatstat_Task
{
	float rmin;
	float rincr;
	float rmax;
	unsigned int hst1d_cnt;
	unsigned int sdm3d_cnt;
	unsigned int targ_nbors_cnt;
	unsigned int pos_firstnbor;
	unsigned int kthorder;
	unsigned int taskid;		//not a computing task in terms of CPU or hardware but a physical task, i.e. scientific analysis task...
	bool WhichLabel;
	bool IsRDF;
	bool IsKNN;
	bool IsSDM;
	MPI_Spatstat_Task() : rmin(0.0), rincr(0.0), rmax(0.0), hst1d_cnt(0), sdm3d_cnt(0), targ_nbors_cnt(0), pos_firstnbor(0),
			kthorder(0), taskid(0), WhichLabel(false), IsRDF(false), IsKNN(false), IsSDM(false) {}
	MPI_Spatstat_Task( const float _rmi, const float _ric, const float _rmx, const unsigned int _hst1d, const unsigned int _sdm3d,
			const unsigned int _tgnb, const unsigned int _pos, const unsigned int _kth, const unsigned int _id,
			const bool _lbl, const bool _rdf, const bool _knn, const bool _sdm ) :
				rmin(_rmi), rincr(_ric), rmax(_rmx), hst1d_cnt(_hst1d), sdm3d_cnt(_sdm3d), targ_nbors_cnt(_tgnb),
				pos_firstnbor(_pos), kthorder(_kth), taskid(_id),  WhichLabel(_lbl), IsRDF(_rdf), IsKNN(_knn), IsSDM(_sdm) {}
};


struct MPI_Quaternion
{
	float q0;
	float q1;
	float q2;
	float q3;
	MPI_Quaternion() : q0(1.f), q1(0.f), q2(0.f), q3(0.f) {}
	MPI_Quaternion( const float _q0, const float _q1, const float _q2, const float _q3 ) :
		q0(_q0), q1(_q1), q2(_q2), q3(_q3) {}
};


/*//MPI IO container for reading EPOS file

typedef struct
{
	float x;		//coordinates
	float y;
	float z;
	float m_q;		//mass-to-charge ratio
	float tof;		//time of flight
	float vdc;		//voltage DC
	float vpu;		//pulse voltage
	float detx;		//detector coordinates
	float dety;
	int nulls;		//nulls - is the time between each pulse
	int nat_pulse;	//###number of atoms?
} MPI_EPOS_EventIO;
*/

#endif
