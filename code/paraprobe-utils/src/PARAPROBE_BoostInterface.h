//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_BOOSTINTERFACE_H__
#define __PARAPROBE_UTILS_BOOSTINTERFACE_H__

//#define UTILIZE_BOOST

#ifdef UTILIZE_BOOST
	//boost
	#include <boost/algorithm/string.hpp>
	#include <boost/math/special_functions/bessel.hpp>
#else
	//C++ processing of regular expression to avoid having to use boost for now
	#include <regex>
	using std::regex;
	using std::string;
	using std::sregex_token_iterator;
#endif


#endif
