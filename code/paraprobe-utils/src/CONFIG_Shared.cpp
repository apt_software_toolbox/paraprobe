/*
	Copyright Max-Planck-Institut f\"ur Eisenforschung, GmbH, D\"sseldorf
	Data structure, code design, parallel implementation:
	Markus K\"uhbach, 2017-2019

	Third-party contributions:
	Andrew Breen - sequential Matlab code snippets for reconstruction and EPOS
	Markus G\"otz et al. - HPDBScan
	Kartik Kukreja - path compressed union/find
	Lester Hedges - AABBTree

	PARAPROBE --- is an MPI/OpenMP/SIMD-parallelized tool for efficient scalable
	processing of Atom Probe Tomography data targeting back-end processing.
	
	This file is part of PARAPROBE.

	PARAPROBE is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

 	PARAPROBE is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with paraprobe.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CONFIG_Shared.h"

apt_real str2real( const string str )
{
#ifdef EMPLOY_SINGLEPRECISION
	return stof( str );
#else
	return stod( str );
#endif
}


string read_xml_attribute_string( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) )
		return in->first_node(keyword.c_str())->value();
	else
		return "";
}


apt_real read_xml_attribute_float( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) ) {
		return str2real(in->first_node(keyword.c_str())->value());
	}
	else {
#ifdef EMPLOY_SINGLEPRECISION
		return 0.f;
#else
		return 0.0;
#endif
	}
}


unsigned int read_xml_attribute_uint32( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) )
		return stoul(in->first_node(keyword.c_str())->value());
	else
		return 0;
}


int read_xml_attribute_int32( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) )
		return atoi(in->first_node(keyword.c_str())->value());
	else
		return 0;
}


bool read_xml_attribute_bool( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) ) {
		int i32 = atoi(in->first_node(keyword.c_str())->value());
		if ( i32 == 1 )
			return true;
		else
			return false;
	}
	return false;
}


unsigned long read_xml_attribute_uint64( xml_node<> const * in, const string keyword )
{
	if ( 0 != in->first_node(keyword.c_str()) )
		return stoul(in->first_node(keyword.c_str())->value());
	else
		return 0;
}




unsigned int ConfigShared::SimID = 0;

//predefined values
unsigned int ConfigShared::RndDescrStatsPRNGSeed = -1;
unsigned int ConfigShared::RndDescrStatsPRNGDiscard = 700000;

