//##MK::GPLV3

#include "PARAPROBE_FFT.h"

cfftn::cfftn()
{
	mm = 0;
	nn = 1;
}


cfftn::~cfftn()
{
	mm = 0;
	nn = 0;
	fftTemp = vector<complex<cfft_real>>();
}

void cfftn::init_plan( const long M )
{
	mm = M;
	//Calculate the number of points, MK::power-of-two transforms 2^M !
	nn = 1;
	for(long i = 0; i < mm; i++) {
		nn <<= 1;
	}
	fftTemp = vector<complex<cfft_real>>( nn, complex<cfft_real>(0.0, 0.0) );
}


void cfftn::create_plan( vector<float> const & in, size_t offset )
{
	//single precision real to complex domain fft
	for( long i = 0; i < nn; i++ ) { //reset all existent plans
		fftTemp[i] = complex<cfft_real>( 0.f, 0.f );
	}
	for( long i = 0; i < nn; i++ ) {
		fftTemp[i] = complex<cfft_real>( in[offset+i], 0.f ); //provide real values only
	}
}


void cfftn::execute_fwfft_report_magnitude( vector<float> & out, size_t offset )
{
	/*
	   Modification of Paul Bourkes FFT code by Peter Cusack
	   to utilise the Microsoft complex type.

	   This computes an in-place complex-to-complex FFT
	   x and y are the real and imaginary arrays of 2^m points.
	   dir =  1 gives forward transform, here hard-coded
	   dir = -1 gives reverse transform
	*/

	/* Do the bit reversal */
	long m = mm;
	long n = nn;
	long i2 = n >> 1;
	long j = 0;

	long i, i1, k, l, l1;

	for (i = 0; i < n-1; i++) {
		if (i < j)
			swap( fftTemp[i], fftTemp[j] );

	  k = i2;
	  while (k <= j) {
		  j -= k;
		  k >>= 1;
	  }
	  j += k;
	}

	/* Compute the FFT */
	complex<cfft_real> tx, t1, u, c;
	c = complex<cfft_real>( -1.0, 0.0 );
	//c.real(-1.0);
	//c.imag(0.0);
	long l2 = 1;
	for (l = 0; l < m; l++) {
		l1 = l2;
		l2 <<= 1;
		u.real(1.0);
		u.imag(0.0);
		for (j = 0; j < l1; j++) {
			for (i = j; i < n; i += l2) {
				i1 = i + l1;
	            t1 = u * fftTemp[i1];
	            fftTemp[i1] = fftTemp[i] - t1;
	            fftTemp[i] += t1;
			}
			u = u * c;
		}
		//c.imag(sqrt((1.0 - c.real()) / 2.0));
		//if (dir == 1)
		//	c.imag(-c.imag());
		//hard-coded dir == 1
		c.imag(-sqrt((1.0 - c.real()) / 2.0));
		c.real(+sqrt((1.0 + c.real()) / 2.0));
	}

	//##MK::debug report me the values first
	/*
	for( i = 0; i < n; i++ ) {
		cout << "Before scaling Freq bin " << i << "\treal/imag\t" << fftTemp[i].real() << "\t\t" << fftTemp[i].imag() << "\n";
	}
	*/

	// /* hard-coded scaling for forward transform (dir == 1) only*/
	//for( i = 0; i < n; i++ ) {
	//	fftTemp[i] /= static_cast<cfft_real>(n);
	//}

	/*
	for( i = 0; i < n; i++ ) {
		//cout << "After scaling Freq bin " << i << "\treal/imag\t" << fftTemp[i].real() << "\t\t" << fftTemp[i].imag() << "\n";
		cout << "Unscaled freq bin " << i << "\treal/imag\t" << fftTemp[i].real() << "\t\t" << fftTemp[i].imag() << "\n";
	}
	*/

	//MK::getMagnitude overwrites [0,NumberOfBinsHalf) with sqrt(zR^2+zI^2)
	long firsthalf = n/2;
	for( i = 0; i < firsthalf; i++ ) {
		out.at(offset+i) = static_cast<float>( sqrt( SQR(fftTemp[i].real()) + SQR(fftTemp[i].imag()) ) );
	}
}
