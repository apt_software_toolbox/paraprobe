//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_FFT_H__
#define __PARAPROBE_UTILS_FFT_H__


#include <complex>

#include "PARAPROBE_CPUGPUWorkloadStructs.h"

//this is a hardcoded thread-safe 1D in-place Cooley-Tukey-type complex to complex forward fast fourier transform

typedef float cfft_real;

class cfftn
{
	//since MKL version 2018.4 calls to the rfftn implementation systematically segfault when called even with
	//mkl_set_num_threads(1) and MKL_NUM_THREADS=1 within the plan creation stage during mkl_avx512_malloc
	//whenever more than one OpenMP thread attempts to create a plan even within an encapsulated threadlocal subroutine
	//in MKL2018.0 this poses though no problem, this is potentially a bug in the IMKL implementation itself
	//unfortunately on TALOS, only >=2018.4 is available for this reason and given that we have to execute
	//very many but small 1d FFTs we here use a thread sequential fallback
	//specifically, Paul Bourke/Peter Cousacks implementation of a in-place 1d FFT using Cooley-Tukey's algorithm
	//http://paulbourke.net/miscellaneous/dft/fft_ms.c
public:
	cfftn();
	~cfftn();

	void init_plan( const long M );
	void create_plan( vector<float> const & in, size_t offset );
	void execute_fwfft_report_magnitude( vector<float> & out, size_t offset );

private:
	long mm;
	long nn;
	vector<complex<cfft_real>> fftTemp;
};

#endif
