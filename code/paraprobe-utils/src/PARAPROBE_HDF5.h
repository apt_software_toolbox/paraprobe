//##MK::GPLV3

#ifndef __PARAPROBE_UTILS_HDFIO_H__
#define __PARAPROBE_UTILS_HDFIO_H__

#include "CONFIG_Shared.h"
#include "PARAPROBE_APTH5.h"

//all metadata definitions per paraprobe-tool
#include "metadata/PARAPROBE_SyntheticMetadataDefsH5.h"
#include "metadata/PARAPROBE_RangerMetadataDefsH5.h"
#include "metadata/PARAPROBE_SurfacerMetadataDefsH5.h"
#include "metadata/PARAPROBE_SpatstatMetadataDefsH5.h"
#include "metadata/PARAPROBE_FourierMetadataDefsH5.h"
#include "metadata/PARAPROBE_AraulloMetadataDefsH5.h"
#include "metadata/PARAPROBE_IndexerMetadataDefsH5.h"

//comment out if no HDF5 support desired and switch then also EMPLOY_HDFSUPPORT OFF in the CMake file

#define UTILIZE_HDF5

/*
#define PARAPROBE_RANGING						"/Ranging"
#define PARAPROBE_RANGING_IONTYPE_IDS			"/Ranging/IontypeID"
#define PARAPROBE_RANGING_IONTYPE_MQ			"/Ranging/IontypeMQ"

#define PARAPROBE_AUTORANGING					"/RangingAutomatic"
#define PARAPROBE_AUTORANGING_SETTINGS			"/RangingAutomatic/Settings"
#define PARAPROBE_AUTORANGING_TOFCNTS			"/RangingAutomatic/SqrtMQHist"
#define PARAPROBE_AUTORANGING_BASELINE			"/RangingAutomatic/BaselineHist"
#define PARAPROBE_AUTORANGING_SAVGOLAY			"/RangingAutomatic/SmoothHist"
#define PARAPROBE_AUTORANGING_PEAKS				"/RangingAutomatic/Peaks"
#define PARAPROBE_AUTORANGING_PEAKS_SUMM		"/RangingAutomatic/Peaks/Summary"
#define PARAPROBE_AUTORANGING_PEAKS_CANDS		"/RangingAutomatic/Peaks/Candidates"
#define PARAPROBE_AUTORANGING_PEAKS_SCORES		"/RangingAutomatic/Peaks/HeuristicScore"


#define PARAPROBE_VOLRECON						"/VolumeRecon"
#define PARAPROBE_VOLRECON_XYZ					"/VolumeRecon/XYZ"
#define PARAPROBE_VOLRECON_XYZ_NCMAX			3
#define PARAPROBE_VOLRECON_MQ					"/VolumeRecon/MQ"
#define PARAPROBE_VOLRECON_MQ_NCMAX				1
#define PARAPROBE_VOLRECON_TOPO					"/VolumeRecon/Positions"
#define PARAPROBE_VOLRECON_IONTYPE				"/VolumeRecon/Iontype"
#define PARAPROBE_VOLRECON_IONTYPE_NCMAX		1

//#define PARAPROBE_VOLRECON_SURFDISTSQR			"/VolumeRecon/SurfaceDistSQR"

#define PARAPROBE_SURFRECON						"/SurfaceRecon"
#define PARAPROBE_SURFRECON_ASHAPE				"/SurfaceRecon/AlphaShape"
#define PARAPROBE_SURFRECON_ASHAPE_HULL			"/SurfaceRecon/AlphaShape/TriangleHull"
#define PARAPROBE_SURFRECON_ASHAPE_INFO			"/SurfaceRecon/AlphaShape/DescrStats"
#define PARAPROBE_SURFRECON_ASHAPE_HULL_TOPO	"/SurfaceRecon/AlphaShape/TriangleHull/XDMFTopologyValues"
#define PARAPROBE_SURFRECON_ASHAPE_HULL_GEOM	"/SurfaceRecon/AlphaShape/TriangleHull/XDMFxyzValues"
#define PARAPROBE_SURFRECON_ASHAPE_HULL_GEOM_NCMAX	9
#define PARAPROBE_SURFRECON_ASHAPE_ION2DIST		"/SurfaceRecon/AlphaShape/Ion2Distance"
#define PARAPROBE_SURFRECON_ASHAPE_ION2DIST_NCMAX		1

#define PARAPROBE_CRYSTALLO						"/Crystallography"
#define PARAPROBE_CRYSTALLO_MATPOINT_TOPO		"/Crystallography/SamplingPointCloudTopo"
#define PARAPROBE_CRYSTALLO_MATPOINT_XYZ		"/Crystallography/SamplingPointCloudXYZ"
#define PARAPROBE_CRYSTALLO_MATPOINT_IDS		"/Crystallography/SamplingPointIDs"
#define PARAPROBE_CRYSTALLO_MATPOINT_META		"/Crystallography/SamplingPointCloudMeta"
#define PARAPROBE_CRYSTALLO_MATPOINT_UVW		"/Crystallography/SamplingPointXYZWrtLattice"
#define PARAPROBE_CRYSTALLO_MATPOINT_PROFILING	"/Crystallography/SamplingPointCloudTiming"
#define PARAPROBE_CRYSTALLO_ELEVAZIM_ELAZ		"/Crystallography/S2GridElevAzim"
#define PARAPROBE_CRYSTALLO_ELEVAZIM_XYZ		"/Crystallography/S2GridXYZ"
#define PARAPROBE_CRYSTALLO_ELEVAZIM_TOPO		"/Crystallography/S2GridXDMFTopo"
#define PARAPROBE_CRYSTALLO_THREESTRONGEST		"/Crystallography/ThreeStrongest"
#define PARAPROBE_CRYSTALLO_ALLPEAKS			"/Crystallography/AllPeaks"
#define PARAPROBE_CRYSTALLO_HISTCNTS			"/Crystallography/HistogramCnts"
#define PARAPROBE_CRYSTALLO_SPECIFICPEAK		"/Crystallography/SpecificPeak"
#define PARAPROBE_CRYSTALLO_FILTERED_MPID		"/Crystallography/SecondStrongestMPID"
#define PARAPROBE_CRYSTALLO_FILTERED_VAL		"/Crystallography/SecondStrongestValues"

#define PARAPROBE_DESCRSTATS					"/DescrSpatStats"
#define PARAPROBE_DESCRSTATS_RDF				"/DescrSpatStats/RDF"
#define PARAPROBE_DESCRSTATS_RDF_PDF			"/DescrSpatStats/RDF/PDF"
#define PARAPROBE_DESCRSTATS_RDF_CDF			"/DescrSpatStats/RDF/CDF"
#define PARAPROBE_DESCRSTATS_KNN				"/DescrSpatStats/KNN"
#define PARAPROBE_DESCRSTATS_NCORR				"/DescrSpatStats/TwoPointStats"
#define PARAPROBE_DESCRSTATS_NCORR_CELLCENTER	"/DescrSpatStats/TwoPointStats/BinCenterXYZ"
#define PARAPROBE_DESCRSTATS_NCORR_BINNING		"/DescrSpatStats/TwoPointStats/BinNXNYNZ"
#define PARAPROBE_DESCRSTATS_NCORR_CELLTOPO		"/DescrSpatStats/TwoPointStats/BinCenterTopo"

#define PARAPROBE_CLUST							"/Clustering"
#define PARAPROBE_CLUST_MAXSEP					"/Clustering/MaximumSeparation"
#define PARAPROBE_CLUST_MAXSEP_SZOUT			"/Clustering/MaximumSeparation/PrecSizeOut"
#define PARAPROBE_CLUST_MAXSEP_SZINN			"/Clustering/MaximumSeparation/PrecSizeInn"
#define PARAPROBE_CLUST_MAXSEP_XYZOUT			"/Clustering/MaximumSeparation/PrecXYZOut"
#define PARAPROBE_CLUST_MAXSEP_XYZINN			"/Clustering/MaximumSeparation/PrecXYZInn"
#define PARAPROBE_CLUST_MAXSEP_SZALL_CDF		"/Clustering/MaximumSeparation/PrecSizeAllCDF"
#define PARAPROBE_CLUST_MAXSEP_SZINN_CDF		"/Clustering/MaximumSeparation/PrecSizeInnCDF"


//##MK::so far Voronoi tessellation is generated in exclusive file as it might get extremely large
#define PARAPROBE_VOLTESS						"/VoronoiTess"
#define PARAPROBE_VOLTESS_DESCRSTATS			"/VoronoiTess/DescrStats"
#define PARAPROBE_VOLTESS_DESCRSTATS_NCELLS		"/VoronoiTess/DescrStats/NumberOfCells"
#define PARAPROBE_VOLTESS_DESCRSTATS_I2CELL		"/VoronoiTess/DescrStats/Ion2CellMappingValues"
#define PARAPROBE_VOLTESS_DESCRSTATS_I2TYPE		"/VoronoiTess/DescrStats/Ion2Iontype"
#define PARAPROBE_VOLTESS_DESCRSTATS_CELLPOS	"/VoronoiTess/DescrStats/CellPositions"
#define PARAPROBE_VOLTESS_DESCRSTATS_VOL		"/VoronoiTess/DescrStats/VolumeValues"
#define PARAPROBE_VOLTESS_DESCRSTATS_THREADID	"/VoronoiTess/DescrStats/ThreadID"
#define PARAPROBE_VOLTESS_DESCRSTATS_NFACES		"/VoronoiTess/DescrStats/NumberOfFacesValues"
#define PARAPROBE_VOLTESS_DESCRSTATS_BND		"/VoronoiTess/DescrStats/BoxContact"
#define PARAPROBE_VOLTESS_DESCRSTATS_NFTOTAL	"/VoronoiTess/DescrStats/NumberOfFacetsTotal"

#define PARAPROBE_VOLTESS_CELLS					"/VoronoiTess/CellGeometry"
#define PARAPROBE_VOLTESS_CELLS_TOPOLOGY		"/VoronoiTess/CellGeometry/XDMFTopologyValues"
#define PARAPROBE_VOLTESS_CELLS_GEOMETRY		"/VoronoiTess/CellGeometry/XDMFxyzValues"
#define PARAPROBE_VOLTESS_CELLS_THREADIDATTR	"/VoronoiTess/CellGeometry/XDMFThreadID"

#define PARAPROBE_DEBUG							"/Debugging"
#define PARAPROBE_DEBUG_APTOIM					"/Debugging/APTOIM"
#define PARAPROBE_DEBUG_APTOIM_TOPO				"/Debugging/APTOIM/Positions"
#define PARAPROBE_DEBUG_APTOIM_XYZ				"/Debugging/APTOIM/XYZ"
#define PARAPROBE_DEBUG_APTOIM_GID				"/Debugging/APTOIM/GrainID"

#define PARAPROBE_APTOIM_S2GRID					"/Crystallography/Indexing/S2Grid"
#define PARAPROBE_APTOIM_SO3GRID				"/Crystallography/Indexing/SO3Grid"
#define PARAPROBE_APTOIM_TEMPLATES				"/Crystallography/MasterTemplates/"
#define PARAPROBE_APTOIM_TEMPLATES_DEBUG		"/Crystallography/SpecificPeak/"

#define PARAPROBE_APTOIM_INDEXING				"/Indexing"
#define PARAPROBE_APTOIM_S2GRID_MAPPING			"/Indexing/S2MappingArrayIndices"
#define PARAPROBE_APTOIM_S2GRID_ACCURACY		"/Indexing/S2MappingGreatCircleDistances"
#define PARAPROBE_APTOIM_INDEXING_SOLUTION		"/Indexing/Solutions"
#define PARAPROBE_APTOIM_INDEXING_S2GRID		"/Crystallography/S2GridElevAzim" //##MK::redundant..


//##MK::debugging and verification that the FE mesh rotation routines are doing what they should
//##MK::on the one hand for the initial rotation of the reference template back into default identity configuration...
#define PARAPROBE_APTOIM_DEBUG					"/Verification"
#define PARAPROBE_APTOIM_DEBUG_REVERSE_TOPO		"/Verification/RevRotateRefXDMFTopo"
#define PARAPROBE_APTOIM_DEBUG_REVERSE_XYZ		"/Verification/RevRotateRefXDMFXYZ"
#define PARAPROBE_APTOIM_DEBUG_REVERSE_SINT		"/Verification/RevRotateRefS2Int"
//##MK::on the other hand for the forward rotation of the FE points with peak and valleys into the candidate orientations...
#define PARAPROBE_APTOIM_DEBUG_CANDORI_TOPO		"/Verification/CandoriRotXDMFTopo"
#define PARAPROBE_APTOIM_DEBUG_CANDORI_XYZ		"/Verification/CandoriRotXDMFXYZ"
#define PARAPROBE_APTOIM_DEBUG_CANDORI_SINT		"/Verification/CandoriRotS2Int"

//##MK::debugging Francois Vurpillots suggestion to do a direct Fourier transform of the lattice
#define PARAPROBE_APTOIM_DEBUG_FT				"/FourierTransform"
#define PARAPROBE_APTOIM_DEBUG_FT_TOPO			"/FourierTransform/HKLGridXDMFTopo"
#define PARAPROBE_APTOIM_DEBUG_FT_XYZ			"/FourierTransform/HKLGridXDMFXYZ"
#define PARAPROBE_APTOIM_DEBUG_FT_REALPART		"/FourierTransform/HKLGridRealPart"
#define PARAPROBE_APTOIM_DEBUG_FT_IMAGPART		"/FourierTransform/HKLGridImagPart"
#define PARAPROBE_APTOIM_DEBUG_FT_MAGNITUDE		"/FourierTransform/HKLGridMagnitude"


#define TAPSIM_ION_TOPO							"/PositionTopo"
#define TAPSIM_SYNTH_XYZ						"/SynthXYZ"
#define TAPSIM_RECON_XYZ						"/ReconXYZ"
#define TAPSIM_ONE_TO_ONE_DXYZ					"/ReconVsSynthXYZ"
#define TAPSIM_ONE_TO_ONE_DIST					"/ReconVsSynthDist"
*/


//#ifdef UTILIZE_HDF5
	#include "hdf5.h"

	void debug_hdf5( void );

	//void reconstruction_read_hdf5( vector<vector<p3d>*> ppp, vector<vector<apt_real>* >)

	/*
	void write_pos_hdf5( vector<vector<pos>*> & in, const string h5_io_fn ); //##MK::add group names
	*/

	//dummy values for filling chunk buffers
	#define HDF5_U32LE_DUMMYVALUE				1 //UINT32MX //##MK::should be 1 if used for topology
	#define HDF5_F64LE_DUMMYVALUE				0.0

	//return codes for wrapped H5 functions
	#define WRAPPED_HDF5_SUCCESS				+1 //MK::following the HDF5 convention that error values are positiv in case of success or negative else
	#define WRAPPED_HDF5_ALLOCERR				-1
	#define WRAPPED_HDF5_OUTOFBOUNDS			-2
	#define WRAPPED_HDF5_ARGINCONSISTENT		-3
	#define WRAPPED_HDF5_EXECUTIONORDERISSUE	-4
	#define WRAPPED_HDF5_INCORRECTLOGIC			-5
	#define WRAPPED_HDF5_INCORRECTDIMENSIONS	-6
	#define WRAPPED_HDF5_INCORRECTOFFSETS		-7
	#define WRAPPED_HDF5_FAILED					-8
	#define WRAPPED_HDF5_RBUFALLOC_FAILED		-9
	#define WRAPPED_HDF5_READFAILED				-10
	#define WRAPPED_HDF5_DOESNOTEXIST			-11

	struct io_bounds
	{
		size_t n;
		size_t s;
		size_t e;
		io_bounds() : n(0), s(0), e(0) {}
		io_bounds( const size_t _n, const size_t _s, const size_t _e ) :
			n(_n), s(_s), e(_e) {}
	};

	struct voro_io_bounds
	{
		//assists coordinates writing of tessellation portions from individual threads
		//by defining the array bounds between which data from the individual threads are placed
		size_t cell_n;
		size_t cell_s; //defining [cell_s, cell_e)
		size_t cell_e;
		size_t topo_n;
		size_t topo_s;
		size_t topo_e;
		size_t geom_n;
		size_t geom_s;
		size_t geom_e;
		voro_io_bounds() : 	cell_n(0), cell_s(0), cell_e(0),
							topo_n(0), topo_s(0), topo_e(0),
							geom_n(0), geom_s(0), geom_e(0) {}
		voro_io_bounds( const size_t _cn, const size_t _cs, const size_t _ce,
						const size_t _tn, const size_t _ts, const size_t _te,
						const size_t _gn, const size_t _gs, const size_t _ge ) :
							cell_n(_cn), cell_s(_cs), cell_e(_ce),
							topo_n(_tn), topo_s(_ts), topo_e(_te),
							geom_n(_gn), geom_s(_gs), geom_e(_ge) {}
	};


	struct voro_io_info
	{
		size_t id;
		size_t nfacets;
		size_t ntopo;
		size_t ngeom;
		voro_io_info() : id(0), nfacets(0), ntopo(0), ngeom(0) {}
		voro_io_info(const size_t _id, const size_t _nf,
				const size_t _nt,	const size_t _ng) :
					id(_id), nfacets(_nf), ntopo(_nt), ngeom(_ng) {}
	};

	struct h5iometa
	{
		string h5fn;
		string dsetnm;
		size_t nr;
		size_t nc;
		h5iometa() : h5fn(""), dsetnm(""), nr(0), nc(0) {}
		h5iometa( const string _dn, const size_t _nr, const size_t _nc ) :
			h5fn(""), dsetnm(_dn), nr(_nr), nc(_nc) {}
		h5iometa( const string _fn, const string _dn, const size_t _nr, const size_t _nc ):
			h5fn(_fn), dsetnm(_dn), nr(_nr), nc(_nc) {}
	};

	ostream& operator<<(ostream& in, h5iometa const & val);


	struct h5dims
	{
		size_t nr;
		size_t nc;
		h5dims() : nr(0), nc(0) {}
		h5dims(const size_t _nr, const size_t _nc) : nr(_nr), nc(_nc) {}
	};


	struct h5offsets
	{
		size_t nr0;			//where to start reading writing off from row ID C indexing, i.e. inclusive bound
		size_t nr1;			//one past where to stop reading writing off from, i.e. exclusive bound
		size_t nc0;			//where to start reading writing off from col ID C indexing
		size_t nc1;
		size_t nrmax;		//maximum dimensions
		size_t ncmax;
		h5offsets() : nr0(-1), nr1(-1), nc0(-1), nc1(-1), nrmax(0), ncmax(0) {}
		h5offsets( const size_t _nr0, const size_t _nr1, const size_t _nc0, const size_t _nc1,
				const size_t _nrmx, const size_t _ncmx ) :
			nr0(_nr0), nr1(_nr1), nc0(_nc0), nc1(_nc1), nrmax(_nrmx), ncmax(_ncmx) {}
		bool is_within_bounds( const size_t _nr0, const size_t _nr1, const size_t _nc0, const size_t _nc1 );
		size_t nrows();
		size_t ncols();
		//size_t interval_length( const size_t _n0, const size_t _n1 );
	};

	ostream& operator<<(ostream& in, h5offsets const & val);


	class h5Hdl
	{
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls
	public:
		h5Hdl();
		~h5Hdl();

		//file generation and closing
		void reinitialize();
		int create_file( const string h5fn );

		/*
		int create_paraprobe_apth5( const string h5fn );
		int create_paraprobe_results_file( const string h5fn, const int rank );
		int create_paraprobe_clust_file( const string h5fn, const int rank );
		int create_paraprobe_cryst_file( const string h5fn, const int rank );
		//int create_paraprobe_voronoi_file( const string h5fn, const int rank );
		int create_paraprobe_oimindexing_file( const string h5fn, const int rank );
		int create_paraprobe_debug_file( const string h5fn, const int rank );
		int create_tapsim_report_file( const string h5fn );
		*/

		//query dataset dimensions
		int query_contiguous_matrix_dims( const string _dn, h5offsets & offsetinfo );


		//group generation
		int create_group( const string grpnm );
		int create_group( const string h5fn, const string grpnm );
		bool probe_group_existence( const string grpnm );

		//contiguous dataset generation and fill in for subsequent hyperslab writing
		int create_contiguous_matrix_u8le( h5iometa const & h5info );
		int create_contiguous_matrix_u16le( h5iometa const & h5info );
		int create_contiguous_matrix_u32le( h5iometa const & h5info );
		int create_contiguous_matrix_i32le( h5iometa const & h5info );
		int create_contiguous_matrix_u64le( h5iometa const & h5info );
		int create_contiguous_matrix_i16le( h5iometa const & h5info );
		int create_contiguous_matrix_f32le( h5iometa const & h5info );
		int create_contiguous_matrix_f64le( h5iometa const & h5info );

		//strings
		int write_string_ascii( const string grpnm, const string val );

		//scalars
		int create_scalar_u64le( const string h5fn, const string grpnm, const size_t val );

		//nr x nc matrices
		int init_chunked_matrix_u32le( const string h5fn, const string dsetnm, const size_t nr, const size_t nc );
		int write_chunked_matrix_u32le(	const string h5fn, const string dsetnm, vector<unsigned int> const & buffer );
		int reset_chunked_matrix_u32le_aftercompletion();

		/*	//##MK::incremental writing of threadlocal results successively into contiguous dataset does not work
		int init_contiguous_matrix_u32le( const string h5fn, const string dsetnm, const size_t nr, const size_t nc );
		int write_contiguous_matrix_u32le(	const string h5fn, const string dsetnm, vector<unsigned int> const & buffer );
		int reset_contiguous_matrix_u32le_aftercompletion();
		*/

		int write_contiguous_matrix_u8le_atonce( const string h5fn, const string dsetnm,
				const size_t nr, const size_t nc, vector<unsigned char> const & buffer );
		int write_contiguous_matrix_u32le_atonce( const string h5fn, const string dsetnm,
				const size_t nr, const size_t nc, vector<unsigned int> const & buffer );
		int write_contiguous_matrix_u64le_atonce( const string h5fn, const string dsetnm,
						const size_t nr, const size_t nc, vector<size_t> const & buffer );
		int write_contiguous_matrix_f32le_atonce( const string h5fn, const string dsetnm,
						const size_t nr, const size_t nc, vector<float> const & buffer );
		int write_contiguous_matrix_f64le_atonce( const string h5fn, const string dsetnm,
				const size_t nr, const size_t nc, vector<double> const & buffer );

		int write_contiguous_matrix_u8le_hyperslab( h5iometa const & h5info,
				h5offsets const & offsetinfo, vector<unsigned char> const & buffer );
		int write_contiguous_matrix_u16le_hyperslab( h5iometa const & h5info,
				h5offsets const & offsetinfo, vector<unsigned short> const & buffer );
		int write_contiguous_matrix_u32le_hyperslab( h5iometa const & h5info,
				h5offsets const & offsetinfo, vector<unsigned int> const & buffer );
		int write_contiguous_matrix_i32le_hyperslab( h5iometa const & h5info,
				h5offsets const & offsetinfo, vector<int> const & buffer );
		int write_contiguous_matrix_u64le_hyperslab( h5iometa const & h5info,
				h5offsets const & offsetinfo, vector<size_t> const & buffer );
		int write_contiguous_matrix_i16le_hyperslab( h5iometa const & h5info,
				h5offsets const & offsetinfo, vector<short> const & buffer );
		int write_contiguous_matrix_f32le_hyperslab( h5iometa const & h5info,
				h5offsets const & offsetinfo, vector<float> const & buffer );
		int write_contiguous_matrix_f64le_hyperslab( h5iometa const & h5info,
				h5offsets const & offsetinfo, vector<double> const & buffer );

		int read_contiguous_matrix_u8le_hyperslab( h5iometa const & h5info,
						h5offsets const & offsetinfo, vector<unsigned char> & buffer );
		int read_contiguous_matrix_u16le_hyperslab( h5iometa const & h5info,
						h5offsets const & offsetinfo, vector<unsigned short> & buffer );
		int read_contiguous_matrix_i32le_hyperslab( h5iometa const & h5info,
						h5offsets const & offsetinfo, vector<int> & buffer );
		int read_contiguous_matrix_f32le_hyperslab( h5iometa const & h5info,
				h5offsets const & offsetinfo, vector<float> & buffer );
		int read_contiguous_matrix_u32le_hyperslab( h5iometa const & h5info,
				h5offsets const & offsetinfo, vector<unsigned int> & buffer );


		int init_chunked_matrix_f64le( const string h5fn, const string dsetnm, const size_t nr, const size_t nc );
		int write_chunked_matrix_f64le( const string h5fn, const string dsetnm, vector<double> const & buffer );
		int reset_chunked_matrix_f64le_aftercompletion();

		//write data
		//we use buffer here that do not cover necessarily the entire dataset to
		//hide the entire writing of data to an HDF5 file from the higher level function call
		//how internally the data stripe [start, end) is stored chunked into only the HDF5 class object takes care of
		int write_chunked_matrix_f64le( const string dsetnm, const size_t nr, const size_t nc,
						const size_t start, const size_t end, vector<p3d> const & buffer );


		//add attributes a posteriori having written a dataset and already closed it...
		int write_attribute_string( const string dsetnm, vector<pair<string,string>> const & attr );

		//higher-level reading of agnostic PARAPROBE data objects
		/*
		bool read_reconstruction_from_apth5( const string fn, vector<p3dm1> & out );
		*/
		bool read_trianglehull_from_apth5( const string fn, vector<tri3d> & out );

		bool read_xyz_from_apth5( const string fn, vector<p3d> & out );
		bool read_iontypes_from_apth5( const string fn, vector<rangedIontype> & out );
		bool read_ityp_from_apth5( const string fn, vector<unsigned char> & out );
		bool read_m2q_from_apth5( const string fn, vector<apt_real> & out );
		bool read_dist_from_apth5( const string fn, vector<apt_real> & out );

		string h5resultsfn;

	//private:
		//hold handler for the H5 file here
		herr_t status;
		hid_t fileid;
		hid_t ftypid;
		hid_t groupid;
		hid_t mtypid;
		hid_t mspcid;
		hid_t dsetid;
		hid_t dspcid;
		hid_t cparms;
		hid_t fspcid;
		hid_t attrid;

		hsize_t dims[2];
		hsize_t maxdims[2];
		hsize_t offset[2];
		hsize_t dimsnew[2];

		hsize_t nrows;
		hsize_t ncols;
		size_t BytesPerChunk;
		size_t RowsPerChunk;
		size_t ColsPerChunk;
		size_t RowsColsPerChunk;
		size_t NChunks;
		size_t TotalWriteHere; 			//specifies how many data elements in the currently processed dset were already written successfully
		size_t TotalWritePortion;		//specifies how many data elements are to be processed in an arbitrary portion as e.g. coming from threadlocal output
		size_t TotalWriteAllUsed;
		size_t TotalWriteAllChunk;			//specifies how many data elements ultimately have to become completed


		//temporary read/write buffer
		unsigned int* u32le_buf;
		double* f64le_buf;
	};

//#endif

#endif
