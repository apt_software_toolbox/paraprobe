//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_RAPIDXMLINTERFACE_H__
#define __PARAPROBE_UTILS_RAPIDXMLINTERFACE_H__

#include "PARAPROBE_Parallelization.h"

//add thirdparty XML reader header library functionality by M. Kalicinski
#include "thirdparty/RapidXML/rapidxml.hpp"

using namespace rapidxml;


#endif
