//##MK::CODE

#include "PARAPROBE_Decompositor.h"


decompositor::decompositor()
{
	halothickness = p3d64();
	kdtree_success = false;
	healthy = true;
}


decompositor::~decompositor()
{
	//do not delete owner it is only a backreference
	for(size_t mt = MASTER; mt < db.size(); ++mt) {
		delete db.at(mt);
		db.at(mt) = NULL;
	}
}


void decompositor::tip_aabb_get_extrema( vector<p3d> const & p )
{
/*
	double tic = MPI_Wtime();
	//computes extremal axis-aligned bounding box enclosing all ion positions of p
*/
	//##MK::for threaded version see main code

	//sequential fallback
	aabb3d tipSEQ = aabb3d();
	for( auto it = p.begin(); it != p.end(); it++ ) {
		if ( it->x <= tipSEQ.xmi )	tipSEQ.xmi = it->x;
		if ( it->x >= tipSEQ.xmx )	tipSEQ.xmx = it->x;
		if ( it->y <= tipSEQ.ymi )	tipSEQ.ymi = it->y;
		if ( it->y >= tipSEQ.ymx )	tipSEQ.ymx = it->y;
		if ( it->z <= tipSEQ.zmi )	tipSEQ.zmi = it->z;
		if ( it->z >= tipSEQ.zmx )	tipSEQ.zmx = it->z;
	}

	tipSEQ.scale();

	tip = tipSEQ;

	cout << "World coordinate system tip bounding box is " << tip << "\n";

/*
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	owner->owner->tictoc.prof_elpsdtime_and_mem( "IonPositionExtrema", APT_UTL, APT_IS_SEQ, mm, tic, toc);
	cout << "TipSEQ AABB identified " << (toc-tic) << " seconds!" << endl;
*/
}


void decompositor::tip_aabb_get_extrema( vector<p3dm1> const & p )
{
/*
	double tic = MPI_Wtime();
	//computes extremal axis-aligned bounding box enclosing all ion positions of p
*/
	//##MK::for threaded version see main code

	//sequential fallback
	aabb3d tipSEQ = aabb3d();
	for( auto it = p.begin(); it != p.end(); it++ ) {
		if ( it->x <= tipSEQ.xmi )	tipSEQ.xmi = it->x;
		if ( it->x >= tipSEQ.xmx )	tipSEQ.xmx = it->x;
		if ( it->y <= tipSEQ.ymi )	tipSEQ.ymi = it->y;
		if ( it->y >= tipSEQ.ymx )	tipSEQ.ymx = it->y;
		if ( it->z <= tipSEQ.zmi )	tipSEQ.zmi = it->z;
		if ( it->z >= tipSEQ.zmx )	tipSEQ.zmx = it->z;
	}

	tipSEQ.scale();

	tip = tipSEQ;

	cout << "World coordinate system tip bounding box is " << tip << "\n";

/*
	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	owner->owner->tictoc.prof_elpsdtime_and_mem( "IonPositionExtrema", APT_UTL, APT_IS_SEQ, mm, tic, toc);
	cout << "TipSEQ AABB identified " << (toc-tic) << " seconds!" << endl;
*/
}


void decompositor::loadpartitioning_subkdtree_per_ityp( vector<p3d> const & p, vector<apt_real> const & d,
		vector<unsigned char> const & lblorg, vector<unsigned char> const & lblrnd, const unsigned char ityp_max )
{
	//into how many snippets approximately to decompose?
	unsigned int PARAPROBE_NUM_THREADS = static_cast<unsigned int>(omp_get_max_threads());
	//cout << "PARAPROBE_NUM_THREADS = " << PARAPROBE_NUM_THREADS << "\n";
	cout << "Analyzing point spatial distribution in z for load balancing..." << "\n";

	//read z coordinates based on which we spatially decompose
	//##MK::for larger datasets (>1.0e9 ions) the effort to sort the list zcoordinates is O(NlogN) may be too way to high, than better random sampling to get approximate quantile locations in z
	//however APT datasets with strong heterogeneity, should always be fully probed, i.e. no random probing

	vector<apt_real> zcoordinates;
	zcoordinates.reserve( p.size() );
	for( auto it = p.begin(); it != p.end(); it++ ) {
		zcoordinates.push_back( it->z ); //we partition along the zdirection
	}

	//cout << zcoordinates.size() << " z-coordinate values extracted, now sorting them..." << "\n";

	//full sort ascendingly of zcoordinates, speed up by sampling each n-th only
	sort(zcoordinates.begin(), zcoordinates.end() );

	//when there is a z-stack of N point cloud portion/sub-domains we have to split N-1 times
	vector<apt_real> qqss;
	for(unsigned int tid = 1; tid < PARAPROBE_NUM_THREADS; tid++ ) {
		apt_real thisq = static_cast<apt_real>(tid) / static_cast<apt_real>(PARAPROBE_NUM_THREADS);
		qqss.push_back( thisq );
		//cout << "Thread " << tid << " quantile z value " << qqss.back() << "\n";
	}

	vector<apt_real> qq = quantiles_nosort( zcoordinates, qqss );
	/*
	for(unsigned int i = 0; i < qq.size(); ++i) {
		cout << "Loadbalancing zmx via z-coordinate quantiles for thread " << i << "\t\t" << qq.at(i) << "\n";
	}
	*/

	//##MK::inject code for volume tessellation

	//define how, for a given number of threads in the pool, the point cloud should be distributed across the threads
	//MK::but we do not yet allocate
	for(size_t mt = MASTER; mt < PARAPROBE_NUM_THREADS; mt++) {
		db.push_back(NULL);
		spatialsplits.push_back( p6d64() );
	}

	#pragma omp parallel shared(qq,qqss, zcoordinates) //shared but only read
	{
		unsigned int nt = static_cast<unsigned int>(omp_get_num_threads()); //##MK::what is the default behavior of parallel region start with all?
		unsigned int mt = static_cast<unsigned int>(omp_get_thread_num());

		p6d64 mii = p6d64();
		mii.zmi = (mt == MASTER) ? (static_cast<apt_real>(zcoordinates.at(0)) - static_cast<apt_real>(AABBINCLUSION_EPSILON)) : qq.at(mt-1);
		mii.zmx = (mt == (nt-1)) ? (static_cast<apt_real>(zcoordinates.back()) + static_cast<apt_real>(AABBINCLUSION_EPSILON)) : qq.at(mt);
		//epsilon environment expansion to catch points exactly on boundary
		//##MK::thereby formally thread local domains at least formally slightly overlap
		//but cells to ion ID mapping is disjoint hence even potential extrem cases double computed VC cells
		//cells are always referred to only once in postprocessable of tessellation result

		bool last = (mt == (nt-1)) ? true : false;

		//threads populate individually without collision their point cloud parts
		#pragma omp critical
		{
			spatialsplits.at(mt) = mii;
			/*
				cout << "Thread " << omp_get_thread_num() << " working on ["
						<< spatialsplits.at(mt).zmi << ";" << spatialsplits.at(mt).zmx << ") is last? " << last << "\n";
			*/
		}

		//MK::points belong to mt if their z position in [zmii, zmxx) for which we probe as follows left boundary z < zmii ? not included : (z < zmxx) ? included : not included

		//thread-local allocation of pinned memory to improve locality
		threadmemory* memlocal = NULL;
		try {
			memlocal = new threadmemory; //allocate surplus first-touch

			p3d64 miihalo = halothickness;
			memlocal->init_localmemory( mii, miihalo, last );

			memlocal->init_localions_subkdtree_per_ityp( p, d, lblorg, lblrnd );

			memlocal->init_localkdtree_subkdtree_per_ityp( ityp_max );

			#pragma omp critical
			{
				db.at(mt) = memlocal;
			}
		}
		catch (bad_alloc &ompexec) {
			cerr << "Thread " << mt << " unable to allocate memory region!" << "\n";
			//##MK::error management through healthy variable
		}
	}
}


/*
void decompositor::loadpartitioning( vector<p3d> const & p )
{
    //into how many snippets approximately to decompose?
	unsigned int PARAPROBE_NUM_THREADS = static_cast<unsigned int>(omp_get_max_threads());
	//cout << "PARAPROBE_NUM_THREADS = " << PARAPROBE_NUM_THREADS << "\n";
cout << "Analyzing point spatial distribution in z for load balancing..." << "\n";

	//read z coordinates based on which we spatially decompose
	//##MK::for larger datasets (>1.0e9 ions) the effort to sort the list zcoordinates is O(NlogN) may be too way to high, than better random sampling to get approximate quantile locations in z
	//however APT datasets with strong heterogeneity, should always be fully probed, i.e. no random probing

	vector<apt_real> zcoordinates;
	zcoordinates.reserve( p.size() );
	for( auto it = p.begin(); it != p.end(); it++ ) {
		zcoordinates.push_back( it->z ); //we partition along the zdirection
	}

	//cout << zcoordinates.size() << " z-coordinate values extracted, now sorting them..." << "\n";

	//full sort ascendingly of zcoordinates, speed up by sampling each n-th only
	sort(zcoordinates.begin(), zcoordinates.end() );

	//when there is a z-stack of N point cloud portion/sub-domains we have to split N-1 times
	vector<apt_real> qqss;
	for(unsigned int tid = 1; tid < PARAPROBE_NUM_THREADS; tid++ ) {
		apt_real thisq = static_cast<apt_real>(tid) / static_cast<apt_real>(PARAPROBE_NUM_THREADS);
		qqss.push_back( thisq );
		//cout << "Thread " << tid << " quantile z value " << qqss.back() << "\n";
	}

	vector<apt_real> qq = quantiles_nosort( zcoordinates, qqss );
//	for(unsigned int i = 0; i < qq.size(); ++i) {
//		cout << "Loadbalancing zmx via z-coordinate quantiles for thread " << i << "\t\t" << qq.at(i) << "\n";
//	}


//	//##MK::we dont do volume tessellation now
//	if ( Settings::VolumeTessellation != E_NOTESS ) {
//		//compute additional bounds when any tessellation is desired for which eventually halo regions are required
//		apt_real lambda = static_cast<apt_real>(zcoordinates.size()) / (tip.xsz*tip.ysz*tip.zsz);
//		//z guard is five times the average distance between points
//		halothickness.x = 0.f; 	halothickness.y = 0.f;
//		apt_real halowidth = Settings::TessellationGuardWidth * pow( (-1.f * log(0.5) * 3.f/(4.f*MYPI*lambda)), (1.f/3.f) );
//		if ( PARAPROBE_NUM_THREADS > 1 ) { //halo in case of one thread only is only AABBINCLUSION_EPSILON wide to prevent leakage of ions at boundary
//			halothickness.z = halowidth;
//		}
//		else { //SINGLETHREADED
//			halothickness.z = static_cast<apt_real>(AABBINCLUSION_EPSILON);
//		}
//		cout << "Threaded " << omp_get_thread_num() << " tessellating halothickness " << halothickness.z << endl;
//	}

	//define how, for a given number of threads in the pool, the point cloud should be distributed across the threads
	//MK::but we do not yet allocate
	for(size_t mt = MASTER; mt < PARAPROBE_NUM_THREADS; mt++) {
		db.push_back(NULL);
		spatialsplits.push_back( p6d64() );
	}

	#pragma omp parallel shared(qq,qqss, zcoordinates) //shared but only read
	{
		unsigned int nt = static_cast<unsigned int>(omp_get_num_threads()); //##MK::what is the default behavior of parallel region start with all?
		unsigned int mt = static_cast<unsigned int>(omp_get_thread_num());

		p6d64 mii = p6d64();
		mii.zmi = (mt == MASTER) ? (static_cast<apt_real>(zcoordinates.at(0)) - static_cast<apt_real>(AABBINCLUSION_EPSILON)) : qq.at(mt-1);
		mii.zmx = (mt == (nt-1)) ? (static_cast<apt_real>(zcoordinates.back()) + static_cast<apt_real>(AABBINCLUSION_EPSILON)) : qq.at(mt);
		//epsilon environment expansion to catch points exactly on boundary
		//##MK::thereby formally thread local domains at least formally slightly overlap
		//but cells to ion ID mapping is disjoint hence even potential extrem cases double computed VC cells
		//cells are always referred to only once in postprocessable of tessellation result

		bool last = (mt == (nt-1)) ? true : false;

		//threads populate individually without collision their point cloud parts
		#pragma omp critical
		{
			spatialsplits.at(mt) = mii;
//			cout << "Thread " << omp_get_thread_num() << " working on ["
//					<< spatialsplits.at(mt).zmi << ";" << spatialsplits.at(mt).zmx << ") is last? " << last << "\n";
		}

		//MK::points belong to mt if their z position in [zmii, zmxx) for which we probe as follows left boundary z < zmii ? not included : (z < zmxx) ? included : not included

		//thread-local allocation of pinned memory to improve locality
		threadmemory* memlocal = NULL;
		try {
			memlocal = new threadmemory; //allocate surplus first-touch

			p3d64 miihalo = halothickness;
			memlocal->init_localmemory( mii, miihalo, last );

			memlocal->init_localions( p );

			memlocal->init_localkdtree();

			#pragma omp critical
			{
				db.at(mt) = memlocal;
			}
		}
		catch (bad_alloc &ompexec) {
			cerr << "Thread " << mt << " unable to allocate memory region!" << "\n";
			//##MK::error management through healthy variable
		}
	}

//	double toc = MPI_Wtime();
//	memsnapshot mm = owner->owner->tictoc.get_memoryconsumption();
//	owner->owner->tictoc.prof_elpsdtime_and_mem( "Loadpartitioning", APT_BVH, APT_IS_PAR, mm, tic, toc);
//	cout << "Decompositor computed a balanced loadpartitioning along Z in " << (toc-tic) << " seconds" << endl;
}
*/


void decompositor::loadpartitioning( vector<p3dm1> const & p )
{
    //into how many snippets approximately to decompose?
	unsigned int PARAPROBE_NUM_THREADS = static_cast<unsigned int>(omp_get_max_threads());	
	//cout << "PARAPROBE_NUM_THREADS = " << PARAPROBE_NUM_THREADS << "\n";
cout << "Analyzing point spatial distribution in z for load balancing..." << "\n";

	//read z coordinates based on which we spatially decompose
	//##MK::for larger datasets (>1.0e9 ions) the effort to sort the list zcoordinates is O(NlogN) may be too way to high, than better random sampling to get approximate quantile locations in z
	//however APT datasets with strong heterogeneity, should always be fully probed, i.e. no random probing

	vector<apt_real> zcoordinates;
	zcoordinates.reserve( p.size() );
	for( auto it = p.begin(); it != p.end(); it++ ) {
		zcoordinates.push_back( it->z ); //we partition along the zdirection
	}

	//cout << zcoordinates.size() << " z-coordinate values extracted, now sorting them..." << "\n";

	//full sort ascendingly of zcoordinates, speed up by sampling each n-th only
	sort(zcoordinates.begin(), zcoordinates.end() );

	//when there is a z-stack of N point cloud portion/sub-domains we have to split N-1 times
	vector<apt_real> qqss;
	for(unsigned int tid = 1; tid < PARAPROBE_NUM_THREADS; tid++ ) {
		apt_real thisq = static_cast<apt_real>(tid) / static_cast<apt_real>(PARAPROBE_NUM_THREADS);
		qqss.push_back( thisq );
		//cout << "Thread " << tid << " quantile z value " << qqss.back() << "\n";
	}

	vector<apt_real> qq = quantiles_nosort( zcoordinates, qqss );
	/*
	for(unsigned int i = 0; i < qq.size(); ++i) {
		cout << "Loadbalancing zmx via z-coordinate quantiles for thread " << i << "\t\t" << qq.at(i) << "\n";
	}
	*/

/*
	//##MK::we dont do volume tessellation now
	if ( Settings::VolumeTessellation != E_NOTESS ) {
		//compute additional bounds when any tessellation is desired for which eventually halo regions are required
		apt_real lambda = static_cast<apt_real>(zcoordinates.size()) / (tip.xsz*tip.ysz*tip.zsz);
		//z guard is five times the average distance between points
		halothickness.x = 0.f; 	halothickness.y = 0.f;
		apt_real halowidth = Settings::TessellationGuardWidth * pow( (-1.f * log(0.5) * 3.f/(4.f*MYPI*lambda)), (1.f/3.f) );
		if ( PARAPROBE_NUM_THREADS > 1 ) { //halo in case of one thread only is only AABBINCLUSION_EPSILON wide to prevent leakage of ions at boundary
			halothickness.z = halowidth;
		}
		else { //SINGLETHREADED
			halothickness.z = static_cast<apt_real>(AABBINCLUSION_EPSILON);
		}
		cout << "Threaded " << omp_get_thread_num() << " tessellating halothickness " << halothickness.z << endl;
	}
*/

	//define how, for a given number of threads in the pool, the point cloud should be distributed across the threads
	//MK::but we do not yet allocate
	for(size_t mt = MASTER; mt < PARAPROBE_NUM_THREADS; mt++) {
		db.push_back(NULL);
		spatialsplits.push_back( p6d64() );
	}

	#pragma omp parallel shared(qq,qqss, zcoordinates) //shared but only read
	{
		unsigned int nt = static_cast<unsigned int>(omp_get_num_threads()); //##MK::what is the default behavior of parallel region start with all?
		unsigned int mt = static_cast<unsigned int>(omp_get_thread_num());

		p6d64 mii = p6d64();
		mii.zmi = (mt == MASTER) ? (static_cast<apt_real>(zcoordinates.at(0)) - static_cast<apt_real>(AABBINCLUSION_EPSILON)) : qq.at(mt-1);
		mii.zmx = (mt == (nt-1)) ? (static_cast<apt_real>(zcoordinates.back()) + static_cast<apt_real>(AABBINCLUSION_EPSILON)) : qq.at(mt);
		//epsilon environment expansion to catch points exactly on boundary
		//##MK::thereby formally thread local domains at least formally slightly overlap
		//but cells to ion ID mapping is disjoint hence even potential extrem cases double computed VC cells
		//cells are always referred to only once in postprocessable of tessellation result

		bool last = (mt == (nt-1)) ? true : false;

		//threads populate individually without collision their point cloud parts
		#pragma omp critical
		{
			spatialsplits.at(mt) = mii;
			/*
			cout << "Thread " << omp_get_thread_num() << " working on ["
					<< spatialsplits.at(mt).zmi << ";" << spatialsplits.at(mt).zmx << ") is last? " << last << "\n";
			*/
		}

		//MK::points belong to mt if their z position in [zmii, zmxx) for which we probe as follows left boundary z < zmii ? not included : (z < zmxx) ? included : not included

		//thread-local allocation of pinned memory to improve locality
		threadmemory* memlocal = NULL;
		try {
			memlocal = new threadmemory; //allocate surplus first-touch

			p3d64 miihalo = halothickness;
			memlocal->init_localmemory( mii, miihalo, last );

			memlocal->init_localions( p );

			memlocal->init_localkdtree();

			#pragma omp critical
			{
				db.at(mt) = memlocal;
			}
		}
		catch (bad_alloc &ompexec) {
			cerr << "Thread " << mt << " unable to allocate memory region!" << "\n";
			//##MK::error management through healthy variable
		}
	}

/*
	double toc = MPI_Wtime();
	memsnapshot mm = owner->owner->tictoc.get_memoryconsumption();
	owner->owner->tictoc.prof_elpsdtime_and_mem( "Loadpartitioning", APT_BVH, APT_IS_PAR, mm, tic, toc);
	cout << "Decompositor computed a balanced loadpartitioning along Z in " << (toc-tic) << " seconds" << endl;
*/
}


/*
void decompositor::reportpartitioning()
{
	double tic, toc;
	tic = MPI_Wtime();

//report geometry and spatial partitioning of the KDTree
	string fn = "PARAPROBE.SimID." + to_string(Settings::SimID) + ".KDTreeNodesAllThreads.csv";
	ofstream plog;
	plog.open( fn.c_str() );

	//plog << "ThreadID;KDTreeNodeID;XMin;YMin;ZMin;XMax;YMax;ZMax;NodeID;SplitDimXYZ012\n";
	plog << "ThreadID;KDTreeNodeID;XMin;XMax;YMin;YMax;ZMin;ZMax;NodeID;SplitDimXYZ012\n"; //more human intuitive layout when splitting boxes along dimensions
	unsigned int nt = db.size();
	for ( unsigned int mt = 0; mt < nt; mt++ ) {
		threadmemory* threadinfo = db.at(mt);
		kd_tree* thistree = threadinfo->threadtree;

		vector<scan_task> boxes;
		thistree->get_allboundingboxes( boxes );

		size_t n = boxes.size();

		for( size_t i = 0; i < n; ++i ) {
			scan_task& t = boxes.at(i);

			plog << mt << ";" << i << ";";
			//plog << boxes[i].bx.min.x << ";" << boxes[i].bx.min.y << ";" << boxes[i].bx.min.z << ";" << boxes[i].bx.max.x << ";" << boxes[i].bx.max.y << ";" << boxes[i].bx.max.z << ";";
			plog << boxes[i].bx.min.x << ";" << boxes[i].bx.max.x << ";" << boxes[i].bx.min.y << ";" << boxes[i].bx.max.y << ";" << boxes[i].bx.min.z << ";" << boxes[i].bx.max.z << ";"; //more human intuitive layout
			plog << t.node << ";" << t.dim << "\n";
		}
	}
	plog.flush();
	plog.close();


//report geometry of threads and meta data
	fn = "PARAPROBE.SimID." + to_string(Settings::SimID) + ".SpatialDistributionAllThreads.csv";
	ofstream tlog;
	tlog.open( fn.c_str() );

	tlog << "ThreadID;ZMin;ZMax;NIons;KDTreeNodes;MemoryConsumption\n"; //more human intuitive layout when splitting boxes along dimensions
	tlog << ";nm;nm;1;1;Bytes\n";
	tlog << "ThreadID;ZMin;ZMax;NIons;KDTreeNodes;MemoryConsumption\n";
	for ( unsigned int mt = 0; mt < nt; mt++ ) {
		threadmemory* threadinfo = db.at(mt);

		tlog << mt << ";" << threadinfo->get_zmi() << ";" << threadinfo->get_zmx() << ";" << threadinfo->ionpp3_kdtree.size() << ";";
		tlog << threadinfo->threadtree->nodes.size() << ";" << threadinfo->get_memory_consumption() << "\n";
	}

	tlog.flush();
	tlog.close();

	toc = MPI_Wtime();
	cout << "Wrote spatial partitioning to file in " << (toc-tic) << " seconds" << endl;
}
*/
