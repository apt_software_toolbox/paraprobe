//##MK::GPLV3

#include "PARAPROBE_VolumeSampler.h"

ostream& operator << (ostream& in, mp3d const & val) {
	in << val.mpid << ";" << val.x << ";" << val.y << ";" << val.z << val.R << ";" << val.nions << "\n";
	return in;
}

volsampler::volsampler()
{
	window = aabb3d();
}


volsampler::~volsampler()
{
}


void volsampler::define_probe_volume( aabb3d const & wdw )
{
	window = wdw;
	//##MK::implement cuboidal sectioning of the volume here
}


void volsampler::define_regular_grid( const apt_real dx, const apt_real dy, const apt_real dz, const apt_real R )
{
	//define a grid with respect to window
	//MK::grid is defined symmetric about center of the window
	cout << "Window is " << window << "\n";

	cuboidgrid3d w = cuboidgrid3d();
	w.ve = window;
	w.ve.scale();

	w.binwidths = p3d( dx, dy, dz );
	w.gridcells = p6i(
			static_cast<int>(floor((w.ve.xmi - window.center().x) / w.binwidths.x)),
			static_cast<int>(ceil((w.ve.xmx - window.center().x) / w.binwidths.x)),
			static_cast<int>(floor((w.ve.ymi - window.center().y) / w.binwidths.y)),
			static_cast<int>(ceil((w.ve.ymx - window.center().y) / w.binwidths.y)),
			static_cast<int>(floor((w.ve.zmi - window.center().z) / w.binwidths.z)),
			static_cast<int>(ceil((w.ve.zmx - window.center().z) / w.binwidths.z))     );

	cout << "The suggested sampling point grid is " << w.gridcells << "\n";
	cout << "The chosen ve is " << w.ve << "\n";
	cout << "The chosen binwidths are " << w.binwidths << "\n";

	if ( w.ngridcells() >= static_cast<size_t>(UINT32MX-1) ) {
		cerr << "The proposed grid will have too many points to be handled with the current implementation!" << "\n"; return;
	}
	unsigned int GlobalMatPointID = 0; //MK::global, i.e. the same state for every process
	for( int z = w.gridcells.zmi; z <= w.gridcells.zmx; ++z) {
		for( int y = w.gridcells.ymi; y <= w.gridcells.ymx; ++y) {
			for( int x = w.gridcells.xmi; x <= w.gridcells.xmx; ++x) {
				//define the center coordinate of the cubic voxel of the grid
				p3d p = w.where( x, y, z);

				//is this coordinate an acceptable one?
				if ( p.x < (F32MX-EPSILON) && p.y < (F32MX-EPSILON) && p.z < (F32MX-EPSILON) ) {
					if ( p.x >= w.ve.xmi && p.x <= w.ve.xmx && p.y >= w.ve.ymi && p.y <= w.ve.ymx && p.z >= w.ve.zmi && p.z <= w.ve.zmx ) {
						//##MK::here handle relative location of ROI center to specimen and possibly discard points, aka boundary correction
						matpoints.push_back( mp3d( p.x, p.y, p.z, R, GlobalMatPointID, 0 ) ); //MK::now different inspection radii are possible
						GlobalMatPointID++;
//cout << matpoints.back() << "\n";
					}
				}
			} //next x grid point
		}
	}
	cout << "An ensemble of material points for mining was defined " << matpoints.size() << "\n";
}


void volsampler::define_single_point( p3d const & p, const apt_real R )
{
	cout << "Window is " << window << "\n";
	
	unsigned int GlobalMatPointID = 0;
	if ( p.x < (F32MX-EPSILON) && p.y < (F32MX-EPSILON) && p.z < (F32MX-EPSILON) ) {
		if ( p.x >= window.xmi && p.x <= window.xmx && p.y >= window.ymi && p.y <= window.ymx && p.z >= window.zmi && p.z <= window.zmx ) {
			//##MK::here handle relative location of ROI center to specimen and possibly discard points, aka boundary correction
			matpoints.push_back( mp3d( p.x, p.y, p.z, R, GlobalMatPointID, 0 ) ); //MK::now different inspection radii are possible
			GlobalMatPointID++;
//cout << matpoints.back() << "\n";
		}
	}
	cout << "An ensemble of material points for mining was defined " << matpoints.size() << "\n";
}


void volsampler::define_random_grid()
{
}
