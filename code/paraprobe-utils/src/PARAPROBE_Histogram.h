//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_HISTOGRAM_H__
#define __PARAPROBE_UTILS_HISTOGRAM_H__


#include "PARAPROBE_PeriodicTable.h"

struct lival
{
	apt_real min;
	apt_real incr;
	apt_real max;
	lival() : min(0.f), incr(1.f), max(0.f) {}
	lival( const apt_real _mi, const apt_real _inc, const apt_real _mx ) :
		min(_mi), incr(_inc), max(_mx) {}
};


struct linear_ival
{
	double min;
	double incr;
	double max;
	linear_ival() : min(0.0), incr(0.0), max(0.0) {}
	linear_ival( const double _mi, const double _inc, const double _mx ) :
		min(_mi), incr(_inc), max(_mx) {}
};


class histogram
{
public :
	histogram();
	histogram( linear_ival const & iv );
	~histogram();
	
	void add_dump_yes( const apt_real x );
	void normalize();

	double report( const unsigned int b );
	double left( const unsigned int b );
	double center( const unsigned int b );
	double right( const unsigned int b );

	double cnts_lowest;
	vector<double> cnts;
	double cnts_highest;

	linear_ival bounds;
	double tmp;
	unsigned int nbins;
};


#endif
