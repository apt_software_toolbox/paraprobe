//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_APTH5_H__
#define __PARAPROBE_UTILS_APTH5_H__

//based on discussions of International Field Emission Society (IFES) APT Technical committee Skype meeting 2019/04/04
//##MK::add authors


//metadata contextualizing a specific experiment performed with a specific tool aka instrument
//domain specific HDF5 keywords and data fields
#define APTH5_ACQ_00 						"/00_ExperimentContext"
#define APTH5_ACQ_00_EXPERIMENT_TYPE		"/00_ExperimentContext/ExperimentType"
#define APTH5_ACQ_00_SAMPLE_DESCR			"/00_ExperimentContext/SampleDescription"


//metadata specifying the environment in which the tool was used
#define APTH5_ACQ_01						"/01_ToolEnvironment"
#define APTH5_ACQ_01_GLOBAL_STARTDATE		"/01_ToolEnvironment/ExperimentStartDateGlobal"
#define APTH5_ACQ_01_LOCAL_STARTDATE		"/01_ToolEnvironment/ExperimentStartDateLocal"
#define APTH5_ACQ_01_GLOBAL_ENDDATE			"/01_ToolEnvironment/ExperimentEndDateGlobal"


//metadata specifying details and pieces of information about the tool
#define APTH5_ACQ_02						"/02_ToolStateAndSettings"
#define APTH5_ACQ_02_SAMPLE_NAME			"/02_ToolStateAndSettings/SampleName"
#define APTH5_ACQ_02_DETECTOR_INFO			"/02_ToolStateAndSettings/DetectorInfo"
#define APTH5_ACQ_02_DETECTOR_SIZE			"/02_ToolStateAndSettings/DetectorSize"
#define APTH5_ACQ_02_DETECTOR_GEOREAL		"/02_ToolStateAndSettings/DetectorGeometryReal"
#define APTH5_ACQ_02_DETECTOR_GEOOPTEQV		"/02_ToolStateAndSettings/DetectorGeometryOpticalEquiv"
#define APTH5_ACQ_02_INSTRUMENT_INFO		"/02_ToolStateAndSettings/InstrumentInfo"
#define APTH5_ACQ_02_FLIGHTPATH_TIMING		"/02_ToolStateAndSettings/FlightPathTiming"
#define APTH5_ACQ_02_FLIGHTPATH_SPATIAL		"/02_ToolStateAndSettings/FlightPathSpatial"
#define APTH5_ACQ_02_LASER_WAVELENGTH		"/02_ToolStateAndSettings/LaserWavelength"
#define APTH5_ACQ_02_LASER_INCIDENCE		"/02_ToolStateAndSettings/LaserIncidence"
#define APTH5_ACQ_02_PULSE_NUMBER			"/02_ToolStateAndSettings/PulseNumber"
#define APTH5_ACQ_02_TIP2RECON_MAPPING		"/02_ToolStateAndSettings/Tip2ReconSpaceMapping"
#define APTH5_ACQ_02_TIPGEOM_REAL			"/02_ToolStateAndSettings/TipGeometryReal"
#define APTH5_ACQ_02_TIPGEOM_OPTEQV			"/02_ToolStateAndSettings/TipGeometryOpticalEquiv"
#define APTH5_ACQ_02_REFLECTRON_INFO		"/02_ToolStateAndSettings/ReflectronInfo"

//measured quantities during the course of this experiment with this specific tool for this specific sample
#define APTH5_ACQ_03						"/03_ExperimentResults"
#define APTH5_ACQ_03_REFLECTRON_VOLTAGE		"/03_ExperimentResults/VoltageReflectron"
#define APTH5_ACQ_03_DETECTOR_HITPOS		"/03_ExperimentResults/DetectorHitPositions"
#define APTH5_ACQ_03_DETECTOR_HITMULTPLY	"/03_ExperimentResults/DetectorHitMultiplicity"
#define APTH5_ACQ_03_DETECTOR_DEADPULSES	"/03_ExperimentResults/DetectorDeadPulses"
#define APTH5_ACQ_03_TIME_OF_FLIGHT			"/03_ExperimentResults/TimeOfFlight"
#define APTH5_ACQ_03_STANDING_VOLTAGE		"/03_ExperimentResults/StandingVoltage"
#define APTH5_ACQ_03_PULSE_VOLTAGE			"/03_ExperimentResults/PulseVoltage"
#define APTH5_ACQ_03_TIP_TEMPERATURE		"/03_ExperimentResults/TipTemperature"
#define APTH5_ACQ_03_PULSE_FREQUENCY		"/03_ExperimentResults/PulseFrequency"
#define APTH5_ACQ_03_LASER_PULSE_ENERGY		"/03_ExperimentResults/LaserPulseEnergy"
#define APTH5_ACQ_03_LASER_POSITION			"/03_ExperimentResults/LaserPosition"
#define APTH5_ACQ_03_STAGE_POSITION			"/03_ExperimentResults/StagePosition"
#define APTH5_ACQ_03_FINALIZATION_STATUS	"/03_ExperimentResults/ExperimentFinalizationStatus"

//Finalization statii
#define APTH5_ACQ_03_FINALIZATION_ASDESIRED_SUCCESS				"+1"	//experiment was successful or had only acceptable negligible glitches
#define APTH5_ACQ_03_FINALIZATION_PREMATURE_FRACTURE			"-1"	//undesired fracture
#define APTH5_ACQ_03_FINALIZATION_PREMATURE_TOOLERROR			"-2"	//undesired halt or stop because of tool error
#define APTH5_ACQ_03_FINALIZATION_PREMATURE_USERTERMINATED		"-3"	//willing user interruption


#endif

