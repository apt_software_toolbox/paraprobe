//##MK::CODE

//specify here which specific results this tool writes into H5 files

#ifndef __PARAPROBE_FOURIER_METADATA_DEFS_H__
#define __PARAPROBE_FOURIER_METADATA_DEFS_H__


#define PARAPROBE_FOURIER								"/DirectFT"

#define PARAPROBE_FOURIER_META							"/DirectFT/Metadata"
#define PARAPROBE_FOURIER_META_MP						"/DirectFT/Metadata/MatPoints"
#define PARAPROBE_FOURIER_META_MP_XYZ					"/DirectFT/Metadata/MatPoints/XYZ"					//3x float
#define PARAPROBE_FOURIER_META_MP_XYZ_NCMAX				3
#define PARAPROBE_FOURIER_META_MP_TOPO					"/DirectFT/Metadata/MatPoints/Topo"					//1x unsigned int
#define PARAPROBE_FOURIER_META_MP_TOPO_NCMAX			1
#define PARAPROBE_FOURIER_META_MP_ID					"/DirectFT/Metadata/MatPoints/MatPointIDs"			//1x unsigned int
#define PARAPROBE_FOURIER_META_MP_ID_NCMAX				1
#define PARAPROBE_FOURIER_META_MP_NIONS					"/DirectFT/Metadata/MatPoints/NumberOfIons"			//1x unsigned int
#define PARAPROBE_FOURIER_META_MP_NIONS_NCMAX			1

#define PARAPROBE_FOURIER_META_R						"/DirectFT/Metadata/ROIRadius"						//1x float
#define PARAPROBE_FOURIER_META_R_NCMAX					1

#define PARAPROBE_FOURIER_META_HKL						"/DirectFT/Metadata/ReciprocSpace"
#define PARAPROBE_FOURIER_META_HKL_NIJK					"/DirectFT/Metadata/ReciprocSpace/Discretization"	//3x unsigned int
#define PARAPROBE_FOURIER_META_HKL_NIJK_NCMAX			3
#define PARAPROBE_FOURIER_META_HKL_IVAL					"/DirectFT/Metadata/ReciprocSpace/Positions"		//1x float ##MK::??
#define PARAPROBE_FOURIER_META_HKL_IVAL_NCMAX			1


#define PARAPROBE_FOURIER_RES							"/DirectFT/Results"
#define PARAPROBE_FOURIER_RES_HKL						"/DirectFT/Results/ReciprocSpace"
#define PARAPROBE_FOURIER_RES_HKL_MPID					"/DirectFT/Results/ReciprocSpace/MatPointIDs"		//1x unsigned int
#define PARAPROBE_FOURIER_RES_HKL_MPID_NCMAX			1
#define PARAPROBE_FOURIER_RES_HKL_MAXIJK				"/DirectFT/Results/ReciprocSpace/MaxIJK"			//1x unsigned int
#define PARAPROBE_FOURIER_RES_HKL_MAXIJK_NCMAX			1
#define PARAPROBE_FOURIER_RES_HKL_MAXVAL				"/DirectFT/Results/ReciprocSpace/MaxCnts"			//1x float
#define PARAPROBE_FOURIER_RES_HKL_MAXVAL_NCMAX			1


#endif
