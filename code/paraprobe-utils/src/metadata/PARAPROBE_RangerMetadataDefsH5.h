//##MK::CODE

//specify here which specific results this tool writes into H5 files

#ifndef __PARAPROBE_RANGER_METADATA_DEFS_H__
#define __PARAPROBE_RANGER_METADATA_DEFS_H__

#define PARAPROBE_RANGER								"/Ranging"

#define PARAPROBE_RANGER_META							"/Ranging/Metadata"
#define PARAPROBE_RANGER_META_TYPID_DICT				"/Ranging/Metadata/Iontypes"					
#define PARAPROBE_RANGER_META_TYPID_DICT_ID				"/Ranging/Metadata/Iontypes/IDs"					//1x unsigned char, [0,255] which iontype
#define PARAPROBE_RANGER_META_TYPID_DICT_ID_NCMAX		1
#define PARAPROBE_RANGER_META_TYPID_DICT_WHAT			"/Ranging/Metadata/Iontypes/What"					//evapion3, i.e. 8x unsigned char
#define PARAPROBE_RANGER_META_TYPID_DICT_WHAT_NCMAX		8
#define PARAPROBE_RANGER_META_TYPID_DICT_IVAL			"/Ranging/Metadata/Iontypes/MQIVal"					//2x float
#define PARAPROBE_RANGER_META_TYPID_DICT_IVAL_NCMAX		2
#define PARAPROBE_RANGER_META_TYPID_DICT_ASSN			"/Ranging/Metadata/Iontypes/MQ2IDs"					//1x unsigned char
//to which Iontypes/IDs do the range belong, each type can have multiple ranges!
#define PARAPROBE_RANGER_META_TYPID_DICT_ASSN_NCMAX		1

#define PARAPROBE_RANGER_RES							"/Ranging/Results"
#define PARAPROBE_RANGER_RES_TYPID						"/Ranging/Results/IontypeIDs"						//unsigned char
#define PARAPROBE_RANGER_RES_TYPID_NCMAX				1

#endif
