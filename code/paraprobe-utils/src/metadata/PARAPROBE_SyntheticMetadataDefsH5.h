//##MK::CODE

#ifndef __PARAPROBE_SYNTHETIC_METADATA_DEFS_H__
#define __PARAPROBE_SYNTHETIC_METADATA_DEFS_H__

#define PARAPROBE_HARDWARE								"/HardwareDetails"
#define PARAPROBE_HARDWARE_META							"/HardwareDetails/Metadata"
#define PARAPROBE_HARDWARE_META_NOMP					"/HardwareDetails/Metadata/OMPNUMTHREADS"		//1x unsigned int
#define PARAPROBE_HARDWARE_META_NOMP_NCMAX				1

#define PARAPROBE_SYNTH_VOLRECON						"/VolumeRecon"

#define PARAPROBE_SYNTH_VOLRECON_META					"/VolumeRecon/Metadata"

#define PARAPROBE_SYNTH_VOLRECON_META_GRNS				"/VolumeRecon/Metadata/GrainAggregate"
#define PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID			"/VolumeRecon/Metadata/GrainAggregate/GrainIDs"	//unsigned int
#define PARAPROBE_SYNTH_VOLRECON_META_GRNS_ID_NCMAX		1
#define PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI			"/VolumeRecon/Metadata/GrainAggregate/GrainOrientation" //4x float
#define PARAPROBE_SYNTH_VOLRECON_META_GRNS_ORI_NCMAX	4
#define PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS		"/VolumeRecon/Metadata/GrainAggregate/GrainNAtoms" //1x unsigned int
#define PARAPROBE_SYNTH_VOLRECON_META_GRNS_NATMS_NCMAX	1
#define PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL			"/VolumeRecon/Metadata/GrainAggregate/GrainVolumes" //1x double if measured
#define PARAPROBE_SYNTH_VOLRECON_META_GRNS_VOL_NCMAX	1


#define PARAPROBE_SYNTH_VOLRECON_META_TESS				"/VolumeRecon/Metadata/PoiVoronoiAggregate"
#define PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO			"/VolumeRecon/Metadata/PoiVoronoiAggregate/CellFacetTopo" //3x unsigned int
#define PARAPROBE_SYNTH_VOLRECON_META_TESS_TOPO_NCMAX	1
#define PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ			"/VolumeRecon/Metadata/PoiVoronoiAggregate/CellVerticesXYZ" //3x float
#define PARAPROBE_SYNTH_VOLRECON_META_TESS_XYZ_NCMAX	3
#define PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO			"/VolumeRecon/Metadata/PoiVoronoiAggregate/CellHaloIDs" //1x int, the neighbors
#define PARAPROBE_SYNTH_VOLRECON_META_TESS_HALO_NCMAX	1


#define PARAPROBE_SYNTH_VOLRECON_META_PREC				"/VolumeRecon/Metadata/PrecipitateModel"
//##MK::specify which model
#define PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ			"/VolumeRecon/Metadata/PrecipitateModel/ParticleXYZ" //3x float
#define PARAPROBE_SYNTH_VOLRECON_META_PREC_XYZ_NCMAX	3
#define PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO			"/VolumeRecon/Metadata/PrecipitateModel/ParticleTopo" //3x unsigned int
#define PARAPROBE_SYNTH_VOLRECON_META_PREC_TOPO_NCMAX	1
#define PARAPROBE_SYNTH_VOLRECON_META_PREC_R			"/VolumeRecon/Metadata/PrecipitateModel/ParticleR" //1x float
#define PARAPROBE_SYNTH_VOLRECON_META_PREC_R_NCMAX		1
#define PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT			"/VolumeRecon/Metadata/PrecipitateModel/ParticleRotation" //4x float
#define PARAPROBE_SYNTH_VOLRECON_META_PREC_ROT_NCMAX	4



#define PARAPROBE_SYNTH_VOLRECON_RES					"/VolumeRecon/Results"

#define PARAPROBE_SYNTH_VOLRECON_RES_XYZ				"/VolumeRecon/Results/XYZ"		//3x float
#define PARAPROBE_SYNTH_VOLRECON_RES_XYZ_NCMAX			3
#define PARAPROBE_SYNTH_VOLRECON_RES_MQ					"/VolumeRecon/Results/MQ"		//1x float
#define PARAPROBE_SYNTH_VOLRECON_RES_MQ_NCMAX			1
#define PARAPROBE_SYNTH_VOLRECON_RES_TOPO				"/VolumeRecon/Results/Topo"		//1x unsigned int
#define PARAPROBE_SYNTH_VOLRECON_RES_TOPO_NCMAX			1


#endif

