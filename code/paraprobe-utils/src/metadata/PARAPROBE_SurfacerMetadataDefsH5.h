//##MK::CODE

#ifndef __PARAPROBE_SURFACER_METADATA_DEFS_H__
#define __PARAPROBE_SURFACER_METADATA_DEFS_H__

#define PARAPROBE_SURF								"/SurfaceRecon"
#define PARAPROBE_SURF_ASHAPE						"/SurfaceRecon/AlphaShape"

#define PARAPROBE_SURF_ASHAPE_META					"/SurfaceRecon/AlphaShape/Metadata"
#define PARAPROBE_SURF_ASHAPE_META_AVALUE			"/SurfaceRecon/AlphaShape/Metadata/AlphaValue"			//1x float
#define PARAPROBE_SURF_ASHAPE_META_AVALUE_NCMAX		1
#define PARAPROBE_SURF_ASHAPE_META_CAND_N			"/SurfaceRecon/AlphaShape/Metadata/CandidatesN"			//1x unsigned int
#define PARAPROBE_SURF_ASHAPE_META_CAND_N_NCMAX		1
#define PARAPROBE_SURF_ASHAPE_META_CAND_XYZ			"/SurfaceRecon/AlphaShape/Metadata/CandidatesXYZ"		//3x float
#define PARAPROBE_SURF_ASHAPE_META_CAND_XYZ_NCMAX	3
#define PARAPROBE_SURF_ASHAPE_META_CAND_TOPO		"/SurfaceRecon/AlphaShape/Metadata/CandidatesTopo"		//1x unsigned int
#define PARAPROBE_SURF_ASHAPE_META_CAND_TOPO_NCMAX	1
#define PARAPROBE_SURF_ASHAPE_META_VXL_WIDTH		"/SurfaceRecon/AlphaShape/Metadata/VoxelizationWidth"	//1x float
#define PARAPROBE_SURF_ASHAPE_META_VXL_WIDTH_NCMAX	1

//##MK::add results from HoshenKopelman analyses

#define PARAPROBE_SURF_ASHAPE_RES					"/SurfaceRecon/AlphaShape/Results"
#define PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ			"/SurfaceRecon/AlphaShape/Results/HullXYZ"				//9x float
#define PARAPROBE_SURF_ASHAPE_RES_HULL_XYZ_NCMAX	9
#define PARAPROBE_SURF_ASHAPE_RES_HULL_TOPO			"/SurfaceRecon/AlphaShape/Results/HullTopo"				//1x unsigned int
#define PARAPROBE_SURF_ASHAPE_RES_HULL_TOPO_NCMAX	1



//#define PARAPROBE_SURFRECON_ASHAPE_INFO			"/SurfaceRecon/AlphaShape/DescrStats"

#define PARAPROBE_SURF_DIST							"/SurfaceRecon/Ion2Surface"
#define PARAPROBE_SURF_DIST_META					"/SurfaceRecon/Ion2Surface/Metadata"

#define PARAPROBE_SURF_DIST_META_NTRI				"/SurfaceRecon/Ion2Surface/Metadata/TriangleTests"		//1x unsigned int
#define PARAPROBE_SURF_DIST_META_NTRI_NCMAX			1

#define PARAPROBE_SURF_DIST_RES						"/SurfaceRecon/Ion2Surface/Results"
#define PARAPROBE_SURF_DIST_RES_D					"/SurfaceRecon/Ion2Surface/Results/Distance"			//1x float
#define PARAPROBE_SURF_DIST_RES_D_NCMAX				1

#endif

