//##MK::CODE

//specify here which specific results this tool writes into H5 files

#ifndef __PARAPROBE_ARAULLO_METADATA_DEFS_H__
#define __PARAPROBE_ARAULLO_METADATA_DEFS_H__

#define PARAPROBE_ARAULLO								"/AraulloPeters"

#define PARAPROBE_ARAULLO_META							"/AraulloPeters/Metadata"
#define PARAPROBE_ARAULLO_META_MP						"/AraulloPeters/Metadata/MatPoints"

#define PARAPROBE_ARAULLO_META_MP_XYZ					"/AraulloPeters/Metadata/MatPoints/XYZ"			//3x float
#define PARAPROBE_ARAULLO_META_MP_XYZ_NCMAX				3
#define PARAPROBE_ARAULLO_META_MP_TOPO					"/AraulloPeters/Metadata/MatPoints/Topo"		//1x unsigned int
#define PARAPROBE_ARAULLO_META_MP_TOPO_NCMAX			1
#define PARAPROBE_ARAULLO_META_MP_ID					"/AraulloPeters/Metadata/MatPoints/IDs"			//1x unsigned int
#define PARAPROBE_ARAULLO_META_MP_ID_NCMAX				1
#define PARAPROBE_ARAULLO_META_MP_R						"/AraulloPeters/Metadata/MatPoints/Radius"		//1x float
#define PARAPROBE_ARAULLO_META_MP_R_NCMAX				1

#define PARAPROBE_ARAULLO_META_PHCAND					"/AraulloPeters/Metadata/PhaseCandidates"
#define PARAPROBE_ARAULLO_META_PHCAND_N					"/AraulloPeters/Metadata/PhaseCandidates/NumberOfCandidates"	//1x unsigned int
#define PARAPROBE_ARAULLO_META_PHCAND_N_NCMAX			1
//+per candidate
#define PARAPROBE_ARAULLO_META_PHCAND_ID				"IontypesID"									//1x unsigned char, [0,255] which iontype
#define PARAPROBE_ARAULLO_META_PHCAND_ID_NCMAX			1
#define PARAPROBE_ARAULLO_META_PHCAND_WHAT				"IontypesWhat"									//evapion3, i.e. 8x unsigned char
#define PARAPROBE_ARAULLO_META_PHCAND_WHAT_NCMAX		8
#define PARAPROBE_ARAULLO_META_PHCAND_RSP				"RealspacePosition"								//1x float
#define PARAPROBE_ARAULLO_META_PHCAND_RSP_NCMAX			1
#define PARAPROBE_ARAULLO_META_PHCAND_VAL				"RefIntensity"									//1x float
#define PARAPROBE_ARAULLO_META_PHCAND_VAL_NCMAX			1


#define PARAPROBE_ARAULLO_META_DIR						"/AraulloPeters/Metadata/Directions"
#define PARAPROBE_ARAULLO_META_DIR_ELAZ					"/AraulloPeters/Metadata/Directions/ElevAzim"	//2x float
#define PARAPROBE_ARAULLO_META_DIR_ELAZ_NCMAX			2
#define PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ				"/AraulloPeters/Metadata/Directions/XYZ"		//3x float
#define PARAPROBE_ARAULLO_META_DIR_ELAZ_XYZ_NCMAX		3
#define PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO			"/AraulloPeters/Metadata/Directions/Topo"		//1x unsigned int
#define PARAPROBE_ARAULLO_META_DIR_ELAZ_TOPO_NCMAX		1


#define PARAPROBE_ARAULLO_RES							"/AraulloPeters/Results"
#define PARAPROBE_ARAULLO_RES_MP_IOID					"/AraulloPeters/Results/MatPointIDsWithResults"	//1x int
#define PARAPROBE_ARAULLO_RES_MP_IOID_NCMAX				1
//#define PARAPROBE_ARAULLO_RES_SPECMETA					"/AraulloPeters/Results/SpecificMeta"
#define PARAPROBE_ARAULLO_RES_SPECPEAK					"/AraulloPeters/Results/SpecificPeak"
#define PARAPROBE_ARAULLO_RES_SPECPEAK_NCMAX			1
#define PARAPROBE_ARAULLO_RES_THREEPEAK					"/AraulloPeters/Results/ThreeStrongest"
#define PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX			6

#define PARAPROBE_ARAULLO_RES_HISTO_CNTS				"/AraulloPeters/Results/Histogramm"
#define PARAPROBE_ARAULLO_RES_HISTO_MAGN				"/AraulloPeters/Results/FFTMagnitude"


#endif
