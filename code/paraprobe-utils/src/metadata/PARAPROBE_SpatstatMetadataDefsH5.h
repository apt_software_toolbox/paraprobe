//##MK::CODE

#ifndef __PARAPROBE_SPATSTAT_METADATA_DEFS_H__
#define __PARAPROBE_SPATSTAT_METADATA_DEFS_H__

#define PARAPROBE_SPST								"/SpatialStatistics"

//for RDF, KNN
//one result per ion group e.g two combinations of ion types are tested Al-Fe and Al-Al
//string dsnm = PARAPROBE_SPST_RDF_META + to_string(CaseID) + PARAPROBE_SPST_RDF_META_TYPID --> "/SpatialStatistics/RDF/Metadata/1/IontypeIDs"


#define PARAPROBE_SPST_RDF							"/SpatialStatistics/RDF"
#define PARAPROBE_SPST_RDF_META						"/SpatialStatistics/RDF/Metadata"  
//+per case 
#define PARAPROBE_SPST_RDF_META_TYPID_TG			"IontypeTargets"				//evapion3 as many rows as combinations, zero-th row is always central ion
#define PARAPROBE_SPST_RDF_META_TYPID_TG_NCMAX		9
#define PARAPROBE_SPST_RDF_META_TYPID_NB			"IontypeNeighbors"
#define PARAPROBE_SPST_RDF_META_TYPID_NB_NCMAX		9


#define PARAPROBE_SPST_RDF_RES						"/SpatialStatistics/RDF/Results"
//+per case
//#define PARAPROBE_SPST_RDF_RES_RVAL					"ROIRadii"				//1x float, as many rows as values defined
//#define PARAPROBE_SPST_RDF_RES_RVAL_NCMAX			1
#define PARAPROBE_SPST_RDF_RES_CNTS					"Cnts"					//2x double
#define PARAPROBE_SPST_RDF_RES_CNTS_NCMAX			2


#define PARAPROBE_SPST_KNN							"/SpatialStatistics/KNN"
#define PARAPROBE_SPST_KNN_META						"/SpatialStatistics/KNN/Metadata"
//+per case
#define PARAPROBE_SPST_KNN_META_TYPID_TG			"IontypeTargets"			//evapion3 as above
#define PARAPROBE_SPST_KNN_META_TYPID_TG_NCMAX		9
#define PARAPROBE_SPST_KNN_META_TYPID_NB			"IontypeNeighbors"			//evapion3 as above
#define PARAPROBE_SPST_KNN_META_TYPID_NB_NCMAX		9

#define PARAPROBE_SPST_KNN_META_KTH					"KthOrder"				//1x unsigned int
#define PARAPROBE_SPST_KNN_META_KTH_NCMAX			1

#define PARAPROBE_SPST_KNN_RES						"/SpatialStatistics/KNN/Results"
//+per case
//#define PARAPROBE_SPST_KNN_RES_RVAL					"ROIRadii"				//1x float
//#define PARAPROBE_SPST_KNN_RES_RVAL_NCMAX			1
#define PARAPROBE_SPST_KNN_RES_CNTS					"Cnts"					//2x double
#define PARAPROBE_SPST_KNN_RES_CNTS_NCMAX			2

#define PARAPROBE_SPST_SDM							"/SpatialStatistics/SDM"
#define PARAPROBE_SPST_SDM_META						"/SpatialStatistics/SDM/Metadata"
#define PARAPROBE_SPST_SDM_META_TYPID_TG			"IontypeTargets"
#define PARAPROBE_SPST_SDM_META_TYPID_TG_NCMAX		9
#define PARAPROBE_SPST_SDM_META_TYPID_NB			"IontypeNeighbors"
#define PARAPROBE_SPST_SDM_META_TYPID_NB_NCMAX		9

#define PARAPROBE_SPST_SDM_META_R					"ROIRadius"				//1x float
#define PARAPROBE_SPST_SDM_META_R_NCMAX				1
#define PARAPROBE_SPST_SDM_META_NXYZ				"ROIDiscretization"		//3x int
#define PARAPROBE_SPST_SDM_META_NXYZ_NCMAX			3

#define PARAPROBE_SPST_SDM_META_XYZ					"/SpatialStatistics/SDM/Metadata/BincenterXYZ" //3x float
#define PARAPROBE_SPST_SDM_META_XYZ_NCMAX			3
//##MK::currently we use the same Radii settings for all SDM tasks
#define PARAPROBE_SPST_SDM_META_TOPO				"/SpatialStatistics/SDM/Metadata/BincenterTopo" //3x unsigned int
#define PARAPROBE_SPST_SDM_META_TOPO_NCMAX			1

#define PARAPROBE_SPST_SDM_RES						"/SpatialStatistics/SDM/Results"
//+per case
#define PARAPROBE_SPST_SDM_RES_CNTS					"Cnts"					//1x unsigned int
#define PARAPROBE_SPST_SDM_RES_CNTS_NCMAX			1


#endif

