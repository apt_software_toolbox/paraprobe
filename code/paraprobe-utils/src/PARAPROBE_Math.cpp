//##MK::CODE

#include "PARAPROBE_Math.h"


ostream& operator<<(ostream& in, circumsphere const & val)
{
	in << val.x << ";" << val.y << ";" << val.z << "," << val.R << endl;
	return in;
}



apt_real lerp(const apt_real v0, const apt_real v1, const apt_real t)
{
	return (1.f - t)*v0 + t*v1;
}


vector<apt_real> quantiles_nosort( vector<apt_real> const & in, vector<apt_real> const & q )
{
	if ( in.size() > 1 ) { //most likely more than one or nothing
		//##MK::if desired use random sub-sampling here
		//vector<apt_real> data = in;

		//##MK::can be improved for instance if only quantiles << 1 are sought via using n_th element
		//sort(data.begin(), data.end());

		vector<apt_real> quants;
		for (size_t i = 0; i < q.size(); ++i)
		{
			apt_real poi = lerp(-0.5, static_cast<apt_real>(in.size()) - 0.5, q.at(i));

			size_t left = max( static_cast<int64_t>(floor(poi)), static_cast<int64_t>(0) );
			size_t right = min( static_cast<int64_t>(ceil(poi)), static_cast<int64_t>(in.size() - 1) );

			apt_real datLeft = in.at(left);
			apt_real datRight = in.at(right);

			apt_real quantile = lerp(datLeft, datRight, poi - static_cast<apt_real>(left) );

			quants.push_back(quantile);
		}

		return quants;
	}
	 else if ( in.size() == 1) {
		return vector<apt_real>(1, in[0]);
	 }
	 else {
		 return vector<apt_real>();
	 }
}


/* 
	Distance Between Point and Triangle in 3D
	David Eberly
	Geometric Tools, LLC
	http://www.geometrictools.com/
	Copyright 
	c 1998-2016. All Rights Reserved.
	Created: September 28, 1999
	Last Modified: March 1, 2008
	vector3 closesPointOnTriangle( const vector3 *triangle, const vector3 &sourcePosition )
*/


apt_real closestPointOnTriangle( const tri3d face, const p3d src )
{
	v3d edge0( face.x2-face.x1, face.y2-face.y1, face.z2-face.z1 );
	v3d edge1( face.x3-face.x1, face.y3-face.y1, face.z3-face.z1 );
	v3d v0( face.x1-src.x, face.y1-src.y, face.z1-src.z );

	apt_real a = SQR(edge0.u) + SQR(edge0.v) + SQR(edge0.w); //edge0.dot( edge0 );
	apt_real b = edge0.u*edge1.u + edge0.v*edge1.v + edge0.w*edge1.w; //edge0.dot( edge1 );
	apt_real c = SQR(edge1.u) + SQR(edge1.v) + SQR(edge1.w); //edge1.dot( edge1 );
	apt_real d = edge0.u*v0.u + edge0.v*v0.v + edge0.w*v0.w; //edge0.dot( v0 );
	apt_real e = edge1.u*v0.u + edge1.v*v0.v + edge1.w*v0.w; //edge1.dot( v0 );

	apt_real det = a*c - b*b;
	apt_real s = b*e - c*d;
	apt_real t = b*d - a*e;

	if ( s + t < det ) {
		if ( s < 0.f ) {
			if ( t < 0.f ) { //region 4
				if ( d < 0.f ) {
					s = CLAMP( -d/a, 0.f, 1.f );
					t = 0.f;
				}
				else {
					s = 0.f;
					t = CLAMP( -e/c, 0.f, 1.f );
				}
			}
			else {//region 3
				s = 0.f;
				t = CLAMP( -e/c, 0.f, 1.f );
			}
		}
		else if ( t < 0.f ) { //region 5
			s = CLAMP( -d/a, 0.f, 1.f );
			t = 0.f;
		}
		else { //region 0
			apt_real invDet = 1.f / det;
			s *= invDet;
			t *= invDet;
		}
	}
	else {
		if ( s < 0.f ) { //region 2
			apt_real tmp0 = b+d;
			apt_real tmp1 = c+e;
			if ( tmp1 > tmp0 ) {
				apt_real numer = tmp1 - tmp0;
				apt_real denom = a-2*b+c;
				s = CLAMP( numer/denom, 0.f, 1.f );
				t = 1-s;
			}
			else {
				t = CLAMP( -e/c, 0.f, 1.f );
				s = 0.f;
			}
		}
		else if ( t < 0.f ) { //region 6
			if ( a+d > b+e ) {
				apt_real numer = c+e-b-d;
				apt_real denom = a-2*b+c;
				s = CLAMP( numer/denom, 0.f, 1.f );
				t = 1-s;
			}
			else {
				s = CLAMP( -e/c, 0.f, 1.f );
				t = 0.f;
			}
		}
		else { //region 1
			apt_real numer = c+e-b-d;
			apt_real denom = a-2*b+c;
			s = CLAMP( numer/denom, 0.f, 1.f );
			t = 1.f - s;
		}
	}

	//closest point is cp
	p3d cp( face.x1 + s*edge0.u + t*edge1.u, face.y1 + s*edge0.v + t*edge1.v, face.z1 + s*edge0.w + t*edge1.w );

	//so return SQR of difference to avoid sqrt in the code
	return ( SQR(cp.x-src.x) + SQR(cp.y-src.y) + SQR(cp.z-src.z) );
}


circumsphere circumsphere_about_triangle( const tri3d tri )
{
	//compute the center and radius of a sphere which circumscribes the triangle tri
	//##https://en.wikipedia.org/wiki/Circumscribed_circle#Higher_dimensions
	//##MK::use operator overloading
	//bring vertices to origin, x3,y3,z3 as c
	p3d a = p3d( tri.x1 - tri.x3, tri.y1 - tri.x3, tri.z1 - tri.z3 );
	p3d b = p3d( tri.x2 - tri.x3, tri.y2 - tri.x3, tri.z2 - tri.z3 );
	p3d ab = p3d( a.x - b.x, a.y - b.y, a.z - b.z );
	
	apt_real a_nrm = vnorm( a );
	apt_real b_nrm = vnorm( b );
	apt_real ab_nrm = vnorm( ab );
	p3d cr_ab = cross( a, b );
	apt_real cr_ab_nrm = vnorm( cr_ab );
	
	p3d abba = p3d( SQR(a_nrm)*b.x - SQR(b_nrm)*a.x, SQR(a_nrm)*b.y - SQR(b_nrm)*a.y, SQR(a_nrm)*b.z - SQR(b_nrm)*a.z );
	p3d abba_ab = cross( abba, cr_ab );
	
	circumsphere res = circumsphere();
	res.x = ( abba_ab.x / (2.0*SQR(cr_ab_nrm)) ) + tri.x3;
	res.y = ( abba_ab.y / (2.0*SQR(cr_ab_nrm)) ) + tri.y3;
	res.z = ( abba_ab.z / (2.0*SQR(cr_ab_nrm)) ) + tri.z3;	
	res.R = ( a_nrm * b_nrm * ab_nrm ) / (2.0 * cr_ab_nrm ) + EPSILON;
	
	return res;	
}
