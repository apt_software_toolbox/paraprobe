//##MK::GPLV3


#ifndef __PARAPROBE_UTILS_ORIMATH_H__
#define __PARAPROBE_UTILS_ORIMATH_H__

#include "PARAPROBE_MPIDatatypes.h"

#define P_IJK			-1

struct bunge;

struct squat
{
	//w,v,x,z
	apt_real q0;
	apt_real q1;
	apt_real q2;
	apt_real q3;
	squat() : q0(1.f), q1(0.f), q2(0.f), q3(0.f) {}
	squat( const apt_real _q0, const apt_real _q1, const apt_real _q2, const apt_real _q3 );
	squat( const apt_real X0, const apt_real X1, const apt_real X2 );

	void normalize();
	squat invert(); //to switch active <=> passive
	squat conjugate();
	squat multiply_quaternion( squat const & q);

	p3d Lqr( d3d const & r );
	apt_real disorientation_angle_fcc( squat & q ); //me being p
	
	//conversion routine D. Rowenhorst et al. MSMSE 23 2015
	t3x3 qu2om();
	bunge qu2eu();
};

ostream& operator<<(ostream& in, squat const & val);

squat multiply_quaternion( squat const & p, squat const & q);
apt_real disorientation_angle_fcc_grimmer( squat const & qcand );

pair<bool,squat> disoriquaternion_cubic_cubic( squat const & ori_p, squat const & ori_q );

struct bunge
{
	//Bunge Texture Orientations book
	//phi1, Phi, phi2
	apt_real phi1;
	apt_real Phi;
	apt_real phi2;
	bunge() : phi1(0.f), Phi(0.f), phi2(0.f) {}
	bunge( const apt_real _e1, const apt_real _e2, const apt_real _e3 ) :
		phi1(_e1), Phi(_e2), phi2(_e3) {}
	//bunge( const apt_real _q0, const apt_real _q1, const apt_real _q2, const apt_real _q3 ); //qu2eu
	bunge( const string parseme );

	squat eu2qu();
	t3x3 eu2om();
};


ostream& operator<<(ostream& in, bunge const & val);


//cubic crystal symmetry operators
//##MK::http://pajarito.materials.cmu.edu/rollett/27750/L13-Grain_Bndries_RFspace-15Mar16.pdf
//see here also https://github.com/BlueQuartzSoftware/DREAM3D/commit/0446b45616257e421c0e6b4550d412b1af171020

//mind that dream3d has VectorScale and non vector scale i.e. x,y,z,w and w, x,y,z quaternions!
//PARAPROBE uses w, x, y, z !

//identity
//
//DREAM3D 1
#define CUBIC_SYMM_L01_Q0		(+1.f)
#define CUBIC_SYMM_L01_Q1		(+0.f)
#define CUBIC_SYMM_L01_Q2		(+0.f)
#define CUBIC_SYMM_L01_Q3		(+0.f)

//2fold
//DREAM3D
/*
#define CUBIC_SYMM_L02_Q0		(-1.f)
#define CUBIC_SYMM_L02_Q1		(+0.f)
#define CUBIC_SYMM_L02_Q2		(+0.f)
#define CUBIC_SYMM_L02_Q3		(+0.f)
*/

//corresponding in DREAM3D assuming DREAM3D encodes x,y,z,w
//2,3,4
#define CUBIC_SYMM_L03_Q0		(+0.f)
#define CUBIC_SYMM_L03_Q1		(+1.f)
#define CUBIC_SYMM_L03_Q2		(+0.f)
#define CUBIC_SYMM_L03_Q3		(+0.f)

/*
#define CUBIC_SYMM_L04_Q0		(+0.f)
#define CUBIC_SYMM_L04_Q1		(-1.f)
#define CUBIC_SYMM_L04_Q2		(+0.f)
#define CUBIC_SYMM_L04_Q3		(+0.f)
*/

#define CUBIC_SYMM_L05_Q0		(+0.f)
#define CUBIC_SYMM_L05_Q1		(+0.f)
#define CUBIC_SYMM_L05_Q2		(+1.f)
#define CUBIC_SYMM_L05_Q3		(+0.f)

/* ##OBSOLETE
#define CUBIC_SYMM_L06_Q0		(+0.f)
#define CUBIC_SYMM_L06_Q1		(+0.f)
#define CUBIC_SYMM_L06_Q2		(-1.f)
#define CUBIC_SYMM_L06_Q3		(+0.f)
*/

#define CUBIC_SYMM_L06_Q0		(+0.f)
#define CUBIC_SYMM_L06_Q1		(+0.f)
#define CUBIC_SYMM_L06_Q2		(+0.f)
#define CUBIC_SYMM_L06_Q3		(+1.f)


#define ONE_OVER_SQRT2			(1.f/sqrt(2.f))
//4fold <100>
//DREAM3D 19, 26,7,21,27,9,23,25,15,17,11,13
#define CUBIC_SYMM_L07_Q0		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L07_Q1		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L07_Q2		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L07_Q3		(+ONE_OVER_SQRT2 * +1.f)

/*
#define CUBIC_SYMM_L08_Q0		(-ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L08_Q1		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L08_Q2		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L08_Q3		(-ONE_OVER_SQRT2 * +1.f)
*/

//##OBSOLETE
/*
#define CUBIC_SYMM_L09_Q0		(+ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L09_Q1		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L09_Q2		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L09_Q3		(+ONE_OVER_SQRT2 * +1.f)
*/
#define CUBIC_SYMM_L09_Q0		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L09_Q1		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L09_Q2		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L09_Q3		(+ONE_OVER_SQRT2 * -1.f)



/*
#define CUBIC_SYMM_L10_Q0		(-ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L10_Q1		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L10_Q2		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L10_Q3		(-ONE_OVER_SQRT2 * +1.f)
*/

#define CUBIC_SYMM_L11_Q0		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L11_Q1		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L11_Q2		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L11_Q3		(+ONE_OVER_SQRT2 * +1.f)

/*
#define CUBIC_SYMM_L12_Q0		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L12_Q1		(-ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L12_Q2		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L12_Q3		(-ONE_OVER_SQRT2 * +1.f)
*/

#define CUBIC_SYMM_L13_Q0		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L13_Q1		(+ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L13_Q2		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L13_Q3		(+ONE_OVER_SQRT2 * +1.f)

/*
#define CUBIC_SYMM_L14_Q0		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L14_Q1		(-ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L14_Q2		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L14_Q3		(-ONE_OVER_SQRT2 * +1.f)
*/

#define CUBIC_SYMM_L15_Q0		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L15_Q1		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L15_Q2		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L15_Q3		(+ONE_OVER_SQRT2 * +1.f)

/*
#define CUBIC_SYMM_L16_Q0		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L16_Q1		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L16_Q2		(-ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L16_Q3		(-ONE_OVER_SQRT2 * +1.f)
*/

#define CUBIC_SYMM_L17_Q0		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L17_Q1		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L17_Q2		(+ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L17_Q3		(+ONE_OVER_SQRT2 * +1.f)

/*
#define CUBIC_SYMM_L18_Q0		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L18_Q1		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L18_Q2		(-ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L18_Q3		(-ONE_OVER_SQRT2 * +1.f)
*/

//2fold <110>
#define CUBIC_SYMM_L19_Q0		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L19_Q1		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L19_Q2		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L19_Q3		(+ONE_OVER_SQRT2 * +0.f)

/*
#define CUBIC_SYMM_L20_Q0		(-ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L20_Q1		(-ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L20_Q2		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L20_Q3		(-ONE_OVER_SQRT2 * +0.f)
*/

//##OBSOLETE
/*
#define CUBIC_SYMM_L21_Q0		(+ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L21_Q1		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L21_Q2		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L21_Q3		(+ONE_OVER_SQRT2 * +0.f)
*/
#define CUBIC_SYMM_L21_Q0		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L21_Q1		(+ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L21_Q2		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L21_Q3		(+ONE_OVER_SQRT2 * +0.f)


/*
#define CUBIC_SYMM_L22_Q0		(-ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L22_Q1		(-ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L22_Q2		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L22_Q3		(-ONE_OVER_SQRT2 * +0.f)
*/

#define CUBIC_SYMM_L23_Q0		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L23_Q1		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L23_Q2		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L23_Q3		(+ONE_OVER_SQRT2 * +0.f)

/*
#define CUBIC_SYMM_L24_Q0		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L24_Q1		(-ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L24_Q2		(-ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L24_Q3		(-ONE_OVER_SQRT2 * +0.f)
*/

//##OBSOLETE
/*
#define CUBIC_SYMM_L25_Q0		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L25_Q1		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L25_Q2		(+ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L25_Q3		(+ONE_OVER_SQRT2 * +0.f)
*/
#define CUBIC_SYMM_L25_Q0		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L25_Q1		(+ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L25_Q2		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L25_Q3		(+ONE_OVER_SQRT2 * +0.f)


/*
//##OBSOLETE
#define CUBIC_SYMM_L26_Q0		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L26_Q1		(-ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L26_Q2		(-ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L26_Q3		(-ONE_OVER_SQRT2 * +0.f)
*/

#define CUBIC_SYMM_L26_Q0		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L26_Q1		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L26_Q2		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L26_Q3		(+ONE_OVER_SQRT2 * +0.f)

#define CUBIC_SYMM_L27_Q0		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L27_Q1		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L27_Q2		(+ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L27_Q3		(+ONE_OVER_SQRT2 * +0.f)


/*
#define CUBIC_SYMM_L28_Q0		(-ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L28_Q1		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L28_Q2		(-ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L28_Q3		(-ONE_OVER_SQRT2 * +0.f)
*/

#define CUBIC_SYMM_L29_Q0		(+ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L29_Q1		(+ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L29_Q2		(+ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L29_Q3		(+ONE_OVER_SQRT2 * +0.f)

/*
#define CUBIC_SYMM_L30_Q0		(-ONE_OVER_SQRT2 * -1.f)
#define CUBIC_SYMM_L30_Q1		(-ONE_OVER_SQRT2 * +0.f)
#define CUBIC_SYMM_L30_Q2		(-ONE_OVER_SQRT2 * +1.f)
#define CUBIC_SYMM_L30_Q3		(-ONE_OVER_SQRT2 * +0.f)
*/

#define ONE_HALF				(0.5)
//3-fold <111>
//DREAM3D assuming x, y, z, w in DREAM3D 31,45,37,41,35,39,43,33
#define CUBIC_SYMM_L31_Q0		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L31_Q1		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L31_Q2		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L31_Q3		(+ONE_HALF * +1.f)

/*
#define CUBIC_SYMM_L32_Q0		(-ONE_HALF * +1.f)
#define CUBIC_SYMM_L32_Q1		(-ONE_HALF * +1.f)
#define CUBIC_SYMM_L32_Q2		(-ONE_HALF * +1.f)
#define CUBIC_SYMM_L32_Q3		(-ONE_HALF * +1.f)
*/

//##OBSOLETE
/*
#define CUBIC_SYMM_L33_Q0		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L33_Q1		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L33_Q2		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L33_Q3		(+ONE_HALF * +1.f)
*/
#define CUBIC_SYMM_L33_Q0		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L33_Q1		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L33_Q2		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L33_Q3		(+ONE_HALF * -1.f)



/*
#define CUBIC_SYMM_L34_Q0		(-ONE_HALF * -1.f)
#define CUBIC_SYMM_L34_Q1		(-ONE_HALF * +1.f)
#define CUBIC_SYMM_L34_Q2		(-ONE_HALF * +1.f)
#define CUBIC_SYMM_L34_Q3		(-ONE_HALF * +1.f)
*/


#define CUBIC_SYMM_L35_Q0		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L35_Q1		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L35_Q2		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L35_Q3		(+ONE_HALF * +1.f)

/*
#define CUBIC_SYMM_L36_Q0		(-ONE_HALF * +1.f)
#define CUBIC_SYMM_L36_Q1		(-ONE_HALF * -1.f)
#define CUBIC_SYMM_L36_Q2		(-ONE_HALF * +1.f)
#define CUBIC_SYMM_L36_Q3		(-ONE_HALF * +1.f)
*/

#define CUBIC_SYMM_L37_Q0		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L37_Q1		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L37_Q2		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L37_Q3		(+ONE_HALF * +1.f)

/*
#define CUBIC_SYMM_L38_Q0		(-ONE_HALF * +1.f)
#define CUBIC_SYMM_L38_Q1		(-ONE_HALF * +1.f)
#define CUBIC_SYMM_L38_Q2		(-ONE_HALF * -1.f)
#define CUBIC_SYMM_L38_Q3		(-ONE_HALF * +1.f)
*/

//##OBSOLETE
/*
#define CUBIC_SYMM_L39_Q0		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L39_Q1		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L39_Q2		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L39_Q3		(+ONE_HALF * +1.f)
*/
#define CUBIC_SYMM_L39_Q0		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L39_Q1		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L39_Q2		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L39_Q3		(+ONE_HALF * -1.f)



/*
#define CUBIC_SYMM_L40_Q0		(-ONE_HALF * -1.f)
#define CUBIC_SYMM_L40_Q1		(-ONE_HALF * -1.f)
#define CUBIC_SYMM_L40_Q2		(-ONE_HALF * +1.f)
#define CUBIC_SYMM_L40_Q3		(-ONE_HALF * +1.f)
*/

//##OBSOLETE
/*
#define CUBIC_SYMM_L41_Q0		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L41_Q1		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L41_Q2		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L41_Q3		(+ONE_HALF * +1.f)
*/
#define CUBIC_SYMM_L41_Q0		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L41_Q1		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L41_Q2		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L41_Q3		(+ONE_HALF * -1.f)



/*
#define CUBIC_SYMM_L42_Q0		(-ONE_HALF * -1.f)
#define CUBIC_SYMM_L42_Q1		(-ONE_HALF * +1.f)
#define CUBIC_SYMM_L42_Q2		(-ONE_HALF * -1.f)
#define CUBIC_SYMM_L42_Q3		(-ONE_HALF * +1.f)
*/

#define CUBIC_SYMM_L43_Q0		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L43_Q1		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L43_Q2		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L43_Q3		(+ONE_HALF * +1.f)

/*
#define CUBIC_SYMM_L44_Q0		(-ONE_HALF * +1.f)
#define CUBIC_SYMM_L44_Q1		(-ONE_HALF * -1.f)
#define CUBIC_SYMM_L44_Q2		(-ONE_HALF * -1.f)
#define CUBIC_SYMM_L44_Q3		(-ONE_HALF * +1.f)
*/

//##OBSOLETE
/*
#define CUBIC_SYMM_L45_Q0		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L45_Q1		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L45_Q2		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L45_Q3		(+ONE_HALF * +1.f)
*/
#define CUBIC_SYMM_L45_Q0		(+ONE_HALF * +1.f)
#define CUBIC_SYMM_L45_Q1		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L45_Q2		(+ONE_HALF * -1.f)
#define CUBIC_SYMM_L45_Q3		(+ONE_HALF * -1.f)


/*
#define CUBIC_SYMM_L46_Q0		(-ONE_HALF * -1.f)
#define CUBIC_SYMM_L46_Q1		(-ONE_HALF * -1.f)
#define CUBIC_SYMM_L46_Q2		(-ONE_HALF * -1.f)
#define CUBIC_SYMM_L46_Q3		(-ONE_HALF * +1.f)
*/


#endif
