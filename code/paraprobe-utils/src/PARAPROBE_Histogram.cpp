//##MK::GPLV3

#include "PARAPROBE_Histogram.h"

histogram::histogram()
{
	cnts_lowest = 0.0;
	cnts_highest = 0.0;
	bounds = linear_ival();
	tmp = 0.0;
	nbins = 0;
}


histogram::histogram( linear_ival const & iv )
{
	cnts_lowest = 0.0;
	cnts_highest = 0.0;
	bounds = linear_ival();
	tmp = 0.0;
	nbins = 0;
	
	if ( iv.incr > EPSILON ) {
		size_t _nbins = (iv.max - iv.min) / iv.incr;
//cout << "Histogram construction [" << _start << ":" << _width << ":" << _end << "] " << _nbins << " bins" << endl;
		if ( _nbins > 0 && _nbins <= UINT32MX ) {
			bounds = iv;
			nbins = static_cast<unsigned int>(_nbins); //type cast from size_t to uint32 safe
			try {
				cnts = vector<double>( _nbins, 0.0 );
			}
			catch (bad_alloc &croak) {
				cerr << "Unable to allocate memory in histogram class object" << "\n";
			}
			tmp = 1.0 / (bounds.max - bounds.min) * static_cast<double>(nbins);
		}
		//else histogram cnts is empty
	}
}


histogram::~histogram()
{
	cnts = vector<double>();
}


void histogram::add_dump_yes( const apt_real x )
{
	//increment cnts in specific bin at correct bin
	double xx = x;
	if ( xx >= bounds.min && xx <= bounds.max ) {
		double where = (xx - bounds.min) * tmp;
		unsigned int b = static_cast<unsigned int>( floor(where) );
		if ( b < nbins )
			cnts[b] += 1.0;
		else
			cnts_highest += 1.0;
	}
	else {
		if ( xx < bounds.min ) {
			cnts_lowest += 1.0;
		}
		if ( xx > bounds.max ) {
			cnts_highest += 1.0;
		}
	}
}


void histogram::normalize()
{
	double sum = 0.0;
	for ( auto it = cnts.begin(); it != cnts.end(); it++ ) {
		sum = sum + *it;
	}
	
	if ( sum >= (1.0-EPSILON) ) { //at least one count
		for ( unsigned int b = 0; b < nbins; b++ ) {
			cnts[b] /= sum;
		}
	}
}


double histogram::report( const unsigned int b )
{
	if ( b < nbins ) {
		return cnts[b];
	}
	else {
		return 0.0;
	}
}


double histogram::left( const unsigned int b )
{
	if ( b < nbins ) {
		return bounds.min + bounds.incr * static_cast<double>(b);
	}
	else {
		return F64MI;
	}
}
	
	
double histogram::center( const unsigned int b )
{
	if ( b < nbins ) {
		return bounds.min + bounds.incr * (static_cast<double>(b) + 0.5);
	}
	else {
		return 0.0;
	}
}


double histogram::right( const unsigned int b )
{
	if ( b < nbins ) {
		return bounds.min + bounds.incr * static_cast<double>(b+1);
	}
	else {
		return F64MX;
	}
}

