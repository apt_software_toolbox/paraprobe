//##MK::CODESPLIT

#include "PARAPROBE_AraulloHdl.h"


//define what is passed upon execution
#define SIMID									1
#define CONTROLFILE								2

//##MK::for querying system maximum physical memory see
//https://stackoverflow.com/questions/2513505/how-to-get-available-memory-c-g


bool init(  int pargc, char** pargv )
{
	ConfigShared::SimID = stoul( pargv[SIMID] );
	try {
		ConfigAraullo::readXML(pargv[CONTROLFILE]);
	}
	catch (exception& e) {
		cerr << "\n" << "Unable to parse control file! Details:" << "\n" << e.what() << "\n"; return false;
	}
	if ( ConfigAraullo::checkUserInput() == false ) {
		cerr << "\n" << "Control file settings failed the validity check!" << "\n"; return false;
	}
	else {
		cout << "\n" << "\t\tInput is valid under SimulationID = " << "SimID." <<  ConfigShared::SimID << "\n";
	}
	cout << endl;
	return true;
}


void many_local_araullo_peters_on_reconstruction( const int r, const int nr, char** pargv )
{
	//allocate process-level instance of a araulloHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	araulloHdl* sdm = NULL;

	double ttic = MPI_Wtime();

	int localhealth = 1;
	int globalhealth = nr;
	try {
		sdm = new araulloHdl;
		sdm->set_myrank(r);
		sdm->set_nranks(nr);
		sdm->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Unable to allocate an araulloHdl class object instance" << "\n"; localhealth = 0;
	}

	if ( sdm->read_periodictable() == true ) {
		cout << "Rank " << r << " reads PSE successfully rng.nuclides.isotopes.size() " << sdm->rng.nuclides.isotopes.size() << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of PeriodicTableOfElements failed!" << "\n"; localhealth = 0;
	}

	string xmlfn = pargv[CONTROLFILE];
	if ( sdm->read_iontype_combinations( xmlfn ) == true ) {
		cout << "Rank " << r << " reads IontypeCombinations successfully " << "\n";
	}
	else {
		cerr << "Rank " << r << " loading of IontypeCombinations failed!" << "\n"; localhealth = 0;
	}

	//do we have all processes on board?
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete sdm; sdm = NULL; return;
	}
	
	double ttoc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	sdm->araullo_tictoc.prof_elpsdtime_and_mem( "ReadXMLParameter", APT_XX, APT_IS_SEQ, mm, ttic, ttoc );
	ttic = MPI_Wtime();

	//we have all on board, read reconstruction and triangle hull
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast, we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( sdm->get_myrank() == MASTER ) {

		if ( sdm->read_reconxyz_ranging_from_apth5() == true ) {
			cout << "MASTER read successfully all ion coordinates and ranging information" << "\n";
		}
		else {
			cerr << "MASTER was unable to read ion coordinates and ranging information!" << "\n"; localhealth = 0;
		}
		if ( ConfigAraullo::SamplingGridRemoveBndPoints == true ) {

			if ( sdm->read_dist2hull_from_apth5() == true ) {
				cout << "MASTER read successfully all ion distance to triangularized dataset boundary!" << "\n";
			}
			else {
				cerr << "MASTER was unable to read ion distance values!" << "\n"; localhealth = 0;
			}
			/*if ( sdm->xyz.size() != sdm->dist2hull.size() ) { //##MK::activate once dist2hull is implemented!
				cerr << "MASTER found that not for every position there is at all or only one distance value!" << "\n"; localhealth = 0;
			}*/
		}
		else {
			cout << "WARNING:: MASTER defines as default very large dist2hull values to use for evaluating where ions are considered!" << "\n";
			cout << "WARNING:: MASTER does so because SamplingGridRemoveBndPoints == 0 so some ROI crystallography results may be biased!" << "\n";
			try {
				sdm->dist2hull = vector<apt_real>( sdm->xyz.size(), F32MX );
			}
			catch (bad_alloc &mecroak) {
				cerr << "MASTER allocation of dist2hull failed!"; localhealth = 0;
			}
		}

		if ( sdm->read_directions_from_apth5() == true ) {
			cout << "MASTER read successfully all elevation/azimuth directions to probe" << "\n";
		}
		else {
			cerr << "MASTER was unable to read elevation/azimuth directions!" << "\n"; localhealth = 0;
		}
	}
	//else {} //slaves processes wait
	//ttoc = MPI_Wtime();
	//mm = sdm->araullo_tictoc.get_memoryconsumption();
	//sdm->araullo_tictoc.prof_elpsdtime_and_mem( "ReadMaterialsDataAPTH5", APT_XX, APT_IS_SEQ, mm, ttic, ttoc );
	//ttic = MPI_Wtime();
	//necessary? second order issue wrt to performance for as few as 80 processes like on TALOS...?
	MPI_Barrier( MPI_COMM_WORLD );
	
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that all data have arrived!" << "\n";
		delete sdm; sdm = NULL; return;
	}
	
	if ( sdm->broadcast_reconxyz() == true ) {
		cout << "Rank " << r << " has synchronized reconstruction" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize reconstruction" << "\n"; localhealth = 0;
	}
	if ( sdm->broadcast_ranging() == true ) {
		cout << "Rank " << r << " has synchronized ranging" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize ranging" << "\n"; localhealth = 0;
	}
	if ( sdm->broadcast_distances() == true ) { //GB/s bidirectional bandwidth one 4B float per ion, so at most 8 GB ~ a few seconds...
	 	cout << "Rank " << r << " has synchronized distances" << "\n";
	}
	else {
	 	cerr << "Rank " << r << " failed to synchronized distances" << "\n"; localhealth = 0;
	}

	if ( sdm->broadcast_directions() == true ) {
		cout << "Rank " << r << " has synchronized elevation/azimuth directions" << "\n";
	}
	else {
		cerr << "Rank " << r << " failed to synchronize elevation/azimuth directions" << "\n"; localhealth = 0;
	}

	//ttoc = MPI_Wtime();
	//mm = sdm->araullo_tictoc.get_memoryconsumption();
	//sdm->araullo_tictoc.prof_elpsdtime_and_mem( "BroadcastMaterialsData", APT_XX, APT_IS_PAR, mm, ttic, ttoc );

	MPI_Barrier( MPI_COMM_WORLD );

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized the reconstruction, direction, and triangle hull!" << "\n";
		delete sdm; sdm = NULL; return;
	}

	if ( sdm->define_phase_candidates() == true ) {
		cout << "Rank " << r << " has defined the analysis tasks successfully" << "\n";
	}
	else {
		cout << "Rank " << r << " detected inconsistencies during defining analysis tasks!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized the analysis tasks!" << "\n";
		delete sdm; sdm = NULL; return;
	}
	
	//now processes can proceed and process massively parallel and independently
	//MK::each process computes the same grid and sets the same deterministic a priori known work partitioning for now
	sdm->itype_sensitive_spatial_decomposition(); //sdm->spatial_decomposition();

	sdm->define_matpoint_volume_grid();
	
	sdm->distribute_matpoints_on_processes_roundrobin();
	
	sdm->execute_local_workpackage2();
	
	cout << "Rank " << r << " has completed its workpackage" << "\n";

	MPI_Barrier(MPI_COMM_WORLD);
	
	
	ttic = MPI_Wtime();

	if ( sdm->init_target_file() == true ) {
		cout << "Rank " << r << " successfully initialized APTH5 results file" << "\n";
	}
	else {
		cerr << "Rank " << r << " unable to initialize results APTH5 file" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes have synchronized after generation of the target file!" << "\n";
		delete sdm; sdm = NULL; return;
	}

	if ( sdm->get_myrank() == MASTER ) {
		if ( sdm->write_materialpoints_to_apth5() == true ) { //does not require MPI communication all results already on the master
			cout << "Rank " << MASTER << " successfully wrote all materialpoints to an APTH5 file" << "\n";
		}
		else {
			cerr << "Rank " << MASTER << " unable to write all materialpoints to the APTH5 file" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that MASTER was not successful in writing material points to the APTH5 file" << "\n";
		delete sdm; sdm = NULL; return;
	}

	ttoc = MPI_Wtime();
	mm = sdm->araullo_tictoc.get_memoryconsumption();
	sdm->araullo_tictoc.prof_elpsdtime_and_mem( "InitializeH5ResultsFile", APT_XX, APT_IS_SEQ, mm, ttic, ttoc);

	ttic = MPI_Wtime();

	//required phase-specific results groups need to exists before process can populate them!
	MPI_Barrier( MPI_COMM_WORLD );

	//##MK::naive sequential writing of results
	for( int rk = MASTER; rk < sdm->get_nranks(); rk++ ) {
		if ( rk == sdm->get_myrank() ) {
			if ( sdm->write_results_to_apth5() == true ) {
				cout << "Rank " << rk << " successfully wrote all materialpoint-related results to H5 file" << "\n";
			}
			else {
				cerr << "Rank " << rk << " encountered errors when attempting to write all materialpoint-related results!" << "\n"; localhealth = 0;
			}
		}
		MPI_Barrier( MPI_COMM_WORLD );
	}
	
	//check if all process have successfully been able to write their results to the HDF5 file
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes were successful in writing all their material points with results to the H5 file" << "\n";
		delete sdm; sdm = NULL; return;
	}

	//now that all matpoints with existent results have ended up in the file we can communicate across the processes
	//the matpoint IDs for which results exist and write this into the HDF5 file as an I/O hint for subsequent processing
	//to avoid that in these later processing stages one needs to scan and probe first the existence of each of the the possible million
	//matpoint datasets

	if ( sdm->write_shortlist_matpoints_withresults_to_apth5() == true ) {
		cout << "Rank " << r << " has participated in creating ishortlist of all matpoints with results" << "\n";
	}
	else {
		cerr << "Rank " << r << " has recognized that an error occurred while creating the matpoint with results shortlist!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != nr ) {
		cerr << "Rank " << r << " has recognized that not all processes were successful in writing all their material points with results to the H5 file" << "\n";
		delete sdm; sdm = NULL; return;
	}

	ttoc = MPI_Wtime();
	mm = sdm->araullo_tictoc.get_memoryconsumption();
	sdm->araullo_tictoc.prof_elpsdtime_and_mem( "WritingH5ResultsFile", APT_XX, APT_IS_SEQ, mm, ttic, ttoc);

	sdm->araullo_tictoc.spit_profiling( "Araullo", ConfigShared::SimID, sdm->get_myrank() );
	//release resources
	delete sdm; sdm = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	//helloworld( argc, argv );

	if ( init( argc, argv ) == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
	
//EXECUTE SPECIFIC TASK
	many_local_araullo_peters_on_reconstruction( r, nr, argv );
	
//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	double toc = omp_get_wtime();
	
	cout << "paraprobe-araullo took " << (toc-tic) << " seconds wall-clock time in total" << endl;
	return 0;
}
