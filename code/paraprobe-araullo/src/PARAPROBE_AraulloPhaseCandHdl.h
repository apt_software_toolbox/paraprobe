//##MK::CODESPLIT


#ifndef __PARAPROBE_ARAULLO_PHASECANDHDL_H__
#define __PARAPROBE_ARAULLO_PHASECANDHDL_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_AraulloStructs.h"

struct TypeCombi
{
	string targets;
	string realspacestr;
	TypeCombi() : targets(""), realspacestr("") {}
	TypeCombi( const string _trg, const string _rsp ) : targets(_trg), realspacestr(_rsp) {}
};

ostream& operator << (ostream& in, TypeCombi const & val);

struct Evapion3Combi
{
	vector<evapion3> targets;
	apt_real realspace;
	Evapion3Combi() : targets(vector<evapion3>()), realspace(0.0) {}
};


struct UC8Combi
{
	vector<unsigned char> targets;
	apt_real realspace;
	UC8Combi() : targets(vector<unsigned char>()), realspace(0.0) {}
};


/*
string tg;		//string of human-readable element name and molecular ion types
string a;		//lattice plane distance to probe
vector<evapion3> targets;
*/
#define TARGET_LIST_SIZE	4		//it is possible to have at most so many different ion types as target

struct PhaseCandidate
{
	unsigned char targets[TARGET_LIST_SIZE];	//##MK::currently 4*1 Byte
	apt_real realspace;
	unsigned int candid;
	
	PhaseCandidate();
	PhaseCandidate( vector<unsigned char> const & tg, const apt_real _a, const unsigned int _tskid );
};


class itypeCombiHdl
{
	//class which translates human-readable single/molecular ion type combination strings into the internal itype unsigned char
	//format with which internally all ions are analyzed
public:
	itypeCombiHdl();
	~itypeCombiHdl();

	bool load_iontype_combinations( string xmlfn );
	
	vector<TypeCombi> icombis;
	vector<UC8Combi> combinations;
	vector<PhaseCandidate> iphcands;
};


#endif
