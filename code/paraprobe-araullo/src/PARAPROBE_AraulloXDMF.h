//##MK::CODE

#ifndef __PARAPROBE_ARAULLO_XDMF_H__
#define __PARAPROBE_ARAULLO_XDMF_H__

#include "PARAPROBE_AraulloHDF5.h"

class araullo_xdmf : public xdmfHdl
{
	//tool-specific sub-class of a XDMF inheriting all methods of the base class xdmfHdl
	//coordinating instance handling all (sequential) writing to XDMF text file to supplement visualization of HDF5 content

public:
	araullo_xdmf();
	~araullo_xdmf();

	int create_materialpoint_file( const string xmlfn, const size_t nmp, const string h5ref );
	int create_phaseresults_file( const string xmlfn, const size_t ndir, const string h5ref,
			const unsigned int phcandid, vector<unsigned int> const & mpids );

//private:
};

#endif
