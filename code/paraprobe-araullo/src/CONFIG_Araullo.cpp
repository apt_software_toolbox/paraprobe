//##MK::CODE

#include "CONFIG_Araullo.h"

string ConfigAraullo::InputfilePSE = "";
string ConfigAraullo::InputfileReconstruction = "";
//string ConfigAraullo::InputfileHullAndDistances = "";
//string ConfigAraullo::Outputfile = "";
	
VOLUME_SAMPLING_METHOD ConfigAraullo::VolumeSamplingMethod = CUBOIDAL_GRID;
WINDOWING_METHOD ConfigAraullo::WindowingMethod = RECTANGULAR;

apt_real ConfigAraullo::ROIRadiusMax = 0.0;
apt_real ConfigAraullo::SamplingGridBinWidthX = 0.0;
apt_real ConfigAraullo::SamplingGridBinWidthY = 0.0;
apt_real ConfigAraullo::SamplingGridBinWidthZ = 0.0;
string ConfigAraullo::SamplingPosition = "";
unsigned int ConfigAraullo::SDMBinExponent = 8;
apt_real ConfigAraullo::KaiserAlpha = 0.0;
	
bool ConfigAraullo::SamplingGridRemoveBndPoints = false;
bool ConfigAraullo::SDMNormalize = true;

bool ConfigAraullo::IOHistoCnts = false;
bool ConfigAraullo::IOHistoFFTMagn = false;
bool ConfigAraullo::IOSpecificPeaks = false;
bool ConfigAraullo::IOStrongPeaks = false;

//string ConfigAraullo::LatticeDistancesToProbe = "";
//string ConfigAraullo::IontypesToProbe = "";
string ConfigAraullo::InputfileElevAzimGrid = "PARAPROBE.Araullo.GeodesicSphere40962.dat";
unsigned long ConfigAraullo::MaxMemoryPerNode = 0;
unsigned int ConfigAraullo::ProcessesPerMPITeam = 1;
unsigned int ConfigAraullo::GPUsPerNode = 0;
unsigned int ConfigAraullo::GPUPerProcess = 0;
unsigned int ConfigAraullo::GPUWorkloadFactor = 1;
//vector<pair<unsigned int, unsigned int>> ConfigAraullo::ProbeThisBinThisType = vector<pair<unsigned int, unsigned int>>();

//unsigned int ConfigAraullo::NumberOfFFTMagnImages = 0;
unsigned int ConfigAraullo::NumberOfSO2Directions = 0;


bool ConfigAraullo::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Araullo.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigAraullo")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;

	//WHAT TYPE OF ANALYSIS TO DO
	mode = read_xml_attribute_uint32( rootNode, "VolumeSamplingMethod" );
	switch (mode)
	{
		case CUBOIDAL_GRID:
			VolumeSamplingMethod = CUBOIDAL_GRID; break;
		case SINGLE_POINT_USER:
			VolumeSamplingMethod = SINGLE_POINT_USER; break;
		case SINGLE_POINT_CENTER:
			VolumeSamplingMethod = SINGLE_POINT_CENTER; break;
		default:
			cerr << "Unknown VolumeSamplingMethod!" << "\n"; 
			return false;
	}
	
	InputfilePSE = read_xml_attribute_string( rootNode, "InputfilePeriodicTableOfElements" );
	InputfileReconstruction = read_xml_attribute_string( rootNode, "InputfileReconstruction" );
	//InputfileHullAndDistances = read_xml_attribute_string( rootNode, "InputfileHullAndDistances" );
	//Outputfile = read_xml_attribute_string( rootNode, "Outputfile" );
	
	ROIRadiusMax = read_xml_attribute_float( rootNode, "ROIRadiusMax" );
	switch (VolumeSamplingMethod) {
		case CUBOIDAL_GRID:
			SamplingGridBinWidthX = read_xml_attribute_float( rootNode, "SamplingGridBinWidthX" );
			SamplingGridBinWidthY = read_xml_attribute_float( rootNode, "SamplingGridBinWidthY" );
			SamplingGridBinWidthZ = read_xml_attribute_float( rootNode, "SamplingGridBinWidthZ" );
			break;
		case SINGLE_POINT_USER:
			SamplingPosition = read_xml_attribute_string( rootNode, "SamplingPosition" );
			break;
		default:
			return false;
	}
	
	SDMBinExponent = read_xml_attribute_uint32( rootNode, "SDMBinExponent" );
	SDMNormalize = read_xml_attribute_bool( rootNode, "SDMNormalize" );
	
	mode = read_xml_attribute_uint32( rootNode, "WindowingMethod" );
	switch (mode)
	{
		case RECTANGULAR:
			WindowingMethod = RECTANGULAR; break;
		case KAISER:
			WindowingMethod = KAISER; break;			
		default:
			WindowingMethod = RECTANGULAR;
			cout << "WARNING: An invalid windowing method was specified we use the default, rectangular windowing!" << "\n";
	}
	
	if ( WindowingMethod == KAISER ) {
		KaiserAlpha = read_xml_attribute_float( rootNode, "KaiserAlpha" );
	}
	
	IOHistoCnts = read_xml_attribute_bool( rootNode, "IOStoreHistoCnts" );
	IOHistoFFTMagn = read_xml_attribute_bool( rootNode, "IOStoreHistoFFTMagn" );
	IOSpecificPeaks = read_xml_attribute_bool( rootNode, "IOStoreSpecificPeaks" );
	IOStrongPeaks = read_xml_attribute_bool( rootNode, "IOStoreThreeStrongestPeaks" );
	//##MK::some are not necessary

	//LatticeDistancesToProbe = read_xml_attribute_string( rootNode, "LatticeDistancesToProbe" );
	//IontypesToProbe = read_xml_attribute_string( rootNode, "IontypesToProbe" );
	
	InputfileElevAzimGrid = read_xml_attribute_string( rootNode, "InputfileElevAzimGrid" );
	MaxMemoryPerNode = read_xml_attribute_uint64( rootNode, "MaxMemoryPerNode" );
	//##MK::replace by system call at some point
	//https://stackoverflow.com/questions/349889/how-do-you-determine-the-amount-of-linux-system-ram-in-c
	//https://stackoverflow.com/questions/5553665/get-ram-system-size
	ProcessesPerMPITeam = read_xml_attribute_uint32( rootNode, "ProcessesPerMPITeam" );
	GPUsPerNode = read_xml_attribute_int32( rootNode, "GPUsPerComputingNode" );
	GPUPerProcess = read_xml_attribute_uint32( rootNode, "GPUPerProcess" );
	GPUWorkloadFactor = read_xml_attribute_uint32( rootNode, "GPUWorkloadFactor" );
	
	SamplingGridRemoveBndPoints = false;

	return true;
}


bool ConfigAraullo::checkUserInput()
{
	cout << "ConfigAraullo::" << "\n";
	switch (VolumeSamplingMethod)
	{
		case CUBOIDAL_GRID:
			cout << "Building voxel volume grid at positions displaced from the specimen main axis" << "\n"; break;
		case SINGLE_POINT_USER:
			cout << "Building a single point user-specified at " << SamplingPosition << "\n"; break;
		case SINGLE_POINT_CENTER:
			cout << "Building a single point in the center of a specimen bounding box " << "\n"; break;
		default:
			cerr << "Unknown Volume sampling method chosen" << "\n";
			return false;
	}
	
	if ( ROIRadiusMax < EPSILON ) {
		cerr << "ROIRadiusMax must be positive and finite!" << "\n"; return false;
	}
	cout << "ROIRadiusMax " << ROIRadiusMax << "\n";
	if ( VolumeSamplingMethod == CUBOIDAL_GRID ) {
		if ( SamplingGridBinWidthX < EPSILON ) {
			cerr << "SamplingGridBinWidthX needs to be positive and finite!" << "\n"; return false;
		}
		cout << "SamplingGridBinWidthX " << SamplingGridBinWidthX << "\n";
		if ( SamplingGridBinWidthY < EPSILON ) {
			cerr << "SamplingGridBinWidthY needs to be positive and finite!" << "\n"; return false;
		}
		cout << "SamplingGridBinWidthY " << SamplingGridBinWidthY << "\n";
		if ( SamplingGridBinWidthZ < EPSILON ) {
			cerr << "SamplingGridBinWidthY needs to be positive and finite!" << "\n"; return false;
		}
		cout << "SamplingGridBinWidthZ " << SamplingGridBinWidthZ << "\n";
	}
	if ( VolumeSamplingMethod == SINGLE_POINT_USER ) {
		stringstream parsethis;
		parsethis << SamplingPosition;
		string datapiece;
		size_t nsemicolon = count( SamplingPosition.begin(), SamplingPosition.end(), ';' );
		if ( nsemicolon != 2 ) {
			cerr << "SamplingPosition has invalid format must be x;y;z!" << "\n"; return false;
		}
		cout << "SamplingPosition " << SamplingPosition << "\n";
	}
		
	if ( SDMBinExponent < 8 || SDMBinExponent > 12 ) {
		cerr << "SDMBinExponent should be integer in-between [8, 12]!" << "\n"; return false;
	}
	cout << "SDMBinExponent " << SDMBinExponent << "\n";
	cout << "SDMNormalize " << SDMNormalize << "\n";

	switch (WindowingMethod)
	{
		case RECTANGULAR:
			cout << "Rectangular windowing will be used" << "\n"; break;
		case KAISER:
			cout << "Kaiser windowing will be used!" << "\n";
		break;
		default:
			return false;
	}
	if ( WindowingMethod == KAISER && KaiserAlpha < EPSILON ) {
		cerr << "KaiserAlpha constant must be positive and finite!" << "\n"; return false;
	}
	
	cout << "IOHistoCnts " << IOHistoCnts << "\n";
	cout << "IOHistoFFTMagn " << IOHistoFFTMagn << "\n";
	cout << "IOSpecificPeak " << IOSpecificPeaks << "\n";
	cout << "IOStrongPeaks " << IOStrongPeaks << "\n";

	/*
	cout << "LatticeDistancesToProbe " << LatticeDistancesToProbe << "\n";
	cout << "IontypesToProbe " << IontypesToProbe << "\n";
	stringstream parsethis1;
	parsethis1 << LatticeDistancesToProbe;
	string datapiece1;
	stringstream parsethis2;
	parsethis2 << IontypesToProbe;
	string datapiece2;
	apt_real nbins_half = pow(	static_cast<apt_real>(2), static_cast<apt_real>(SDMBinExponent-1) );
	unsigned int nbins = static_cast<unsigned int>(nbins_half + 0.5); //to assure that bins are on [0,2^(m-1)) symmetric about ROI
	size_t nsemicolon1 = count( LatticeDistancesToProbe.begin(), LatticeDistancesToProbe.end(), ';' );
	size_t nsemicolon2 = count( IontypesToProbe.begin(), IontypesToProbe.end(), ';' );
	//https://stackoverflow.com/questions/8888748/how-to-check-if-given-c-string-or-char-contains-only-digits
	if ( LatticeDistancesToProbe.size() < 1 ) {
		cerr << "No LatticeDistancesToProbe were passed!" << "\n"; return false;
	}
	if ( IontypesToProbe.size() < 1 ) {
		cerr << "No IontypesToProbe were passed!" << "\n"; return false;
	}
	if ( nsemicolon1 != nsemicolon2 ) {
		cerr << "Not for every LatticeDistance there is an IontypeToProbe which is forbidden!" << "\n"; return false;
	}
cout << "LatticesDistances parsed on interval [ 0, " << nbins << "), images desired " << 1+nsemicolon1 << " IontypesToProbe " << 1+nsemicolon2 << "\n";

	unsigned int nimages = 0;
	for( size_t i = 0; i < nsemicolon1 + 1; i++ ) { //parse at least the single value
		getline( parsethis1, datapiece1, ';');
		apt_real val = (stof(datapiece1) / ROIRadiusMax) * nbins_half; //val/ROIRadiusMax*2^(SDMBinExponent-1)
		unsigned int bin = static_cast<unsigned int>(val + 0.5); //##addition plus 1 ?
		getline( parsethis2, datapiece2, ';');
		unsigned int iontype = atoi(datapiece2.c_str());
		//##MK::add type check
		if ( bin < nbins_half ) {
			ProbeThisBinThisType.push_back( pair<unsigned int, unsigned int>(bin,iontype)  );
			nimages++;
cout << "Image " << i << "\t\t" << stof(datapiece1) << ";" << iontype << "\t\t" << val <<
				"\t\t" << ProbeThisBinThisType.back().first << ";" << ProbeThisBinThisType.back().second << "\n";
		}
	}
	//
	//https://www.geeksforgeeks.org/sorting-vector-of-pairs-in-c-set-1-sort-by-first-and-second/
	sort( ProbeThisBinThisType.begin(), ProbeThisBinThisType.end() ); //will sort in ascending order by default for bin type

	for( auto it = ProbeThisBinThisType.begin(); it != ProbeThisBinThisType.end(); it++ ) {
		cout << "Image of FFT magnitude values compiled over direction array for bin " << it->first << ";" << it->second << "\n";
	}
	NumberOfFFTMagnImages = nimages;
	cout << "NumberOfFFTMagnImages " << NumberOfFFTMagnImages << "\n";
	*/
	
	cout << "InputfilePeriodicTableOfElements read from " << InputfilePSE << "\n";
	cout << "InputfileReconstruction read from " << InputfileReconstruction << "\n";
	//cout << "InputHullsAndDistances read from " << InputfileHullAndDistances << "\n";
	//cout << "Output written to " << Outputfile << "\n";
	
	cout << "InputElevAzimGrid read from " << InputfileElevAzimGrid << "\n";
	if ( MaxMemoryPerNode > 0 ) {
		cout << "MaxMemoryPerNode " << MaxMemoryPerNode << "\n"; 
	}
	else {
		cerr << "MaxMemoryPerNode is needs to be finite" << "\n"; return false;
	}
	cout << "ProcessesPerMPITeam " << ProcessesPerMPITeam << "\n";
	cout << "GPUsPerNode " << GPUsPerNode << "\n";
	cout << "GPUPerProcess " << GPUPerProcess << "\n";
	cout << "GPUWorkloadFactor " << GPUWorkloadFactor << "\n";
	
	if ( SamplingGridRemoveBndPoints == true ) {
		cout << "Material points outside the inside voxel or within closer than ROIRadiusMax to the triangle hull are discarded!" << "\n";
	}
	else {
		cout << "WARNING finite dataset boundary affects are not accounted for so some sampling points will have insignificant ion support!" << "\n";
	}
	
	cout << "\n";
	return true;
}

