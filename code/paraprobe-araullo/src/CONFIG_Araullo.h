//##MK::CODE

#ifndef __PARAPROBE_CONFIG_ARAULLO_H__
#define __PARAPROBE_CONFIG_ARAULLO_H__

//#include "../../paraprobe-utils/src/CONFIG_Shared.h"
//#include "../../paraprobe-utils/src/PARAPROBE_VolumeSampler.h"
//#include "../../paraprobe-utils/src/PARAPROBE_CPUGPUWorkloadStructs.h"
#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"


enum VOLUME_SAMPLING_METHOD {
	CUBOIDAL_GRID,
	SINGLE_POINT_USER,
	SINGLE_POINT_CENTER
};


enum WINDOWING_METHOD {
	RECTANGULAR,
	KAISER
};


class ConfigAraullo
{
public:
	
	static string InputfilePSE;
	static string InputfileReconstruction;
	//static string InputfileHullAndDistances;
	//static string Outputfile;
		
	static VOLUME_SAMPLING_METHOD VolumeSamplingMethod;
	static WINDOWING_METHOD WindowingMethod;
	
	static apt_real ROIRadiusMax;
	static apt_real SamplingGridBinWidthX;
	static apt_real SamplingGridBinWidthY;
	static apt_real SamplingGridBinWidthZ;
	static string SamplingPosition;
	static unsigned int SDMBinExponent;
	static apt_real KaiserAlpha;
		
	static bool SamplingGridRemoveBndPoints;
	static bool SDMNormalize;
	
	static bool IOHistoCnts;
	static bool IOHistoFFTMagn;
	static bool IOSpecificPeaks;
	static bool IOStrongPeaks;
	
	//static string LatticeDistancesToProbe;
	//static string IontypesToProbe;
	
	static string InputfileElevAzimGrid;
	static unsigned long MaxMemoryPerNode;
	static unsigned int ProcessesPerMPITeam;
	static unsigned int GPUsPerNode;
	static unsigned int GPUPerProcess;
	static unsigned int GPUWorkloadFactor;
	
	//internals
	//static vector<pair<unsigned int,unsigned int>> ProbeThisBinThisType;
	//static unsigned int NumberOfFFTMagnImages;
	static unsigned int NumberOfSO2Directions;
	

	static bool readXML( string filename = "" );
	static bool checkUserInput();
};

#endif
