//##MK::CODESPLIT

#include "PARAPROBE_AraulloAcquisor.h"


araullo_res::araullo_res()
{
	owner = NULL;
	mp = p3d(0.f, 0.f, 0.f); //, 0);
	//phcandid = UINT32MX;

	ft.init_plan( ConfigAraullo::SDMBinExponent );

	tictoc[TICTOC_IONSINROI] = 0.0;
	tictoc[TICTOC_THREADID] = 0.0;
	tictoc[TICTOC_PROJECT] = 0.0;
	tictoc[TICTOC_FFT] = 0.0;
	tictoc[TICTOC_FINDPKS] = 0.0;
	//tictoc = {0.0, 0.0, 0.0, 0.0, 0.0};
}


araullo_res::~araullo_res()
{
	SpecificPeaksTbl = vector<float>();
	ThreePeaksTbl = vector<float>();

	//IV = vector<iontype_ival>();
	DiffVecNeighbors = vector<p3d>();
	//FFTBinsTbl = vector<float>();
	SDMHistoTblU32 = vector<unsigned int>();
	SDMHistoTblF32 = vector<float>();

	//##MK::do not dereference the own only backreference!
}


void araullo_res::project_cpu( vector<p3d> const & cand, vector<elaz> const & dir )
{
	double mytic = omp_get_wtime();

	//only a single phase candidate and only positions of ions with itypes specific for the PhaseCandidate
	//allows as such to probe for planes that are composed of specific ions like in Perovskites
	//for each FFT magnitude spherical image to acquire/measure/compute sort the respective iontypes needed
	DiffVecNeighbors.reserve( cand.size() );
	for( auto it = cand.begin(); it != cand.end(); it++ ) {
		DiffVecNeighbors.push_back( p3d(it->x - mp.x, it->y - mp.y, it->z - mp.z) );
	}

	//actual projection elevation/azimuth loop nest
	binning_info ifo = owner->cfg;
	vector<float> const & hklpos = owner->reciprocal_pos;
	vector<float> const & wdwnrm = owner->window_coeff;

	//one image only because specific for phasecandidate owner->phcandid;
	size_t nso2 = ConfigAraullo::NumberOfSO2Directions;
	size_t nbins = ifo.NumberOfBins;
	bool normalize = ConfigAraullo::SDMNormalize;

	//initialize empty table of unsigned int and float histograms
	SDMHistoTblU32 = vector<unsigned int>( nso2 * nbins, 0 );
	//eventually 40962*4096*4B = 672MB/(thread*matpoint), typically 1*40962*1024*4B = 168MB/(thread*matpoint), nbins changes fastest, nimg slowest
	SDMHistoTblF32 = vector<float>( nso2 * nbins, 0.f );

	//elevation/azimuth nested main loop
	if ( DiffVecNeighbors.size() > 0 ) {
		size_t roffset = 0;
		for( size_t ea = 0; ea < nso2; ea++ ) {
			float e = dir[ea].elevation;
			float a = dir[ea].azimuth;
			//for( apt_real e = Settings::ElevationAngleMin; e <= Settings::ElevationAngleMax; e += Settings::ElevationAngleIncr ) {
			float sin_e = sin(e);
			float cos_e = cos(e);
			//for ( apt_real a = Settings::AzimuthAngleMin; a <= Settings::AzimuthAngleMax; a += Settings::AzimuthAngleIncr ) {
			float sin_a = sin(a);
			float cos_a = cos(a);
			/*
			//old: clockwise about z start at x with beta complementary from el = 0 when aligned with z axis tilting into xy plane!
				float se_ca = +1.f*sin_e*cos_a;
				float se_sa = -1.f*sin_e*sin_a;
				float ce = +1.f*cos_e;
			*/
			//new:geographic definition active rotation about azimuth counter-clockwise with az = 0 beginning at x axis, right handed CS
			//new:elevation from xy plane upwards positive
			float ca_ce = 	cos_a * cos_e;
			float sa_ce = 	sin_a * cos_e;
			float se = 		sin_e;

			roffset = ea*nbins;
			for( auto jt = DiffVecNeighbors.begin(); jt != DiffVecNeighbors.end(); jt++ ) {

				//histogram of projected signed distance of every point to current plane
				//given the current plane normal vector parameterized through elevation and azimuth
				/*
					//old:
					float d = se_ca*kt->u + se_sa*kt->v + ce*kt->w;
				*/
				//new:
				float d = ca_ce * jt->x + sa_ce * jt->y + se * jt->z;
				float b = floor((d + ifo.RdR)*ifo.binner);

				unsigned int bin = static_cast<unsigned int>(b);
				//int bin = static_cast<int>(b);
				//collect histogram, quasi random writes into specific row of the implicit 2D array
				SDMHistoTblU32[roffset+bin]++;
			} //next candidate
		} //next direction, imagine that we successively populate the histogram rows of SDMHistoTblU32

		//translate histogram to floating point values and potentially normalize
		roffset = 0;
		float NormalizingMultiplier = 1.f;
		if ( normalize == true ) {
			NormalizingMultiplier = 1.f / static_cast<float>(DiffVecNeighbors.size());
		}
		for( size_t ea = 0; ea < nso2; ea++ ) {
			roffset = ea*nbins;
			for( size_t b = 1; b < ifo.NumberOfBins-1; b++ ) {
				SDMHistoTblF32.at(roffset+b) = NormalizingMultiplier * wdwnrm[b] * static_cast<float>(SDMHistoTblU32.at(roffset+b));
			} //window the bins
		} //next direction
	}
	//else nions == 0, no need to normalize or window anything because all histogram cnts are 0

	//earliest point when to release this temporary
	if ( ConfigAraullo::IOHistoCnts == false ) {
		SDMHistoTblU32 = vector<unsigned int>();
	}

	double mytoc = omp_get_wtime();
	tictoc[TICTOC_PROJECT] = mytoc-mytic;
	/*#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " projections executed in " << (mytoc-mytic) << " seconds DiffVecNeighbors.size() " << DiffVecNeighbors.size() << "\n";
	}*/

	//earliest point when the DiffVecNeighbors can be cleared
	DiffVecNeighbors = vector<p3d>();
}


void araullo_res::fft_cpu()
{
	double mytic = omp_get_wtime();

	//actual fast Fourier transform 1d real to complex elevation/azimuth loop nest
	binning_info ifo = owner->cfg;
	size_t nso2 = ConfigAraullo::NumberOfSO2Directions;
	size_t nbins = ifo.NumberOfBins;

	ft.init_plan( ConfigAraullo::SDMBinExponent );

	//##MK::replace with IMKL batched 1d FFT and MKL_NUM_THREADS 1

	//elevation/azimuth nested main loop
	size_t rwoffset = 0;
	for( size_t ea = 0; ea < nso2; ea++ ) {
		rwoffset = ea*nbins;
		ft.create_plan( SDMHistoTblF32, rwoffset );
		ft.execute_fwfft_report_magnitude( SDMHistoTblF32, rwoffset );
	}

	double mytoc = omp_get_wtime();
	tictoc[TICTOC_FFT] = mytoc-mytic;
	/*#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " Fourier transforms executed in " << (mytoc-mytic) << " seconds" << "\n";
	}*/
}


void araullo_res::findpks_cpu( const float spec_peak_position )
{
	double mytic = omp_get_wtime();

	//actual projection elevation/azimuth loop nest
	binning_info ifo = owner->cfg;
	vector<float> const & hklpos = owner->reciprocal_pos;
	size_t nso2 = ConfigAraullo::NumberOfSO2Directions;
	size_t nbins = ifo.NumberOfBins;
	size_t nbins2 = ifo.NumberOfBinsHalf;

	if ( ConfigAraullo::IOSpecificPeaks == true ) {
		SpecificPeaksTbl = vector<float>( nso2 * 1, 0.f ); //the specific peak at Fourier bin one planned for this PhaseCandidate!
	}

	if ( ConfigAraullo::IOStrongPeaks == true ) {
		//three peaks are useful to tell how much long-range periodicity in the 1d SDM is there at all which could be used
		//as a descriptor for local crystallinity because periodicity is ideal if windowed FFT and pristine lattice positions
		//however if spatial noise or missing ions periodicity is noised
		//0-th Fourier peak gives ion count within the dataset, second and third tell if any other
		//if 1-th peak is close to 0-th then binning is too fine
		ThreePeaksTbl = vector<float>( nso2 * PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX, 0.f ); //threepeaks()
	}

	size_t roffset = 0;
	size_t woffset = 0;

	float b = floor((spec_peak_position + ifo.RdR)*ifo.binner);
	//int bin = static_cast<int>(b);
	unsigned int bin = static_cast<unsigned int>(b); //##MK::########## ConfigAraullo::ProbeThisBinThisType.at(img).first;

	for( size_t ea = 0; ea < nso2; ea++ ) { //find specific value
		roffset = ea*nbins;
		woffset = ea*1;

		if ( ConfigAraullo::IOSpecificPeaks == true ) {
			SpecificPeaksTbl.at(woffset+0) = SDMHistoTblF32.at(roffset+bin);
		}

		if ( ConfigAraullo::IOStrongPeaks == true ) {
			//find three strongest peaks on [0,NumberOfBinsHalf), NumberOfBinsHalf is guaranteed = pow(2, (Settings::CrystalloHistoM-1) )
			//##MK::first shot, O(n) finding of all clear localmaxima, first/last value checked separately to avoid if inside for loop,
			vector<localmaximum> peaks;
			roffset = ea*nbins;
			//first bin a peak?
			if ( SDMHistoTblF32.at(roffset+0) > SDMHistoTblF32.at(roffset+1) ) {
				peaks.push_back( localmaximum( hklpos[0], SDMHistoTblF32.at(roffset+0) ) ); //frequently the case 0-freq peak
			}
			//peaks somewhere in the middle, ##MK::divide and conquer for better than O(n) complexity
			for ( size_t b = 1; b < nbins2-1; b++ ) {
				if ( SDMHistoTblF32.at(roffset+b-1) >= SDMHistoTblF32.at(roffset+b) )
					continue;
				if ( SDMHistoTblF32.at(roffset+b) <= SDMHistoTblF32.at(roffset+b+1) )
					continue;
				//not continued
				peaks.push_back( localmaximum( hklpos[b], SDMHistoTblF32.at(roffset+b) ) );
			}
			//last bin a peak?
			if ( SDMHistoTblF32.at(roffset+nbins2-2) < SDMHistoTblF32.at(roffset+nbins2-1) ) {
				peaks.push_back( localmaximum( hklpos[nbins2-1], SDMHistoTblF32.at(roffset+nbins2-1) ) );
			}

			//lastly find the three strongest peaks
			//MK::a naive full sort on peaks has O(NlgN) time complexity, consider multiply dhistogram with -1.f and do nth_element partial sort
			sort( peaks.begin(), peaks.end(), SortLocalMaximaForStrength ); //ascending orderr

			//evaluate 3 strongest peaks of current transform and report as result
			size_t npks = peaks.size();

			size_t woffset = ea*PARAPROBE_ARAULLO_RES_THREEPEAK_NCMAX;
			if ( npks >= 3) {
				ThreePeaksTbl.at(woffset+5) = peaks[npks-3].strength; //3th strongest maximum
				ThreePeaksTbl.at(woffset+4) = peaks[npks-3].position;
				ThreePeaksTbl.at(woffset+3) = peaks[npks-2].strength; //2th strongest maximum
				ThreePeaksTbl.at(woffset+2) = peaks[npks-2].position;
				ThreePeaksTbl.at(woffset+1) = peaks[npks-1].strength; //1th strongest maximum, sum of ions if not normalized
				ThreePeaksTbl.at(woffset+0) = peaks[npks-1].position;
			}
		}
	}

	//earliest point where SDMHistoTblF32 maybe release no temporaries
	if ( ConfigAraullo::IOHistoFFTMagn == false ) {
		SDMHistoTblF32 = vector<float>();
	}

	double mytoc = omp_get_wtime();
	tictoc[TICTOC_FINDPKS] = mytoc-mytic;
	/*#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " found peaks executed in " << (mytoc-mytic) << " seconds" << "\n";
	}*/
}


#ifdef UTILIZE_GPUS
void araullo_res::project_gpu1( vector<p3d> const & cand, vector<elaz> const & dir )
{
	double mytic = omp_get_wtime();

	//only a single phase candidate and only positions of ions with itypes specific for the PhaseCandidate
	//allows as such to probe for planes that are composed of specific ions like in Perovskites
	//for each FFT magnitude spherical image to acquire/measure/compute sort the respective iontypes needed
	//use GPU buffer directly instead of DiffVecNeighbors
	float* DiffVecNeighborsGPU = NULL;
	int nnbors = static_cast<int>(cand.size());
	DiffVecNeighborsGPU = new float[3*nnbors];
	size_t i = 0;
	for( auto it = cand.begin(); it != cand.end(); it++, i += 3 ) {
		DiffVecNeighborsGPU[i+0] = it->x - mp.x;
		DiffVecNeighborsGPU[i+1] = it->y - mp.y;
		DiffVecNeighborsGPU[i+2] = it->z - mp.z;
	}

	//actual projection elevation/azimuth loop nest
	binning_info ifo = owner->cfg;
	vector<float> const & hklpos = owner->reciprocal_pos;
	vector<float> const & wdwnrm = owner->window_coeff;

	//one image only because specific for phasecandidate owner->phcandid;
	//##MK::int instead of size_t on GPU !
	int nso2 = static_cast<int>(ConfigAraullo::NumberOfSO2Directions);
	int nbins = static_cast<int>(ifo.NumberOfBins);
	bool normalize = ConfigAraullo::SDMNormalize;

	//initialize empty table of unsigned int and float histograms
	SDMHistoTblU32 = vector<unsigned int>( nso2 * nbins, 0 );
	//eventually 40962*4096*4B = 672MB/(thread*matpoint), typically 1*40962*1024*4B = 168MB/(thread*matpoint), nbins changes fastest, nimg slowest
	SDMHistoTblF32 = vector<float>( nso2 * nbins, 0.f );

	//hyperphobic implementation
	float* DirGPU = NULL;
	DirGPU = new float[2*nso2];
	i = 0;
	for( auto it = dir.begin(); it != dir.end(); it++, i += 2 ) {
		DirGPU[i+0] = it->elevation;
		DirGPU[i+1] = it->azimuth;
	}

	unsigned int* SDMHistoTblU32GPU = NULL;
	SDMHistoTblU32GPU = SDMHistoTblU32.data();
	float* SDMHistoTblF32GPU = NULL;
	SDMHistoTblF32GPU = SDMHistoTblF32.data();

	if ( cand.size() > 0 ) {
		//elevation/azimuth nested main loop
		#pragma acc data copy(SDMHistoTblU32GPU[0:nso2*nbins]) async
		{
			int inb;
			int nnbors;
			float gpuRdR = ifo.RdR;
			float gpuBinner = ifo.binner;
			int n0 = 3*nnbors;

			#pragma acc parallel loop copyin(DirGPU[0:2*nso2],DiffVecNeighborsGPU[0:n0]) present (SDMHistoTblU32GPU[0:nso2*nbins]) async private(inb,n0,gpuRdR,gpuBinner)
			for( int ea = 0; ea < nso2; ea++ ) {
				float e = DirGPU[2*ea+0];
				float a = DirGPU[2*ea+1];
				//for( apt_real e = Settings::ElevationAngleMin; e <= Settings::ElevationAngleMax; e += Settings::ElevationAngleIncr ) {
				float sin_e = sin(e);
				float cos_e = cos(e);
				//for ( apt_real a = Settings::AzimuthAngleMin; a <= Settings::AzimuthAngleMax; a += Settings::AzimuthAngleIncr ) {
				float sin_a = sin(a);
				float cos_a = cos(a);
				/*
				//old: clockwise about z start at x with beta complementary from el = 0 when aligned with z axis tilting into xy plane!
					float se_ca = +1.f*sin_e*cos_a;
					float se_sa = -1.f*sin_e*sin_a;
					float ce = +1.f*cos_e;
				*/
				//new:geographic definition active rotation about azimuth counter-clockwise with az = 0 beginning at x axis, right handed CS
				//new:elevation from xy plane upwards positive
				float ca_ce = 	cos_a * cos_e;
				float sa_ce = 	sin_a * cos_e;
				float se = 		sin_e;

				//int roffset = ea*nbins;
				for( inb = 0; inb < n0; inb += 3 ) {
					//histogram of projected signed distance of every point to current plane
					//given the current plane normal vector parameterized through elevation and azimuth
					/*
						//old:
						float d = se_ca*kt->u + se_sa*kt->v + ce*kt->w;
					 */
					//new:
					float d = ca_ce * DiffVecNeighborsGPU[inb+0] + sa_ce * DiffVecNeighborsGPU[inb+1] + se * DiffVecNeighborsGPU[inb+2];
					float b = floor((d + gpuRdR)*gpuBinner); //##MK::maybe floorf

					unsigned int bin = b;
					//int bin = static_cast<int>(b);
					//collect histogram, quasi random writes into specific row of the implicit 2D array

					//##MK::complex loop carried write dependence prevents effective parallelization of this part
					SDMHistoTblU32GPU[ea*nbins+bin]++;
				}
			} //next candidate
		} //next direction, imagine that we successively populate the histogram rows of SDMHistoTblU32
		//destroyed SDMHistoTblU32GPU on the device
		//pragma acc data copy allocates memory on GPU and copies data from host to GPU when entering
		//the region and copies the data to the host when exiting region

		//##MK::normalization part done on the CPU, translate histogram to floating point values and potentially normalize
		size_t roffset = 0;
		float NormalizingMultiplier = 1.f;
		if ( normalize == true ) {
			NormalizingMultiplier = 1.f / static_cast<float>(DiffVecNeighbors.size());
		}
		for( size_t ea = 0; ea < nso2; ea++ ) {
			roffset = ea*nbins;
			for( size_t b = 1; b < ifo.NumberOfBins-1; b++ ) {
				SDMHistoTblF32.at(roffset+b) = NormalizingMultiplier * wdwnrm[b] * static_cast<float>(SDMHistoTblU32.at(roffset+b));
			} //windowing of the bins
		} //next direction
	}
	//else nions == 0, no need to normalize or window anything because all histogram cnts are 0

	//earliest point when to release this temporary
	if ( ConfigAraullo::IOHistoCnts == false ) {
		SDMHistoTblU32 = vector<unsigned int>();
	}

	delete DiffVecNeighborsGPU; //DiffVecNeighborsGPU = NULL;
	delete DirGPU; //DirGPU = NULL;
	//##MK::do not delete SDMHistoTblU32GPU and SDMHistoTblF32GPU only pointers to data !

	double mytoc = omp_get_wtime();
	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " projections gpu executed in " << (mytoc-mytic) << " seconds" << "\n";
	}
}


void araullo_res::project_gpu2( vector<p3d> const & cand, vector<elaz> const & dir )
{
	double mytic = omp_get_wtime();

	//only a single phase candidate and only positions of ions with itypes specific for the PhaseCandidate
	//allows as such to probe for planes that are composed of specific ions like in Perovskites
	//for each FFT magnitude spherical image to acquire/measure/compute sort the respective iontypes needed
	//use GPU buffer directly instead of DiffVecNeighbors
	float* DiffVecNeighborsGPU = NULL;
	int nnbors = static_cast<int>(cand.size());
	int n0 = 3*nnbors;
	DiffVecNeighborsGPU = new float[n0];
	size_t i = 0;
	for( auto it = cand.begin(); it != cand.end(); it++, i += 3 ) {
		DiffVecNeighborsGPU[i+0] = it->x - mp.x;
		DiffVecNeighborsGPU[i+1] = it->y - mp.y;
		DiffVecNeighborsGPU[i+2] = it->z - mp.z;
	}

	//actual projection elevation/azimuth loop nest
	binning_info ifo = owner->cfg;
	vector<float> const & hklpos = owner->reciprocal_pos;
	vector<float> const & wdwnrm = owner->window_coeff;

	//one image only because specific for phasecandidate owner->phcandid;
	//##MK::int instead of size_t on GPU !
	int nso2 = static_cast<int>(ConfigAraullo::NumberOfSO2Directions);
	int nbins = static_cast<int>(ifo.NumberOfBins);
	bool normalize = ConfigAraullo::SDMNormalize;

	//initialize empty table of unsigned int and float histograms
	SDMHistoTblU32 = vector<unsigned int>( nso2 * nbins, 0 );
	//eventually 40962*4096*4B = 672MB/(thread*matpoint), typically 1*40962*1024*4B = 168MB/(thread*matpoint), nbins changes fastest, nimg slowest
	SDMHistoTblF32 = vector<float>( nso2 * nbins, 0.f );

	//create a temporary buffer where the thread can dump its counts for a particular bin
	unsigned int* BinPositions = NULL;
	BinPositions = new unsigned int[nnbors];
	for ( i = 0; i < nnbors; i++ ) {
		BinPositions[i] = 0;
	}

	unsigned int* SDMHistoTblU32GPU = NULL;
	SDMHistoTblU32GPU = SDMHistoTblU32.data();

	if ( cand.size() > 0 ) {
		//elevation/azimuth nested main loop
		#pragma acc data copy(SDMHistoTblU32GPU[0:nso2*nbins],DiffVecNeighborsGPU[0:n0],BinPositions[0:nnbors])
		{
			int inb;
			int nnbors;
			float gpuRdR = ifo.RdR;
			float gpuBinner = ifo.binner;

			for( int ea = 0; ea < nso2; ea++ ) {
				float e = dir[ea].elevation;
				float a = dir[ea].azimuth;
				//for( apt_real e = Settings::ElevationAngleMin; e <= Settings::ElevationAngleMax; e += Settings::ElevationAngleIncr ) {
				float sin_e = sin(e);
				float cos_e = cos(e);
				//for ( apt_real a = Settings::AzimuthAngleMin; a <= Settings::AzimuthAngleMax; a += Settings::AzimuthAngleIncr ) {
				float sin_a = sin(a);
				float cos_a = cos(a);
				/*
				//old: clockwise about z start at x with beta complementary from el = 0 when aligned with z axis tilting into xy plane!
					float se_ca = +1.f*sin_e*cos_a;
					float se_sa = -1.f*sin_e*sin_a;
					float ce = +1.f*cos_e;
				*/
				//new:geographic definition active rotation about azimuth counter-clockwise with az = 0 beginning at x axis, right handed CS
				//new:elevation from xy plane upwards positive
				float ca_ce = 	cos_a * cos_e;
				float sa_ce = 	sin_a * cos_e;
				float se = 		sin_e;

				int roffset = ea*nbins;
				#pragma acc parallel loop present (SDMHistoTblU32GPU[0:nso2*nbins],DiffVecNeighborsGPU[0:n0],BinPositions[0:nnbors]) private(inb,n0,gpuRdR,gpuBinner)
				for( inb = 0; inb < n0; inb += 3 ) {
					//histogram of projected signed distance of every point to current plane
					//given the current plane normal vector parameterized through elevation and azimuth
					/*
						//old:
						float d = se_ca*kt->u + se_sa*kt->v + ce*kt->w;
					 */
					//new:
					float d = ca_ce * DiffVecNeighborsGPU[inb+0] + sa_ce * DiffVecNeighborsGPU[inb+1] + se * DiffVecNeighborsGPU[inb+2];
					float b = floor((d + gpuRdR)*gpuBinner); //##MK::maybe floorf

					unsigned int bin = b;
					//int bin = static_cast<int>(b);
					//collect histogram, quasi random writes into specific row of the implicit 2D array

					//##MK::complex loop carried write dependence prevents effective parallelization of this part
					BinPositions[inb/3] = 0;
					BinPositions[inb/3] = bin;
				}
				#pragma acc update host(BinPositions[0:nnbors])
				//#pragma acc wait

				for( int i = 0; i < nnbors; i++ ) {
					unsigned int bin = BinPositions[i];
					BinPositions[i] = 0;
					SDMHistoTblU32[roffset+bin]++;
				}
			} //next candidate
		} //next direction, imagine that we successively populate the histogram rows of SDMHistoTblU32
		//destroys SDMHistoTblU32GPU,DiffVecNeighborsGPU,BinPositions on the device
		//pragma acc data copy allocates memory on GPU and copies data from host to GPU when entering
		//the region and copies the data to the host when exiting region

		//##MK::normalization part done on the CPU, translate histogram to floating point values and potentially normalize
		size_t roffset = 0;
		float NormalizingMultiplier = 1.f;
		if ( normalize == true ) {
			NormalizingMultiplier = 1.f / static_cast<float>(DiffVecNeighbors.size());
		}
		for( size_t ea = 0; ea < nso2; ea++ ) {
			roffset = ea*nbins;
			for( size_t b = 1; b < ifo.NumberOfBins-1; b++ ) {
				SDMHistoTblF32.at(roffset+b) = NormalizingMultiplier * wdwnrm[b] * static_cast<float>(SDMHistoTblU32.at(roffset+b));
			} //windowing of the bins
		} //next direction
	}
	//else nions == 0, no need to normalize or window anything because all histogram cnts are 0

	//earliest point when to release this temporary
	if ( ConfigAraullo::IOHistoCnts == false ) {
		SDMHistoTblU32 = vector<unsigned int>();
	}

	delete DiffVecNeighborsGPU; //DiffVecNeighborsGPU = NULL;
	delete BinPositions; //BinPositions = NULL;
	//##MK::do not delete SDMHistoTblU32GPU only pointers to data !

	double mytoc = omp_get_wtime();
	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " projections gpu executed in " << (mytoc-mytic) << " seconds" << "\n";
	}
}


void araullo_res::fft_gpu()
{
	double mytic = omp_get_wtime();

	//actual fast Fourier transform 1d real to complex elevation/azimuth loop nest
	binning_info ifo = owner->cfg;
	size_t nso2 = ConfigAraullo::NumberOfSO2Directions;
	size_t nbins = ifo.NumberOfBins;

	ft.init_plan( ConfigAraullo::SDMBinExponent );

	//##MK::replace with IMKL batched 1d FFT and MKL_NUM_THREADS 1

	//elevation/azimuth nested main loop
	size_t rwoffset = 0;
	for( size_t ea = 0; ea < nso2; ea++ ) {
		rwoffset = ea*nbins;
		ft.create_plan( SDMHistoTblF32, rwoffset );
		ft.execute_fwfft_report_magnitude( SDMHistoTblF32, rwoffset );
	}

	double mytoc = omp_get_wtime();
	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " Fourier transforms executed in " << (mytoc-mytic) << " seconds" << "\n";
	}

}


void araullo_res::findpks_gpu( const float spec_peak_position )
{
}
#endif


acquisor::acquisor()
{
	cfg = binning_info();
}


acquisor::~acquisor()
{
	for( auto it = res.begin(); it != res.end(); it++ ) {
		if ( it->dat != NULL ) {
			delete it->dat;
			it->dat = NULL;
		}
	}
	//res = vector<pair<int,araullo_res*>>();
	res = vector<araullo_res_node>();
}


void acquisor::configure()
{
	//for every point we need to scan through elevation and azimuth space, we generate 1D spatial distribution maps, and Fourier transform
	apt_real val = pow( static_cast<apt_real>(2.0), static_cast<apt_real>(ConfigAraullo::SDMBinExponent) );
	//##MK::NumberOfBins has to be even integer
	cfg.NumberOfBins = static_cast<unsigned int>(val + 0.5);
	cfg.NumberOfBinsHalf = static_cast<unsigned int>(ceil(0.5*static_cast<double>(cfg.NumberOfBins)));
	cfg.R = ConfigAraullo::ROIRadiusMax;

	//how much padding bins on either side of the histogram to append
	cfg.Rpadding = 1;
	cfg.dR = static_cast<apt_real>(2.0)*cfg.R / (static_cast<apt_real>(cfg.NumberOfBins) - static_cast<apt_real>(2*cfg.Rpadding));
	cfg.RdR = cfg.R + static_cast<apt_real>(cfg.Rpadding)*cfg.dR;
	cfg.binner = static_cast<apt_real>(cfg.NumberOfBins) /
			( static_cast<apt_real>(2.f)*cfg.R + static_cast<apt_real>(2.f)*static_cast<apt_real>(cfg.Rpadding)*cfg.dR );

	/*
	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << "\n" << cfg << "\n";
	}
	*/

	//precompute windowing coefficients
	//J. F. Kaiser, R. W. Schafer, 1980, On the use of the I_0-sinh window for spectrum analysis,
	//doi::10.1109/TASSP.1980.1163349
	window_coeff = vector<float>( cfg.NumberOfBins, 1.f );
	//window_coeff = vector<double>( NumberOfBins, 1.f );
	if ( ConfigAraullo::WindowingMethod == KAISER ) {
#ifdef UTILIZE_BOOST
		double alpha = ConfigAraullo::KaiserAlpha;
		double zero = 0.f;
		double I0a = boost::math::cyl_bessel_i( zero, alpha );

		for ( unsigned int i = 0; i < cfg.NumberOfBins; i++ ) {
			//w(n) modified Bessel function of first kind with shape parameter WindowingAlpha
			//w(n) = I_0(alpha*sqrt(1-(n-(N/2)/(N/2)))^2)) / I_0(alpha) for 0<= n <= N-1  otherwise w(n) = 0

			double nN = (static_cast<double>(i) - (static_cast<double>(cfg.NumberOfBins) / 2.0)) / (static_cast<double>(cfg.NumberOfBins) / 2.0);
			double x = alpha * sqrt( 1.0 - SQR(nN) );
			double I0x = boost::math::cyl_bessel_i( zero, x );

			window_coeff.at(i) = static_cast<float>(I0x / I0a); //high precision bessel then cut precision
//cout << setprecision(32) << "Computing modified Bessel function of first kind I0(v,x) coefficients " << i << "\t\t" << window_coeff.at(i) << "\n";
		}
//cout << "Kaiser windowing defined" << endl;
#else
		cout << "Kaiser windowing uses Boost to compute Bessel functions but Boost is currently not used so fallback to rectangular windowing" << "\n";
#endif
	}
	else { //E_RECTANGULAR_WINDOW
		window_coeff.at(0) = 0.f;
		window_coeff.back() = 0.f;
//cout << "Rectangular windowing defined" << endl;
	}

	//mapping of FFT bins to reciprocal frequency bins
	reciprocal_pos = vector<float>( cfg.NumberOfBinsHalf, 0.f);
	float NormalizerFreq = cfg.RdR / static_cast<float>(cfg.NumberOfBinsHalf);
	for( unsigned int i = 0; i < cfg.NumberOfBinsHalf; i++) {
		reciprocal_pos.at(i) = static_cast<float>(i) * NormalizerFreq;
	}

	/*
	#pragma omp critical
	{
		cout << "Thread " << omp_get_thread_num() << " configured" << "\n";
	}
	*/
}
