//##MK::CODESPLIT


#ifndef __PARAPROBE_ARAULLO_HDL_H__
#define __PARAPROBE_ARAULLO_HDL_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_AraulloAcquisor.h"


class araulloHdl
{
	//process-level class which implements the worker instance which executes spatial distribution maps according to the method
	//of Vicente Araullo-Peters et al. using OpenMP and OpenACC i.e. CPU and GPUs simultaneously
	//specimens result are written to a specifically-formatted HDF5 file

public:
	araulloHdl();
	~araulloHdl();
	
	bool read_periodictable();
	bool read_iontype_combinations( string fn );

	bool read_reconxyz_ranging_from_apth5();
	bool read_dist2hull_from_apth5();

	//bool read_reconstruction_from_apth5();
	bool read_directions_from_apth5();
	//bool read_trianglehull_from_apth5();


	bool broadcast_reconxyz();
	bool broadcast_ranging();
	bool broadcast_distances();
	bool broadcast_directions();
	/*bool broadcast_reconstruction();*/
	/*bool broadcast_triangles();*/
	
	bool define_phase_candidates();

	//void spatial_decomposition();
	void itype_sensitive_spatial_decomposition();
	void define_matpoint_volume_grid();
	void distribute_matpoints_on_processes_roundrobin();

/*
	gpu_cpu_max_workload plan_max_per_epoch( const int ngpu, const int nthr, const int nmp );
	gpu_cpu_now_workload plan_now_per_epoch( const int ngpu, const int nthr, const int nmp, const int mp, const gpu_cpu_max_workload mxl );
	void generate_debug_roi( mt19937 & mydice, vector<dft_real> * out );
	void query_ions_within_roi( mp3d const & roicenter, vector<dft_real> * out );
	void debug_epoch_profiling( vector<epoch_log> const & in, vector<epoch_prof> const & timing );
*/

	void execute_local_workpackage();	//multi-threaded CPU only
	void execute_local_workpackage2(); 	//multi-threaded CPU on the non-master + 1x GPU

	bool init_target_file();
	bool write_materialpoints_to_apth5();
	bool write_results_to_apth5();
	bool write_shortlist_matpoints_withresults_to_apth5();

	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();

	rangeTable rng;
	itypeCombiHdl itsk;
	vector<PhaseCandidate> cands;
	unsigned char maximum_iontype_uc;

	decompositor sp;
	volsampler mp;
	vector<int> mp2rk;								//mapping of material points => to ranks
	vector<int> mp2me;
	//vector<MPI_MatPoint_Info> mpifo;				//individual results
	/*
	vector<MPI_Fourier_ROI> mproi;					//only significant for MASTER to organize results writing
	vector<MPI_Fourier_HKLValue> mpres;				//only significant for MASTER to organize results writing
	*/
	
	//xdmfHdl debugxdmfHdl;
	h5Hdl inputReconH5Hdl;
	h5Hdl inputElevAzimH5Hdl;
	//h5Hdl inputTriHullH5Hdl;
	araullo_h5 debugh5Hdl;
	araullo_xdmf debugxdmf;
	
	vector<p3dm1> reconstruction;					//the reconstructed and ranged ion point cloud
	vector<p3d> xyz;								//reconstructed ion positions
	vector<unsigned char> ityp_org;					//iontypes ranged, original
	vector<elaz> directions;						//the SDM elevation/azimuth directions on the sphere to probe SDMs along
	//vector<tri3d> trianglehull;					//a triangle hull to the data set
	vector<apt_real> dist2hull;						//distance field

	/*
	vector<mp3d> myworkload;						//the material points of the process
	vector<hklval> myhklval;						//##MK::DEBUG for now the actual data of interest for science
	*/
	
	vector<acquisor*> workers;

	profiler araullo_tictoc;

private:
	//MPI related
	int myrank;								//my MPI ID in the MPI_COMM_WORLD
	int nranks;

	//MPI_Datatype MPI_Ion_Type;
	MPI_Datatype MPI_Ranger_DictKeyVal_Type;
	MPI_Datatype MPI_Synth_XYZ_Type;
	MPI_Datatype MPI_Ranger_Iontype_Type;
	MPI_Datatype MPI_Ranger_MQIval_Type;
	MPI_Datatype MPI_ElevAzim_Type;
	//MPI_Datatype MPI_Spatstat_Task_Type;
};


#endif

