//##MK::CODE

#include "PARAPROBE_AraulloHDF5.h"
//https://www.learncpp.com/cpp-tutorial/114-constructors-and-initialization-of-derived-classes/


araullo_h5::araullo_h5()
{
}


araullo_h5::~araullo_h5()
{
}


int araullo_h5::create_araullo_apth5( const string h5fn )
{
	h5resultsfn = h5fn;
	fileid = H5Fcreate( h5resultsfn.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
	if ( fileid < 0 ) {
		cerr << "Create araullo apth5 file creation failed! " << fileid << "\n"; return WRAPPED_HDF5_FAILED;
	}

	//domain specific HDF5 keywords and data fields
	groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_ARAULLO failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_ARAULLO failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_META, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_ARAULLO_META failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_ARAULLO_META failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}
	groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_META_MP, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_ARAULLO_META_MP failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_ARAULLO_META_MP failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_META_PHCAND, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_ARAULLO_META_PHCAND failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_ARAULLO_META_PHCAND failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_META_DIR, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_ARAULLO_META_DIR failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_ARAULLO_META_DIR failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_RES, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	if ( groupid < 0 ) {
		cerr << "Create group PARAPROBE_ARAULLO_RES failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
	}
	status = H5Gclose(groupid);
	if ( status < 0 ) {
		cerr << "Close group PARAPROBE_ARAULLO_RES failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	if ( ConfigAraullo::IOSpecificPeaks == true ) {
		groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_RES_SPECPEAK, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_ARAULLO_RES_SPECPEAK failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_ARAULLO_RES_SPECPEAK failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
	}
	if ( ConfigAraullo::IOStrongPeaks == true ) {
		groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_RES_THREEPEAK, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_ARAULLO_RES_THREEPEAK failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_ARAULLO_RES_THREEPEAK failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
	}
	if ( ConfigAraullo::IOHistoCnts == true ) {
		groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_RES_HISTO_CNTS, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_ARAULLO_RES_HISTO_CNTS failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_ARAULLO_RES_HISTO_CNTS failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
	}

	if ( ConfigAraullo::IOHistoFFTMagn == true ) {
		groupid = H5Gcreate2(fileid, PARAPROBE_ARAULLO_RES_HISTO_MAGN, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		if ( groupid < 0 ) {
			cerr << "Create group PARAPROBE_ARAULLO_RES_HISTO_MAGN failed! " << groupid << "\n"; return WRAPPED_HDF5_FAILED;
		}
		status = H5Gclose(groupid);
		if ( status < 0 ) {
			cerr << "Close group PARAPROBE_ARAULLO_RES_HISTO_MAGN failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
		}
	}

	//close file
	status = H5Fclose(fileid);
	if ( status < 0 ) {
		cerr << "Close file " << h5resultsfn << " failed! " << status << "\n"; return WRAPPED_HDF5_FAILED;
	}

	return WRAPPED_HDF5_SUCCESS;
}
