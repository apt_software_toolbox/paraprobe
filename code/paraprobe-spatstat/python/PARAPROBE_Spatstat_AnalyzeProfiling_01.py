h5wkdir='H:/BIGMAX_RELATED/Conferences/WorkshopNRWAPT_Nov2019/Content/paraprobe-spatstat/run'

import os, sys, glob, subprocess
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy import stats
import csv
import pandas as pd
import seaborn
import probscale
clear_bkgd = {'axes.facecolor':'none', 'figure.facecolor':'none'}
seaborn.set(style='ticks', context='talk', color_codes=True, rc=clear_bkgd)

# load up some example data from the seaborn package
##tips = seaborn.load_dataset("tips")
##iris = seaborn.load_dataset("iris")

#MPIE colors
#poster section background MPIE logo foreground green-blue #0073AA
#MPIE logo background light-blue #77B2D2
#MPIE 100year logo orange #EF8321
#Parula green #9DC943
#Gray Markus MMM2018, #DAD5CB
#Black #000000

sys.path.append(h5wkdir)
##sys.path.append(idxdir)
#be determinsitic
np.random.seed(0)
import h5py # if it is in Cstyle stored in the HDF5 file it will remain Cstyle ordered in numpy so no implicit transformation

np.set_printoptions(precision=3,suppress=False)


MYTOOLPATH='H:/BIGMAX_RELATED/MPIE-APTFIM-TOOLBOX/paraprobe/code/paraprobe-autoreporter'
sys.path.append(MYTOOLPATH + '/' + 'src')
sys.path.append(MYTOOLPATH + '/' + 'src' + '/' + 'metadata')

np.random.seed(0) #to be determinsitic
#if it is in Cstyle stored in the HDF5 file it will remain Cstyle ordered in numpy so no implicit transformation
#http://christopherlovell.co.uk/blog/2016/04/27/h5py-intro.html
import h5py

from PARAPROBE_Numerics import *
from PARAPROBE_CorporateDesign import *
from PARAPROBE_MetadataSpatstatDefsH5 import * 
from PARAPROBE_MetadataSurfacerDefsH5 import *

##load profiling data from individual MPI processes
NRANKS=150
simid=2003000
qq=np.zeros((NRANKS,3)) #rank, elapsed time fft, total 
for rank in range(0,NRANKS):
    csvfn=h5wkdir + '/'  + 'PARAPROBE.Spatstat.SimID.' + str(simid) + '.Rank.' + str(rank) + '.MyProfiling.csv'
    df = pd.read_csv(csvfn, header=2, sep=';')
    qq[rank,0] = rank
    qq[rank,1] = df.loc[df['What'] == 'ExecuteLocalWorkpackage']['WallClock'].iloc[0]
    qq[rank,2] = np.sum(df['WallClock'])
    print(str(rank) + ' processed')

np.savetxt( h5wkdir + '/' + 'PARAPROBE.Surfacer.SimID.' + str(simid) + '.AllRanks.MyProfiling.csv', qq, delimiter=';', header="Rank;Ion2SurfDistancing;WallClockTotal" )    
    
    
    
    
    
    
plt.title(r'Direct Fourier Transform MPI=50, OMP=01, GPU=02') ## SimID.' + str(simid))
plt.scatter(qq[:,0],qq[:,1],s=30,color='#9DC943',alpha=1.0,linewidths=0.0)
#plt.boxplot(qq[:,1])
#plt.plot(qq[:,0],qq[:,1])
#plt.xticks([1, 2, 3, 4, 5, 6, 7, 8], [r'$1^{st}$',r'$2^{nd}$',r'$3^{rd}$',r'$5^{th}$',r'$10^{th}$',r'$50^{th}$',r'$100^{th}$',r'$10000^{th}$']); #, '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
plt.xlabel('Rank')
plt.ylabel(r'Wall clock time (s)')
pngfn= acqdir + '/' + 'PARAPROBE.SimID.' + str(simid) + '.QueryAndTransformWallClockDistros.png'
plt.savefig(pngfn, dpi=600, facecolor='w', edgecolor='w', orientation='portrait', papertype='a3', format='png',
    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)
plt.clf()

##INDEXING
NRANKS=80;
simid=10001;
qq=np.zeros((NRANKS,3)) #rank, elapsed time araullo, total 
for rank in range(0,NRANKS):
    csvfn=idxdir + '/'  + 'PARAPROBE.SimID.' + str(simid) + '.Rank.' + str(rank) + '.MyProfiling.csv'
    df = pd.read_csv(csvfn, header=2, sep=';')
    qq[rank,0] = rank
    qq[rank,1] = df.loc[df['What'] == 'IndexingProcessMPIOMPLocal']['WallClock'].iloc[0]
    qq[rank,2] = np.sum(df['WallClock'])
    print(str(rank) + ' processed')
    
plt.title(r'IndexingProcessMPIOMPLocal TALOS MPI=80, OMP=40 SimID.' + str(simid))
plt.scatter(qq[:,0],qq[:,1],s=30,color='#9DC943',alpha=1.0,linewidths=0.0)
plt.xlabel('Rank')
plt.ylabel(r'Wall clock time (s)')
pngfn= idxdir + '/' + 'PARAPROBE.SimID.' + str(simid) + '.IndexingWallClockDistros.png'
plt.savefig(pngfn, dpi=600, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png',
    transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)












##################
x = np.linspace(1,nquantiles,nquantiles)
mylabels=['$\sigma_{xy} = 0.000, \sigma_{z} = 0.000$',
          '$\sigma_{xy} = 0.050, \sigma_{z} = 0.025$',
          '$\sigma_{xy} = 0.100, \sigma_{z} = 0.050$',
          '$\sigma_{xy} = 0.200, \sigma_{z} = 0.100$']
mycolors=['#9DC943','#0073AA','#77B2D2','#EF8321']
fig, ax = plt.subplots()
ax.set_title(r'Mean of specific $\mathcal{S}^2$ intensity quantiles $\eta = 1.00$')
for c in range(0,ncases):
    ax.scatter(x,mmean2[c,0,:],s=30,color=mycolors[c],label=mylabels[c],alpha=1.0,linewidths=0.0)
ax.legend()
plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
  
    print(df)
    print(df[0,0])
    here = df.set_index(['What'])
print(df.loc['one'])
str(df.loc[df['What'] == 'Loadpartitioning']['WallClock'])
            #xx=df.loc[df['What'] == 'Loadpartitioning']
    
    with open(csvfn, 'r') as csvfile:
        fid = csv.reader(csvfile, delimiter=';', quotechar='|')
        for row in fid:
            str(row)

h5wkdir=wkdir #'D:/BIGMAX_RELATED/Paper/Paper18_APT3DOIM/run/Batch_FiniteDetEffSpatResu_03'
woffset=1
interesting_cases=np.array((1,2,7,13,25,26,50,51,75,76,100))
which_values=np.array((1,2,3,5,10,50,100,10000))
NSIM=len(interesting_cases)
NM=10000
NSOL=len(which_values)
#quantiles=np.array(np.linspace(0,1,101))
qq = np.zeros((NSIM,NM,NSOL,3)) #100 sims, 10000 matpoints per sim, NSOL representative solutions taken lowest solutions taken checked, #which ori, #2 which image value, #3 which disori angle
uu = np.zeros((NSIM,NM,NSOL)) #100sims, 10000matpoints per sim, how many unique candidate IDs
for c in range(0,NSIM):
    simid=interesting_cases[c]
    h5fn = h5wkdir + '/' + 'PARAPROBE.SimID.' + str(simid) + '.Indexing.h5'
    hf = h5py.File(h5fn, 'r')

    #load best NSOL solutions for every materialpoint from the h5 result file
    here = c
    for matpointID in range(0,NM):
        h5solsfn='/Indexing/Solutions/'+str(matpointID)
        peaktbl = np.array(hf.get(h5solsfn))
        #get quantile values
        for w in range(0,NSOL):
            thisone=which_values[w]-1
            qq[here,matpointID,w,:] = peaktbl[thisone,0:3]
            uu[here,matpointID,0] = 1 #trivial case
            uu[here,matpointID,1] = unique(pre)
        #if matpointID % 100:
        #    print(matpointID)
    #disoriangle from radiant to degree
    qq[here,:,:,2] = qq[here,:,:,2]/np.pi*180.0
    print('SimID.' + str(simid) + ' loaded')
    #generate plot of distribution of image difference value for ten best solutions across matpoint ensemble
    
for c in range(0,NSIM):
    simid=interesting_cases[c] 
    h5fn = h5wkdir + '/' + 'PARAPROBE.SimID.' + str(simid) + '.Indexing.h5'
    here = c
    
    ##how much image quality difference?
    data = [qq[here,:,0,1],qq[here,:,1,1],qq[here,:,2,1], qq[here,:,3,1],qq[here,:,4,1],
            qq[here,:,5,1],qq[here,:,6,1],qq[here,:,7,1]]
    fig1, ax1 = plt.subplots()
    ax1.set_title(r'Solution quality in comparison SimID.' + str(simid))
    plt.boxplot(data)
    plt.xticks([1, 2, 3, 4, 5, 6, 7, 8], [r'$1^{st}$',r'$2^{nd}$',r'$3^{rd}$',r'$5^{th}$',r'$10^{th}$',r'$50^{th}$',r'$100^{th}$',r'$10000^{th}$']); #, '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
    plt.xlabel('Solution')
    ax1.set_yscale('log')
    ax1.set_ylim([0.001, 70.0])
    plt.ylabel(r'$\mathcal{S}^2$ image difference (a.u.)')
    pngfn=h5fn + '.S2IntTenBestDistros.png'
    plt.savefig(pngfn, dpi=600, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png',
        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)
    
    ##how much disorientation ?
    data = [qq[here,:,0,2],qq[here,:,1,2],qq[here,:,2,2], qq[here,:,3,2],qq[here,:,4,2],
            qq[here,:,5,2],qq[here,:,6,2],qq[here,:,7,2]] 
    fig1, ax1 = plt.subplots()
    ax1.set_title(r'Solution quality in comparison SimID.' + str(simid))
    plt.boxplot(data)
    plt.xticks([1, 2, 3, 4, 5, 6, 7,8], [r'$1^{st}$',r'$2^{nd}$',r'$3^{rd}$',r'$5^{th}$',r'$10^{th}$',r'$50^{th}$',r'$100^{th}$',r'$10000^{th}$']); #, '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
    plt.xlabel('Solution')
    ax1.set_yscale('log')
    ax1.set_ylim([0.3, 90.0])
    plt.ylabel(r'Disorientation angle (°)')
    pngfn=h5fn + '.DisoriTenBestDistros.png'
    plt.savefig(pngfn, dpi=600, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png',
        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)
    max(qq[here,:,0,2])
    min(qq[here,:,0,2])
    print('SimID.' + str(simid) + ' processed')
    
    ##how many unique candidate orientations chosen?
    


    

##ONLY VACANCIES BUT NO POSITIONAL NOISE
vac_only_noise_none=np.array((101,126,151,176))
ncases=len(vac_only_noise_none)
mmean1=np.zeros((ncases,1,nquantiles))
woffset=101
for c in range(0,ncases):
    for q in range(0,nquantiles):
        mmean1[c,0,q] = np.mean(qq[vac_only_noise_none[c]-woffset,:,q])
mstdv1=np.zeros((ncases,1,nquantiles))
for c in range(0,ncases):
    for q in range(0,nquantiles):
        mstdv1[c,0,q] = np.std(qq[vac_only_noise_none[c]-woffset,:,q])
x = np.linspace(1,nquantiles,nquantiles)
mylabels=['$\eta = 1.00$',
          '$\eta = 0.75$',
          '$\eta = 0.50$',
          '$\eta = 0.25$']
mycolors=['#9DC943','#0073AA','#77B2D2','#EF8321']
fig, ax = plt.subplots()
ax.set_title(r'Mean of specific $\mathcal{S}^2$ intensity quantiles $\sigma_{xy} = 0.000, \sigma_{z} = 0.000$')
for c in range(0,ncases):
    ax.scatter(x,mmean1[c,0,:],s=30,color=mycolors[c],label=mylabels[c],alpha=1.0,linewidths=0.0)
ax.legend()
plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
ax.set_yscale('log')
#ax.set_ylim([2e-5, 2.0])
pngfn=h5fn + '.DegradationVacanciesMean.png'
plt.savefig(pngfn, dpi=600, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png',
        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)
fig, ax = plt.subplots()
ax.set_title(r'Stddev of specific $\mathcal{S}^2$ intensity quantiles $\sigma_{xy} = 0.000, \sigma_{z} = 0.000$')
for c in range(0,ncases):
    ax.scatter(x,mstdv1[c,0,:],s=30,color=mycolors[c],label=mylabels[c],alpha=1.0,linewidths=0.0)
ax.legend()
plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
ax.set_yscale('log')
#ax.set_ylim([2e-5, 2.0])
pngfn=h5fn + '.DegradationVacanciesStdv.png'
plt.savefig(pngfn, dpi=600, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png',
        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)


###NO VACANCIES BUT POSITIONAL NOISE
vac_none_noise_only=np.array((101,107,113,125))
ncases=len(vac_none_noise_only)
mmean2=np.zeros((ncases,1,nquantiles))
woffset=101
for c in range(0,ncases):
    for q in range(0,nquantiles):
        mmean2[c,0,q] = np.mean(qq[vac_none_noise_only[c]-woffset,:,q])
mstdv2=np.zeros((ncases,1,nquantiles))
for c in range(0,ncases):
    for q in range(0,nquantiles):
        mstdv2[c,0,q] = np.std(qq[vac_none_noise_only[c]-woffset,:,q])
x = np.linspace(1,nquantiles,nquantiles)
mylabels=['$\sigma_{xy} = 0.000, \sigma_{z} = 0.000$',
          '$\sigma_{xy} = 0.050, \sigma_{z} = 0.025$',
          '$\sigma_{xy} = 0.100, \sigma_{z} = 0.050$',
          '$\sigma_{xy} = 0.200, \sigma_{z} = 0.100$']
mycolors=['#9DC943','#0073AA','#77B2D2','#EF8321']
fig, ax = plt.subplots()
ax.set_title(r'Mean of specific $\mathcal{S}^2$ intensity quantiles $\eta = 1.00$')
for c in range(0,ncases):
    ax.scatter(x,mmean2[c,0,:],s=30,color=mycolors[c],label=mylabels[c],alpha=1.0,linewidths=0.0)
ax.legend()
plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
ax.set_yscale('log')
#ax.set_ylim([2e-5, 2.0])
pngfn=h5fn + '.DegradationNoiseMean.png'
plt.savefig(pngfn, dpi=600, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png',
        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)
fig, ax = plt.subplots()
ax.set_title(r'Stddev of specific $\mathcal{S}^2$ intensity quantiles $\eta = 1.00$')
for c in range(0,ncases):
    ax.scatter(x,mstdv2[c,0,:],s=30,color=mycolors[c],label=mylabels[c],alpha=1.0,linewidths=0.0)
ax.legend()
plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
ax.set_yscale('log')
#ax.set_ylim([2e-5, 2.0])
pngfn=h5fn + '.DegradationNoiseStdv.png'
plt.savefig(pngfn, dpi=600, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png',
        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)


###BOTH VACANCIES AND POSITIONAL NOISE
vac_yes_noise_yes=np.array((101,150,175,200))
ncases=len(vac_yes_noise_yes)
mmean3=np.zeros((ncases,1,nquantiles))
woffset=101
for c in range(0,ncases):
    for q in range(0,nquantiles):
        mmean3[c,0,q] = np.mean(qq[vac_yes_noise_yes[c]-woffset,:,q])
mstdv3=np.zeros((ncases,1,nquantiles))
for c in range(0,ncases):
    for q in range(0,nquantiles):
        mstdv3[c,0,q] = np.std(qq[vac_yes_noise_yes[c]-woffset,:,q])
x = np.linspace(1,nquantiles,nquantiles)
mylabels=['$\eta = 1.00, \sigma_{xy} = 0.000, \sigma_{z} = 0.000$',
          '$\eta = 0.75, \sigma_{xy} = 0.200, \sigma_{z} = 0.100$',
          '$\eta = 0.50, \sigma_{xy} = 0.200, \sigma_{z} = 0.100$',
          '$\eta = 0.25, \sigma_{xy} = 0.200, \sigma_{z} = 0.100$']
mycolors=['#9DC943','#0073AA','#77B2D2','#EF8321']
fig, ax = plt.subplots()
ax.set_title(r'Mean of specific $\mathcal{S}^2$ intensity quantiles')
for c in range(0,ncases):
    ax.scatter(x,mmean3[c,0,:],s=30,color=mycolors[c],label=mylabels[c],alpha=1.0,linewidths=0.0)
ax.legend()
plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
ax.set_yscale('log')
#ax.set_ylim([2e-5, 2.0])
pngfn=h5fn + '.DegradationBothMean.png'
plt.savefig(pngfn, dpi=600, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png',
        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)
fig, ax = plt.subplots()
ax.set_title(r'Stddev of specific $\mathcal{S}^2$ intensity quantiles')
for c in range(0,ncases):
    ax.scatter(x,mstdv3[c,0,:],s=30,color=mycolors[c],label=mylabels[c],alpha=1.0,linewidths=0.0)
ax.legend()
plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['Min', '0.0025', '0.025', '0.25', '0.50', '0.75', '0.975', '0.9975', 'Max'])
ax.set_yscale('log')
#ax.set_ylim([2e-5, 2.0])
pngfn=h5fn + '.DegradationBothStdv.png'
plt.savefig(pngfn, dpi=600, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png',
        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)








#stats.probplot(qq[:,1000],plot=plt)
#
##plot population of quantile values
#plt.figure(1)
#for simid in range(0,100):
#    sorted_data = np.sort(qq[simid,:,0]) #eg all minimum intensities on S2 for all matpoints of sim simid
#    yvals=np.arange(len(sorted_data))/float(len(sorted_data)-1)
#    plt.plot(sorted_data,yvals)
#    
#    
#    #sorted_data = np.sort(qq[0,:,1000])
#    #yvals=np.arange(len(sorted_data))/float(len(sorted_data)-1)
#    #plt.plot(sorted_data,yvals)
#    #sorted_data = np.sort(qq[:,20])
#    #yvals=np.arange(len(sorted_data))/float(len(sorted_data)-1)
#    #plt.plot(sorted_data,yvals)
#    plt.xlabel(r'FFT peak20 bin intensity') #use latex notation if desired
#    plt.ylabel(r'CDF')
#    ##sort
#    idx=np.argpartition(qq[0,0,:],-10)
#    qq[0,0,idx[-10:]]
#    aa=np.sort(qq[0,0,:])
#    qq[1,5001,0]
#    
#    #plt.xlim([0.0, 10.0])
#    #plt.ylim([1E1, 1E8])
#    plt.legend()
#    sorted_data = np.sort(qq[:,0])
#    yvals=np.arange(len(sorted_data))/float(len(sorted_data)-1)
#    plt.plot(sorted_data,yvals)
#
#plt.show()
#      
#
#
#
#preamble = add_package_definitions()
#titlepage = add_title_page()
#
#content = preamble + titlepage + add_header()
#
#
#main = 'Im writing automatic \LaTeX reports with Python to complement \ivas !'
#
#peaks = pp_peaktable( peaktbl, 6, 'The six strongest peaks', 'OrderedPeakList' )
#
#
##load mq_trsfhist ie mass spectrum from h5 file
#h5trsfhist='/RangingAutomatic/SqrtMQHist'
#rawmspec = np.array(hf.get(h5trsfhist))
#h5basehist='/RangingAutomatic/BaselineHist'
#basemspec = np.array(hf.get(h5basehist))
#
#
##https://matplotlib.org/api/_as_gen/matplotlib.pyplot.subplots.html
##https://python-graph-gallery.com/122-multiple-lines-chart/
##df=pd.DataFrame({'x': range(1,11), 'y1': np.random.randn(10), 'y2': np.random.randn(10)+range(1,11), 'y3': np.random.randn(10)+range(11,21) })
# 
#
##import seaborn as sns
### plot of n strongest peaks
##N=6
##tmp=peaktbl[0:N,0]
##df = pd.DataFrame({
##'sqrtmq': peaktbl[0:N,1],
##'lgcnts': peaktbl[0:N,2],
##'group': [str(int(peaktbl[0,0])),str(int(peaktbl[1,0])),str(int(peaktbl[2,0])),str(int(peaktbl[3,0])),str(int(peaktbl[4,0])),str(int(peaktbl[5,0]))] 
##}) 
##sns.regplot(data=df, x="sqrtmq", y="lgcnts", fit_reg=False, marker="+", color="skyblue")
##df.set_value()
#
## multiple line plot
##np.log10(rawmspec[:,1])
#plt.figure(1)
#plt.semilogy( rawmspec[:,0],rawmspec[:,1], nonposy='clip', color='skyblue', linewidth=1.5, label='log10 raw') #marker='', markerfacecolor='blue', markersize=0,
#plt.semilogy( basemspec[:,0], basemspec[:,1], nonposy='clip', color='olive', linewidth=1.5, label='log10 phon') #marker='', 
#plt.xlabel(r'$\sqrt{\frac{m}{q}}$')
#plt.ylabel(r'lg(cnts)')
#plt.xlim([0.0, 10.0])
#plt.ylim([1E1, 1E8])
##plt.plot( 'x', 'y3', data=df, marker='', color='olive', linewidth=2, linestyle='dashed', label="toto")
#plt.legend()
#
#N=10
#for cc in range(0, N):
#    plt.annotate(str(int(peaktbl[cc,0])),
#            xy=(peaktbl[cc,1], peaktbl[cc,2]), xycoords='data',
#            xytext=(+10, +120), textcoords='offset points',
#            arrowprops=dict(arrowstyle="->",
#                            connectionstyle="angle,angleA=0,angleB=90,rad=10")) #connectionstyle="arc3,rad=.2"))
#
#
#pngfn=h5fn + '.MassSpec.png'
##https://matplotlib.org/api/_as_gen/matplotlib.pyplot.savefig.html
#plt.savefig(pngfn, dpi=600, facecolor='w', edgecolor='w', orientation='landscape', papertype='a3', format='png',
#        transparent=False, bbox_inches='tight', pad_inches=0.1, frameon=None, metadata=None)
#
#testfig =  pp_singlefigure( pngfn, 'The spectrum', 'MassSpecOverview' )
#
#content = content + main + peaks + testfig
#
#h5cand='/RangingAutomatic/Peaks/Candidates'
##https://texwelt.de/wissen/fragen/3218/latex-error-too-many-unprocessed-floats-wie-verarbeite-ich-viele-abbildungen-und-tabellen
#for cc in range(0, N): #Latex buffer at most 16 floats peaktbl.shape[0]):
#    thisone=peaktbl[cc,0]
#   
#    candtbl=[]
#    candtbl=np.array(hf.get(h5cand + '/' + 'Combination_' + str(int(thisone)) ))
#    scortbl=[]
#    scortbl=np.array(hf.get(h5cand + '/' + 'MetaScore_' + str(int(thisone)) ))
#   
#    testcand=[]
#    testcand = pp_peakcandidate( thisone, candtbl, scortbl, 0.005 )
#    
#    print(thisone)
#    content = content + testcand
#    
#    
#content = content + add_footer()
#
#
#with open(texfn + '.tex','w') as f:
#     f.write(content)
#
#
#
###commandLine = subprocess.Popen(['xelatex -synctex=1 -interaction=nonstopmode', texfn + '.tex'])
###commandLine.communicate()
##commandLine = subprocess.Popen(['pdflatex', texfn + '.tex'])
#
###os.unlink(texfn + '.aux')
###os.unlink(texfn + '.log')
##os.unlink(texfn + '.tex')
