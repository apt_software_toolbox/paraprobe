//##MK::CODE

#ifndef __PARAPROBE_CONFIG_SPATSTAT_H__
#define __PARAPROBE_CONFIG_SPATSTAT_H__

//#include "../../paraprobe-utils/src/CONFIG_Shared.h"
#include "../../paraprobe-utils/src/PARAPROBE_Histogram.h"
#include "../../paraprobe-utils/src/PARAPROBE_SDM3D.h"
#include "../../paraprobe-utils/src/PARAPROBE_FFT.h"

//##MK::ostream how to

pair<string,bool> parse_knn_neighbors( const string in, vector<unsigned int> & out );


class ConfigSpatstat
{
public:
	
	static string InputfilePSE;			//for loading the periodic table of elements
	static string InputfileReconstruction;
	static string InputfileHullAndDistances;
	//static string Outputfile;
		
	static lival ROIRadiiRDF;
	static lival ROIRadiiKNN;
	static lival ROIRadiiSDM;


	static bool AnalyzeRDF;
	static bool AnalyzeKNN;
	static bool AnalyzeSDM;
	static bool AnalyzeRandomize;

	static string KOrderForKNN;
	static string KOrderForSDM;

	//static bool ROICenterInsideOnly;
	static bool ROIVolumeInsideOnly;
		
	//internals
	static string PRNGType;
	static size_t PRNGWarmup;
	static long PRNGWorldSeed;

	static size_t MaxSizeCachedResPerNode;

	static bool readXML( string filename = "" );
	static bool checkUserInput();
};

#endif
