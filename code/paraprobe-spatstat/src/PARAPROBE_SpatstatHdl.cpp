//##MK::CODESPLIT

#include "PARAPROBE_SpatstatHdl.h"


spatstatHdl::spatstatHdl()
{
	myrank = MASTER;
	nranks = 1;
	maximum_iontype_uc = 0;
}


spatstatHdl::~spatstatHdl()
{
    xyz = vector<p3d>();
    ityp_org = vector<unsigned char>();
    ityp_rnd = vector<unsigned char>();
    dist2hull = vector<apt_real>();

    for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++ ) {
    	if ( tsk->hst1d_res != NULL ) {
    		delete tsk->hst1d_res; tsk->hst1d_res = NULL;
    	}
    	if ( tsk->sdm3d_res != NULL ) {
    		delete tsk->sdm3d_res; tsk->sdm3d_res = NULL;
    	}
    }

    for( auto tsk = results.begin(); tsk != results.end(); tsk++ ) {
		if ( tsk->hst1d_res != NULL ) {
			delete tsk->hst1d_res; tsk->hst1d_res = NULL;
		}
		if ( tsk->sdm3d_res != NULL ) {
			delete tsk->sdm3d_res; tsk->sdm3d_res = NULL;
		}
	}
}


bool spatstatHdl::read_periodictable()
{
	//all read PeriodicTable
	if ( rng.nuclides.load_isotope_library( ConfigSpatstat::InputfilePSE ) == false ) {
		cerr << "Rank " << get_myrank() << " loading of PeriodicTableOfElements failed!" << "\n"; return false;
	}
	cout << "Rank " << get_myrank() << " loaded rng.nuclides.isotopes.size() " << rng.nuclides.isotopes.size() << " isotopes" << "\n";
	return true;
}


bool spatstatHdl::read_iontype_combinations( string fn )
{
	//all read IontypeCombinations
	if ( itsk.load_iontype_combinations( fn ) == false ) {
		cerr << "Rank " << get_myrank() << " loading of IontypeCombinations failed!" << "\n"; return false;
	}
	cout << "Rank " << get_myrank() << " itsk.icombis.size() " << itsk.icombis.size() << " icombis " << "\n";
	return true;
}


bool spatstatHdl::read_reconxyz_ranging_from_apth5()
{
	//double tic = MPI_Wtime();

	//load xyz positions
	if ( inputReconH5Hdl.read_xyz_from_apth5( ConfigSpatstat::InputfileReconstruction, xyz ) == true ) {
		cout << ConfigSpatstat::InputfileReconstruction << " read positions successfully" << "\n";
	}
	else {
		cerr << ConfigSpatstat::InputfileReconstruction << " read positions failed!" << "\n"; return false;
	}

	//load ranging information
	if ( inputReconH5Hdl.read_iontypes_from_apth5( ConfigSpatstat::InputfileReconstruction, rng.iontypes ) == true ) {
		cout << ConfigSpatstat::InputfileReconstruction << " read ranging information successfully" << "\n";
	}
	else {
		cerr << ConfigSpatstat::InputfileReconstruction << " read ranging information failed!" << "\n"; return false;
	}

	//create iontype_dictionary
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		size_t id = static_cast<size_t>(it->id);
		size_t hashval = rng.hash_molecular_ion( it->strct.Z1, it->strct.Z2, it->strct.Z3 );
cout << id << "\t\t" << hashval << "\t\t" << (int) it->strct.Z1 << ";" << (int) it->strct.Z2 << ";" << (int) it->strct.Z3 << "\n";
		if ( id != 0 && hashval != 0 ) { //skip adding existent UNKNOWN_IONTYPE
			auto jt = rng.iontypes_dict.find( hashval );
			if ( jt == rng.iontypes_dict.end() ) { //really a new iontype for which not yet mqival were defined
cout << "--->Adding new type " << id << "\t\t" << hashval << "\n";
				//add iontype in dictionary
				rng.iontypes_dict.insert( pair<size_t,size_t>( hashval, id ) );
			}
			else {
cerr << "--->Detected replicated iontype which should not happen as iontypes have to be mutually exclusive!" << "\n"; return false;
			}
		}
		else {
cout << "--->Me skipping the UNKNOWN_IONTYPE" << "\n";
		}
	}

	//load actual itypes per xyz position
	if ( inputReconH5Hdl.read_ityp_from_apth5( ConfigSpatstat::InputfileReconstruction, ityp_org ) == true ) {
		cout << ConfigSpatstat::InputfileReconstruction << " read itypes successfully" << "\n";
	}
	else {
		cerr << ConfigSpatstat::InputfileReconstruction << " read itypes failed!" << "\n"; return false;
	}

	//double toc = MPI_Wtime();
	//memsnapshot mm = memsnapshot();
	//spatstat_tictoc.prof_elpsdtime_and_mem( "ReadReconstructedXYZFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


bool spatstatHdl::read_dist2hull_from_apth5()
{
	//double tic = MPI_Wtime();

	if ( this->inputDistH5Hdl.read_dist_from_apth5( ConfigSpatstat::InputfileHullAndDistances, dist2hull ) == true ) {
		cout << ConfigSpatstat::InputfileReconstruction << " read distances successfully" << "\n";
	}
	else {
		cerr << ConfigSpatstat::InputfileReconstruction << " read distances failed!" << "\n"; return false;
	}

	//double toc = MPI_Wtime();
	//memsnapshot mm = memsnapshot();
	//spatstat_tictoc.prof_elpsdtime_and_mem( "ReadDistancesToTriangleHullFromAPTH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

    return true;
}


bool spatstatHdl::broadcast_reconxyz()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	//double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( xyz.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ion reconstructions!" << "\n"; localhealth = 0;
		}
		if ( xyz.size() == 0 ) {
			cerr << "Ion positions is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's xyz size and state!" << "\n"; return false;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<int>(xyz.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Synth_XYZ* buf = NULL;
	try {
		buf = new MPI_Synth_XYZ[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for MPI_Synth_XYZ buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < xyz.size(); i++ ) {
			buf[i] = MPI_Synth_XYZ( xyz.at(i) );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_Synth_XYZ_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			xyz.reserve ( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				xyz.push_back( p3d( buf[i].x, buf[i].y, buf[i].z ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate xyz!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";

	//double toc = MPI_Wtime();
	//memsnapshot mm = memsnapshot();
	//spatstat_tictoc.prof_elpsdtime_and_mem( "BroadcastReconXYZ", APT_XX, APT_IS_PAR, mm, tic, toc);
	return true;
}


bool spatstatHdl::broadcast_ranging()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	//double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( rng.iontypes.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " iontypesinfo!" << "\n"; localhealth = 0;
		}
		if ( rng.iontypes.size() == 0 ) {
			cerr << "MASTER's rng.iontypes is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
		if ( ityp_org.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ityp_org per ion!" << "\n"; localhealth = 0;
		}
		if ( ityp_org.size() == 0 ) {
			cerr << "MASTER's ityp_org is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's ranging size and state!" << "\n"; return false;
	}

	//communicate metadata first
	int nrng_ivl_evt_dict[4] = {0, 0, 0, 0};
	if ( get_myrank() == MASTER ) {
		nrng_ivl_evt_dict[0] = static_cast<int>(rng.iontypes.size());
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
			nrng_ivl_evt_dict[1] += static_cast<int>(it->rng.size());
		}
		nrng_ivl_evt_dict[2] = static_cast<int>(ityp_org.size());
		nrng_ivl_evt_dict[3] = static_cast<int>(rng.iontypes_dict.size());
	}
	MPI_Bcast( nrng_ivl_evt_dict, 4, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	MPI_Ranger_Iontype* buf1 = NULL;
	MPI_Ranger_MQIval* buf2 = NULL;
	unsigned char* buf3 = NULL;
	MPI_Ranger_DictKeyVal* buf4 = NULL;
	try {
		buf1 = new MPI_Ranger_Iontype[nrng_ivl_evt_dict[0]];
		buf2 = new MPI_Ranger_MQIval[nrng_ivl_evt_dict[1]];
		buf3 = new unsigned char[nrng_ivl_evt_dict[2]];
		buf4 = new MPI_Ranger_DictKeyVal[nrng_ivl_evt_dict[3]];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for buf1, buf2, buf3, and buf4!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf1; buf1 = NULL;
		delete [] buf2; buf2 = NULL;
		delete [] buf3; buf3 = NULL;
		delete [] buf4; buf4 = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		size_t iv = 0;
		size_t id = 0;
		for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++, id++ ) {
			unsigned char typid = static_cast<unsigned char>(it->id);
			buf1[id] = MPI_Ranger_Iontype( typid, it->strct.Z1, it->strct.N1, it->strct.Z2, it->strct.N2,
												it->strct.Z3, it->strct.N3, it->strct.sign, it->strct.charge );
			for( auto jt = it->rng.begin(); jt != it->rng.end(); jt++, iv++ ) {
				buf2[iv] = MPI_Ranger_MQIval( jt->lo, jt->hi, typid );
			}
		}
		for( size_t i = 0; i < ityp_org.size(); i++ ) {
			buf3[i] = ityp_org.at(i);
		}
		size_t ii = 0;
		for( auto kvt = rng.iontypes_dict.begin(); kvt != rng.iontypes_dict.end(); kvt++, ii++ ) {
			buf4[ii] = MPI_Ranger_DictKeyVal( static_cast<unsigned long long>(kvt->first), static_cast<unsigned long long>(kvt->second) );
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf1, nrng_ivl_evt_dict[0], MPI_Ranger_Iontype_Type, MASTER, MPI_COMM_WORLD );
	MPI_Bcast( buf2, nrng_ivl_evt_dict[1], MPI_Ranger_MQIval_Type, MASTER, MPI_COMM_WORLD);
	MPI_Bcast( buf3, nrng_ivl_evt_dict[2], MPI_UNSIGNED_CHAR, MASTER, MPI_COMM_WORLD );
	MPI_Bcast( buf4, nrng_ivl_evt_dict[3], MPI_Ranger_DictKeyVal_Type, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			rng.iontypes.reserve( static_cast<size_t>(nrng_ivl_evt_dict[0]) );
			for( int i = 0; i < nrng_ivl_evt_dict[0]; i++ ) {
				rng.iontypes.push_back( rangedIontype() );
				rng.iontypes.back().id = static_cast<unsigned int>( buf1[i].id );
				rng.iontypes.back().strct = evapion3( buf1[i].Z1, buf1[i].N1, buf1[i].Z2, buf1[i].N2, buf1[i].Z3, buf1[i].N3, buf1[i].sign, buf1[i].charge );
				for( int j = 0; j < nrng_ivl_evt_dict[1]; j++ ) { //was [2], ##MK:avoid O(N^2) search, but should not be a problem because N << 256
					if ( static_cast<unsigned int>(buf2[j].id) != rng.iontypes.back().id ) {
						continue;
					}
					else {
						rng.iontypes.back().rng.push_back( mqival( buf2[j].low, buf2[j].high ) );
					}
				}
			}
			ityp_org.reserve( static_cast<size_t>(nrng_ivl_evt_dict[2]) );
			for( int k = 0; k < nrng_ivl_evt_dict[2]; k++ ) {
				ityp_org.push_back( buf3[k] );
			}

			for ( int kv = 0; kv < nrng_ivl_evt_dict[3]; kv++ ) {
				rng.iontypes_dict.insert( pair<size_t,size_t>(
						static_cast<size_t>(buf4[kv].key), static_cast<size_t>(buf4[kv].val) ) );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate rng.iontypes and ityp_org!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf1; buf1 = NULL;
	delete [] buf2; buf2 = NULL;
	delete [] buf3; buf3 = NULL;
	delete [] buf4; buf4 = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";

	//double toc = MPI_Wtime();
	//memsnapshot mm = memsnapshot();
	//spatstat_tictoc.prof_elpsdtime_and_mem( "BroadcastRangingData", APT_XX, APT_IS_PAR, mm, tic, toc);
	return true;
}


bool spatstatHdl::broadcast_distances()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER has recognized that only one MPI process is used so we can skip the broadcasting steps now..." << "\n";
		return true;
	}

	//double tic = MPI_Wtime();

	//communicate state of the MASTER's dataset
	int localhealth = 1;
	int globalhealth = 0;
	if ( get_myrank() == MASTER ) { //MK::only the master so far knows state and size of the datasets!
		if ( dist2hull.size() >= (INT32MX-1) ) { //##MK::can be changed easily, just move to blockwise data transfer
			cerr << "Current implementation does not handle the case of larger than " << (INT32MX-1) << " ion dist2hull info!" << "\n"; localhealth = 0;
		}
		if ( dist2hull.size() == 0 ) {
			cerr << "MASTER's dist2hull is empty, so nothing to communicate!" << "\n"; localhealth = 0;
		}
	}
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that something is wrong with the MASTER's dist2hull size and state!" << "\n"; return false;
	}

	//communicate size of the master's dataset
	int nevt = 0;
	if ( get_myrank() == MASTER ) {
		nevt = static_cast<int>(dist2hull.size());
	}
	MPI_Bcast( &nevt, 1, MPI_INT, MASTER, MPI_COMM_WORLD );

	//with this info, master and slaves allocate memory for communication, and assure that all master and slaves have such buffer
	localhealth = 1;
	float* buf = NULL;
	try {
		buf = new float[nevt];
	}
	catch(bad_alloc &mecroak) {
		cerr << "Rank " << get_myrank() << " allocation error for float buffer!" << "\n"; localhealth = 0;
	}

	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that not all ranks have allocated communication buffers!" << "\n";
		delete [] buf; buf = NULL;
		return false;
	}

	//master populates communication buffer
	if ( get_myrank() == MASTER ) {
		for( size_t i = 0; i < dist2hull.size(); i++ ) {
			buf[i] = dist2hull.at(i);
		}
	}

	//collective call MPI_Bcast, MASTER broadcasts, slaves listen
	MPI_Bcast( buf, nevt, MPI_FLOAT, MASTER, MPI_COMM_WORLD );

	//slave nodes pull from buffer
	if ( get_myrank() != MASTER ) {
		try {
			dist2hull.reserve ( static_cast<size_t>(nevt) );
			for( int i = 0; i < nevt; i++ ) {
				dist2hull.push_back( buf[i] );
			}
		}
		catch(bad_alloc &mecroak) {
			cerr << "Rank " << get_myrank() << " allocation error during buf readout to instantiate dist2hull!" << "\n"; localhealth = 0;
		}
	}

	//all release the buffer, delete on NULL pointer gets compiled as no op
	delete [] buf; buf = NULL;

	//final check whether all were successful so far
	globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth != get_nranks() ) {
		cerr << "Rank " << get_myrank() << " has recognized that there were inconsistencies in the communication!" << "\n";	return false;
	}

//cout << "Rank " << get_myrank() << " listened to MASTER reconstruction.size() " << reconstruction.size() << "\n";

	//double toc = MPI_Wtime();
	//memsnapshot mm = memsnapshot();
	//spatstat_tictoc.prof_elpsdtime_and_mem( "BroadcastDist2Hull", APT_XX, APT_IS_PAR, mm, tic, toc);
	return true;
}


bool spatstatHdl::define_analysis_tasks()
{
	//all create the internal spatial statistics and two-point statistics (SDM) task list,
	//i.e. which iontype combinations are probed
	double tic = MPI_Wtime();

	size_t max_memory_per_node = spatstat_tictoc.get_memory_max_on_node();
	size_t max_memory_for_buf = 0.1 * max_memory_per_node;

	cout << "Rank " << get_myrank() << " max_memory_per_node" << max_memory_per_node << " B" << "\n";
	cout << "Rank " << get_myrank() << " max_memory_for_buf " << max_memory_for_buf << " B" << "\n";

	//paraprobe-spatstat buffers for each thread and each task 2x histograms and/or 2x SDMs respectively, to
	//allow for thread-memory-local parallelized writing back of results without global sync
	//this may create situations where the computing node provides not enough memory to allocate all the buffers
	//consequently, we want to catch this as earliest as possible to warn the user and advise less workpackages per run
	//max_memory_per_node in Byte, approximately MK::just a not too naive mechanism to avoid
	//that unexperienced user issue instruct e.g. 1000^3 large SDM with 100-th of combinations

	//firstly, we transform the human-readable icombis targ/nbor strings pairs into evapion3 objects
	vector<Evapion3Combi> tmp;
	for( auto it = itsk.icombis.begin(); it != itsk.icombis.end(); it++ ) {
		tmp.push_back( Evapion3Combi() );

		rng.add_evapion3( it->targets, tmp.back().targets );
		rng.add_evapion3( it->nbors, tmp.back().nbors );

		//##BEGIN DEBUG
		cout << "Rank " << get_myrank() << " targets __" << it->targets << "__ ---->" << "\n";
		for( auto jt = tmp.back().targets.begin(); jt != tmp.back().targets.end(); jt++ ) {
			cout << (int) jt->Z1 << ";" << (int) jt->Z2 << ";" << (int) jt->Z3  << "\n";
		}
		cout << "Rank " << get_myrank() << " nbors __" << it->nbors << "__ ---->" << "\n";
		for( auto jt = tmp.back().nbors.begin(); jt != tmp.back().nbors.end(); jt++ ) {
			cout << (int) jt->Z1 << ";" << (int) jt->Z2 << ";" << (int) jt->Z3  << "\n";
		}
		//##END DEBUG
	}

	//secondly, we have to understand that these user-combinations may not at all be present in the ranging data, i.e.
	//are possibly not all evapion3 types that we identifiable in the analyzed dataset as ranging data,
	//sure, in most cases user will make correct input but in every case we need to make sure
	map<size_t,size_t>::iterator does_not_exist = rng.iontypes_dict.end();
	for( auto jt = tmp.begin(); jt != tmp.end(); jt++ ) {
		UC8Combi valid;
		for( auto tg = jt->targets.begin(); tg != jt->targets.end(); tg++ ) {
			size_t hashval = rng.hash_molecular_ion( tg->Z1, tg->Z2, tg->Z3 );
			//MK::it is important to understand here that the user is not enforced to write the molecular ions
			//within the XML input file in order, because above preprocessing assures that combinations such as AlH: or H:Al will be sorted
			//into a unique representation namely Al, H, UNOCCUPIED as it holds also for the ranging
			//Consequently, internal ion type handling is handling commutativity to please the user
			map<size_t,size_t>::iterator vt = rng.iontypes_dict.find( hashval );
			if ( vt != does_not_exist ) { //kt is an ion type which exists within the ranging data
cout << " Adding an identified ion from the ranging process as target\t\t" << vt->first << "\t\t" << vt->second << "\n";
				valid.targets.push_back( vt->second );
			}
		}
		for( auto nb = jt->nbors.begin(); nb != jt->nbors.end(); nb++ ) {
			size_t hashval = rng.hash_molecular_ion( nb->Z1, nb->Z2, nb->Z3 );
			//MK::it is important to understand here that the user is not enforced to write the molecular ions
			//within the XML input file in order, because above preprocessing assures that combinations such as AlH: or H:Al will be sorted
			//into a unique representation namely Al, H, UNOCCUPIED as it holds also for the ranging
			//Consequently, internal ion type handling is handling commutativity to please the user
			map<size_t,size_t>::iterator vt = rng.iontypes_dict.find( hashval );
			if ( vt != does_not_exist ) { //kt is an ion type which exists within the ranging data
cout << " Adding an identified ion from the ranging process as neighb\t\t" << vt->first << "\t\t" << vt->second << "\n";
				valid.nbors.push_back( vt->second );
			}
		}

		itsk.combinations.push_back( UC8Combi() );
		for( auto tgs = valid.targets.begin(); tgs != valid.targets.end(); tgs++ ) { //explicit deep copy
			itsk.combinations.back().targets.push_back( *tgs );
		}
		for( auto nbs = valid.nbors.begin(); nbs != valid.nbors.end(); nbs++ ) {
			itsk.combinations.back().nbors.push_back( *nbs );
		}

		//1.) check if any evapion3 matches to any known ranged
		//2.) if so create a new UC8Combi object and
		//3.) translate evapion3 to known unsigned char iontype values from the ConfigSpatstat::InputfileReconstruction "/Ranging..."
		//MK::the trick here is to end up that eventually the actual analysis of iontypes will use exclusively
		//unsigned char keys. e.g. lets assume we have Al-Al and Al was ranged from the input as unsigned char == 0x01 then
		//the pairing Al-Al becomes UC8Combi.targets = 0x01 and UC8Combi.nbors = 0x01
		//targets/nbors == 0x00 means always all ions
		//we do not allow for negations of types as this can always be reformulated
		//5.) we store eventually in this->
		//this->combinations.push_back( UC8Combi() );
		//combinations.back().targets.push_back()
		//combinations.back().nbors.push_back();
	}
	//get rid of temporaries
	tmp = vector<Evapion3Combi>();


	//thirdly, and now having the IontypeCombinations we plan the actual tasks
	unsigned int tskid = 0; //scientific analysis tasks start at 0
	size_t max_memory_expected = 0; //in bytes

	if ( itsk.combinations.size() < 1 ) {
		cerr << "Combinations were specified but not parsable after having evaluated InputfileReconstruction ranging data and settings!" << "\n";
		return false;
	}

cout << "About to prepare RDF tasks..." << "\n";
	if ( ConfigSpatstat::AnalyzeRDF == true ) {
		linear_ival iv_rdf = linear_ival( ConfigSpatstat::ROIRadiiRDF.min, ConfigSpatstat::ROIRadiiRDF.incr, ConfigSpatstat::ROIRadiiRDF.max );
		histogram hst1d_rdf = histogram( iv_rdf );
		size_t mem_per_hst1d_rdf = static_cast<size_t>(hst1d_rdf.nbins)*sizeof(double) + sizeof(histogram);

		for( auto it = itsk.combinations.begin(); it != itsk.combinations.end(); it++ ) {
			//for ORIGINAL labels
			itsk.itasks.push_back( SpatstatTask( ConfigSpatstat::ROIRadiiRDF, tskid, ORIGINAL_IONTYPES, WANT_RDF, WANT_NONE, WANT_NONE ) );
			for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
				itsk.itasks.back().targ_nbors.push_back( *tgs );
			}
			itsk.itasks.back().pos_firstnbor = itsk.itasks.back().targ_nbors.size(); //first all target iontypes on [0, pos_firstnbor), next all neighbors on [pos_firstnbor,targ_nbors.size())
			for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
				itsk.itasks.back().targ_nbors.push_back( *nbs );
			}
			tskid++;
			//for RANDOMIZED labels
			itsk.itasks.push_back( SpatstatTask( ConfigSpatstat::ROIRadiiRDF, tskid, RANDOMIZED_IONTYPES, WANT_RDF, WANT_NONE, WANT_NONE ) );
			for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
				itsk.itasks.back().targ_nbors.push_back( *tgs );
			}
			itsk.itasks.back().pos_firstnbor = itsk.itasks.back().targ_nbors.size();
			for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
				itsk.itasks.back().targ_nbors.push_back( *nbs );
			}
			tskid++;
			//res buffer size expectation
			max_memory_expected += 2*mem_per_hst1d_rdf; //because one for ORIGINAL and one for RANDOMIZED
		}
	}

	//for KNN multiple KNN values are possible need to parse ConfigSpatstat::KOrderForKNN
cout << "About to prepare KNN tasks..." << "\n";
	if ( ConfigSpatstat::AnalyzeKNN == true ) {
		vector<unsigned int> knn_kth;
		size_t nsemicolon_knn = count( ConfigSpatstat::KOrderForKNN.begin(), ConfigSpatstat::KOrderForKNN.end(), ';' );
		stringstream parsethis_knn;
		parsethis_knn << ConfigSpatstat::KOrderForKNN;
		string datapiece_knn;
		for( size_t i = 0; i < nsemicolon_knn + 1; i++ ) { //parse at least the single value
			getline( parsethis_knn, datapiece_knn, ';');
			unsigned long val = stoul(datapiece_knn);
			if ( val < static_cast<unsigned long>(UINT32MX) ) {
				knn_kth.push_back( static_cast<unsigned int>(val) );
			}
		}

		if ( knn_kth.size() > 0 ) {
			linear_ival iv_knn = linear_ival( ConfigSpatstat::ROIRadiiKNN.min, ConfigSpatstat::ROIRadiiKNN.incr, ConfigSpatstat::ROIRadiiKNN.max );
			histogram hst1d_knn = histogram( iv_knn );
			size_t mem_per_hst1d_knn = static_cast<size_t>(hst1d_knn.nbins)*sizeof(double) + sizeof(histogram);

			for( auto kt = knn_kth.begin(); kt != knn_kth.end(); kt++ ) {
	cout << "k = " << *kt << "\n";
				for( auto it = itsk.combinations.begin(); it != itsk.combinations.end(); it++ ) {
					//for ORIGINAL labels
					itsk.itasks.push_back( SpatstatTask( ConfigSpatstat::ROIRadiiKNN, *kt, tskid,
							ORIGINAL_IONTYPES, WANT_NONE, WANT_KNN, WANT_NONE ) );
					for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
						itsk.itasks.back().targ_nbors.push_back( *tgs );
					}
					itsk.itasks.back().pos_firstnbor = itsk.itasks.back().targ_nbors.size(); //first all target iontypes on [0, pos_firstnbor), next all neighbors on [pos_firstnbor,targ_nbors.size())
					for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
						itsk.itasks.back().targ_nbors.push_back( *nbs );
					}
					tskid++;
					//for RANDOMIZED labels
					itsk.itasks.push_back( SpatstatTask( ConfigSpatstat::ROIRadiiKNN, *kt, tskid,
							RANDOMIZED_IONTYPES, WANT_NONE, WANT_KNN, WANT_NONE ) );
					for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
						itsk.itasks.back().targ_nbors.push_back( *tgs );
					}
					itsk.itasks.back().pos_firstnbor = itsk.itasks.back().targ_nbors.size();
					for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
						itsk.itasks.back().targ_nbors.push_back( *nbs );
					}
					tskid++;
					//res buffer size expectation
					max_memory_expected += 2*mem_per_hst1d_knn; //because one for ORIGINAL and one for RANDOMIZED
				} //next combination
			} //next kth
			knn_kth = vector<unsigned int>();
		}
		else {
			cout << "WARNING::Even though KNN jobs were desired no k-th order values were parsable from " << ConfigSpatstat::KOrderForKNN << "\n";
		}
	}

	//also for SDM multiple KNN values are possible need to parse ConfigSpatstat::KOrderForSDM
cout << "About to prepare SDM tasks..." << "\n";
	if ( ConfigSpatstat::AnalyzeSDM == true ) {
		vector<unsigned int> sdm_kth;
		size_t nsemicolon_sdm = count( ConfigSpatstat::KOrderForSDM.begin(), ConfigSpatstat::KOrderForSDM.end(), ';' );
		stringstream parsethis_sdm;
		parsethis_sdm << ConfigSpatstat::KOrderForSDM;
		string datapiece_sdm;
		for( size_t i = 0; i < nsemicolon_sdm + 1; i++ ) { //parse at least the single value
			getline( parsethis_sdm, datapiece_sdm, ';');
			unsigned long val = stoul(datapiece_sdm);
			if ( val < static_cast<unsigned long>(UINT32MX) ) {
				sdm_kth.push_back( static_cast<unsigned int>(val) );
			}
		}

		if ( sdm_kth.size() > 0 ) {
			//sdm3d sdmprobe = sdm3d( ConfigSpatstat::ROIRadii.max, ConfigSpatstat::ROIRadii.incr, 201 );
			size_t mem_per_sdm3d = CUBE(201) * sizeof(unsigned int) + sizeof(sdm3d); //sdmprobe.box.NXYZ * sizeof(unsigned int) + sizeof(sdm3d);
			for( auto kt = sdm_kth.begin(); kt != sdm_kth.end(); kt++ ) {
	cout << "k = " << *kt << "\n";
				for( auto it = itsk.combinations.begin(); it != itsk.combinations.end(); it++ ) {
					//for ORIGINAL labels
					itsk.itasks.push_back( SpatstatTask( ConfigSpatstat::ROIRadiiSDM, *kt, tskid,
							ORIGINAL_IONTYPES, WANT_NONE, WANT_NONE, WANT_SDM ) );
					for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
						itsk.itasks.back().targ_nbors.push_back( *tgs );
					}
					itsk.itasks.back().pos_firstnbor = itsk.itasks.back().targ_nbors.size(); //first all target iontypes on [0, pos_firstnbor), next all neighbors on [pos_firstnbor,targ_nbors.size())
					for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
						itsk.itasks.back().targ_nbors.push_back( *nbs );
					}
					tskid++;
					//for RANDOMIZED labels
					itsk.itasks.push_back( SpatstatTask( ConfigSpatstat::ROIRadiiSDM, *kt, tskid,
							RANDOMIZED_IONTYPES, WANT_NONE, WANT_NONE, WANT_SDM ) );
					for( auto tgs = it->targets.begin(); tgs != it->targets.end(); tgs++ ) {
						itsk.itasks.back().targ_nbors.push_back( *tgs );
					}
					itsk.itasks.back().pos_firstnbor = itsk.itasks.back().targ_nbors.size();
					for( auto nbs = it->nbors.begin(); nbs != it->nbors.end(); nbs++ ) {
						itsk.itasks.back().targ_nbors.push_back( *nbs );
					}
					tskid++;
					//res buffer size expectation
					max_memory_expected += 2*mem_per_sdm3d; //because one for ORIGINAL and one for RANDOMIZED
				} //next combination
			} //next kth
		}
		else {
			cout << "WARNING::Even though SDM jobs were desired no k-th order values were parsable from " << ConfigSpatstat::KOrderForSDM << "\n";
		}
		sdm_kth = vector<unsigned int>();
	}

	//final check would all these buffers fit in memory?
	if ( ( max_memory_expected * static_cast<size_t>(1 + omp_get_max_threads()) ) >= max_memory_per_node ) { //+1 because there is the process-local copy to decouple critical regions of non-master threads and master thread
		cerr << "With " << max_memory_expected << " * " << omp_get_max_threads() << " threads we would exceed the max_memory_per_node!" << "\n"; return false;
	}
	else {
		cout << "Rank expected memory demands for buffering intermediate results to be " << max_memory_expected << " B" << "\n";
	}

	//##BEGIN DEBUG
	for( int rk = MASTER; rk < get_nranks(); rk++ ) {
		if ( rk == get_myrank() ) {
		cout << "Rank " << rk << " prepared all tasks now verifying successful deep copies of values..." << "\n";
			for( auto ic = itsk.icombis.begin(); ic != itsk.icombis.end(); ic++ ) {
				cout << ic->targets << "\t\t" << ic->nbors << "\n";
			}
			for( auto jt = itsk.combinations.begin(); jt != itsk.combinations.end(); jt++ ) {
				cout << "Rank " << rk << " itsk.combiations " << jt - itsk.combinations.begin() << "\n";
				for( auto tg = jt->targets.begin(); tg != jt->targets.end(); tg++ ) {
					cout << "Rank " << rk << " tg = " << (int) *tg << "\n";
				}
				for( auto nb = jt->nbors.begin(); nb != jt->nbors.end(); nb++ ) {
					cout << "Rank " << rk << " nb = " << (int) *nb << "\n";
				}
			}
			for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++ ) {
				cout << "Rank " << rk << " isk.istask" << "\n";
				cout << "hst1d_res = " << tsk->hst1d_res << " sdm3d_res = " << tsk->sdm3d_res << " pos_firstnbor = " << tsk->pos_firstnbor << "\n";
				cout << "R [" << tsk->R.min << ":" << tsk->R.incr << ":" << tsk->R.max << "]" << " kthorder = " << tsk->kthorder << " tskid = " << tsk->taskid << "\n";
				cout << "WhichLabel " << ((tsk->WhichLabel == ORIGINAL_IONTYPES) ? "ORG" : "RND") << " RDF/KNN/SDM " << tsk->IsRDF << ";" << tsk->IsKNN << ";" << tsk->IsSDM << "\n";
				for( size_t uc = 0; uc < tsk->pos_firstnbor; uc++ ) {
					cout << "Target\t\t" << (int) tsk->targ_nbors.at(uc) << "\n";
				}
				for( size_t uc = tsk->pos_firstnbor; uc < tsk->targ_nbors.size(); uc++ ) {
					cout << "Neighb\t\t" << (int) tsk->targ_nbors.at(uc) << "\n";
				}
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	//##END DEBUG

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	spatstat_tictoc.prof_elpsdtime_and_mem( "DefineAnalysisTasks", APT_XX, APT_IS_PAR, mm, tic, toc );

	return true;
}


void spatstatHdl::randomize_iontype_labels()
{
	double tic = MPI_Wtime();

	//https://meetingcpp.com/blog/items/stdrandom_shuffle-is-deprecated.html
	mt19937 urng;
	urng.seed( ConfigSpatstat::PRNGWorldSeed );
	urng.discard( ConfigSpatstat::PRNGWarmup );

	try {
		ityp_rnd = ityp_org;
	}
	catch(bad_alloc &mecroak) {
		cerr << "Allocation of ityp_rnd failed!" << "\n";
	}

	//randomize order on array via uniform Mersenne twister PRNG shuffling, maintaining total number of ions of specific type
	//##MK::evaluate throws
	shuffle( ityp_rnd.begin(), ityp_rnd.end(), urng);

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	spatstat_tictoc.prof_elpsdtime_and_mem( "RandomizeIontypeLabels", APT_XX, APT_IS_SEQ, mm, tic, toc );

}


void spatstatHdl::itype_sensitive_spatial_decomposition()
{
	double tic = MPI_Wtime();

	sp.tip_aabb_get_extrema( xyz );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	spatstat_tictoc.prof_elpsdtime_and_mem( "SpecimenAABB3DGetExtrema", APT_XX, APT_IS_SEQ, mm, tic, toc);

	tic = MPI_Wtime();

	//get maximum iontype
	unsigned int maximum_ityp = 0;
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		if ( it->id < static_cast<unsigned char>(UCHARMX) ) {
			if ( it->id > maximum_ityp ) {
				maximum_ityp = it->id;
			}
		}
		else {
			cerr << "Detected an invalid iontype during itype_sensitive_spatial_decomposition!" << "\n"; return;
		}
	}

	maximum_iontype_uc = static_cast<unsigned char>(maximum_ityp);
cout << "Rank " << get_myrank() << " maximum itype identified is " << (int) maximum_iontype_uc << "\n";


	sp.loadpartitioning_subkdtree_per_ityp( xyz, dist2hull, ityp_org, ityp_rnd, maximum_iontype_uc );

	//first point were ityp_org and ityp_rnd are no longer necessary and could in principle be deleted

	toc = MPI_Wtime();
	mm = spatstat_tictoc.get_memoryconsumption();
	spatstat_tictoc.prof_elpsdtime_and_mem( "AABB3DLoadPartitioning", APT_XX, APT_IS_PAR, mm, tic, toc);
}


bool spatstatHdl::prepare_processlocal_resultsbuffer()
{
	//build thread-local results buffer
	double tic = MPI_Wtime();

	for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++ ) {
		results.push_back( SpatstatTask() );
		results.back().taskid = tsk->taskid;
		results.back().WhichLabel = tsk->WhichLabel;
		results.back().IsRDF = tsk->IsRDF;
		results.back().IsKNN = tsk->IsKNN;
		results.back().IsSDM = tsk->IsSDM;
		if ( (tsk->IsRDF == true || tsk->IsKNN == true) && tsk->IsSDM == false ) {
			linear_ival rr = linear_ival( tsk->R.min, tsk->R.incr, tsk->R.max );
			try {
				results.back().hst1d_res = new histogram( rr );
			}
			catch (bad_alloc &croak) {
				cerr << "results.back().hst1d_res allocation error at task " << tsk->taskid << "\n"; return false;
			}
		}
		if ( (tsk->IsRDF == false && tsk->IsKNN == false) && tsk->IsSDM == true ) {
			try {
				results.back().sdm3d_res = new sdm3d( tsk->R.max, tsk->R.incr, 201 );
			}
			catch (bad_alloc &croak) {
				cerr << "results.back().sdm3d_res allocation error at task " << tsk->taskid << "\n"; return false;
			}
		}
		results.back().kthorder = tsk->kthorder;
		results.back().pos_firstnbor = tsk->pos_firstnbor;
		results.back().targ_nbors = tsk->targ_nbors;
		results.back().R = tsk->R;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = spatstat_tictoc.get_memoryconsumption();
	spatstat_tictoc.prof_elpsdtime_and_mem( "PrepareProcessResultsBuffer", APT_XX, APT_IS_SEQ, mm, tic, toc );

	cout << "Rank " << get_myrank() << " has allocated process-local results buffer !" << "\n";
	return true;
}


void spatstatHdl::execute_local_workpackage()
{
	//ions are distributed round robin across processes
	//ions of a process are stored in thread-local region, all threads machine off these regions successively
	//to maximize dynamic workpartitioning, we have all members of the team participating in processing a region
	//while processing, each thread buffers statistics results in thread-local memory before entering one critical accumulation step
	//here data are accumulated in the buffer of the master thread first, thereafter the buffers will be repointered to the process-local result
	//these results are latter gathered across MPI_COMM_WORLD per task
	double tic = MPI_Wtime();
	cout << "Rank " << get_myrank() << " participating in MPI/OMP-parallelized spatial statistics..." << "\n";

//string ranklog = "";

	#pragma omp parallel
	{
		double mytic, mytoc;
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		bool healthy = true;

		//init thread-local copy of tasks and corresponding buffers for results
		vector<SpatstatTask> mytsk_org;
		vector<SpatstatTask> mytsk_rnd;

		//radii can differ per task, but we do not want to requery
		apt_real rsmallest = F32MX;
		apt_real rlargest = F32MI;
		try {
			for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++ ) {
				if ( tsk->WhichLabel == ORIGINAL_IONTYPES ) {
					mytsk_org.push_back( SpatstatTask() );
					mytsk_org.back().taskid = tsk->taskid;
					mytsk_org.back().WhichLabel = tsk->WhichLabel;
					mytsk_org.back().IsRDF = tsk->IsRDF;
					mytsk_org.back().IsKNN = tsk->IsKNN;
					mytsk_org.back().IsSDM = tsk->IsSDM;
					if ( (tsk->IsRDF == true || tsk->IsKNN == true) && tsk->IsSDM == false ) {
						linear_ival rr = linear_ival( tsk->R.min, tsk->R.incr, tsk->R.max );
						mytsk_org.back().hst1d_res = new histogram( rr );
					}
					if ( (tsk->IsRDF == false && tsk->IsKNN == false) && tsk->IsSDM == true ) {
						mytsk_org.back().sdm3d_res = new sdm3d( tsk->R.max, tsk->R.incr, 201 );
					}
					mytsk_org.back().kthorder = tsk->kthorder;
					mytsk_org.back().pos_firstnbor = tsk->pos_firstnbor;
					mytsk_org.back().targ_nbors = tsk->targ_nbors;
					mytsk_org.back().R = tsk->R;
				}
				else { //tsk->WhichLabel == RANDOMIZED_IONTYPES
					mytsk_rnd.push_back( SpatstatTask() );
					mytsk_rnd.back().taskid = tsk->taskid;
					mytsk_rnd.back().WhichLabel = tsk->WhichLabel;
					mytsk_rnd.back().IsRDF = tsk->IsRDF;
					mytsk_rnd.back().IsKNN = tsk->IsKNN;
					mytsk_rnd.back().IsSDM = tsk->IsSDM;
					if ( (tsk->IsRDF == true || tsk->IsKNN == true) && tsk->IsSDM == false ) {
						linear_ival rr = linear_ival( tsk->R.min, tsk->R.incr, tsk->R.max );
						mytsk_rnd.back().hst1d_res = new histogram( rr );
					}
					if ( (tsk->IsRDF == false && tsk->IsKNN == false) && tsk->IsSDM == true ) {
						mytsk_rnd.back().sdm3d_res = new sdm3d( tsk->R.max, tsk->R.incr, 201 );
					}
					mytsk_rnd.back().kthorder = tsk->kthorder;
					mytsk_rnd.back().pos_firstnbor = tsk->pos_firstnbor;
					mytsk_rnd.back().targ_nbors = tsk->targ_nbors;
					mytsk_rnd.back().R = tsk->R;
				}
				rsmallest = min( tsk->R.max, rsmallest );
				rlargest = max( tsk->R.max, rlargest );
			} //next tsk
		}
		catch (bad_alloc &mecroak) {
			#pragma omp critical
			{
				cerr << "Rank " << get_myrank() << " thread " << mt << " mytsk_org / mytsk_rnd thread-local allocation failure!" << "\n";
			}
			healthy = false;
		}

		//all distances squared instead of sqrt computation for efficiency unless nbor objects
		//basic operation is as follows take each ion, if it is sufficiently distant from tip take into consideration
		apt_real rsmallestsqr = SQR(rsmallest);
		apt_real rlargestsqr = SQR(rlargest);
		unsigned int MyProcess = get_myrank();
		unsigned int NProcesses = get_nranks();
		bool RDF_KNN_Tasks_Exist = true;
		if ( ConfigSpatstat::AnalyzeRDF == false && ConfigSpatstat::AnalyzeKNN == false) {
			RDF_KNN_Tasks_Exist = false;
		}
		bool SDM_Tasks_Exist = true;
		if ( ConfigSpatstat::AnalyzeSDM == false ){
			SDM_Tasks_Exist = false;
		}

#pragma omp critical
{
	cout << "Rank " << get_myrank() << " thread " << mt << " rsmallest/rlargest " << rsmallest << ";" << rlargest << "\n";
}

		//identify which iontype kd-subtrees we should at all attempt to probe as targets across all regions
		set<int> relevant_trees_org;
		set<int> relevant_trees_rnd;
		for( int thr = MASTER; thr < nt; thr++ ) {
			threadmemory* thisone = sp.db.at(thr);
			int nitypes = thisone->ionpp1_kdtree_org.size(); //int suffices, only at most 256 itypes!
			for( int ii = 0; ii < nitypes; ii++ ) {
				if ( thisone->ionpp1_kdtree_org.at(ii) != NULL ) { //for a sub-tree to be relavant it first of all needs to contain ions
					//but also it should handle an iontype which is part of any analysis task
					for( auto mtsk = mytsk_org.begin(); mtsk != mytsk_org.end(); mtsk++ ) {
						if ( mtsk->WhichLabel == ORIGINAL_IONTYPES ) {
							for( auto tntypes = mtsk->targ_nbors.begin(); tntypes != mtsk->targ_nbors.end(); tntypes++ ) {
								if ( static_cast<int>(*tntypes) != ii ) {
									continue;
								}
								relevant_trees_org.insert( ii );
								break;
							}
						}
					}
				}
			}
			int njtypes = thisone->ionpp1_kdtree_rnd.size();
			for( int jj = 0; jj < njtypes; jj++ ) {
				if ( thisone->ionpp1_kdtree_rnd.at(jj) != NULL ) { //for a sub-tree to be relavant it first of all needs to contain ions
					//but also it should handle an iontype which is part of any analysis task
					for( auto mtsk = mytsk_rnd.begin(); mtsk != mytsk_rnd.end(); mtsk++ ) {
						if ( mtsk->WhichLabel == RANDOMIZED_IONTYPES) {
							for( auto tntypes = mtsk->targ_nbors.begin(); tntypes != mtsk->targ_nbors.end(); tntypes++ ) {
								if ( static_cast<int>(*tntypes) != jj ) {
									continue;
								}
								relevant_trees_rnd.insert( jj );
								break;
							}
						}
					}
				}
			}
		}

		//actual ion scanning
		size_t nivals = 1+static_cast<size_t>((int) maximum_iontype_uc); //MK::do forget that we have also the zero-th type so we need 1+nival accessors for subkdtrees

#pragma omp master
{
	cout << "Rank " << get_myrank() << " thread " << mt << " nivals " << nivals << "\n";
}

		for( int thr = MASTER; thr < nt; thr++ ) { //thread team is instructed to machine off region by region, currently thr

#pragma omp master
{
	cout << "Rank " << MyProcess << " Region " << thr << "\n";
}

			mytic = omp_get_wtime();
			threadmemory* thisregion = sp.db.at(thr);

			//start processing original labels
			for( auto typit = relevant_trees_org.begin(); typit != relevant_trees_org.end(); typit++ ) { //process as target ions all from the relevant trees
				vector<p3d>* ppp = thisregion->ionpp1_kdtree_org.at(*typit);
				vector<p3difo>* ifo = thisregion->ionifo_kdtree_org.at(*typit);
				apt_real thisregion_zmi = thisregion->get_zmi();
				apt_real thisregion_zmx = thisregion->get_zmx();
//cout << "\tTypit " << *typit << "\n";
				if ( ppp != NULL && ifo != NULL ) {
					#pragma omp for schedule(dynamic,1) nowait
					for( size_t ip = 0; ip < ppp->size(); ip++ ) {
						p3d me_xyz = ppp->at(ip);
						p3difo me_ifo = ifo->at(ip);
						//all processes have duplicated the dataset, therefore each thread team needs to process only 1/NProcesses-th part on average
//ranklog += "ORG-Rank " + to_string(MyProcess) + ";" + to_string(me_ifo.IonID) + ";" + to_string(me_ifo.IonID % NProcesses) + "\n";
						if ( me_ifo.IonID % NProcesses == MyProcess ) {
//cout << "\t\t" << me_ifo.IonID << "\n";

							if ( me_ifo.dist >= rsmallest ) { //consider at least in some task
//cout << "\t\tIP " << ip << "\t\tme_ifo.dist " << me_ifo.dist << "\n";
								//query neighbors
								vector<p3d> nbors_xyz;
								vector<ival> nbors_ityp_block = vector<ival>( nivals, ival(0,0) ); //build an array of neighbors within largest ROI
								size_t prev = 0;
								size_t next = 0;
								for( auto tt = relevant_trees_org.begin(); tt != relevant_trees_org.end(); tt++ ) {
//cout << "\t\t\t\t\ttt " << *tt << "\n";
									//scan all the relevant sub-trees
									prev = nbors_xyz.size();
									//two cases for ROI spatial extent to distinguish
									//1.) ion is > rlargest within current region only probe current region
									//2.) ROI protrudes into different regions probe multiple regions
//cout << "\t\t\t\t xyz/zmi/zmx " << me_xyz.x << ";" << me_xyz.y << ";" << me_xyz.z << ";" << thisregion_zmi << ";" << thisregion_zmx << "\n";
									if ( (me_xyz.z - rlargest) > thisregion_zmi && ( me_xyz.z + rlargest ) < thisregion_zmx ) {
										//only current region
										kd_tree* thistree = thisregion->ityp_kdtree_org.at(*tt);
										vector<p3d> const & thisppp = *(thisregion->ionpp1_kdtree_org.at(*tt));
										if ( *tt == *typit ) {
											thistree->range_rball_append_exclude_myself( ip, thisppp, rlargestsqr, nbors_xyz );
										}
										else {
											thistree->range_rball_append_external( me_xyz, thisppp, rlargestsqr, nbors_xyz );
										}
//cout << "\t\t\t\tmyregion done!" << "\n";
									}
									else {
										//current region first
//cout << "\t\t\t\tnbregion begin!" << "\n";
										kd_tree* thistree = thisregion->ityp_kdtree_org.at(*tt);
										vector<p3d> const & thisppp = *(thisregion->ionpp1_kdtree_org.at(*tt));
										if ( *tt == *typit ) {
											thistree->range_rball_append_exclude_myself( ip, thisppp, rlargestsqr, nbors_xyz );
										}
										else {
											thistree->range_rball_append_external( me_xyz, thisppp, rlargestsqr, nbors_xyz );
										}
										//probe top neighbors and climb up, when i am not the topmost region
										if ( thr < (mt-1) ) {
											for( int upp = (thr+1); upp < nt; upp++) { //will eventually climb up to the topmost threadregion, also in practice this will never happen
												threadmemory* nbregion = sp.db.at(upp);
												kd_tree* nbtree = nbregion->ityp_kdtree_org.at(*tt);
												vector<p3d> const & nbppp = *(nbregion->ionpp1_kdtree_org.at(*tt));
												nbtree->range_rball_append_external( me_xyz, nbppp, rlargestsqr, nbors_xyz );
											}
										}
										//probe bottom neighbors and climb down, when i am not the bottommost region
										if ( thr > MASTER ) {
											for( int dwn = (thr-1); dwn > -1; dwn-- ) {
												threadmemory* nbregion = sp.db.at(dwn);
												kd_tree* nbtree = nbregion->ityp_kdtree_org.at(*tt);
												vector<p3d> const & nbppp = *(nbregion->ionpp1_kdtree_org.at(*tt));
												nbtree->range_rball_append_external( me_xyz, nbppp, rlargestsqr, nbors_xyz );
											}
										}
									}
									//done processing iontype tt update access field
									next = nbors_xyz.size();
									nbors_ityp_block.at(*tt) = ival( prev, next ); //assess information for empty or unvisited fields remains empty
								} //done querying ions from all relevant iontype trees

								//now that we know the relevant neighbors process tasks
								vector<apt_real> nbors_dist; //order is retained, so nbors_ityp_block tells us which iontypes
								if ( RDF_KNN_Tasks_Exist == true ) {
									for( auto nbt = nbors_xyz.begin(); nbt != nbors_xyz.end(); nbt++ ) {
										nbors_dist.push_back( sqrt(SQR(nbt->x-me_xyz.x)+SQR(nbt->y-me_xyz.y)+SQR(nbt->z-me_xyz.z)) );
									}
								}
								//MK::DO NOT PERFORM ANY SORTING DIRECTLY ON nbors_dist or nbors_d3dd DURING EXECUTING tasks as that would kill the order relation!
								vector<d3dd> nbors_d3dd;
								if ( SDM_Tasks_Exist == true ) {
									for( auto nbt = nbors_xyz.begin(); nbt != nbors_xyz.end(); nbt++ ) {
										nbors_d3dd.push_back( d3dd( nbt->x-me_xyz.x, nbt->y-me_xyz.y, nbt->z-me_xyz.z ) );
									}
								}

/*
//BEGIN DEBUG
for( auto ttt = nbors_ityp_block.begin(); ttt != nbors_ityp_block.end(); ttt++ ) {
	cout << "\t\t\t\t\t ttt " << ttt - nbors_ityp_block.begin() << ";" << ttt->incl_left << ";" << ttt->excl_right << "\n";
}
cout << "\t\t\t\t\t nbors_xyz / dist size " << nbors_xyz.size() << "\t\t" << nbors_dist.size() << "\n";
//END DEBUG
*/

								for( auto mtsk = mytsk_org.begin(); mtsk != mytsk_org.end(); mtsk++ ) {

									if ( mtsk->IsRDF == true ) {
										if ( me_ifo.dist >= mtsk->R.max ) {
//cout << "\t\t\t\t\t RDF taskid " << mtsk->taskid << " RDF " << "\n";
											//is ion me any possible target in this task?
											for( size_t iii = 0; iii < mtsk->pos_firstnbor; iii++ ) {
												if ( mtsk->targ_nbors.at(iii) != me_ifo.i_org ) { //no, me is a different type ion
													continue;
												}
												else { //yes me is sought after type ion targets
													//so add all those nbors of the same iontype class
													//RDF collect all radii <= R.max
													for( size_t jjj = mtsk->pos_firstnbor; jjj < mtsk->targ_nbors.size(); jjj++ ) {
														int jjtt = static_cast<int>(mtsk->targ_nbors.at(jjj));
														for( size_t k = nbors_ityp_block.at(jjtt).incl_left; k < nbors_ityp_block.at(jjtt).excl_right; k++ ) {
															apt_real val = nbors_dist.at(k);
															if ( val <= mtsk->R.max ) {
																mtsk->hst1d_res->add_dump_yes( val );
															}
														}
													} //done adding all ions within ROI of the relevant types
													break; //done for the target type
												} //done for the yes case
											} //keep checking if a possible target wrt to iontypes
										} //keep checking if far enough from hull
										continue; //each task can only be one, either RDF, KNN, or SDM
									}

									if ( mtsk->IsKNN == true ) {
										//is ion me really sufficiently far enough away from the boundary?
										if ( me_ifo.dist >= mtsk->R.max ) {
//cout << "\t\t\t\t\t KNN taskid " << mtsk->taskid << " KNN " << "\n";
											//is ion me any possible target in this task?
											for( size_t iii = 0; iii < mtsk->pos_firstnbor; iii++ ) {
												if ( mtsk->targ_nbors.at(iii) != me_ifo.i_org ) { //no, me is a different type ion
													continue;
												}
												else { //yes me is sought after type ion targets
													//so add all those nbors of the same iontype class
													vector<apt_real> candidates;
													for( size_t jjj = mtsk->pos_firstnbor; jjj < mtsk->targ_nbors.size(); jjj++ ) {
														int jjtt = static_cast<int>(mtsk->targ_nbors.at(jjj));
														for( size_t k = nbors_ityp_block.at(jjtt).incl_left; k < nbors_ityp_block.at(jjtt).excl_right; k++ ) {
															apt_real val = nbors_dist.at(k);
															if ( val <= mtsk->R.max ) {
//cout << "\t\t\t\t\t\t KNN adding " << val << "\n";
																candidates.push_back( val );
															}
														}
													} //done adding all ions within ROI of the relevant types
													size_t kth = static_cast<size_t>(mtsk->kthorder);
													if ( candidates.size() > kth ) {
														nth_element( candidates.begin(), candidates.begin() + kth, candidates.end() ); //MK::will sort ascendingly by default!
//cout << "\t\t\t\t\t KNN ---> kth = " << kth << " val = " << candidates.at(kth) << "\n";
														mtsk->hst1d_res->add_dump_yes( candidates.at(kth) );
													}
													else {
														mtsk->hst1d_res->add_dump_yes( mtsk->R.max + EPSILON );
													}
													break; //done for the target type
												} //done for the yes case
											} //keep checking if a possible target wrt to iontypes
										} //keep checking if far enough from hull
										continue;
									}

									if ( mtsk->IsSDM == true ) {
										//is ion me really sufficiently far enough away from the boundary?
										if ( me_ifo.dist >= mtsk->R.max ) {
//cout << "\t\t\t\t\t SDM taskid " << mtsk->taskid << " SDM " << "\n";
											//is ion me any possible target in this task?
											for( size_t iii = 0; iii < mtsk->pos_firstnbor; iii++ ) {
												if ( mtsk->targ_nbors.at(iii) != me_ifo.i_org ) { //no, me is a different type ion
													continue;
												}
												else { //yes me is sought after type ion targets
													//so add all those nbors of the same iontype class
													vector<d3dd> candidates;
													apt_real mtsk_rmax_sqr = SQR(mtsk->R.max);
													for( size_t jjj = mtsk->pos_firstnbor; jjj < mtsk->targ_nbors.size(); jjj++ ) {
														int jjtt = static_cast<int>(mtsk->targ_nbors.at(jjj));
														for( size_t k = nbors_ityp_block.at(jjtt).incl_left; k < nbors_ityp_block.at(jjtt).excl_right; k++ ) {
															d3dd val = nbors_d3dd.at(k);
															if ( val.dSQR <= mtsk_rmax_sqr ) {
//cout << "\t\t\t\t\t\t SDM adding " << val << "\n";
																candidates.push_back( val );
															}
														}
													} //done adding all ions within ROI of the relevant types
													size_t kth = static_cast<size_t>(mtsk->kthorder);
													if ( candidates.size() > kth ) {
														nth_element( candidates.begin(), candidates.begin() + kth, candidates.end(), SortDiffForAscDistance );
//cout << "\t\t\t\t\t SDM---> kth = " << kth << " val = " << candidates.at(kth) << "\n";
														mtsk->sdm3d_res->add( candidates.at(kth) );
													}
													break; //done for the target type
												} //done for the yes case
											} //keep checking if a possible target wrt to iontypes
										} //keep checking if far enough from hull
										continue;
									}
								} //re-utilize queried ROI to process next task
							} //was at least in one task
						} //was processed by this MPI rank
					} //threads machine off ip
				}
			} //hunt for ions of the next relevant type for original labels

			//start processing randomized types
			for( auto typit = relevant_trees_rnd.begin(); typit != relevant_trees_rnd.end(); typit++ ) { //process as target ions all from the relevant trees
				vector<p3d>* ppp = thisregion->ionpp1_kdtree_rnd.at(*typit);
				vector<p3difo>* ifo = thisregion->ionifo_kdtree_rnd.at(*typit);
				apt_real thisregion_zmi = thisregion->get_zmi();
				apt_real thisregion_zmx = thisregion->get_zmx();
//cout << "\tTypit " << *typit << "\n";
				if ( ppp != NULL && ifo != NULL ) {
					#pragma omp for schedule(dynamic,1) nowait
					for( size_t ip = 0; ip < ppp->size(); ip++ ) {
						p3d me_xyz = ppp->at(ip);
						p3difo me_ifo = ifo->at(ip);
						//all processes have duplicated the dataset, therefore each thread team needs to process only 1/NProcesses-th part on average
//ranklog += "RND-Rank " + to_string(MyProcess) + ";" + to_string(me_ifo.IonID) + ";" + to_string(me_ifo.IonID % NProcesses) + "\n";

						if ( me_ifo.IonID % NProcesses == MyProcess ) {
//cout << "\t\t" << me_ifo.IonID << "\n";

							if ( me_ifo.dist >= rsmallest ) { //consider at least in some task
//cout << "\t\tIP " << ip << "\t\tme_ifo.dist " << me_ifo.dist << "\n";
								//query neighbors
								vector<p3d> nbors_xyz;
								vector<ival> nbors_ityp_block = vector<ival>( nivals, ival(0,0) ); //build an array of neighbors within largest ROI
								size_t prev = 0;
								size_t next = 0;
								for( auto tt = relevant_trees_rnd.begin(); tt != relevant_trees_rnd.end(); tt++ ) {
//cout << "\t\t\t\t\ttt " << *tt << "\n";
									//scan all the relevant sub-trees
									prev = nbors_xyz.size();
									//two cases for ROI spatial extent to distinguish
									//1.) ion is > rlargest within current region only probe current region
									//2.) ROI protrudes into different regions probe multiple regions
//cout << "\t\t\t\t xyz/zmi/zmx " << me_xyz.x << ";" << me_xyz.y << ";" << me_xyz.z << ";" << thisregion_zmi << ";" << thisregion_zmx << "\n";
									if ( (me_xyz.z - rlargest) > thisregion_zmi && ( me_xyz.z + rlargest ) < thisregion_zmx ) {
										//only current region
										kd_tree* thistree = thisregion->ityp_kdtree_rnd.at(*tt);
										vector<p3d> const & thisppp = *(thisregion->ionpp1_kdtree_rnd.at(*tt));
										if ( *tt == *typit ) {
											thistree->range_rball_append_exclude_myself( ip, thisppp, rlargestsqr, nbors_xyz );
										}
										else {
											thistree->range_rball_append_external( me_xyz, thisppp, rlargestsqr, nbors_xyz );
										}
//cout << "\t\t\t\tmyregion done!" << "\n";
									}
									else {
										//current region first
//cout << "\t\t\t\tnbregion begin!" << "\n";
										kd_tree* thistree = thisregion->ityp_kdtree_rnd.at(*tt);
										vector<p3d> const & thisppp = *(thisregion->ionpp1_kdtree_rnd.at(*tt));
										if ( *tt == *typit ) {
											thistree->range_rball_append_exclude_myself( ip, thisppp, rlargestsqr, nbors_xyz );
										}
										else {
											thistree->range_rball_append_external( me_xyz, thisppp, rlargestsqr, nbors_xyz );
										}
										//probe top neighbors and climb up, when i am not the topmost region
										if ( thr < (mt-1) ) {
											for( int upp = (thr+1); upp < nt; upp++) { //will eventually climb up to the topmost threadregion, also in practice this will never happen
												threadmemory* nbregion = sp.db.at(upp);
												kd_tree* nbtree = nbregion->ityp_kdtree_rnd.at(*tt);
												vector<p3d> const & nbppp = *(nbregion->ionpp1_kdtree_rnd.at(*tt));
												nbtree->range_rball_append_external( me_xyz, nbppp, rlargestsqr, nbors_xyz );
											}
										}
										//probe bottom neighbors and climb down, when i am not the bottommost region
										if ( thr > MASTER ) {
											for( int dwn = (thr-1); dwn > -1; dwn-- ) {
												threadmemory* nbregion = sp.db.at(dwn);
												kd_tree* nbtree = nbregion->ityp_kdtree_rnd.at(*tt);
												vector<p3d> const & nbppp = *(nbregion->ionpp1_kdtree_rnd.at(*tt));
												nbtree->range_rball_append_external( me_xyz, nbppp, rlargestsqr, nbors_xyz );
											}
										}
									}
									//done processing iontype tt update access field
									next = nbors_xyz.size();
									nbors_ityp_block.at(*tt) = ival( prev, next ); //assess information for empty or unvisited fields remains empty
								} //done querying ions from all relevant iontype trees

								//now that we know the relevant neighbors process tasks
								vector<apt_real> nbors_dist; //order is retained, so nbors_ityp_block tells us which iontypes
								if ( RDF_KNN_Tasks_Exist == true ) {
									for( auto nbt = nbors_xyz.begin(); nbt != nbors_xyz.end(); nbt++ ) {
										nbors_dist.push_back( sqrt(SQR(nbt->x-me_xyz.x)+SQR(nbt->y-me_xyz.y)+SQR(nbt->z-me_xyz.z)) );
									}
								}
								//MK::DO NOT PERFORM ANY SORTING DIRECTLY ON nbors_dist or nbors_d3dd DURING EXECUTING tasks as that would kill the order relation!
								vector<d3dd> nbors_d3dd;
								if ( SDM_Tasks_Exist == true ) {
									for( auto nbt = nbors_xyz.begin(); nbt != nbors_xyz.end(); nbt++ ) {
										nbors_d3dd.push_back( d3dd( nbt->x-me_xyz.x, nbt->y-me_xyz.y, nbt->z-me_xyz.z ) );
									}
								}
/*
//BEGIN DEBUG
for( auto ttt = nbors_ityp_block.begin(); ttt != nbors_ityp_block.end(); ttt++ ) {
	cout << "\t\t\t\t\t ttt " << ttt - nbors_ityp_block.begin() << ";" << ttt->incl_left << ";" << ttt->excl_right << "\n";
}
cout << "\t\t\t\t\t nbors_xyz / dist size " << nbors_xyz.size() << "\t\t" << nbors_dist.size() << "\n";
//END DEBUG
*/

								for( auto mtsk = mytsk_rnd.begin(); mtsk != mytsk_rnd.end(); mtsk++ ) {

									if ( mtsk->IsRDF == true ) {
										if ( me_ifo.dist >= mtsk->R.max ) {
//cout << "\t\t\t\t\t RDF taskid " << mtsk->taskid << " RDF " << "\n";
											//is ion me any possible target in this task?
											for( size_t iii = 0; iii < mtsk->pos_firstnbor; iii++ ) {
												if ( mtsk->targ_nbors.at(iii) != me_ifo.i_rnd ) { //no, me is a different type ion
													continue;
												}
												else { //yes me is sought after type ion targets
													//so add all those nbors of the same iontype class
													//RDF collect all radii <= R.max
													for( size_t jjj = mtsk->pos_firstnbor; jjj < mtsk->targ_nbors.size(); jjj++ ) {
														int jjtt = static_cast<int>(mtsk->targ_nbors.at(jjj));
														for( size_t k = nbors_ityp_block.at(jjtt).incl_left; k < nbors_ityp_block.at(jjtt).excl_right; k++ ) {
															apt_real val = nbors_dist.at(k);
															if ( val <= mtsk->R.max ) {
																mtsk->hst1d_res->add_dump_yes( val );
															}
														}
													} //done adding all ions within ROI of the relevant types
													break; //done for the target type
												} //done for the yes case
											} //keep checking if a possible target wrt to iontypes
										} //keep checking if far enough from hull
										continue; //each task can only be one, either RDF, KNN, or SDM
									}

									if ( mtsk->IsKNN == true ) {
										//is ion me really sufficiently far enough away from the boundary?
										if ( me_ifo.dist >= mtsk->R.max ) {
//cout << "\t\t\t\t\t KNN taskid " << mtsk->taskid << " KNN " << "\n";
											//is ion me any possible target in this task?
											for( size_t iii = 0; iii < mtsk->pos_firstnbor; iii++ ) {
												if ( mtsk->targ_nbors.at(iii) != me_ifo.i_rnd ) { //no, me is a different type ion
													continue;
												}
												else { //yes me is sought after type ion targets
													//so add all those nbors of the same iontype class
													vector<apt_real> candidates;
													for( size_t jjj = mtsk->pos_firstnbor; jjj < mtsk->targ_nbors.size(); jjj++ ) {
														int jjtt = static_cast<int>(mtsk->targ_nbors.at(jjj));
														for( size_t k = nbors_ityp_block.at(jjtt).incl_left; k < nbors_ityp_block.at(jjtt).excl_right; k++ ) {
															apt_real val = nbors_dist.at(k);
															if ( val <= mtsk->R.max ) {
//cout << "\t\t\t\t\t\t KNN adding " << val << "\n";
																candidates.push_back( val );
															}
														}
													} //done adding all ions within ROI of the relevant types
													size_t kth = static_cast<size_t>(mtsk->kthorder);
													if ( candidates.size() > kth ) {
														nth_element( candidates.begin(), candidates.begin() + kth, candidates.end() ); //MK::will sort ascendingly by default!
//cout << "\t\t\t\t\t KNN ---> kth = " << kth << " val = " << candidates.at(kth) << "\n";
														mtsk->hst1d_res->add_dump_yes( candidates.at(kth) );
													}
													else {
														mtsk->hst1d_res->add_dump_yes( mtsk->R.max + EPSILON );
													}
													break; //done for the target type
												} //done for the yes case
											} //keep checking if a possible target wrt to iontypes
										} //keep checking if far enough from hull
										continue;
									}

									if ( mtsk->IsSDM == true ) {
										//is ion me really sufficiently far enough away from the boundary?
										if ( me_ifo.dist >= mtsk->R.max ) {
//cout << "\t\t\t\t\t SDM taskid " << mtsk->taskid << " SDM " << "\n";
											//is ion me any possible target in this task?
											for( size_t iii = 0; iii < mtsk->pos_firstnbor; iii++ ) {
												if ( mtsk->targ_nbors.at(iii) != me_ifo.i_rnd ) { //no, me is a different type ion
													continue;
												}
												else { //yes me is sought after type ion targets
													//so add all those nbors of the same iontype class
													vector<d3dd> candidates;
													apt_real mtsk_rmax_sqr = SQR(mtsk->R.max);
													for( size_t jjj = mtsk->pos_firstnbor; jjj < mtsk->targ_nbors.size(); jjj++ ) {
														int jjtt = static_cast<int>(mtsk->targ_nbors.at(jjj));
														for( size_t k = nbors_ityp_block.at(jjtt).incl_left; k < nbors_ityp_block.at(jjtt).excl_right; k++ ) {
															d3dd val = nbors_d3dd.at(k);
															if ( val.dSQR <= mtsk_rmax_sqr ) {
//cout << "\t\t\t\t\t\t SDM adding " << val << "\n";
																candidates.push_back( val );
															}
														}
													} //done adding all ions within ROI of the relevant types
													size_t kth = static_cast<size_t>(mtsk->kthorder);
													if ( candidates.size() > kth ) {
														nth_element( candidates.begin(), candidates.begin() + kth, candidates.end(), SortDiffForAscDistance );
//cout << "\t\t\t\t\t SDM---> kth = " << kth << " val = " << candidates.at(kth) << "\n";
														mtsk->sdm3d_res->add( candidates.at(kth) );
													}
													break; //done for the target type
												} //done for the yes case
											} //keep checking if a possible target wrt to iontypes
										} //keep checking if far enough from hull
										continue;
									}
								} //re-utilize queried ROI to process next task
							} //was at least in one task
						} //was processed by this MPI rank
					} //threads machine off ip
				}
			} //hunt for ions of the next relevant type for randomized labels


		} //proceed with the next region

		//now that we are done with all regions, original and randomized types, thr accumulates results into process-local buffer

//cout << ranklog << "\n";
		//##MK::to really get the point of maximal memory consumption we would need a barrier here because threads release resources
		//already during following pragma omp critical data accumulation stage
		/*#pragma omp master
		{
		memsnapshot mm = spatstat_tictoc.get_memoryconsumption();
		}
		#pragma omp barrier*/

		#pragma omp critical
		{
			//accumulation for org types
			for( auto mtsk = mytsk_org.begin(); mtsk != mytsk_org.end(); mtsk++ ) {	 //collect results on process buffer
				unsigned int tskID = mtsk->taskid;
				if ( (mtsk->IsRDF == true || mtsk->IsKNN == true) && mtsk->IsSDM == false ) {
cout << "Rank " << get_myrank() << " thread " << mt << " taskid " << mtsk->taskid << " RDF/KNN accumulate" << "\n";
					if ( results.at(tskID).hst1d_res != NULL ) {
						if ( results.at(tskID).hst1d_res->nbins == mtsk->hst1d_res->nbins ) {
cout << "Rank " << get_myrank() << " thread " << mt << " taskid " << mtsk->taskid << " hst1d_res prepping..." << "\n";
							results.at(tskID).hst1d_res->cnts_lowest += mtsk->hst1d_res->cnts_lowest;
							unsigned int nb = mtsk->hst1d_res->nbins;
							vector<double> const & readhere = mtsk->hst1d_res->cnts;
							vector<double> & writehere = results.at(tskID).hst1d_res->cnts;
cout << "Rank " << get_myrank() << " thread " << mt << " taskid " << mtsk->taskid << " hstd1d_res writing..." << "\n";
							for( unsigned int b = 0; b < nb; b++ ) {
								writehere.at(b) += readhere.at(b);
							}
							results.at(tskID).hst1d_res->cnts_highest += mtsk->hst1d_res->cnts_highest;
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << mt << " accumulation error for task " << tskID << " hst1d_res nbins dissimilar!" << "\n";
						}
						//release resources
						delete mtsk->hst1d_res;
						mtsk->hst1d_res = NULL;
					}
					else {
						cerr << "Rank " << get_myrank() << " thread " << mt << " hst1d_res was not allocated for task " << tskID << " !" << "\n";
					}
				}
				if ( (mtsk->IsRDF == false || mtsk->IsKNN == false) && mtsk->IsSDM == true ) {
cout << "Rank " << get_myrank() << " thread " << mt << " taskid " << mtsk->taskid << " SDM accumulate" << "\n";
					if ( results.at(tskID).sdm3d_res != NULL ) {
						if ( results.at(tskID).sdm3d_res->get_nxyz() == mtsk->sdm3d_res->get_nxyz() ) {
cout << "Rank " << get_myrank() << " thread " << mt << " taskid " << mtsk->taskid << " sdm3d_res prepping..." << "\n";
							unsigned int nb = mtsk->sdm3d_res->get_nxyz();
							vector<unsigned int> const & readhere = mtsk->sdm3d_res->cnts;
							vector<unsigned int> & writehere = results.at(tskID).sdm3d_res->cnts;
cout << "Rank " << get_myrank() << " thread " << mt << " taskid " << mtsk->taskid << " sdm3d_res writing..." << "\n";
							for( unsigned int b = 0; b < nb; b++ ) {
								writehere.at(b) += readhere.at(b);
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << mt << " accumulation error for task " << tskID << " sdm3d_res nbins dissimilar!" << "\n";
						}
						//release resources
						delete mtsk->sdm3d_res;
						mtsk->sdm3d_res = NULL;
					}
					else {
						cerr << "Rank " << get_myrank() << " thread " << mt << " sdm3d_res was not allocated for task " << tskID << " !" << "\n";
					}
				}
			} //done accumulating results for org tasks
cout << "Rank " << get_myrank() << " thread " << mt << " successfully accumulated all results for all org tasks!" << "\n";

			//accumulation for rnd tasks
			for( auto mtsk = mytsk_rnd.begin(); mtsk != mytsk_rnd.end(); mtsk++ ) {	 //collect results on process buffer
				unsigned int tskID = mtsk->taskid;
				if ( (mtsk->IsRDF == true || mtsk->IsKNN == true) && mtsk->IsSDM == false ) {
			cout << "Rank " << get_myrank() << " thread " << mt << " taskid " << mtsk->taskid << " RDF/KNN accumulate" << "\n";
					if ( results.at(tskID).hst1d_res != NULL ) {
						if ( results.at(tskID).hst1d_res->nbins == mtsk->hst1d_res->nbins ) {
			cout << "Rank " << get_myrank() << " thread " << mt << " taskid " << mtsk->taskid << " hst1d_res prepping..." << "\n";
							results.at(tskID).hst1d_res->cnts_lowest += mtsk->hst1d_res->cnts_lowest;
							unsigned int nb = mtsk->hst1d_res->nbins;
							vector<double> const & readhere = mtsk->hst1d_res->cnts;
							vector<double> & writehere = results.at(tskID).hst1d_res->cnts;
			cout << "Rank " << get_myrank() << " thread " << mt << " taskid " << mtsk->taskid << " hstd1d_res writing..." << "\n";
							for( unsigned int b = 0; b < nb; b++ ) {
								writehere.at(b) += readhere.at(b);
							}
							results.at(tskID).hst1d_res->cnts_highest += mtsk->hst1d_res->cnts_highest;
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << mt << " accumulation error for task " << tskID << " hst1d_res nbins dissimilar!" << "\n";
						}
						//release resources
						delete mtsk->hst1d_res;
						mtsk->hst1d_res = NULL;
					}
					else {
						cerr << "Rank " << get_myrank() << " thread " << mt << " hst1d_res was not allocated for task " << tskID << " !" << "\n";
					}
				}
				if ( (mtsk->IsRDF == false || mtsk->IsKNN == false) && mtsk->IsSDM == true ) {
			cout << "Rank " << get_myrank() << " thread " << mt << " taskid " << mtsk->taskid << " SDM accumulate" << "\n";
					if ( results.at(tskID).sdm3d_res != NULL ) {
						if ( results.at(tskID).sdm3d_res->get_nxyz() == mtsk->sdm3d_res->get_nxyz() ) {
			cout << "Rank " << get_myrank() << " thread " << mt << " taskid " << mtsk->taskid << " sdm3d_res prepping..." << "\n";
							unsigned int nb = mtsk->sdm3d_res->get_nxyz();
							vector<unsigned int> const & readhere = mtsk->sdm3d_res->cnts;
							vector<unsigned int> & writehere = results.at(tskID).sdm3d_res->cnts;
			cout << "Rank " << get_myrank() << " thread " << mt << " taskid " << mtsk->taskid << " sdm3d_res writing..." << "\n";
							for( unsigned int b = 0; b < nb; b++ ) {
								writehere.at(b) += readhere.at(b);
							}
						}
						else {
							cerr << "Rank " << get_myrank() << " thread " << mt << " accumulation error for task " << tskID << " sdm3d_res nbins dissimilar!" << "\n";
						}
						//release resources
						delete mtsk->sdm3d_res;
						mtsk->sdm3d_res = NULL;
					}
					else {
						cerr << "Rank " << get_myrank() << " thread " << mt << " sdm3d_res was not allocated for task " << tskID << " !" << "\n";
					}
				}
			} //done accumulating results for rnd tasks
		} //end of critical region
	} //end of parallel region

	double toc = MPI_Wtime();
	memsnapshot mm = spatstat_tictoc.get_memoryconsumption();
	spatstat_tictoc.prof_elpsdtime_and_mem( "ExecuteLocalWorkpackage", APT_XX, APT_IS_PAR, mm, tic, toc );

	cout << "Rank " << get_myrank() << " spatial and two-point statistics took " << (toc-tic) << " seconds!" << "\n";
}


bool spatstatHdl::init_target_file()
{
	double tic = MPI_Wtime();

	//##MK::generate group hierarchy of an open source IFES APTTC APTH5 HDF5 file
	string h5fn_out = "PARAPROBE.Spatstat.Results.SimID." + to_string(ConfigShared::SimID) + ".h5";
cout << "Initializing target file " << h5fn_out << "\n";

	if ( debugh5Hdl.create_spatstat_apth5( h5fn_out ) != WRAPPED_HDF5_SUCCESS ) {
		return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	spatstat_tictoc.prof_elpsdtime_and_mem( "InitTargetFile", APT_XX, APT_IS_SEQ, mm, tic, toc );

	return true;
}


bool spatstatHdl::collect_results_on_masterprocess()
{
	if ( get_nranks() < 2 ) {
		cout << "MASTER is skipping results collection step because he has already all solutions!" << "\n";
		return true;
	}

	double tic = MPI_Wtime();

	auto mastertsk = results.begin();
	//for( auto tsk = itsk.itasks.begin(); tsk != itsk.itasks.end(); tsk++, mastertsk++ ) {
	for( auto tsk = results.begin(); tsk != results.end(); tsk++, mastertsk++ ) {
		//every process has this task and partial results accumulated in process-local histograms 1d and 3d of the same size
		int localhealth = 1;
		int globalhealth = 0;

		//assure that every process has the same view on a task as the MASTER
		MPI_Spatstat_Task ifo = MPI_Spatstat_Task();
		if ( get_myrank() == MASTER ) {
			ifo.rmin = mastertsk->R.min;
			ifo.rincr = mastertsk->R.incr;
			ifo.rmax = mastertsk->R.max;
			ifo.hst1d_cnt = ( mastertsk->hst1d_res != NULL ) ? static_cast<unsigned int>(mastertsk->hst1d_res->cnts.size()) : 0;
			ifo.sdm3d_cnt = ( mastertsk->sdm3d_res != NULL ) ? mastertsk->sdm3d_res->get_nxyz() : 0;
			ifo.targ_nbors_cnt = static_cast<unsigned int>(mastertsk->targ_nbors.size());
			ifo.pos_firstnbor = static_cast<unsigned int>(mastertsk->pos_firstnbor);
			ifo.kthorder = mastertsk->kthorder;
			ifo.taskid = mastertsk->taskid;
			ifo.WhichLabel = mastertsk->WhichLabel;
			ifo.IsRDF = mastertsk->IsRDF;
			ifo.IsKNN = mastertsk->IsKNN;
			ifo.IsSDM = mastertsk->IsSDM;
		}

		MPI_Bcast( &ifo, 1, MPI_Spatstat_Task_Type, MASTER, MPI_COMM_WORLD );

		if ( get_myrank() != MASTER ) {
			if ( fabs(ifo.rmin - tsk->R.min) > EPSILON || fabs(ifo.rincr - tsk->R.incr) > EPSILON || fabs(ifo.rmax - tsk->R.max) > EPSILON ) {
				cerr << "Rank " << get_myrank() << " detected inconsistency of own tsk->R  with that of the MASTER!" << "\n"; localhealth = 0;
			}
			if ( ifo.hst1d_cnt > 0 ) {
				if ( tsk->hst1d_res != NULL ) {
					if ( tsk->hst1d_res->cnts.size() != ifo.hst1d_cnt ) {
						cerr << "Rank " << get_myrank() << " detected inconsistent size of tsk->hst1d_res with that of the MASTER!" << "\n"; localhealth = 0;
					}
				}
				else {
					cerr << "Rank " << get_myrank() << " detected that it has not allocated hst1d_res but the MASTER!" << "\n"; localhealth = 0;
				}
			}
			else { //== 0
				if ( tsk->hst1d_res != NULL ) {
					cerr << "Rank " << get_myrank() << " detected MASTER has a hst1d_res == NULL but for me it is != NULL!" << "\n"; localhealth = 0;
				}
			}
			if ( ifo.sdm3d_cnt > 0 ) {
				if ( tsk->sdm3d_res != NULL ) {
					if ( tsk->sdm3d_res->get_nxyz() != ifo.sdm3d_cnt ) {
						cerr << "Rank " << get_myrank() << " detected inconsistent size of tsk->sdm3d_res with that of the MASTER!" << "\n"; localhealth = 0;
					}
				}
				else {
					cerr << "Rank " << get_myrank() << " detected that it has not allocated sdm3d_res but the MASTER!" << "\n"; localhealth = 0;
				}
			}
			else { //==0
				if ( tsk->sdm3d_res != NULL ) {
					cerr << "Rank " << get_myrank() << " detected MASTER has a sdm3d_res == NULL but for me it is != NULL!" << "\n"; localhealth = 0;
				}
			}
			if ( ifo.targ_nbors_cnt != static_cast<unsigned int>(tsk->targ_nbors.size()) || ifo.pos_firstnbor != static_cast<unsigned int>(tsk->pos_firstnbor) ||
					ifo.kthorder != tsk->kthorder || ifo.taskid != tsk->taskid ) {
				cerr << "Rank " << get_myrank() << " detected that targ_nbors, pos_firstnbor, kthorder, taskid is inconsistent to MASTER!" << "\n"; localhealth = 0;
			}
			if ( ifo.WhichLabel != tsk->WhichLabel || ifo.IsRDF != tsk->IsRDF || ifo.IsKNN != tsk->IsKNN || ifo.IsSDM != tsk->IsSDM ) {
				cerr << "Rank " << get_myrank() << " detected that WhichLabel, IsRDF, IsKNN, IsSDM  is inconsistent to MASTER!" << "\n"; localhealth = 0;
			}
		}

		//if there is an error all go out and not just only one process
		MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
		if ( globalhealth != get_nranks() ) {
			cerr << "Rank " << get_myrank() << " has recognized that not all processes have same sized buffers for task " << ifo.taskid << " !" << "\n"; return false;
		}

		if ( ( ifo.IsRDF == true || ifo.IsKNN == true ) && ifo.IsSDM == false ) {
cout << "Rank " << get_myrank() << " prepare RDF/KNN ifo.taskid " << ifo.taskid << "\n";
			localhealth = 1;
			globalhealth = 0;

			double* snd = NULL;
			double* rcv = NULL;
			size_t nb = 0;
			if ( ifo.hst1d_cnt > 0 ) {
				nb = ifo.hst1d_cnt + 2; //+2 for lowest and highest dump!
				snd = new double[nb];
				if ( snd != NULL ) {
					for( unsigned int b = 0; b < nb-2; b++ ) {
						snd[b] = tsk->hst1d_res->cnts.at(b);
					}
					snd[nb-2] = tsk->hst1d_res->cnts_lowest;
					snd[nb-1] = tsk->hst1d_res->cnts_highest;
				}
				else {
					cerr << "Rank " << get_myrank() << " was unable hst1d_res to allocate snd buffer for task " << ifo.taskid << " !" << "\n"; localhealth = 0;
				}
				//different to snd, buffer rcv is significant only at root/MASTER

				if ( get_myrank() == MASTER ) {
					rcv = new double[nb];
					if ( rcv != NULL ) {
						for( unsigned int b = 0; b < nb; b++ ) {
							rcv[b] = 0.0;
						}
					}
					else {
						cerr << "Rank " << MASTER << " was unable hst1d_res to allocate snd buffer for task " << ifo.taskid << " !" << "\n"; localhealth = 0;
					}
				}

				MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
				if ( globalhealth != get_nranks() ) {
					cerr << "Rank " << get_myrank() << " has recognized that not all processes have same sized buffers for task " << ifo.taskid << " !" << "\n"; return false;
				}

cout << "Rank " << get_myrank() << " reduce RDF/KNN ifo.taskid " << ifo.taskid << "\n";
				//pull bin values and low/highest from all people
				MPI_Reduce( snd, rcv, static_cast<int>(nb), MPI_DOUBLE, MPI_SUM, MASTER, MPI_COMM_WORLD );

				delete [] snd; snd = NULL;
				if ( get_myrank() == MASTER ) {
					for( unsigned int b = 0; b < nb-2; b++ ) {
						mastertsk->hst1d_res->cnts.at(b) = rcv[b];
					}
					mastertsk->hst1d_res->cnts_lowest = rcv[nb-2];
					mastertsk->hst1d_res->cnts_highest = rcv[nb-1];
					delete [] rcv; rcv = NULL;
				}
			}

			//##MK::for debugging but performance killer
cout << "Rank " << get_myrank() << " rcv/snd buffer synced " << ifo.taskid << "\n";

			MPI_Barrier( MPI_COMM_WORLD );
			continue;
		}

		if ( ( ifo.IsRDF == false && ifo.IsKNN == false ) && ifo.IsSDM == true ) {
cout << "Rank " << get_myrank() << " prepare SDM ifo.taskid " << ifo.taskid << "\n";
			localhealth = 1;
			globalhealth = 0;

			unsigned int* snd = NULL;
			unsigned int* rcv = NULL;
			size_t nb = 0;
			if ( ifo.sdm3d_cnt > 0 ) {
				nb = ifo.sdm3d_cnt;
				snd = new unsigned int[nb];
				if ( snd != NULL ) {
					for( unsigned int b = 0; b < nb; b++ ) {
						snd[b] = tsk->sdm3d_res->cnts.at(b);
					}
				}
				else {
					cerr << "Rank " << get_myrank() << " was unable sdm3d_res to allocate snd buffer for task " << ifo.taskid << " !" << "\n"; localhealth = 0;
				}
				//different to snd, buffer rcv is significant only at root/MASTER
				if ( get_myrank() == MASTER ) {
					rcv = new unsigned int[nb];
					if ( rcv != NULL ) {
						for( unsigned int b = 0; b < nb; b++ ) {
							rcv[b] = 0;
						}
					}
					else {
						cerr << "Rank " << MASTER << " was unable sdm3d_res to allocate snd buffer for task " << ifo.taskid << " !" << "\n"; localhealth = 0;
					}
				}

				MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
				if ( globalhealth != get_nranks() ) {
					cerr << "Rank " << get_myrank() << " has recognized that not all processes have same sized buffers for task " << ifo.taskid << " !" << "\n"; return false;
				}

cout << "Rank " << get_myrank() << " reduce SDM ifo.taskid " << ifo.taskid << "\n";
				//pull bin values and low/highest from all people
				MPI_Reduce( snd, rcv, static_cast<int>(nb), MPI_UNSIGNED, MPI_SUM, MASTER, MPI_COMM_WORLD );

				delete [] snd; snd = NULL;
				if ( get_myrank() == MASTER ) {
					for( unsigned int b = 0; b < nb; b++ ) {
						mastertsk->sdm3d_res->cnts.at(b) = rcv[b];
					}
					delete [] rcv; rcv = NULL;
				}
			}

			//##MK::for debugging but performance killer
cout << "Rank " << get_myrank() << " rcv/snd buffer synced " << ifo.taskid << "\n";

			MPI_Barrier( MPI_COMM_WORLD );
			continue;
		}
	} //next task


	double toc = MPI_Wtime();
	memsnapshot mm = spatstat_tictoc.get_memoryconsumption();
	spatstat_tictoc.prof_elpsdtime_and_mem( "CollectResultsOnMasterProcess", APT_XX, APT_IS_PAR, mm, tic, toc);
	cout << "Rank " << get_myrank() << " participated successfully in results collection stage took " << (toc-tic) << " seconds" << "\n";

    return true;
}


bool spatstatHdl::write_materialpoints_to_apth5()
{
	if ( get_myrank() != MASTER ) {
		cout << "Non-MASTER processes do not participate in results I/O!" << "\n"; return true;
	}

	double tic = MPI_Wtime();

	string fwslash = "/";
	int status = 0;
	h5iometa ifo = h5iometa();
	h5offsets offs = h5offsets();
	string dsnm = "";
	string grnm = "";

	vector<unsigned char> uc8_tg;
	vector<unsigned char> uc8_nb;
	bool SDM_SupportGrid_Required = false;
	vector<SpatstatSDMXDMF> xdmfattr;

	for( auto tsk = results.begin(); tsk != results.end(); tsk++ ) {
cout << "taskid/IsRDF/IsKNN/IsSDM = " << tsk->taskid << ";" << tsk->IsRDF << ";" << tsk->IsKNN << ";"<< tsk->IsSDM << "\n";

		//iontypes
		uc8_tg = vector<unsigned char>();
		uc8_nb = vector<unsigned char>();
		for( auto it = tsk->targ_nbors.begin(); it != (tsk->targ_nbors.begin() + tsk->pos_firstnbor); it++ ) {
			uc8_tg.push_back( *it );
			size_t itypid = static_cast<size_t>(*it);
			evapion3 const & jt = rng.iontypes.at(itypid).strct;
			uc8_tg.push_back( jt.Z1 ); uc8_tg.push_back( jt.N1 );
			uc8_tg.push_back( jt.Z2 ); uc8_tg.push_back( jt.N2 );
			uc8_tg.push_back( jt.Z3 ); uc8_tg.push_back( jt.N3 );
			uc8_tg.push_back( jt.sign); uc8_tg.push_back( jt.charge );
		}
		for( auto it = (tsk->targ_nbors.begin() + tsk->pos_firstnbor); it != tsk->targ_nbors.end(); it++ ) {
			uc8_nb.push_back( *it );
			size_t itypid = static_cast<size_t>(*it);
			evapion3 const & jt = rng.iontypes.at(itypid).strct;
			uc8_nb.push_back( jt.Z1 ); uc8_nb.push_back( jt.N1 );
			uc8_nb.push_back( jt.Z2 ); uc8_nb.push_back( jt.N2 );
			uc8_nb.push_back( jt.Z3 ); uc8_nb.push_back( jt.N3 );
			uc8_nb.push_back( jt.sign); uc8_nb.push_back( jt.charge );
		}

		if ( tsk->IsRDF == true && tsk->IsKNN == false && tsk->IsSDM == false ) {
			if ( tsk->hst1d_res != NULL ) {
cout << "taskid/hst1d_res " << tsk->taskid << ";" << tsk->hst1d_res << "\n";
				//results
				grnm = PARAPROBE_SPST_RDF_RES + fwslash + "Task" + to_string(tsk->taskid);
				status = debugh5Hdl.create_group( grnm );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create group " << grnm << "\n"; return false;
				}
				dsnm = grnm + fwslash + PARAPROBE_SPST_RDF_RES_CNTS;
				unsigned int nb = tsk->hst1d_res->cnts.size();
				ifo = h5iometa( dsnm, nb+2, 2 ); //+2 for lower and upper, radius;value
				vector<double> f64 = vector<double>( ifo.nr*ifo.nc, 0.0 );

				//###additional columns are necessary to normalize...
				f64.at(0*2+0) = tsk->hst1d_res->bounds.min; //cnts_lowest dump
				f64.at(0*2+1) = tsk->hst1d_res->cnts_lowest;
				for( unsigned int b = 0; b < nb; b++ ) {
					f64.at(1*2+b*2+0) = tsk->hst1d_res->right(b);
					f64.at(1*2+b*2+1) = tsk->hst1d_res->cnts.at(b);
				}
				f64.at(1*2+nb*2+0*2+0) = F64MX;
				f64.at(1*2+nb*2+0*2+1) = tsk->hst1d_res->cnts_highest;

				status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, nb+2, 0, PARAPROBE_SPST_RDF_RES_CNTS_NCMAX, nb+2, PARAPROBE_SPST_RDF_RES_CNTS_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				f64 = vector<double>();

				//metadata
				grnm = PARAPROBE_SPST_RDF_META + fwslash + "Task" + to_string(tsk->taskid);
				status = debugh5Hdl.create_group( grnm );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create group " << grnm << "\n"; return false;
				}

				//iontypes used
				dsnm = grnm + fwslash + PARAPROBE_SPST_RDF_META_TYPID_TG;
				ifo = h5iometa( dsnm, tsk->pos_firstnbor, PARAPROBE_SPST_RDF_META_TYPID_TG_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_SPST_RDF_META_TYPID_TG_NCMAX, ifo.nr, PARAPROBE_SPST_RDF_META_TYPID_TG_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_tg );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}

				dsnm = grnm + fwslash + PARAPROBE_SPST_RDF_META_TYPID_NB;
				ifo = h5iometa( dsnm, tsk->targ_nbors.size()-tsk->pos_firstnbor, PARAPROBE_SPST_RDF_META_TYPID_NB_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_SPST_RDF_META_TYPID_NB_NCMAX, ifo.nr, PARAPROBE_SPST_RDF_META_TYPID_NB_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_nb );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}

				//randomization info
				vector<unsigned char> uc8_rnd;
				if (tsk->WhichLabel == RANDOMIZED_IONTYPES ) {
					uc8_rnd.push_back( 0x01 );
					dsnm = grnm + fwslash + "RandomizedLabels";
				}
				else { //ORIGINAL_IONTYPES
					uc8_rnd.push_back( 0x00 );
					dsnm = grnm + fwslash + "OriginalLabels";
				}
				ifo = h5iometa( dsnm, 1, 1 );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, 1, 0, 1, 1, 1);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_rnd );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				uc8_rnd = vector<unsigned char>();

				delete tsk->hst1d_res;
				tsk->hst1d_res = NULL;
			}
			continue;
		}
		if ( tsk->IsRDF == false && tsk->IsKNN == true && tsk->IsSDM == false ) {
			if ( tsk->hst1d_res != NULL ) {
cout << "taskid/hst1d_res " << tsk->taskid << ";" << tsk->hst1d_res << "\n";
				//results
				grnm = PARAPROBE_SPST_KNN_RES + fwslash + "Task" + to_string(tsk->taskid);
				status = debugh5Hdl.create_group( grnm );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create group " << grnm << "\n"; return false;
				}
				dsnm = grnm + fwslash + PARAPROBE_SPST_KNN_RES_CNTS;
				unsigned int nb = tsk->hst1d_res->cnts.size();
				ifo = h5iometa( dsnm, nb+2, 2 ); //+2 for lower and upper, radius;value
				vector<double> f64 = vector<double>( ifo.nr*ifo.nc, 0.0 );

				f64.at(0*2+0) = tsk->hst1d_res->bounds.min; //cnts_lowest dump
				f64.at(0*2+1) = tsk->hst1d_res->cnts_lowest;
				for( unsigned int b = 0; b < nb; b++ ) {
					f64.at(1*2+b*2+0) = tsk->hst1d_res->right(b);
					f64.at(1*2+b*2+1) = tsk->hst1d_res->cnts.at(b);
				}
				f64.at(1*2+nb*2+0*2+0) = F64MX;
				f64.at(1*2+nb*2+0*2+1) = tsk->hst1d_res->cnts_highest;

				status = debugh5Hdl.create_contiguous_matrix_f64le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, nb+2, 0, PARAPROBE_SPST_KNN_RES_CNTS_NCMAX, nb+2, PARAPROBE_SPST_KNN_RES_CNTS_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_f64le_hyperslab( ifo, offs, f64 );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				f64 = vector<double>();

				//metadata
				grnm = PARAPROBE_SPST_KNN_META + fwslash + "Task" + to_string(tsk->taskid);
				status = debugh5Hdl.create_group( grnm );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create group " << grnm << "\n"; return false;
				}

				//iontypes used
				dsnm = grnm + fwslash + PARAPROBE_SPST_KNN_META_TYPID_TG;
				ifo = h5iometa( dsnm, tsk->pos_firstnbor, PARAPROBE_SPST_KNN_META_TYPID_TG_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_SPST_KNN_META_TYPID_TG_NCMAX, ifo.nr, PARAPROBE_SPST_KNN_META_TYPID_TG_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_tg );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				dsnm = grnm + fwslash + PARAPROBE_SPST_KNN_META_TYPID_NB;
				ifo = h5iometa( dsnm, tsk->targ_nbors.size()-tsk->pos_firstnbor, PARAPROBE_SPST_KNN_META_TYPID_NB_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_SPST_KNN_META_TYPID_NB_NCMAX, ifo.nr, PARAPROBE_SPST_KNN_META_TYPID_NB_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_nb );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}

				//randomization info
				vector<unsigned char> uc8_rnd;
				if (tsk->WhichLabel == RANDOMIZED_IONTYPES ) {
					uc8_rnd.push_back( 0x01 );
					dsnm = grnm + fwslash + "RandomizedLabels";
				}
				else { //ORIGINAL_IONTYPES
					uc8_rnd.push_back( 0x00 );
					dsnm = grnm + fwslash + "OriginalLabels";
				}
				ifo = h5iometa( dsnm, 1, 1 );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, 1, 0, 1, 1, 1);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_rnd );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				uc8_rnd = vector<unsigned char>();

				//kth order info
				vector<unsigned int> u32;
				u32.push_back( tsk->kthorder );
				dsnm = grnm + fwslash + "kthNearestNeighbor";
				ifo = h5iometa( dsnm, 1, 1);
				status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, 1, 0, 1, 1, 1);
				status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				u32 = vector<unsigned int>();

				delete tsk->hst1d_res;
				tsk->hst1d_res = NULL;
			}
			continue;
		}
		if ( tsk->IsRDF == false && tsk->IsKNN == false && tsk->IsSDM == true ) {
			if ( tsk->sdm3d_res != NULL ) {
				SDM_SupportGrid_Required = true;
cout << "taskid/sdm3d_res " << tsk->taskid << ";" << tsk->sdm3d_res << "\n";
				//results
				grnm = PARAPROBE_SPST_SDM_RES + fwslash + "Task" + to_string(tsk->taskid);
				status = debugh5Hdl.create_group( grnm );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create group " << grnm << "\n"; return false;
				}
				dsnm = grnm + fwslash + PARAPROBE_SPST_SDM_RES_CNTS;
				unsigned int nb = tsk->sdm3d_res->cnts.size();
				ifo = h5iometa( dsnm, nb, 1 );
				vector<unsigned int> u32 = vector<unsigned int>( nb, 0 );

				u32 = tsk->sdm3d_res->cnts; //deep copy

				status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, nb, 0, PARAPROBE_SPST_SDM_RES_CNTS_NCMAX, nb, PARAPROBE_SPST_SDM_RES_CNTS_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				u32 = vector<unsigned int>();

				//metadata
				grnm = PARAPROBE_SPST_SDM_META + fwslash + "Task" + to_string(tsk->taskid);
				status = debugh5Hdl.create_group( grnm );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create group " << grnm << "\n"; return false;
				}

				//iontypes
				dsnm = grnm + fwslash + PARAPROBE_SPST_SDM_META_TYPID_TG;
				ifo = h5iometa( dsnm, tsk->pos_firstnbor, PARAPROBE_SPST_SDM_META_TYPID_TG_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_SPST_SDM_META_TYPID_TG_NCMAX, ifo.nr, PARAPROBE_SPST_SDM_META_TYPID_TG_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_tg );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				uc8_tg = vector<unsigned char>();
				dsnm = grnm + fwslash + PARAPROBE_SPST_SDM_META_TYPID_NB;
				ifo = h5iometa( dsnm, tsk->targ_nbors.size()-tsk->pos_firstnbor, PARAPROBE_SPST_SDM_META_TYPID_NB_NCMAX );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, ifo.nr, 0, PARAPROBE_SPST_SDM_META_TYPID_NB_NCMAX, ifo.nr, PARAPROBE_SPST_SDM_META_TYPID_NB_NCMAX);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_nb );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				uc8_nb = vector<unsigned char>();

				//randomization info
				vector<unsigned char> uc8_rnd;
				if (tsk->WhichLabel == RANDOMIZED_IONTYPES ) {
					uc8_rnd.push_back( 0x01 );
					dsnm = grnm + fwslash + "RandomizedLabels";
				}
				else { //ORIGINAL_IONTYPES
					uc8_rnd.push_back( 0x00 );
					dsnm = grnm + fwslash + "OriginalLabels";
				}
				ifo = h5iometa( dsnm, 1, 1 );
				status = debugh5Hdl.create_contiguous_matrix_u8le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, 1, 0, 1, 1, 1);
				status = debugh5Hdl.write_contiguous_matrix_u8le_hyperslab( ifo, offs, uc8_rnd );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				uc8_rnd = vector<unsigned char>();

				//kth order info
				u32.push_back( tsk->kthorder );
				dsnm = grnm + fwslash + "kthNearestNeighbor";
				ifo = h5iometa( dsnm, 1, 1);
				status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
				}
				offs = h5offsets( 0, 1, 0, 1, 1, 1);
				status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
				if ( status < 0 ) {
					cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
				}
				u32 = vector<unsigned int>();

				xdmfattr.push_back( SpatstatSDMXDMF() );
				xdmfattr.back().nelements = tsk->sdm3d_res->get_nxyz();
				xdmfattr.back().ntopo = 3*tsk->sdm3d_res->get_nxyz();
				xdmfattr.back().nxyz = 1*tsk->sdm3d_res->get_nxyz();
				xdmfattr.back().nm = ( tsk->WhichLabel == RANDOMIZED_IONTYPES ) ?
						"Task" + to_string(tsk->taskid) + "Rnd" : "Task" + to_string(tsk->taskid) + "Org";
				xdmfattr.back().dsnm = PARAPROBE_SPST_SDM_RES + fwslash + "Task" + to_string(tsk->taskid) +
						fwslash + PARAPROBE_SPST_SDM_RES_CNTS;

				delete tsk->sdm3d_res;
				tsk->sdm3d_res = NULL;
			}
			continue;
		}
	}

	//if there are SDM tasks we also store a point grid with which to visualize the SDM directly in Paraview
	if ( SDM_SupportGrid_Required == true ) {
		//##MK::currently we use the same tsk->R radii settings for all SDM tasks! otherwise make this per task!
		vector<float> f32;
		sdm3d* sdm = NULL;
		sdm = new sdm3d( ConfigSpatstat::ROIRadiiSDM.max, ConfigSpatstat::ROIRadiiSDM.incr, 201 );
		size_t nb = 0;
		if ( sdm != NULL ) {
			nb = sdm->get_nxyz();
			f32.reserve( nb * PARAPROBE_SPST_SDM_META_XYZ_NCMAX );
			unsigned int nx = sdm->get_nx();
			for( unsigned int z = 0; z < nx; z++ ) {
				for( unsigned int y = 0; y < nx; y++ ) {
					for( unsigned int x = 0; x < nx; x++ ) {
						p3d here = sdm->get_bincenter( x, y, z );
						f32.push_back( here.x );
						f32.push_back( here.y );
						f32.push_back( here.z );
					}
				}
			}

			dsnm = PARAPROBE_SPST_SDM_META_XYZ;
			ifo = h5iometa( dsnm, nb, PARAPROBE_SPST_SDM_META_XYZ_NCMAX );
			status = debugh5Hdl.create_contiguous_matrix_f32le( ifo );
			if ( status < 0 ) {
				cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
			}
			offs = h5offsets( 0, nb, 0, PARAPROBE_SPST_SDM_META_XYZ_NCMAX, nb, PARAPROBE_SPST_SDM_META_XYZ_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_f32le_hyperslab( ifo, offs, f32 );
			if ( status < 0 ) {
				cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
			}
			f32 = vector<float>();

			delete sdm; sdm = NULL;

			vector<unsigned int> u32 = vector<unsigned int>( 3*nb, 1 ); //XDMF geom primitive and topo type key and vertex ID
			for( size_t b = 0; b < nb; b++ ) {
				u32.at(3*b+2) = static_cast<unsigned int>(b);
			}
			dsnm = PARAPROBE_SPST_SDM_META_TOPO;
			ifo = h5iometa( dsnm, 3*nb, PARAPROBE_SPST_SDM_META_TOPO_NCMAX );
			status = debugh5Hdl.create_contiguous_matrix_u32le( ifo );
			if ( status < 0 ) {
				cerr << "Rank " << MASTER << " failed to create " << dsnm << "\n"; return false;
			}
			offs = h5offsets( 0, 3*nb, 0, PARAPROBE_SPST_SDM_META_TOPO_NCMAX, 3*nb, PARAPROBE_SPST_SDM_META_TOPO_NCMAX);
			status = debugh5Hdl.write_contiguous_matrix_u32le_hyperslab( ifo, offs, u32 );
			if ( status < 0 ) {
				cerr << "Rank " << MASTER << " failed to write " << dsnm << "\n"; return false;
			}
			u32 = vector<unsigned int>();
		}
		else {
			cerr << "Rank " << MASTER << " failed to generate SDM to write PARAPROBE_SPST_SDM_META_XYZ!" << "\n"; return false;
		}
	}

	string xmlfn = "PARAPROBE.Spatstat.Results.SimID." + to_string(ConfigShared::SimID) + ".h5.SDM.xdmf";
	debugxdmf.create_spatstat_file( xmlfn, xdmfattr, debugh5Hdl.h5resultsfn );

	double toc = MPI_Wtime();
	memsnapshot mm = spatstat_tictoc.get_memoryconsumption();
	spatstat_tictoc.prof_elpsdtime_and_mem( "WriteResultsToHDF5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	cout << "Rank " << get_myrank() << " Writing of material infos into H5 file took " << (toc-tic) << " seconds" << "\n";
	return true;
}


int spatstatHdl::get_myrank()
{
	return this->myrank;
}


int spatstatHdl::get_nranks()
{
	return this->nranks;
}


void spatstatHdl::set_myrank( const int rr )
{
	this->myrank = rr;
}


void spatstatHdl::set_nranks( const int nn )
{
	this->nranks = nn;
}


void spatstatHdl::init_mpidatatypes()
{
	MPI_Type_contiguous(2, MPI_UNSIGNED_LONG_LONG, &MPI_Ranger_DictKeyVal_Type);
	MPI_Type_commit(&MPI_Ranger_DictKeyVal_Type);

	MPI_Type_contiguous(3, MPI_FLOAT, &MPI_Synth_XYZ_Type);
	MPI_Type_commit(&MPI_Synth_XYZ_Type);

	MPI_Type_contiguous(9, MPI_UNSIGNED_CHAR, &MPI_Ranger_Iontype_Type);
	MPI_Type_commit(&MPI_Ranger_Iontype_Type);

	int elementCounts[2] = {2, 4};
	MPI_Aint displacements[2] = {0, 2 * MPIIO_OFFSET_INCR_F32};
	MPI_Datatype oldTypes[2] = {MPI_FLOAT, MPI_UNSIGNED_CHAR};
	MPI_Type_create_struct(2, elementCounts, displacements, oldTypes, &MPI_Ranger_MQIval_Type);
	MPI_Type_commit(&MPI_Ranger_MQIval_Type);

	int elementCounts3[3] = {3, 6, 4};
	MPI_Aint displacements3[3] = {0, 3 * MPIIO_OFFSET_INCR_F32, (3 * MPIIO_OFFSET_INCR_F32 + 6 * MPIIO_OFFSET_INCR_UINT32) };
	MPI_Datatype oldTypes3[3] = {MPI_FLOAT, MPI_UNSIGNED, MPI_BYTE};
	MPI_Type_create_struct(3, elementCounts3, displacements3, oldTypes3, &MPI_Spatstat_Task_Type);
	MPI_Type_commit(&MPI_Spatstat_Task_Type);
}
