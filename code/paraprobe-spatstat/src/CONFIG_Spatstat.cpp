//##MK::CODE

#include "CONFIG_Spatstat.h"

string ConfigSpatstat::InputfilePSE = "";
string ConfigSpatstat::InputfileReconstruction = "";
string ConfigSpatstat::InputfileHullAndDistances = "";
//string ConfigSpatstat::Outputfile = "";

lival ConfigSpatstat::ROIRadiiRDF = lival();
lival ConfigSpatstat::ROIRadiiKNN = lival();
lival ConfigSpatstat::ROIRadiiSDM = lival();


bool ConfigSpatstat::AnalyzeRDF = false;
bool ConfigSpatstat::AnalyzeKNN = false;
bool ConfigSpatstat::AnalyzeSDM = false;
bool ConfigSpatstat::AnalyzeRandomize = false;

//vector<TypeCombi> ConfigSpatstat::IontypeCombis = vector<TypeCombi>();
string ConfigSpatstat::KOrderForKNN = "";
string ConfigSpatstat::KOrderForSDM = "";

//bool ConfigSpatstat::ROICenterInsideOnly = false;
bool ConfigSpatstat::ROIVolumeInsideOnly = false;

string ConfigSpatstat::PRNGType = "MT19937";
size_t ConfigSpatstat::PRNGWarmup = 700000;
long ConfigSpatstat::PRNGWorldSeed = -12345678;
size_t ConfigSpatstat::MaxSizeCachedResPerNode = GIGABYTE2BYTE(static_cast<size_t>(16)); //useful value for talos but should be auto-parsed!
//MK::global seeds for pseudorandom number generation (PRNG) need to be the same across all processes!
//##MK::otherwise processes do different label randomization


pair<string,bool> parse_knn_neighbors( const string in, vector<unsigned int> & out )
{
	//parse and chop k-nearest neighbor ID string into values if possible
	pair<string,bool> status = pair<string,bool>( "", true );
	
	stringstream parsethis;
	parsethis << in;
	string datapiece;
	
	size_t nsemicolon = count( in.begin(), in.end(), ';' );
	//https://stackoverflow.com/questions/8888748/how-to-check-if-given-c-string-or-char-contains-only-digits
	if ( in.size() < 1 ) {
		string mess = "No values were passed to parse_knn_neighbors!";
		return pair<string,bool>( mess, false );
	}
	
	for( size_t i = 0; i < nsemicolon + 1; i++ ) { //parse at least the single value
		getline( parsethis, datapiece, ';');
		unsigned long val = stoul(datapiece);
		if ( val < UINT32MX ) {
			out.push_back( static_cast<unsigned int>(val) );
		}
		else {
			string mess = to_string(val) + " is a very large value this seems unrealistic!";
			return pair<string,bool>( mess, false );
		}
	}
	return pair<string,bool>( "", true );
}


bool ConfigSpatstat::readXML( string filename )
{
	if ( 0 == filename.compare("") )
		filename = string("PARAPROBE.Spatstat.Debug.xml");

	ifstream file( filename );
	if ( file.fail() ) {
        cerr << "Unable to locate input file " << filename << "\n";
        return false;
    }

	stringstream contents;
	contents << file.rdbuf();
	string xmlDocument(contents.str());

	xml_document<> tree;
	tree.parse<0>(&xmlDocument[0]);

	xml_node<>* rootNode = tree.first_node();
	if (0 != strcmp(rootNode->name(), "ConfigSpatstat")) {
		cerr << "Undefined parameters file!" << "\n";
        return false;
	}

	unsigned int arg = 0;
	unsigned int mode = 0;
  
	InputfilePSE = read_xml_attribute_string( rootNode, "InputfilePSE" );
	InputfileReconstruction = read_xml_attribute_string( rootNode, "InputfileReconstruction" );
	InputfileHullAndDistances = read_xml_attribute_string( rootNode, "InputfileHullAndDistances" );
	//Outputfile = read_xml_attribute_string( rootNode, "Outputfile" );
    
    AnalyzeRDF = read_xml_attribute_bool( rootNode, "AnalyzeRDF" );
    if ( AnalyzeRDF == true ) {
		ROIRadiiRDF = lival(   	0.0,
								read_xml_attribute_float( rootNode, "ROIRadiusRDFIncr" ),
								read_xml_attribute_float( rootNode, "ROIRadiusRDFMax" )    );
    }
    
    AnalyzeKNN = read_xml_attribute_bool( rootNode, "AnalyzeKNN" );
    if ( AnalyzeKNN == true ) {
		ROIRadiiKNN = lival(   	0.0,
								read_xml_attribute_float( rootNode, "ROIRadiusKNNIncr" ),
								read_xml_attribute_float( rootNode, "ROIRadiusKNNMax" )    );
		KOrderForKNN = read_xml_attribute_string( rootNode, "KOrderForKNN" );
    }

    AnalyzeSDM = read_xml_attribute_bool( rootNode, "AnalyzeSDM" );
    if ( AnalyzeSDM == true ) {
		ROIRadiiSDM = lival(   	0.0,
								read_xml_attribute_float( rootNode, "ROIRadiusSDMIncr" ),
								read_xml_attribute_float( rootNode, "ROIRadiusSDMMax" )    );
		KOrderForSDM = read_xml_attribute_string( rootNode, "KOrderForSDM" );
    }

    AnalyzeRandomize = true; //read_xml_attribute_bool( rootNode, "AnalyzeRandomize" );

    //ROICenterInsideOnly = read_xml_attribute_bool( rootNode, "ROICenterInsideOnly" );
    ROIVolumeInsideOnly = read_xml_attribute_bool( rootNode, "ROIVolumeInsideOnly" );
    
    //MaxSizeCachedResPerNode = GIGABYTE2BYTE(read_xml_attribute_uint32( rootNode, "MaxMemGigabytePerNode"));
	return true;
}


bool ConfigSpatstat::checkUserInput()
{
	cout << "ConfigSpatstat::" << "\n";

	if ( ConfigSpatstat::AnalyzeRDF == false && ConfigSpatstat::AnalyzeKNN == false && ConfigSpatstat::AnalyzeSDM == false )  {
		cerr << "No analysis tasks chosen!" << "\n"; return false;
	}

	cout << "AnalyzeRDF " << AnalyzeRDF << "\n";
	if ( AnalyzeRDF == true ) {
		if ( ROIRadiiRDF.min < 0.0 ) {
			cerr << "ROIRadiusRDFMin must be at least 0.0!" << "\n"; return false;
		}
		if ( ROIRadiiRDF.incr < EPSILON ) {
			cerr << "ROIRadiusRDFIncr must be positive and finite!" << "\n"; return false;
		}
		if ( ROIRadiiRDF.max < EPSILON ) {
			cerr << "ROIRadiusRDFMax must be positive and finite!" << "\n"; return false;
		}
		if ( ROIRadiiRDF.max < ROIRadiiRDF.min ) {
			cerr << "ROIRadiusRDFMax must be at least as large as ROIRadiusRDFMin!" << "\n"; return false;
		}
		cout << "ROIRadiusRDFMin " << ROIRadiiRDF.min << "\n";
		cout << "ROIRadiusRDFIncr " << ROIRadiiRDF.incr << "\n";
		cout << "ROIRadiusRDFMax " << ROIRadiiRDF.max << "\n";
	}

	cout << "AnalyzeKNN " << AnalyzeKNN << "\n";
	if ( AnalyzeKNN == true ) {
		if ( ROIRadiiKNN.min < 0.0 ) {
			cerr << "ROIRadiusKNNMin must be at least 0.0!" << "\n"; return false;
		}
		if ( ROIRadiiKNN.incr < EPSILON ) {
			cerr << "ROIRadiusKNNIncr must be positive and finite!" << "\n"; return false;
		}
		if ( ROIRadiiKNN.max < EPSILON ) {
			cerr << "ROIRadiusKNNMax must be positive and finite!" << "\n"; return false;
		}
		if ( ROIRadiiKNN.max < ROIRadiiKNN.min ) {
			cerr << "ROIRadiusKNNMax must be at least as large as ROIRadiusKNNMin!" << "\n"; return false;
		}
		cout << "ROIRadiusKNNMin " << ROIRadiiKNN.min << "\n";
		cout << "ROIRadiusKNNIncr " << ROIRadiiKNN.incr << "\n";
		cout << "ROIRadiusKNNMax " << ROIRadiiKNN.max << "\n";
	}

    cout << "AnalyzeSDM " << AnalyzeSDM << "\n";
    if ( AnalyzeSDM == true ) {
		if ( ROIRadiiSDM.min < 0.0 ) {
			cerr << "ROIRadiusSDMMin must be at least 0.0!" << "\n"; return false;
		}
		if ( ROIRadiiSDM.incr < EPSILON ) {
			cerr << "ROIRadiusSDMIncr must be positive and finite!" << "\n"; return false;
		}
		if ( ROIRadiiSDM.max < EPSILON ) {
			cerr << "ROIRadiusSDMMax must be positive and finite!" << "\n"; return false;
		}
		if ( ROIRadiiSDM.max < ROIRadiiSDM.min ) {
			cerr << "ROIRadiusSDMMax must be at least as large as ROIRadiusSDMMin!" << "\n"; return false;
		}
		cout << "ROIRadiusSDMMin " << ROIRadiiSDM.min << "\n";
		cout << "ROIRadiusSDMIncr " << ROIRadiiSDM.incr << "\n";
		cout << "ROIRadiusSDMMax " << ROIRadiiSDM.max << "\n";
    }
    
    cout << "AnalyzeRandomize " << AnalyzeRandomize << "\n";

    if ( AnalyzeKNN == true ) {
		vector<unsigned int> tmp = vector<unsigned int>();
		pair<string,bool> status = parse_knn_neighbors( KOrderForKNN, tmp );
		if ( status.second == false ) {
			cerr << status.first << "\n"; return false;
		}
		else {
			cout << "KOrderForKNN " << KOrderForKNN << "\n";
		}
    }
    
    if ( AnalyzeSDM == true ) {
		vector<unsigned int> tmp = vector<unsigned int>();
		pair<string,bool> status = parse_knn_neighbors( KOrderForSDM, tmp );
		if ( status.second == false ) {
			cerr << status.first << "\n"; return false;
		}
		else {
			cout << "KOrderForSDM " << KOrderForSDM << "\n";
		}
    }
    
    cout << "PRNGType " << PRNGType << "\n";
    cout << "PRNGWarmup " << PRNGWarmup << "\n";
    cout << "PRNGWorldSeed " << PRNGWorldSeed << "\n";
    
    cout << "InputfilePSE read from " << InputfilePSE << "\n";
    cout << "InputReconstruction read from " << InputfileReconstruction << "\n";
    cout << "InputHullsAndDistances read from " << InputfileHullAndDistances << "\n";
    //cout << "Output written to " << Outputfile << "\n";

    if ( ROIVolumeInsideOnly == true ) {
        cout << "Material points outside the inside voxel or within closer than ROIRadiusMax to the triangle hull are discarded!" << "\n";
	}
	else {
		cout << "WARNING finite dataset boundary affects are not accounted for so some sampling points will have insignificant ion support!" << "\n";
	}

    //cout << "MaxSizeCachedResPerNode " << MaxSizeCachedResPerNode << "\n";

	cout << "\n";
	return true;
}

