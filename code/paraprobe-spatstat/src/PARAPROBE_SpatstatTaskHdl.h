//##MK::CODESPLIT


#ifndef __PARAPROBE_SPATSTAT_TASKHANDLING_H__
#define __PARAPROBE_SPATSTAT_TASKHANDLING_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_SpatstatStructs.h"


struct TypeCombi
{
	string targets;
	string nbors;
	TypeCombi() : targets(""), nbors("") {}
	TypeCombi( const string _trg, const string _nbr ) : targets(_trg), nbors(_nbr) {}
};

ostream& operator << (ostream& in, TypeCombi const & val);


struct Evapion3Combi
{
	vector<evapion3> targets;
	vector<evapion3> nbors;
	Evapion3Combi() : targets(vector<evapion3>()), nbors(vector<evapion3>()) {}
};


struct UC8Combi
{
	vector<unsigned char> targets;
	vector<unsigned char> nbors;
	UC8Combi() : targets(vector<unsigned char>()), nbors(vector<unsigned char>()) {}
};


#define ORIGINAL_IONTYPES		true
#define RANDOMIZED_IONTYPES		false
#define WANT_RDF				true
#define WANT_KNN				true
#define	WANT_SDM				true
#define WANT_NONE				false


struct SpatstatTask
{
	//helper structure to implement information channeling and wiring across process/thread/task-loop hierarchy
	//3*8+3*4+4+4+4*1+one page
	histogram* hst1d_res;
	sdm3d* sdm3d_res;
	size_t pos_firstnbor;
	lival R;
	unsigned int kthorder;
	unsigned int taskid;		//not a computing task in terms of CPU or hardware but a physical task, i.e. scientific analysis task...
	bool WhichLabel;
	bool IsRDF;
	bool IsKNN;
	bool IsSDM;

	//##MK::maybe determine a maximum admissible size to pack this object even stronger in memory to improve cacheline utilization
	vector<unsigned char> targ_nbors;		//first all target iontypes on [0, pos_firstnbor), next all neighbors on [pos_firstnbor,targ_nbors.size())

	SpatstatTask() : hst1d_res(NULL), sdm3d_res(NULL), pos_firstnbor(0), R(lival()), kthorder(0), taskid(UINT32MX),
			WhichLabel(true), IsRDF(false), IsKNN(false), IsSDM(false)  {}
	//use for RDF
	SpatstatTask( lival const & _iv, const unsigned int _tskid,
			const bool _lbl, const bool _rdf, const bool _knn, const bool _sdm ) :
					hst1d_res(NULL), sdm3d_res(NULL), pos_firstnbor(0),
					R(lival(_iv.min,_iv.incr,_iv.max)), kthorder(0), taskid(_tskid),
					WhichLabel(_lbl), IsRDF(_rdf), IsKNN(_knn), IsSDM(_sdm) {}
	//use for KNN
	SpatstatTask( lival const & _iv, const unsigned int _kth, const unsigned int _tskid,
			const bool _lbl, const bool _rdf, const bool _knn, const bool _sdm ) :
					hst1d_res(NULL), sdm3d_res(NULL), pos_firstnbor(0),
					R(lival(_iv.min,_iv.incr,_iv.max)), kthorder(_kth), taskid(_tskid),
					WhichLabel(_lbl), IsRDF(_rdf), IsKNN(_knn), IsSDM(_sdm) {}
};


class itypeCombiHdl
{
	//class which translates human-readable single/molecular ion type combination strings into the internal itype unsigned char
	//format with which internally all ions are analyzed
public:
	itypeCombiHdl();
	~itypeCombiHdl();

	bool load_iontype_combinations( string xmlfn );
	//bool prepare_iontype_combinations( rangeTable & ranger, const size_t max_memory_per_node );

	vector<TypeCombi> icombis;
	vector<UC8Combi> combinations;
	vector<SpatstatTask> itasks;
};



#endif

