//##MK::CODESPLIT


#ifndef __PARAPROBE_SPATSTAT_HDL_H__
#define __PARAPROBE_SPATSTAT_HDL_H__

//here is the level where we loop in all tool-specific utils
#include "PARAPROBE_SpatstatAcquisor.h"

/*
class ioutilsHdl
{
	//process-level class which implements the worker instance which reads common results
	ioutilsHdl();
	~ioutilsHdl();

	bool read_reconstruction_from_apth5();
	bool read_trianglehull_from_apth5();
	
	bool broadcast_reconstruction();
	bool broadcast_triangles();
	bool broadcast_directions();

	void spatial_decomposition();
	
	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();

	decompositor sp;
	volsampler mp;
	//vector<int> mp2rk;						//mapping of material points => to ranks,
	//vector<int> mp2me;

	vector<p3dm1> reconstruction;					//the reconstructed and ranged ion point cloud
	vector<p3dm2> reconstruction;
	vector<tri3d> trianglehull;					//a triangle hull to the data set
	vector<apt_real> dist2hull;

private:
	//MPI related
	int myrank;								//my MPI ID in the MPI_COMM_WORLD
	int nranks;

	MPI_Datatype MPI_Ion_Type;
};
*/	



class spatstatHdl //: public ioutilsHdl
{
	//process-level class which implements the worker instance which executes spatial distribution maps according to the method
	//of Vicente Araullo-Peters et al. using OpenMP and OpenACC i.e. CPU and GPUs simultaneously
	//specimens result are written to a specifically-formatted HDF5 file

public:
	spatstatHdl();
	~spatstatHdl();

	bool read_periodictable();
	bool read_iontype_combinations( string fn );
	bool read_reconxyz_ranging_from_apth5();
	bool read_dist2hull_from_apth5();
	
	bool broadcast_reconxyz();
	bool broadcast_ranging();
	bool broadcast_distances();

	bool define_analysis_tasks();
	void randomize_iontype_labels();
	void itype_sensitive_spatial_decomposition();
	bool prepare_processlocal_resultsbuffer();
	void execute_local_workpackage();
	//void distribute_matpoints_on_processes();

	bool init_target_file();
	bool collect_results_on_masterprocess();
	bool write_materialpoints_to_apth5();

	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	void init_mpidatatypes();

	rangeTable rng;
	itypeCombiHdl itsk;
	vector<SpatstatTask> results;
	unsigned char maximum_iontype_uc;

/*
	void generate_debug_roi( mt19937 & mydice, vector<dft_real> * out );
*/

	

/*



	bool write_results_to_apth5();
*/
	
	decompositor sp;
	volsampler mp;
	//vector<int> mp2rk;						//mapping of material points => to ranks,
	//vector<int> mp2me;

	//vector<p3dm1> reconstruction;				//the reconstructed and ranged ion point cloud
	vector<p3d> xyz;
	vector<unsigned char> ityp_org;				//iontypes ranged, original
	vector<unsigned char> ityp_rnd;				//iontypes, shuffled using PRNG
	vector<apt_real> dist2hull;

	h5Hdl inputReconH5Hdl;
	h5Hdl inputDistH5Hdl;
	spatstat_h5 debugh5Hdl;
	spatstat_xdmf debugxdmf;
	
	//vector<acquisor*> workers;

	profiler spatstat_tictoc;

private:
	//MPI related
	int myrank;								//my MPI ID in the MPI_COMM_WORLD
	int nranks;

	MPI_Datatype MPI_Ion_Type;
	MPI_Datatype MPI_Ranger_DictKeyVal_Type;
	MPI_Datatype MPI_Synth_XYZ_Type;
	MPI_Datatype MPI_Ranger_Iontype_Type;
	MPI_Datatype MPI_Ranger_MQIval_Type;
	MPI_Datatype MPI_Spatstat_Task_Type;
};


#endif

