//##MK::CODE

#ifndef __PARAPROBE_SPATSTAT_XDMF_H__
#define __PARAPROBE_SPATSTAT_XDMF_H__

#include "PARAPROBE_SpatstatHDF5.h"

struct SpatstatSDMXDMF
{
	unsigned int nelements;
	unsigned int ntopo;
	unsigned int nxyz;
	string nm;
	string dsnm;
	SpatstatSDMXDMF() : nelements(0), ntopo(0), nxyz(0), nm(""), dsnm("") {}
	SpatstatSDMXDMF( const unsigned int _nelem, const unsigned int _ntopo, const unsigned int _nxyz, const string _nm,
			const string _dsnm ) :
		nelements(_nelem), ntopo(_ntopo), nxyz(_nxyz), nm(_nm), dsnm(_dsnm) {}
};


class spatstat_xdmf : public xdmfHdl
{
	//tool-specific sub-class of a XDMF inheriting all methods of the base class xdmfHdl
	//coordinating instance handling all (sequential) writing to XDMF text file to supplement visualization of HDF5 content

public:
	spatstat_xdmf();
	~spatstat_xdmf();

	int create_spatstat_file( const string xmlfn, vector<SpatstatSDMXDMF> const & in, const string h5ref );


//private:
};

#endif
