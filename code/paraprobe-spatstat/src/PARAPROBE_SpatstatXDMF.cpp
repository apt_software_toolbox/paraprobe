//##MK::GPLV3

#include "PARAPROBE_SpatstatXDMF.h"


spatstat_xdmf::spatstat_xdmf()
{
	
}
	
spatstat_xdmf::~spatstat_xdmf()
{
	
}



int spatstat_xdmf::create_spatstat_file( const string xmlfn, vector<SpatstatSDMXDMF> const & in, const string h5ref )
{
	if ( in.size() < 1 ) {
		cout << "WARNING:: SpatstatSDMXDMF ifo field is empty!" << "\n"; return WRAPPED_XDMF_SUCCESS;
	}

	xdmfout.open( xmlfn.c_str() );
	if ( xdmfout.is_open() == true ) {
		xdmfout << XDMF_HEADER_LINE1 << "\n";
		xdmfout << XDMF_HEADER_LINE2 << "\n";
		xdmfout << XDMF_HEADER_LINE3 << "\n";

		xdmfout << "  <Domain>" << "\n";
		//##MK::currently the same for all SDM
		xdmfout << "    <Grid Name=\"SDMGrid\" GridType=\"Uniform\">" << "\n";
		xdmfout << "      <Topology TopologyType=\"Mixed\" NumberOfElements=\"" << in.at(0).nelements << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << in.at(0).ntopo << "\" NumberType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_SPST_SDM_META_TOPO << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Topology>" << "\n";

		xdmfout << "      <Geometry GeometryType=\"XYZ\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << in.at(0).nxyz << " 3\" NumberType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "          " << h5ref << ":" << PARAPROBE_SPST_SDM_META_XYZ << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Geometry>" << "\n";

		for( auto it = in.begin(); it != in.end(); it++ ) {
			xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"" << it->nm << "\">" << "\n";
			xdmfout << "        <DataItem Dimensions=\"" << it->nxyz << " 1\" DataType=\"UInt\" Precision=\"4\" Format=\"HDF\">" << "\n";
			xdmfout << "           " << h5ref << ":" << it->dsnm << "\n";
			xdmfout << "        </DataItem>" << "\n";
			xdmfout << "      </Attribute>" << "\n";
		}

/*
		xdmfout << "      <Attribute AttributeType=\"Scalar\" Center=\"Node\" Name=\"ROIRadius\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << nmp << " 1\" DataType=\"Float\" Precision=\"4\" Format=\"HDF\">" << "\n";
		xdmfout << "           " << h5ref << ":" << PARAPROBE_ARAULLO_META_MATPNT_R << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Attribute>" << "\n";
*/

 		xdmfout << "    </Grid>" << "\n";
		xdmfout << "  </Domain>" << "\n";
		xdmfout << "</Xdmf>" << "\n";

		xdmfout.flush();
		xdmfout.close();

		return WRAPPED_XDMF_SUCCESS;
	}
	else {
		return WRAPPED_XDMF_IOFAILED;
	}
}


